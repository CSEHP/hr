package com.app.system.log4j2;

/**
 * log4j2 对应log4j2-spring.yml中不同日志name值
 * Created by wcf-pc on 2018/12/20.
 *
 * @author wcf-pc
 */
public enum LogEnum {
    //业务日志
    BUSINESS("business"),
    //平台日志
    PLATFORM("platform"),
    //数据库日志
    DB("db"),
    //错误异常日志
    EXCEPTION("exception"),;

    private String category;

    LogEnum(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
