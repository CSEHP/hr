package com.app.system.exception;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by wcf-pc on 2018/4/17.
 * @author wcf-pc
 */
@Controller
public class HttpErrorHandler implements ErrorController {

    private static final String ERROR_PATH = "/error";
    @RequestMapping(value=ERROR_PATH)
    public String handleError(){
        return "error/404";
    }
    @Override
    public String getErrorPath() {
        return ERROR_PATH;
    }
}
