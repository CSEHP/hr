package com.app.system.permission.dao;

import com.app.system.jpa.BaseRepository;
import com.app.system.permission.model.SysRole;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by wcf-pc on 2018/5/29.
 */
@SuppressWarnings("all")
public interface SysRoleDao extends BaseRepository<SysRole, Integer> {

    @Query("select s from SysRole s order by s.sortIndex")
    List<SysRole> findAllBySortIndex();

    @Query("select s from SysRole s where s.role = :role order by s.sortIndex")
    List<SysRole> findByRoleName(@Param("role") String roleName);

    @Transactional
    @Modifying
    @Query("delete from SysRole s where s.id in ?1")
    void deleteByIds(List<Integer> ids);
}
