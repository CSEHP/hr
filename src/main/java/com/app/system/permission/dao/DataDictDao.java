package com.app.system.permission.dao;

import com.app.system.jpa.BaseRepository;
import com.app.system.permission.model.DataDict;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 数据字典
 * Created by wcf-pc on 2018/5/19.
 *
 * @author wcf-pc
 */
@SuppressWarnings("all")
public interface DataDictDao extends BaseRepository<DataDict, Integer> {

    @Query("select d from DataDict d left join d.dataDict dict where dict.id is null order by d.sortIndex ")
    public List<DataDict> parentList();

    @Query("select d from DataDict d inner join d.dataDict dict where dict.id = :parentId order by d.sortIndex asc")
    public List<DataDict> childrenList(@Param("parentId") Integer parentId);

    @Transactional
    @Modifying
    @Query("delete from DataDict dict where dict.dataDict.id in ?1")
    void deleteByParentIds(List<Integer> ids);

    @Transactional
    @Modifying
    @Query("delete from DataDict dict where dict.id in ?1")
    void deleteByIds(List<Integer> ids);

    @Query("select dict from DataDict dict where dict.dataDict.id in ?1")
    DataDict findOneByParentId(int parentId);


    @Query("select dict from DataDict dict where dict.dataDict.name = ?1 and dict.enabled = ?2 order by dict.sortIndex")
    List<DataDict> findListByParentName(String parentName, int enabled);


    @Query("select name from DataDict dict where dict.dataDict.name = ?1 and dict.enabled = ?2 order by dict.sortIndex")
    List<String> findNameListByParentName(String parentName, int enabled);
}
