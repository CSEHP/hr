package com.app.system.permission.dao;

import com.app.system.jpa.BaseRepository;
import com.app.system.permission.model.Department;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 部门dao
 * Created by wcf-pc on 2018/12/25.
 */
@SuppressWarnings("all")
public interface DepartmentDao extends BaseRepository<Department, Integer> {

    @Query("select d from Department d order by d.sortIndex , d.department.id")
    public List<Department> findAllBySortIndexAsc();

    Department findByCode(String code);

    @Query("select d from Department d where d.department.id is null order by d.sortIndex")
    List<Department> findParentList();

    @Query("select d from Department d order by d.id asc")
    List<Department> departmentList();

    Department findByName(String name);
}
