package com.app.system.permission.dao;

import com.app.system.jpa.BaseRepository;
import com.app.system.permission.model.SysPermission;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 权限数据访问
 * Created by wcf-pc on 2018/5/24.
 */
@SuppressWarnings("all")
public interface SysPermissionDao extends BaseRepository<SysPermission, Integer> {

    /**
     * 提取父节点菜单
     *
     * @return
     */
    @Query("select s from SysPermission s where s.sysPermission.id is null and s.resourceType = :resourceType order by s.sortIndex")
    public List<SysPermission> findParentList(@Param("resourceType") String resourceType);

    /**
     * 根据父节点查询功能
     *
     * @param parentId
     * @param resourceType
     * @return
     */
    @Query("select s from SysPermission s where s.sysPermission.id = :parentId and s.resourceType = :resourceType order by s.sortIndex")
    List<SysPermission> findListByParentId(@Param("parentId") int parentId, @Param("resourceType") String resourceType);

    @Transactional
    @Modifying
    @Query("delete from SysPermission s where s.id in ?1")
    void deleteByIds(List<Integer> ids);

    @Query(value = "select distinct s from SysPermission s left join s.sysPermissionList st join s.roles r join r.userInfos u where s.sysPermission.id is null and r.available =:available and s.enabled = 0 and st.enabled = 0 and  u.username = :username order by s.sortIndex ")
    List<SysPermission> findListByUserName(@Param("available") Boolean bl,@Param("username") String username);
}
