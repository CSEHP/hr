package com.app.system.permission.dao;

import com.app.system.jpa.BaseRepository;
import com.app.system.permission.model.SysPermissionInit;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by wcf-pc on 2018/5/15.
 */
@SuppressWarnings("all")
public interface SysPermissionInitDao extends BaseRepository<SysPermissionInit, Integer> {

    @Query("select s from SysPermissionInit s order by sortIndex asc")
    public List<SysPermissionInit> findAllList();

}
