package com.app.system.permission.dao;

import com.app.system.jpa.BaseRepository;
import com.app.system.permission.model.UserInfo;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@SuppressWarnings("all")
public interface UserInfoDao extends BaseRepository<UserInfo, Integer> {
    /**
     * 通过username查找用户信息;
     */
    public UserInfo findByUsername(String username);

    @Query("select u from UserInfo u where u.name = :name order by u.id")
    public List<UserInfo> findByName(@Param("name") String name);

    @Query("select u from UserInfo u where u.name = :name and u.id <> :userInfoId order by u.id")
    public List<UserInfo> findByNameAndNotUserId(@Param("userInfoId") int userInfoId, @Param("name") String name);

    @Query("select u from UserInfo u order by u.id")
    public List<UserInfo> findOrderByidDesc();

    @Transactional
    @Modifying
    @Query("update UserInfo  set password = :password,salt = :salt where id = :id")
    public void updatePwd(@Param("id") Integer id, @Param("password") String password, @Param("salt") String salt);

    @Transactional
    @Modifying
    @Query("delete from UserInfo where id in(:list)")
    void delByIds(@Param("list") List<Integer> ids);
}
