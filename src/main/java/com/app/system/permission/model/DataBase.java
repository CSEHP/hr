package com.app.system.permission.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;

/**
 * 数据库备份实体类
 * Created by wcf-pc on 2019/1/26.
 */
@Getter
@Setter
@Entity
@SuppressWarnings("all")
public class DataBase {
    @Id
    @GeneratedValue
    private Integer id;
    private String fileName;//文件名称
    private String filePath;//文件存放路径
    private Long fileSize;//文件大小
    private Date fileDate;//文件保存日期
    private Long fileDateLong;//文件保存日期
}
