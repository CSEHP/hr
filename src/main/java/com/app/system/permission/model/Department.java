package com.app.system.permission.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 部门实体类
 * Created by wcf-pc on 2018/12/10.
 *
 * @author wcf-pc
 */
@DynamicInsert()
@DynamicUpdate()
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
@Entity
@SuppressWarnings("all")
public class Department {
    @Id
    @GeneratedValue
    private Integer id;//id
    private String code;//编号
    private String name;//名称
    private Integer sortIndex;//排序
    private int state = 1;//状态 1:正常,2：锁定.

    @ManyToOne(fetch = FetchType.EAGER)//立即加载
    @JoinColumn(name = "parentId")
    private Department department;

    @OneToMany(mappedBy = "department", fetch = FetchType.EAGER)//懒加载
    private List<Department> departmentList;

    @OneToMany(mappedBy = "department", fetch = FetchType.LAZY)//懒加载
    private List<UserInfo> userInfoList;


    /**
     * 创建时间
     */
    @CreatedDate
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 修改时间
     */
    @LastModifiedDate
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}
