package com.app.system.permission.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.persistence.Entity;
import java.util.Date;
import java.util.List;

/**
 * 数据字典实体类
 * Created by wcf-pc on 2018/5/19.
 *
 * @author wcf-pc
 */
@DynamicInsert()
@DynamicUpdate()
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
@Entity
@SuppressWarnings("all")
public class DataDict {
    @Id
    @GeneratedValue
    private Integer id;//id
    private String name;//名称
    private String value;//数值
    private String color;//颜色
    private Integer sortIndex;//排序
    private Integer enabled;//是否可用 0是：1：否
    private String memo;//备注

    @ManyToOne(fetch = FetchType.LAZY)//懒加载
    @JoinColumn(name = "parentId")
    private DataDict dataDict;

    @OneToMany(mappedBy = "dataDict", fetch = FetchType.EAGER)//自动加载
    private List<DataDict> dataDictList;


    /**
     * 创建时间
     */
    @CreatedDate
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 修改时间
     */
    @LastModifiedDate
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}
