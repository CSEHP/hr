package com.app.system.permission.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * 用户信息实体类
 */
@DynamicInsert()
@DynamicUpdate()
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
@Entity
@SuppressWarnings("all")
public class UserInfo {
    @Id
    @GeneratedValue
    private Integer id;
    private String name;//名称（昵称或者真实姓名，不同系统不同定义）
    private String telPhone;//联系电话
    @Column(unique = true)
    private String username;//帐号
    private String password; //密码;
    private String salt;//加密密码的盐
    private String checkCode;//本次发送验证码的值（用于验证）
    private Date checkDate;//验证码发送时间
    private int state = 1;//用户状态 1:正常状态,2：用户被锁定.
    private String pic;//头像
    private Integer leavel;//用户级别：1、2、3、4、5、6、7、8、9（数值越大，级别越高，高级别的用户可以管理低级别用户信息）

    private String description;//描述

    private String guid;//标示号码(GUID)
    private String ukey;//密钥(UKEY)
    private String pin;//编号(PIN)

    @ManyToMany(fetch = FetchType.EAGER)//立即从数据库中进行加载数据;
    @JoinTable(name = "SysUserRole", joinColumns = {@JoinColumn(name = "uid")}, inverseJoinColumns = {@JoinColumn(name = "roleId")})
    private Set<SysRole> roleList;// 一个用户具有多个角色


    @ManyToOne(fetch = FetchType.EAGER)//自动加载
    @JoinColumn(name = "departmentId")
    private Department department;

    /**
     * 创建时间
     */
    @CreatedDate
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 修改时间
     */
    @LastModifiedDate
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /**
     * 密码盐.重新对盐重新进行了定义，用户名+salt，这样就更加不容易被破解
     * 该属性并非一个到数据库表的字段的映射
     *
     * @return
     */
    @Transient
    public String getCredentialsSalt() {
        return this.username + this.salt;
    }
}
