package com.app.system.permission.en;

/**
 * Created by wcf-pc on 2018/12/10.
 *
 * @author wcf-pc
 */
@SuppressWarnings("all")
public enum Enabled {
    //可用状态，一般用于是否启用、是否可用等
    YES("是", 0), NO("否", 1);

    // 成员变量
    private String name;
    private int index;

    // 构造方法
    private Enabled(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
