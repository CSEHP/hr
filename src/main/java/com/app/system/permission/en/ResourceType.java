package com.app.system.permission.en;

/**
 * 系统权限功能菜单使用
 * Created by wcf-pc on 2018/5/27.
 */
@SuppressWarnings("all")
public enum ResourceType {
    //菜单类型、按钮类型
    menu, button
}
