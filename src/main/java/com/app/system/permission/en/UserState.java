package com.app.system.permission.en;

/**
 * Created by WCF on 2019/8/29.
 */
@SuppressWarnings("all")
public enum UserState {

    //用户状态：可用、禁用
    AVAILABLE("是", 1), DISABLE("否", 2);

    // 成员变量
    private String name;
    private int index;

    // 构造方法
    private UserState(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
