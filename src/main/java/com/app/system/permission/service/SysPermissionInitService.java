package com.app.system.permission.service;

import com.app.system.permission.dao.SysPermissionInitDao;
import com.app.system.permission.model.SysPermissionInit;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by wcf-pc on 2018/5/15.
 */
@Service
@SuppressWarnings("all")
@CacheConfig(cacheNames = {"sysPermissionInitCache"})
public class SysPermissionInitService {

    @Resource
    private SysPermissionInitDao sysPermissionInitDao;


    //清空所有缓存
    @CacheEvict(value = "sysPermissionInitCache", allEntries = true)
    @Transactional
    public void save(SysPermissionInit sysPermissionInit) {
        this.sysPermissionInitDao.save(sysPermissionInit);
    }

    //清空所有缓存
    @CacheEvict(value = "sysPermissionInitCache", allEntries = true)
    @Transactional
    public void delById(Integer id) {
        this.sysPermissionInitDao.delete(id);
    }

    @Cacheable(key = "targetClass + methodName")
    public List<SysPermissionInit> findAll() {
        return sysPermissionInitDao.findAllList();
    }


    @Cacheable(key = "targetClass + methodName +#p0")
    public SysPermissionInit findOneById(Integer id) {
        return this.sysPermissionInitDao.findOne(id);
    }

}
