package com.app.system.permission.service;

import com.app.system.permission.dao.DepartmentDao;
import com.app.system.permission.model.Department;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wcf-pc on 2018/12/25.
 *
 * @author wcf-pc
 */
@Service
@SuppressWarnings("all")
@CacheConfig(cacheNames = {"departmentCache"})
public class DepartmentService {

    @Resource
    private DepartmentDao departmentDao;

    //清空所有缓存
    @CacheEvict(value = "departmentCache", allEntries = true)
    public void save(Department department) {
        this.departmentDao.save(department);
    }

    //清空所有缓存
    @CacheEvict(value = "departmentCache", allEntries = true)
    public void delById(int id) {
        this.departmentDao.delete(id);
    }

    @Cacheable(key = "targetClass + methodName")
    public List<Department> allDepartmentList() {
        return this.departmentDao.findAllBySortIndexAsc();
    }

    @Cacheable(key = "targetClass + methodName +#p0")
    public Department findById(int id) {
        return this.departmentDao.findOne(id);
    }


    @Cacheable(key = "targetClass + methodName +#p0")
    public Department findByCode(String code) {
        return this.departmentDao.findByCode(code);
    }


    @Cacheable(key = "targetClass + methodName +#p0")
    public Department findByName(String name) {
        return this.departmentDao.findByName(name);
    }


    @Cacheable(key = "targetClass + methodName")
    public List<Department> findParentList() {
        return this.departmentDao.findParentList();
    }
    @Cacheable(key = "targetClass + methodName")
    public List<Department> getDepartmentList() {
        return this.departmentDao.departmentList();
    }

    @Cacheable(key = "targetClass + methodName")
    public Map<String,String> getAllNameBycode() {
        List<Department> departmentList=this.departmentDao.findAllBySortIndexAsc();
        Map<String, String> map = new HashMap();
        for (Department department : departmentList) {
            map.put(department.getCode(), department.getName());
        }
        return map;
    }
}
