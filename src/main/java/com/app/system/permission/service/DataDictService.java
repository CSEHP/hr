package com.app.system.permission.service;

import com.app.system.permission.dao.DataDictDao;
import com.app.system.permission.en.Enabled;
import com.app.system.permission.model.DataDict;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 数据字典
 * 添加了数据缓存，加快了查询速度
 * Created by wcf-pc on 2018/5/19.
 *
 * @author wcf-pc
 */
@Service
@SuppressWarnings("all")
@CacheConfig(cacheNames = {"dataDictCache"})
public class DataDictService {

    @Resource
    private DataDictDao dataDictDao;

    /**
     * 查询一级科目
     *
     * @return
     */
    @Cacheable(key = "targetClass + methodName")
    public List<DataDict> parentList() {
        return this.dataDictDao.parentList();
    }

    @Cacheable(key = "targetClass + methodName +#p0")
    public List<DataDict> childrenList(Integer parentId) {
        return this.dataDictDao.childrenList(parentId);
    }

    @Cacheable(key = "targetClass + methodName +#p0")
    public DataDict findOne(Integer id) {
        return this.dataDictDao.findOne(id);
    }

    //清空所有缓存
    @CacheEvict(value = "dataDictCache", allEntries = true)
    @Transactional
    public void save(DataDict dataDict) {
        this.dataDictDao.save(dataDict);
    }

    /**
     * 子项同时删除
     * 清空所有缓存
     *
     * @param ids
     */
    @CacheEvict(value = "dataDictCache", allEntries = true)
    @Transactional
    public String deleteDataDict(String[] ids) {
        if (ids.length > 0) {
            List<Integer> idsList = Lists.newArrayList();
            for (String id : ids) {
                idsList.add(Integer.valueOf(id));
            }
            this.dataDictDao.deleteByParentIds(idsList);
            this.dataDictDao.deleteByIds(idsList);
        }
        return "ok";
    }

    @Cacheable(key = "targetClass + methodName +#p0")
    public DataDict findOneParentId(int parentId) {
        return this.dataDictDao.findOneByParentId(parentId);
    }

    @Cacheable(key = "targetClass + methodName +#p0")
    public List<DataDict> findListByParentName(String parentName) {
        return this.dataDictDao.findListByParentName(parentName, Enabled.YES.getIndex());
    }

    @Cacheable(key = "targetClass + methodName +#p0")
    public List<String> findNameListByParentName(String parentName) {
        return this.dataDictDao.findNameListByParentName(parentName, Enabled.YES.getIndex());
    }

    /**
     * 越过缓存查询数据字典List
     * @param parentName
     * @return
     */
    public List<DataDict> findListByParentNameUnCache(String parentName) {
        return this.dataDictDao.findListByParentName(parentName, Enabled.YES.getIndex());
    }

    @Cacheable(key = "targetClass + methodName +#p0")
    public Map<String, DataDict> getMapByParentNameForName(String parentName) {
        Map<String, DataDict> map = Maps.newLinkedHashMap();
        List<DataDict> dataDictList = this.dataDictDao.findListByParentName(parentName, Enabled.YES.getIndex());
        if (dataDictList != null) {
            for (DataDict dataDict : dataDictList) {
                map.put(dataDict.getName(), dataDict);
            }
        }
        return map;
    }

    @Cacheable(key = "targetClass + methodName +#p0")
    public Map<String, DataDict> getMapByParentNameForValue(String parentName) {
        Map<String, DataDict> map = Maps.newLinkedHashMap();
        List<DataDict> dataDictList = this.dataDictDao.findListByParentName(parentName, Enabled.YES.getIndex());
        if (dataDictList != null) {
            for (DataDict dataDict : dataDictList) {
                map.put(dataDict.getValue(), dataDict);
            }
        }
        return map;
    }

    public Map<String, String> getMapByParentNameForValue_Name(String attributeName) {
        LinkedHashMap map = new LinkedHashMap();
        List list = this.findListByParentName(attributeName);
        Iterator i$ = list.iterator();

        while(i$.hasNext()) {
            DataDict dd = (DataDict)i$.next();
            map.put(dd.getValue(), dd.getName());
        }

        return map;
    }

    public Map<String, String> getMapByParentNameForName_Value(String attributeName) {
        LinkedHashMap map = new LinkedHashMap();
        List list = this.findListByParentName(attributeName);
        Iterator i$ = list.iterator();

        while(i$.hasNext()) {
            DataDict dd = (DataDict)i$.next();
            map.put(dd.getName(), dd.getValue());
        }

        return map;
    }

    public Map<String, String> getMapByParentNameForValue_Color(String attributeName) {
        LinkedHashMap map = new LinkedHashMap();
        List list = this.findListByParentName(attributeName);
        Iterator i$ = list.iterator();

        while(i$.hasNext()) {
            DataDict dd = (DataDict)i$.next();
            map.put(dd.getValue(), dd.getColor());
        }

        return map;
    }
}
