package com.app.system.permission.service;

import com.app.system.permission.dao.SysRoleDao;
import com.app.system.permission.model.SysRole;
import com.google.common.collect.Lists;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by wcf-pc on 2018/5/29.
 */
@Service
@SuppressWarnings("all")
@CacheConfig(cacheNames = {"sysRoleCache"})
public class SysRoleService {

    @Resource
    private SysRoleDao sysRoleDao;


    //清空所有缓存
    @CacheEvict(value = {"userInfoCache", "sysRoleCache", "sysPermissionCache"}, allEntries = true)
    @Transactional
    public void save(SysRole sysRole) {
        this.sysRoleDao.save(sysRole);
    }


    //清空所有缓存
    @CacheEvict(value = {"userInfoCache", "sysRoleCache", "sysPermissionCache"}, allEntries = true)
    @Transactional
    public String deleteByIds(String[] ids) {
        if (ids.length > 0) {
            List<Integer> idsList = Lists.newArrayList();
            for (String id : ids) {
                idsList.add(Integer.valueOf(id));
            }
            this.sysRoleDao.deleteByIds(idsList);
        }
        return "ok";
    }

    @Cacheable(key = "targetClass + methodName +#p0")
    public SysRole findOne(Integer id) {
        return this.sysRoleDao.findOne(id);
    }

    @Cacheable(key = "targetClass + methodName +#p0")
    public SysRole findByRoleName(String roleName) {
        List<SysRole> roleList = this.sysRoleDao.findByRoleName(roleName);
        return roleList.size() > 0 ? roleList.get(0) : null;
    }

    @Cacheable(key = "targetClass + methodName")
    public List<SysRole> findAll() {
        return this.sysRoleDao.findAllBySortIndex();
    }


}
