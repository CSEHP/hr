package com.app.system.permission.service;

import com.app.system.jpa.JpaBaseDao;
import com.app.system.jpa.PageBean;
import com.app.system.jpa.Wrapper;
import com.app.system.permission.model.DataBase;
import org.springframework.stereotype.Service;

/**
 * 备份数据管理
 * Created by wcf-pc on 2019/1/26.
 */
@Service
@SuppressWarnings("all")
public class DataBaseService extends JpaBaseDao<DataBase, Integer> {

    /**
     * 分页查询数据存储文件数据
     *
     * @return
     */
    public PageBean<DataBase> pageList() {
        //查询内容项
        StringBuffer query = new StringBuffer("d from DataBase d");
        //排序
        StringBuffer orderBy = new StringBuffer("d.fileDate desc");
        Wrapper wrapper = new Wrapper(query, orderBy);
        wrapper.initSearchParams();//查询参数赋值
        return this.pageHql(wrapper);
    }
}
