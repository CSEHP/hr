package com.app.system.permission.web.permission;

import com.app.log.annotation.MyLog;
import com.app.system.permission.model.SysPermissionInit;
import com.app.system.permission.service.ShiroService;
import com.app.system.permission.service.SysPermissionInitService;
import com.app.system.utils.ParamUtils;
import com.app.system.utils.exception.Response;
import com.app.system.utils.page.LayUiUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by wcf-pc on 2018/5/17.
 */
@Controller
@RequestMapping("/permission/sysPermissionInit/*")
@SuppressWarnings("all")
public class SysPermissionInitController {

    @Resource
    private SysPermissionInitService sysPermissionInitService;
    @Resource
    private ShiroService shiroService;

    @RequestMapping("manager")
    @RequiresPermissions("sysPermissionInit:manager")
    public void manager(ModelMap model) {
    }

    @RequestMapping("loadData")
    @ResponseBody
    @RequiresPermissions("sysPermissionInit:loadData")
    public Map loadData() {
        List<SysPermissionInit> list = this.sysPermissionInitService.findAll();
        List<Map> listData = Lists.newArrayList();

        if (!CollectionUtils.isEmpty(list)) {
            for (SysPermissionInit sysPermissionInit : list) {
                Map<String, Object> item = Maps.newHashMap();
                item.put("id", sysPermissionInit.getId());
                item.put("permissionInit", sysPermissionInit.getPermissionInit());
                item.put("url", sysPermissionInit.getUrl());
                item.put("sortIndex", sysPermissionInit.getSortIndex());
                listData.add(item);
            }
        }

        return LayUiUtils.list(listData);
    }

    @RequestMapping("form")
    @RequiresPermissions("sysPermissionInit:form")
    public void form(ModelMap model, HttpServletRequest request) {
        Integer id = ParamUtils.getInt(request, "id", 0);
        SysPermissionInit sysPermissionInit = this.sysPermissionInitService.findOneById(id);
        if (sysPermissionInit == null) {
            sysPermissionInit = new SysPermissionInit();
        }
        model.addAttribute("sysPermissionInit", sysPermissionInit);
    }

    @MyLog(value = "过滤器配置保存")
    @RequestMapping("save")
    @ResponseBody
//    @RequiresPermissions("sysPermissionInit:save")
    public String save(ModelMap model, HttpServletRequest request) {
        Integer id = ParamUtils.getInt(request, "id", 0);
        SysPermissionInit sysPermissionInit = this.sysPermissionInitService.findOneById(id);
        if (sysPermissionInit == null) {
            sysPermissionInit = new SysPermissionInit();
        }
        ServletRequestDataBinder binder = new ServletRequestDataBinder(sysPermissionInit);
        binder.bind(request);
        this.sysPermissionInitService.save(sysPermissionInit);
        return "1";//成功
    }

    @MyLog(value = "过滤器配置删除")
    @RequestMapping("del")
    @ResponseBody
    @RequiresPermissions("sysPermissionInit:del")
    public Response del(ModelMap model, HttpServletRequest request, Integer id) {
        Map<String, Object> map = Maps.newHashMap();
        int status = 0;
        String message = "";
        SysPermissionInit sysPermissionInit = this.sysPermissionInitService.findOneById(id);
        if (sysPermissionInit != null && sysPermissionInit.getId() != null) {
            this.sysPermissionInitService.delById(id);
            return new Response("0001", "删除成功");
        } else {
            return new Response("0002", "删除失败");
        }
    }

    /**
     * 动态更新shiro过滤链
     */
    @MyLog(value = "过滤器配置更新")
    @RequestMapping("updatePermission")
    @ResponseBody
    @RequiresPermissions("sysPermissionInit:updatePermission")
    public Response updatePermission() {
        this.shiroService.updatePermission();
        return new Response("0001", "更新成功");
    }
}
