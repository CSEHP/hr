package com.app.system.permission.web.permission;

import com.app.log.annotation.MyLog;
import com.app.system.permission.model.DataDict;
import com.app.system.permission.service.DataDictService;
import com.app.system.utils.ParamUtils;
import com.app.system.utils.exception.AjaxException;
import com.app.system.utils.exception.Response;
import com.app.system.utils.page.LayUiUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 数据字典
 * Created by wcf-pc on 2018/5/19.
 *
 * @author wcf-pc
 */
@Controller
@RequestMapping("/permission/dataDict/*")
@SuppressWarnings("all")
public class DataDictController {

    @Resource
    private DataDictService dataDictService;

    /**
     * 主界面
     */
    @RequestMapping("manager")
    @RequiresPermissions("dataDict:manager")
    public void manager() {

    }

    /**
     * @return
     */
    @RequestMapping("loadDataDict")
    @ResponseBody
    @RequiresPermissions("dataDict:loadDataDict")
    public Map loadDataDict() {
        List<DataDict> dataDictList = this.dataDictService.parentList();
        List<Map> listData = Lists.newArrayList();
        if (!CollectionUtils.isEmpty(dataDictList)) {
            for (DataDict dataDict : dataDictList) {
                Map<String, Object> item = Maps.newHashMap();
                item.put("id", dataDict.getId());
                item.put("name", dataDict.getName());
                item.put("sortIndex", dataDict.getSortIndex());
                listData.add(item);
            }
        }
        return LayUiUtils.list(listData);
    }

    @RequestMapping("form")
    @RequiresPermissions("dataDict:form")
    public void form(ModelMap model, HttpServletRequest request) {
        int id = ParamUtils.getInt(request, "id", 0);
        DataDict dataDict = this.dataDictService.findOne(id);
        dataDict = dataDict == null ? new DataDict() : dataDict;
        model.addAttribute("dataDict", dataDict);
    }

    /**
     * 新增数据字典、编辑数据字典、内容table单元格保存
     *
     * @param request
     * @return
     */
    @MyLog(value = "数据字典保存")
    @RequestMapping("save")
    @ResponseBody
    @RequiresPermissions("dataDict:save")//此处不配置，则此方法将不经过权限验证
    public Response save(HttpServletRequest request) {
        Integer id = ParamUtils.getInt(request, "id", 0);
        DataDict dataDict = this.dataDictService.findOne(id);
        dataDict = dataDict == null ? new DataDict() : dataDict;
        ServletRequestDataBinder binder = new ServletRequestDataBinder(dataDict);
        binder.bind(request);
        this.dataDictService.save(dataDict);
        return new Response("0001", "更新成功");
    }

    @MyLog(value = "数据字典删除")
    @RequestMapping("deleteDataDict")
    @ResponseBody
    @RequiresPermissions("dataDict:deleteDataDict")
    public Response deleteDataDict(HttpServletRequest request) {
        String idsString = ParamUtils.getString(request, "ids", "");
        if (!Strings.isNullOrEmpty(idsString)) {
            String[] ids = idsString.split(",");
            String result = this.dataDictService.deleteDataDict(ids);
            String ok = "ok";
            if (ok.equals(result)) {
                return new Response("0001", "删除成功");
            }
        }
        return new Response("0002", "删除失败");
    }

    /**
     * @return
     */
    @RequestMapping("loadItemDataDict")
    @ResponseBody
    @RequiresPermissions("dataDict:loadItemDataDict")
    public Map loadItemDataDict(HttpServletRequest request) {
        Integer parentId = ParamUtils.getInt(request, "parentId", 0);
        List<DataDict> dataDictList = this.dataDictService.childrenList(parentId);
        List<Map> listData = Lists.newArrayList();
        if (!CollectionUtils.isEmpty(dataDictList)) {
            for (DataDict dataDict : dataDictList) {
                Map<String, Object> item = Maps.newHashMap();
                item.put("id", dataDict.getId());
                item.put("name", dataDict.getName());
                item.put("color", dataDict.getColor());
                item.put("value", dataDict.getValue());
                item.put("enabled", dataDict.getEnabled());
                item.put("sortIndex", dataDict.getSortIndex());
                item.put("memo", dataDict.getMemo());
                item.put("parentId", dataDict.getDataDict().getId());
                listData.add(item);
            }
        }
        return LayUiUtils.list("请点击需要查看的数据字典", listData);
    }

    @RequestMapping("formItem")
    @RequiresPermissions("dataDict:formItem")
    public void formItem(ModelMap model, HttpServletRequest request) {
        int parentId = ParamUtils.getInt(request, "parentId", 0);
        int id = ParamUtils.getInt(request, "id", 0);
        DataDict dataDict = this.dataDictService.findOne(id);
        if (dataDict == null) {
            dataDict = new DataDict();
            DataDict parent = this.dataDictService.findOne(parentId);
            dataDict.setDataDict(parent);
            dataDict.setEnabled(0);
        }
        model.addAttribute("dataDict", dataDict);
    }

    /**
     * 新增内容保存方法
     *
     * @param request
     * @return
     */
    @MyLog(value = "数据字典保存")
    @RequestMapping("saveItem")
    @ResponseBody
    @RequiresPermissions("dataDict:saveItem")//此处不配置，则此方法将不经过权限验证
    public Response saveItem(HttpServletRequest request) throws AjaxException {
        Integer id = ParamUtils.getInt(request, "id", 0);
        int parentId = ParamUtils.getInt(request, "parentId", 0);
        DataDict dataDict = this.dataDictService.findOne(id);
        DataDict parent = this.dataDictService.findOne(parentId);
        if (parent == null) {
            throw new AjaxException("002", "请选择需要添加的数据字典");
        }
        if (dataDict == null) {
            dataDict = new DataDict();
            dataDict.setDataDict(parent);
        }
        ServletRequestDataBinder binder = new ServletRequestDataBinder(dataDict);
        binder.bind(request);
        this.dataDictService.save(dataDict);
        return new Response("0001", "更新成功");
    }

    @MyLog(value = "数据字典状态设置")
    @RequestMapping("setEnabled")
    @ResponseBody
    @RequiresPermissions("dataDict:setEnabled")//此处不配置，则此方法将不经过权限验证
    public Response setEnabled(HttpServletRequest request) {
        Integer id = ParamUtils.getInt(request, "id", 0);
        Integer enabled = ParamUtils.getInt(request, "enabled", 0);
        DataDict dataDict = this.dataDictService.findOne(id);
        boolean bl = enabled == 0 || enabled == 1;
        if (dataDict == null || !bl) {
            throw new AjaxException("002", "无效操作");
        }
        dataDict.setEnabled(enabled);
        this.dataDictService.save(dataDict);
        return new Response("0001", "更新成功");
    }
}
