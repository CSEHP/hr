package com.app.system.permission.web.permission;

import com.app.system.common.EncipherPwd;
import com.app.system.permission.service.ShiroService;
import com.google.common.base.Strings;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 密码生成管理
 * Created by wcf-pc on 2018/12/5.
 */
@RestController
@SuppressWarnings("all")
public class CreatePassWordController {

    @Resource
    private EncipherPwd encipherPwd;
    @Resource
    private ShiroService shiroService;

    /**
     * 获得一个加密
     *
     * @param username
     * @param password
     * @return
     */
    @RequestMapping(value = {"/createPwd/{username}/{password}"})
    public Map<String, String> getEncipherPwd(@PathVariable("username") String username, @PathVariable("password") String password) {
        if (Strings.isNullOrEmpty(username) || Strings.isNullOrEmpty(password)) {
            return null;
        }
        return this.encipherPwd.getEncipherPwd(username, password);
    }

}
