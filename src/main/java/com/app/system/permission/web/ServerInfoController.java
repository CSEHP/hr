package com.app.system.permission.web;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.hyperic.sigar.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created by WCF on 2019/9/8.
 */
@RestController
@SuppressWarnings("all")
public class ServerInfoController {
    /**
     * 服务器信息
     */

    @RequestMapping(value = "/server")
    public Map serverInfo(HttpServletResponse response) {
        Properties props = System.getProperties();
        Map map = System.getenv();
        Map dataMap = Maps.newLinkedHashMap();
        dataMap.put("server.user.name", map.get("USERNAME")); //用户名
        dataMap.put("server.computer.name", map.get("COMPUTERNAME")); //计算机名
        dataMap.put("server.computer.domain", map.get("USERDOMAIN")); //计算机域名
        InetAddress addr = null;
        try {
            addr = InetAddress.getLocalHost();
            dataMap.put("server.ip", addr.getHostAddress()); //本机ip
            dataMap.put("server.host.name", addr.getHostName()); //本机主机名
            dataMap.put("server.user.home", props.getProperty("user.home")); //用户的主目录
            dataMap.put("server.user.dir", props.getProperty("user.dir")); //用户的当前工作目录
        } catch (Exception e) {
        }
        return dataMap;
    }


    /**
     * 系统信息
     */

    @RequestMapping(value = "/system")
    public Map systemInfo(HttpServletResponse response) {
        OperatingSystem OS = OperatingSystem.getInstance();
        Map dataMap = Maps.newLinkedHashMap();
        dataMap.put("os.name", OS.getVendorName()); //操作系统名称
        dataMap.put("os.arch", OS.getArch()); //内核构架
        dataMap.put("os.description", OS.getDescription()); //操作系统的描述
        dataMap.put("os.version", OS.getVersion()); //操作系统的版本号
        return dataMap;
    }

    /**
     * CPU信息
     *
     * @throws SigarException
     */
    @RequestMapping(value = "/cpu")
    public List cpuInfo(HttpServletResponse response) throws SigarException {
        Sigar sigar = new Sigar();
        CpuInfo infos[] = sigar.getCpuInfoList();
        CpuPerc cpuList[] = sigar.getCpuPercList();
        List<Map> dataMap = Lists.newArrayList();
        for (int i = 0, len = infos.length; i < len; i++) {// 不管是单块CPU还是多CPU都适用
            CpuInfo info = infos[i];
            Map<String, Object> item = Maps.newLinkedHashMap();
            item.put("mhz", info.getMhz()); //CPU的总量MHz
            item.put("company", info.getVendor()); //CPU的厂商
            item.put("model", info.getModel()); //CPU型号类别
            item.put("cache.size", info.getCacheSize()); // 缓冲缓存数量
            CpuPerc cpu = cpuList[i];
            item.put("freq.user", CpuPerc.format(cpu.getUser())); //CPU的用户使用率
            item.put("freq.sys", CpuPerc.format(cpu.getSys())); //CPU的系统使用率
            item.put("freq.wait", CpuPerc.format(cpu.getWait())); //CPU的当前等待率
            item.put("freq.nice", CpuPerc.format(cpu.getNice())); //CPU的当前错误率
            item.put("freq.idle", CpuPerc.format(cpu.getIdle())); //CPU的当前空闲率
            item.put("freq.combined", CpuPerc.format(cpu.getCombined())); //CPU总的使用率
            dataMap.add(item);
        }
        return dataMap;
    }

    /**
     * JVM信息
     *
     * @throws UnknownHostException
     */
    @RequestMapping(value = "/jvm")
    public Map jvmInfo(HttpServletResponse response) throws UnknownHostException {
        Runtime r = Runtime.getRuntime();
        Properties props = System.getProperties();
        Map dataMap = Maps.newLinkedHashMap();
        dataMap.put("jvm.memory.total", r.totalMemory()); //JVM可以使用的总内存
        dataMap.put("jvm.memory.free", r.freeMemory()); //JVM可以使用的剩余内存
        dataMap.put("jvm.processor.avaliable", r.availableProcessors()); //JVM可以使用的处理器个数
        dataMap.put("jvm.java.version", props.getProperty("java.version")); //Java的运行环境版本
        dataMap.put("jvm.java.vendor", props.getProperty("java.vendor")); //Java的运行环境供应商
        dataMap.put("jvm.java.home", props.getProperty("java.home")); //Java的安装路径
        dataMap.put("jvm.java.specification.version", props.getProperty("java.specification.version")); //Java运行时环境规范版本
        dataMap.put("jvm.java.class.path", props.getProperty("java.class.path")); //Java的类路径
        dataMap.put("jvm.java.library.path", props.getProperty("java.library.path")); //Java加载库时搜索的路径列表
        dataMap.put("jvm.java.io.tmpdir", props.getProperty("java.io.tmpdir")); //默认的临时文件路径
        dataMap.put("jvm.java.ext.dirs", props.getProperty("java.ext.dirs")); //扩展目录的路径
        return dataMap;
    }

    /**
     * 内存信息
     *
     * @throws SigarException
     */
    @RequestMapping(value = "/memory")
    public Map memoryInfo(HttpServletResponse response) throws SigarException {
        Sigar sigar = new Sigar();
        Mem mem = sigar.getMem();
        Map dataMap = Maps.newLinkedHashMap();
        dataMap.put("memory.total", mem.getTotal() / (1024 * 1024L));// 内存总量
        dataMap.put("memory.used", mem.getUsed() / (1024 * 1024L));// 当前内存使用量
        dataMap.put("memory.free", mem.getFree() / (1024 * 1024L));// 当前内存剩余量
        Swap swap = sigar.getSwap();
        dataMap.put("memory.swap.total", swap.getTotal() / (1024 * 1024L));// 交换区总量
        dataMap.put("memory.swap.used", swap.getUsed() / (1024 * 1024L));// 当前交换区使用量
        dataMap.put("memory.swap.free", swap.getFree() / (1024 * 1024L));// 当前交换区剩余量
        return dataMap;
    }

    /**
     * 磁盘文件信息
     *
     * @throws SigarException
     */
    @RequestMapping(value = "/file")
    public List fileSystemInfo(HttpServletResponse response) throws SigarException {
        Sigar sigar = new Sigar();
        FileSystem fslist[] = sigar.getFileSystemList();
        List<Map> listMap = Lists.newArrayList();
        for (int i = 0, len = fslist.length; i < len; i++) {
            FileSystem fs = fslist[i];
            Map<String, Object> data = Maps.newLinkedHashMap();
            data.put("dev.name", fs.getDevName()); //分区盘符名称
            data.put("dir.name", fs.getDirName()); //分区盘符名称
            data.put("flags", fs.getFlags()); //分区盘符类型
            data.put("sys.type.name", fs.getSysTypeName()); //文件系统类型
            data.put("type.name", fs.getTypeName()); //分区盘符类型名
            data.put("type", fs.getType()); //分区盘符文件系统类型
            FileSystemUsage usage = null;
            try {
                usage = sigar.getFileSystemUsage(fs.getDirName());
            } catch (Exception e) {
            }
            if (usage == null) {
                continue;
            }
            switch (fs.getType()) {
                case 0: // TYPE_UNKNOWN ：未知
                    break;
                case 1: // TYPE_NONE
                    break;
                case 2: // TYPE_LOCAL_DISK : 本地硬盘
                    data.put("usage.totle", usage.getTotal() / 1024); // 分区总大小
                    data.put("usage.free", usage.getFree() / 1024); // 分区剩余大小
                    data.put("usage.avail", usage.getAvail() / 1024); // 分区可用大小
                    data.put("usage.used", usage.getUsed() / 1024); // 分区已经使用量
                    data.put("usage.use.percent", usage.getUsePercent() * 100D); // 分区资源的利用率
                    break;
                case 3:// TYPE_NETWORK ：网络
                    break;
                case 4:// TYPE_RAM_DISK ：闪存
                    break;
                case 5:// TYPE_CDROM ：光驱
                    break;
                case 6:// TYPE_SWAP ：页面交换
                    break;
            }
            data.put("disk.reads", usage.getDiskReads()); // 读出
            data.put("disk.writes", usage.getDiskWrites()); // 写入
            listMap.add(data);

        }
        return listMap;
    }


    /**
     * 网络信息
     *
     * @throws SigarException
     */
    @RequestMapping(value = "/net")
    public List netInfo(HttpServletResponse response) throws SigarException {
        Sigar sigar = new Sigar();
        String ifNames[] = sigar.getNetInterfaceList();
        List<Map> listMap = Lists.newArrayList();
        for (int i = 0, len = ifNames.length; i < len; i++) {
            String name = ifNames[i];
            Map<String, Object> data = Maps.newLinkedHashMap();
            NetInterfaceConfig ifconfig = sigar.getNetInterfaceConfig(name);
            data.put("name", name); // 网络设备名
            data.put("address", ifconfig.getAddress()); // IP地址
            data.put("mask", ifconfig.getNetmask()); // 子网掩码
            if ((ifconfig.getFlags() & 1L) <= 0L) {
//                logger.info("!IFF_UP...skipping getNetInterfaceStat");
                continue;
            }
            NetInterfaceStat ifstat = sigar.getNetInterfaceStat(name);
            data.put("rx.packets", ifstat.getRxPackets());// 接收的总包裹数
            data.put("tx.packets", ifstat.getTxPackets());// 发送的总包裹数
            data.put("rx.bytes", ifstat.getRxBytes());// 接收到的总字节数
            data.put("tx.bytes", ifstat.getTxBytes());// 发送的总字节数
            data.put("rx.errors", ifstat.getRxErrors());// 接收到的错误包数
            data.put("tx.errors", ifstat.getTxErrors());// 发送数据包时的错误数
            data.put("rx.dropped", ifstat.getRxDropped());// 接收时丢弃的包数
            data.put("tx.dropped", ifstat.getTxDropped());// 发送时丢弃的包数
            listMap.add(data);
        }
        return listMap;
    }


    /**
     * 以太网信息
     *
     * @throws SigarException
     */
    @RequestMapping(value = "/ethernet")
    public List ethernetInfo(HttpServletResponse response) throws SigarException {
        Sigar sigar = new Sigar();
        String[] ifaces = sigar.getNetInterfaceList();
        List<Map> listMap = Lists.newArrayList();
        for (int i = 0, len = ifaces.length; i < len; i++) {
            NetInterfaceConfig cfg = sigar.getNetInterfaceConfig(ifaces[i]);
            if (NetFlags.LOOPBACK_ADDRESS.equals(cfg.getAddress()) || (cfg.getFlags() & NetFlags.IFF_LOOPBACK) != 0 || NetFlags.NULL_HWADDR.equals(cfg.getHwaddr())) {
                continue;
            }
            Map<String, Object> data = Maps.newLinkedHashMap();
            data.put("address", cfg.getAddress());// IP地址
            data.put("broad.cast", cfg.getBroadcast());// 网关广播地址
            data.put("hwaddr", cfg.getHwaddr());// 网卡MAC地址
            data.put("net.mask", cfg.getNetmask());// 子网掩码
            data.put("description", cfg.getDescription());// 网卡描述信息
            data.put("type", cfg.getType());// 网卡类型
            listMap.add(data);
        }
        return listMap;
    }
}
