package com.app.system.permission.web.dataBase;

import com.app.system.jpa.PageBean;
import com.app.system.permission.model.DataBase;
import com.app.system.permission.service.DataBaseService;
import com.app.system.utils.CollectionUtils;
import com.app.system.utils.WebUtils;
import com.app.system.utils.mysql.MySqlBackUtils;
import com.app.system.utils.dataType.DateUtils;
import com.app.system.utils.exception.Response;
import com.app.system.utils.file.FileUtils;
import com.app.system.utils.mysql.MySqlProperty;
import com.app.system.utils.page.LayUiUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.hibernate.annotations.Source;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * application-prod.yml 当数据库配置文件发生变化时，需及时配置信息
 * Created by wcf-pc on 2019/1/26.
 */
@Controller
@RequestMapping("/permission/dataBase/*")
@SuppressWarnings("all")
public class DataBaseManager {


    @Resource
    DataBaseService dataBaseService;
    @Resource
    MySqlProperty mySqlProperty;

    /**
     * 管理列表
     */
    @RequestMapping("list")
    @RequiresPermissions("dataBase:list")
    public void list() {
    }

    /**
     * json数据加载
     */
    @RequestMapping("listJson")
    @ResponseBody
    @RequiresPermissions("dataBase:listJson")
    public Map listJson() {
        List<Map> listData = Lists.newArrayList();
        PageBean<DataBase> pageBean = this.dataBaseService.pageList();
        List<DataBase> resultList = pageBean.getDatas();
        if (CollectionUtils.isNotEmpty(resultList)) {
            Map<String, Object> item = null;
            for (DataBase dataBase : resultList) {
                item = null;
                item = Maps.newHashMap();
                item.put("id", dataBase.getId());
                item.put("fileName", dataBase.getFileName());
                item.put("filePath", dataBase.getFilePath());
                item.put("fileSize", FileUtils.getFileSizeStr(dataBase.getFileSize()));
                item.put("fileDate", DateUtils.dateToStringFormat(dataBase.getFileDateLong(), "yyyy-MM-dd HH:mm:ss.sss"));
                listData.add(item);
            }
        }
        return LayUiUtils.page(pageBean.getTotalCount().intValue(), listData);
    }

    /**
     * 数据库备份
     *
     * @return
     */
    @RequestMapping("back")
    @ResponseBody
    public Response backDataBase() {
        mySqlProperty.back();
        return new Response("0001", "备份成功");
    }

    /**
     * 文件下载
     *
     * @param response
     */
    @RequestMapping("down/{id}")
    public void down(HttpServletResponse response, @PathVariable("id") int id) {
        DataBase dataBase = this.dataBaseService.findOne(id);
        try {
            FileUtils.downOrOnLine(dataBase.getFilePath(), response, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
