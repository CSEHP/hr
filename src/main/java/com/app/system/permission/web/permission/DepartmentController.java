package com.app.system.permission.web.permission;

import com.app.log.annotation.MyLog;
import com.app.system.permission.model.Department;
import com.app.system.permission.service.DepartmentService;
import com.app.system.utils.ParamUtils;
import com.app.system.utils.exception.Precondition;
import com.app.system.utils.exception.Response;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 部门管理
 * Created by wcf-pc on 2018/12/25.
 */
@Controller
@RequestMapping("/permission/department/*")
@SuppressWarnings("all")
public class DepartmentController {

    @Resource
    private DepartmentService departmentService;

    @RequestMapping(value = "list")
    @RequiresPermissions("department:list")
    public void list() {

    }

    @RequestMapping(value = "listJson")
    @ResponseBody
    @RequiresPermissions("department:listJson")
    public List listJson() {
        List<Map> listData = Lists.newLinkedList();
        List<Department> allDepartmentList = this.departmentService.allDepartmentList();
        if (allDepartmentList != null) {
            Map<String, Object> item = null;
            for (Department department : allDepartmentList) {
                item = null;
                item = Maps.newHashMap();
                item.put("id", department.getId());
                item.put("pid", department.getDepartment() == null ? 0 : department.getDepartment().getId());
                item.put("title", department.getName() + "【<span style='color:red;'>" + department.getSortIndex() + "</span>】");
                item.put("code", department.getCode());

                listData.add(item);
            }
        }
//        Map<String, Object> result = Maps.newHashMap();
//        result.put("code", "0");
//        result.put("msg", "");
//        result.put("count", 0);
//        result.put("data", listData);
//        result.put("is", true);
//        result.put("tip", "操作成功");
        return listData;
    }

    @RequestMapping(value = "form")
    @RequiresPermissions("department:form")
    public void form(HttpServletRequest request, ModelMap model) {
        int id = ParamUtils.getInt(request, "id", 0);
        Department department = departmentService.findById(id);
        int pid = ParamUtils.getInt(request, "pid", 0);
        Department parent = departmentService.findById(pid);
        if (department == null) {
            department = new Department();
            department.setDepartment(parent);
        }

        model.addAttribute("department", department);
    }

    @MyLog(value = "部门信息保存")
    @RequestMapping(value = "save")
    @ResponseBody
    @RequiresPermissions("department:save")
    public Response save(HttpServletRequest request, ModelMap model) {
        int id = ParamUtils.getInt(request, "id", 0);
        int parentId = ParamUtils.getInt(request, "parentId", 0);
        Department department = departmentService.findById(id);
        department = department == null ? new Department() : department;
        ServletRequestDataBinder binder = new ServletRequestDataBinder(department);
        binder.bind(request);
        if (parentId > 0 && department.getDepartment() == null) {
            Department parent = new Department();
            parent.setId(parentId);
            department.setDepartment(parent);
        }
        this.departmentService.save(department);

        return new Response("0001", "保存成功");
    }

    @RequestMapping(value = "validateCode")
    @ResponseBody
    public Response validateCode(HttpServletRequest request, ModelMap model) {
        int id = ParamUtils.getInt(request, "id", 0);
        Department department = this.departmentService.findById(id);
        String code = ParamUtils.getString(request, "code", "");
        Department validateDepartment = this.departmentService.findByCode(code);
        Precondition.checkAjaxArguement(validateDepartment == null || (department != null && department.getId().equals(validateDepartment.getId()) && validateDepartment.getCode().equals(department.getCode())), "1111", "编号存在");

        return new Response("0001", "验证通过");
    }

    @MyLog(value = "部门信息删除")
    @RequestMapping(value = "del")
    @ResponseBody
    @RequiresPermissions("department:del")
    public Response del(HttpServletRequest request, ModelMap model) {
        int id = ParamUtils.getInt(request, "id", 0);
        try {
            this.departmentService.delById(id);
        } catch (Exception e) {
            return new Response("0002", "使用中，无法删除");
        }

        return new Response("0001", "删除成功");
    }

    @RequestMapping(value = "getDepartmentTreeJson")
    @ResponseBody
//    @RequiresPermissions("department:getDepartmentTreeJson")
    public List getDepartmentTreeJson(HttpServletRequest request, ModelMap model) {
        List<Map> resultList = Lists.newLinkedList();
        int dptId = ParamUtils.getInt(request, "keyword", 0);
        List<Department> departments = this.departmentService.findParentList();
        if (departments != null) {
            for (Department department : departments) {
                Map map = Maps.newHashMap();
                map.put("name", department.getName());
                map.put("value", department.getId());
                if (department.getId() == dptId) {
                    map.put("selected", "selected");
                }
                List<Map> itemList = Lists.newLinkedList();
                treeJson(itemList, department.getDepartmentList(), dptId);
                map.put("children", itemList);
                resultList.add(map);
            }
        }
        return resultList;
    }

    private void treeJson(List<Map> itemList, List<Department> childrenList, int key) {
        if (childrenList != null) {
            for (Department department : childrenList) {
                Map map = Maps.newHashMap();
                map.put("name", department.getName());
                map.put("value", department.getId());
                if (department.getId() == key) {
                    map.put("selected", "selected");
                }
                List<Map> item = Lists.newLinkedList();
                treeJson(item, department.getDepartmentList(), key);
                map.put("children", item);
                itemList.add(map);
            }
        }
    }
}
