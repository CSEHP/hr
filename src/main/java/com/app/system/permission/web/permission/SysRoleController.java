package com.app.system.permission.web.permission;

import com.app.log.annotation.MyLog;
import com.app.system.permission.model.SysPermission;
import com.app.system.permission.model.SysRole;
import com.app.system.permission.service.SysPermissionService;
import com.app.system.permission.service.SysRoleService;
import com.app.system.utils.ParamUtils;
import com.app.system.utils.exception.Precondition;
import com.app.system.utils.exception.Response;
import com.app.system.utils.page.LayUiUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by wcf-pc on 2018/5/29.
 */
@Controller
@RequestMapping("/permission/sysRole/*")
@SuppressWarnings("all")
public class SysRoleController {

    @Resource
    private SysRoleService sysRoleService;
    @Resource
    private SysPermissionService sysPermissionService;

    @RequestMapping("list")
    @RequiresPermissions("sysRole:list")
    public void list(HttpServletRequest request, ModelMap model) {

    }

    @RequestMapping("loadData")
    @ResponseBody
    @RequiresPermissions("sysRole:loadData")
    public Map loadData() {

        List<SysRole> list = this.sysRoleService.findAll();

        List<Map> listData = Lists.newArrayList();
        if (!CollectionUtils.isEmpty(list)) {
            for (SysRole sysRole : list) {
                Map<String, Object> item = Maps.newHashMap();
                item.put("id", sysRole.getId());
                item.put("role", sysRole.getRole());
                item.put("description", sysRole.getDescription());
                item.put("available", sysRole.getAvailable());
                item.put("sortIndex", sysRole.getSortIndex());

                listData.add(item);
            }
        }


        return LayUiUtils.list("请点击需要查看的数据字典", listData);
    }

    @RequestMapping("form")
    @RequiresPermissions("sysRole:form")
    public void form(HttpServletRequest request, ModelMap model) {
        Integer id = ParamUtils.getInt(request, "id", 0);
        SysRole sysRole = this.sysRoleService.findOne(id);
        if (sysRole == null) {
            sysRole = new SysRole();
        }
        model.addAttribute("sysRole", sysRole);
    }

    @MyLog(value = "角色信息保存")
    @RequestMapping("save")
    @ResponseBody
    @RequiresPermissions("sysRole:save")
    public Response save(HttpServletRequest request, ModelMap model) {
        Integer id = ParamUtils.getInt(request, "id", 0);
        SysRole sysRole = this.sysRoleService.findOne(id);
        if (sysRole == null) {
            sysRole = new SysRole();
        }
        ServletRequestDataBinder binder = new ServletRequestDataBinder(sysRole);
        binder.bind(request);
        this.sysRoleService.save(sysRole);
        return new Response("0001", "保存成功");
    }

    @MyLog(value = "角色信息删除")
    @RequestMapping("deleteData")
    @ResponseBody
    @RequiresPermissions("sysRole:deleteData")
    public Response deleteData(HttpServletRequest request) {
        String idsString = ParamUtils.getString(request, "ids", "");
        if (!Strings.isNullOrEmpty(idsString)) {
            String[] ids = idsString.split(",");
            String result = this.sysRoleService.deleteByIds(ids);
            String ok = "ok";
            if (ok.equals(result)) {
                return new Response("0001", "删除成功");
            }
        }
        return new Response("0002", "删除失败");
    }

    @MyLog(value = "角色信息状态设置")
    @RequestMapping("setAvailable")
    @ResponseBody
    @RequiresPermissions("sysRole:setAvailable")
    public Response setAvailable(HttpServletRequest request) {
        int id = ParamUtils.getInt(request, "id", 0);
        SysRole sysRole = this.sysRoleService.findOne(id);
        Precondition.checkAjaxArguement(sysRole != null, "0002", "无效操作");
        int available = ParamUtils.getInt(request, "available", 0);
        Boolean availableBl = available == 0 ? true : false;
        sysRole.setAvailable(availableBl);
        this.sysRoleService.save(sysRole);
        return new Response("0001", "设置成功");
    }

    @RequestMapping("setPermission")
    @RequiresPermissions("sysRole:setPermission")
    public void setPermission(HttpServletRequest request, ModelMap model) {
        Integer id = ParamUtils.getInt(request, "id", 0);
        SysRole sysRole = this.sysRoleService.findOne(id);
        if (sysRole == null) {
            sysRole = new SysRole();
        }
        model.addAttribute("sysRole", sysRole);
    }

    @RequestMapping("permissionJson")
    @ResponseBody
    @RequiresPermissions("sysRole:permissionJson")
    public Map permissionJson(HttpServletRequest request) {

        List<Map> listData = this.sysPermissionService.mapPermissionJsonData();

        int id = ParamUtils.getInt(request, "id", 0);
        SysRole sysRole = this.sysRoleService.findOne(id);
        List<SysPermission> sysPermissionList = sysRole.getPermissions();
        List<Integer> sysPermissionIds = Lists.newArrayList();
        if (!CollectionUtils.isEmpty(sysPermissionList)) {
            for (SysPermission sysPermission : sysPermissionList) {
                sysPermissionIds.add(sysPermission.getId());
            }
        }
        Map result = Maps.newHashMap();
        result.put("list", listData);
        result.put("checkedIds", sysPermissionIds);
        return result;
    }

    @MyLog(value = "角色信息权限配置保存")
    @RequestMapping("saveRolePermission")
    @ResponseBody
    @RequiresPermissions("sysRole:saveRolePermission")
    public Response saveRolePermission(HttpServletRequest request) {
        int roleId = ParamUtils.getInt(request, "roleId", 0);
        SysRole sysRole = this.sysRoleService.findOne(roleId);
        Precondition.checkAjaxArguement(sysRole != null, "0002", "无效操作");

        String authids = ParamUtils.getString(request, "authids", "");
        List<SysPermission> sysPermissionList = Lists.newArrayList();
        if (authids != null && authids.length() > 0) {
            String[] authArray = authids.split("、");
            for (String s : authArray) {
                SysPermission sys = new SysPermission();
                sys.setId(Integer.valueOf(s));
                sysPermissionList.add(sys);
            }
        }
        sysRole.setPermissions(sysPermissionList);

        this.sysRoleService.save(sysRole);

        return new Response("0001", "设置成功");
    }
}
