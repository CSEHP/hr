package com.app.system.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 配置参数
 * Created by wcf-pc on 2018/12/5.
 */
@Component
@PropertySource(value = {"classpath:config/systemParam.properties"}, ignoreResourceNotFound = true, encoding = "utf-8")
@ConfigurationProperties(prefix = "system")
@SuppressWarnings("all")
public class SystemParamProperties {
 /*以下属性的名称必须和systemParam.properties文件内的属性命名一致*/

    private String loginUrl;//登录指向地址
    private String successUrl;//登录成功及默认页地址

    private boolean moreLogin;//统一个账号重复登录限制

    private String hashedCredentialsMatcherHashAlgorithmName;//加密方式
    private int hashedCredentialsMatcherHashIterations;//加密次数

    private String initPassword;//用户初始密码

    //系统权限日志记录开关
    private boolean isLogForSystem;
    //业务办理日志记录开关
    private boolean isLogForBusiness;
    //钥匙盘使用开关
    private boolean keyUsed;

    private String staticAccessPath;//静态资源对外暴露的访问路径
    private String uploadFolder;//文件上传路径（绝对路径）

    //mysql数据库备份
    private String mysqlBackDataPath;//数据库备份存放绝对路径

    //druid数据监控登录页面用户名、密码
    private String druidUserName;
    private String druidPassword;

    public String getLoginUrl() {
        return loginUrl;
    }

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }

    public String getSuccessUrl() {
        return successUrl;
    }

    public void setSuccessUrl(String successUrl) {
        this.successUrl = successUrl;
    }

    public boolean getMoreLogin() {
        return moreLogin;
    }

    public void setMoreLogin(boolean moreLogin) {
        this.moreLogin = moreLogin;
    }

    public String getHashedCredentialsMatcherHashAlgorithmName() {
        return hashedCredentialsMatcherHashAlgorithmName;
    }

    public void setHashedCredentialsMatcherHashAlgorithmName(String hashedCredentialsMatcherHashAlgorithmName) {
        this.hashedCredentialsMatcherHashAlgorithmName = hashedCredentialsMatcherHashAlgorithmName;
    }

    public int getHashedCredentialsMatcherHashIterations() {
        return hashedCredentialsMatcherHashIterations;
    }

    public void setHashedCredentialsMatcherHashIterations(int hashedCredentialsMatcherHashIterations) {
        this.hashedCredentialsMatcherHashIterations = hashedCredentialsMatcherHashIterations;
    }

    public String getInitPassword() {
        return initPassword;
    }

    public void setInitPassword(String initPassword) {
        this.initPassword = initPassword;
    }

    public String getStaticAccessPath() {
        return staticAccessPath;
    }

    public void setStaticAccessPath(String staticAccessPath) {
        this.staticAccessPath = staticAccessPath;
    }

    public String getUploadFolder() {
        return uploadFolder;
    }

    public void setUploadFolder(String uploadFolder) {
        this.uploadFolder = uploadFolder;
    }

    public String getMysqlBackDataPath() {
        return mysqlBackDataPath;
    }

    public void setMysqlBackDataPath(String mysqlBackDataPath) {
        this.mysqlBackDataPath = mysqlBackDataPath;
    }

    public boolean getIsLogForSystem() {
        return this.isLogForSystem;
    }

    public void setIsLogForSystem(boolean isLogForSystem) {
        this.isLogForSystem = isLogForSystem;
    }

    public boolean getIsLogForBusiness() {
        return this.isLogForBusiness;
    }

    public void setIsLogForBusiness(boolean isLogForBusiness) {
        this.isLogForBusiness = isLogForBusiness;
    }

    public String getDruidUserName() {
        return druidUserName;
    }

    public void setDruidUserName(String druidUserName) {
        this.druidUserName = druidUserName;
    }

    public String getDruidPassword() {
        return druidPassword;
    }

    public void setDruidPassword(String druidPassword) {
        this.druidPassword = druidPassword;
    }

    public boolean isKeyUsed() {
        return keyUsed;
    }

    public void setKeyUsed(boolean keyUsed) {
        this.keyUsed = keyUsed;
    }
}
