package com.app.system.config.task;

import com.app.system.utils.ApplicationContextProvider;
import com.app.system.utils.mysql.MySqlProperty;

import java.util.concurrent.ScheduledFuture;

/**
 * 数据库备份定时器
 * Created by wcf-pc on 2019/1/27.
 */
@SuppressWarnings("all")
public class MySqlBackRunnable implements Runnable {

    public static ScheduledFuture<?> future;//任务

    private MySqlProperty mySqlProperty() {
        return new ApplicationContextProvider().getBean(MySqlProperty.class);
    }

    @Override
    public void run() {
        mySqlProperty().back();
    }
}
