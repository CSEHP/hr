package com.app.system.config.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by wcf-pc on 2018/4/23.
 */
@Component
public class TaskDemo {
    private static final Logger log = LoggerFactory.getLogger(TaskDemo.class);
    /**
     * 每个5分钟执行一次
     */
    @Scheduled(cron = "0 0/5 * * * *")
    public void timer() {
        log.info("");
    }
}
