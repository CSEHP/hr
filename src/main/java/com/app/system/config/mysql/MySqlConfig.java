package com.app.system.config.mysql;

import org.hibernate.dialect.MySQL5InnoDBDialect;

/**
 * Created by ZYK on 2019/3/12.
 */
public class MySqlConfig extends MySQL5InnoDBDialect {
    @Override
    public String getTableTypeString() {
        return " ENGINE=InnoDB DEFAULT CHARSET=utf8";
    }
}
