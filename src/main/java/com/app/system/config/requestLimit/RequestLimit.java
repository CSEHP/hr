package com.app.system.config.requestLimit;

import java.lang.annotation.*;

/**
 * 根据现有业务来说，访问次数每秒50次已是比较大的并发量了
 * 后期若是业务扩大，并发量增加，可以继续增加
 * Created by WCF on 2019/9/4.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface RequestLimit {
    //允许访问的次数，默认值1500（按照1秒最大访问50次计算）
    int count() default 1500;

    // 时间段，单位为毫秒，默认值一分钟
    long time() default 30000;
}
