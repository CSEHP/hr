package com.app.system.common;

import com.app.system.permission.model.DataDict;
import com.app.system.permission.service.DataDictService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.hibernate.annotations.Source;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * Created by wcf-pc on 2018/12/10.
 */
@RestController
@SuppressWarnings("all")
public class ModulesController {

    @Resource
    private DataDictService dataDictService;

    @RequestMapping(value = "/dataDict/{parentName}", produces = "application/json;charset=UTF-8")
    public List dataDictList(@PathVariable("parentName") String parentName) {
        List<DataDict> dataDictList = this.dataDictService.findListByParentName(parentName);
        List<Map> dataList = Lists.newLinkedList();
        if (!CollectionUtils.isEmpty(dataDictList)) {
            for (DataDict dataDict : dataDictList) {
                Map map = Maps.newHashMap();
                map.put("name", dataDict.getName());
                map.put("value", dataDict.getValue());
                map.put("color", dataDict.getColor());
                dataList.add(map);
            }
        }
        return dataList;
    }
}
