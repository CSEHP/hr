package com.app.system.common;

import com.app.system.config.properties.SystemParamProperties;
import com.app.system.utils.dataType.DateUtils;
import com.app.system.utils.ParamUtils;
import com.app.system.utils.upload.UploadInfo;
import com.app.system.utils.upload.UploadInfoUtil;
import com.google.common.collect.Maps;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

/**
 * 档案资料上传管理
 * Created by wcf-pc on 2018/12/16.
 */
@RestController
@SuppressWarnings("all")
public class UpLoadController {

    @Resource
    private SystemParamProperties systemParamProperties;

    /**
     * 上传头像（涉及用户信息图片）
     * 存放在pic文件夹内
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/upload/uploadPic")
    public Map uploadPic(HttpServletRequest request) {

        int width = ParamUtils.getInt(request, "saveW", 200);
        int height = ParamUtils.getInt(request, "saveH", 200);
        String picDir = "pic";
        String uploadDir = systemParamProperties.getUploadFolder() + picDir;//图像上传目录
        String parameterName = "file";
        String random = new SecureRandomNumberGenerator().nextBytes().toHex();
        String saveFileName = DateUtils.dateToStringFormat(new Date(), "yyyyMMddHHmmssSSS") + random;
        boolean isCreateThumbnail = false;
        UploadInfo uploadInfo = UploadInfoUtil.uploadFile(request, uploadDir, parameterName, saveFileName, isCreateThumbnail, width, height);
        Map result = Maps.newLinkedHashMap();

        result.put("fileSize", uploadInfo.getFileSize());//文件大小
        result.put("staticAccessPath", systemParamProperties.getStaticAccessPath());//页面访问相对路径，不含名称
        result.put("uploadPath", picDir + "/" + uploadInfo.getSaveFilename());//上传的文件
        result.put("contentType", uploadInfo.getContentType());
        result.put("code", "0000");//保存成功状态
        result.put("message", "上传成功");//提示信息
        return result;
    }

    /**
     * 上传文件
     * 存放在file文件夹内（涉及系统业务档案资料管理）
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/upload/uploadFile")
    public Map uploadFile(HttpServletRequest request) {

        int width = ParamUtils.getInt(request, "saveW", 200);
        int height = ParamUtils.getInt(request, "saveH", 200);

        Date date = new Date();
        String picDir = "file" + DateUtils.dateToStringFormat(date, "yyyy/MM/dd/HH/");
        String uploadDir = systemParamProperties.getUploadFolder() + picDir;//图像上传目录
        String parameterName = "file";
        String random = new SecureRandomNumberGenerator().nextBytes().toHex();
        String saveFileName = DateUtils.dateToStringFormat(new Date(), "yyyyMMddHHmmssSSS") + random;
        boolean isCreateThumbnail = true;
        UploadInfo uploadInfo = UploadInfoUtil.uploadFile(request, uploadDir, parameterName, saveFileName, isCreateThumbnail, width, height);
        Map result = Maps.newLinkedHashMap();

        result.put("fileSize", uploadInfo.getFileSize());//文件大小
        result.put("staticAccessPath", systemParamProperties.getStaticAccessPath());//页面访问相对路径，不含名称
        result.put("uploadPath", picDir + "/" + uploadInfo.getSaveFilename());//上传的文件
        result.put("contentType", uploadInfo.getContentType());
        result.put("code", "0000");//保存成功状态
        result.put("message", "上传成功");//提示信息
        return result;
    }
}
