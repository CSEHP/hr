package com.app.system.jpa.en;

/**
 * 运算符
 * Created by wcf-pc on 2018/12/15.
 * 可以进行扩展
 */
public enum Operator {
    //等于、相似、不等于、大于、大于等于、小于、小于等于
    EQ, LIKE, NE, GT, GE, LT, LE;

    Operator() {
    }
}
