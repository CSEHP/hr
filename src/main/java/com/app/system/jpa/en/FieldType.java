package com.app.system.jpa.en;

/**
 * 数据类型
 * Created by wcf-pc on 2018/12/15.
 * 可以进行扩展
 *
 * @author wcf-pc
 */
public enum FieldType {
    //字符串类型、整形类型、浮点类型、日期类型（年月日）、日期类型（年月日时分秒）
    STRING, INTEGER, BIGDECIMAL, DATE, DATEFORMAT;

    FieldType() {
    }
}
