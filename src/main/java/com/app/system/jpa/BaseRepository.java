package com.app.system.jpa;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.io.Serializable;
import java.util.List;

/**
 * 构造动态查询仓库接口
 * Created by wcf-pc on 2018/12/7.
 */
@NoRepositoryBean
@SuppressWarnings("all")
public interface BaseRepository<T, ID extends Serializable> extends PagingAndSortingRepository<T, ID> {

    int count(Specification<T> specification);

    Page<T> findAll(Specification<T> specification, Pageable pageable);

    List<T> findAll(Specification<T> specification);

    List<T> findAll(Specification<T> specification, Sort sort);
}
