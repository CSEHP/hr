package com.app.system.jpa;

import com.google.common.collect.Lists;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分页对象
 * Created by wcf-pc on 2018/12/12.
 */
@Data
@SuppressWarnings("all")
public class PageBean<T> implements Serializable {
    /**
     * 总共的数据条数
     */
    private Long totalCount;
    /**
     * 第几页
     */
    private Integer pageNum = 1;
    /**
     * 每页显示多少
     */
    private Integer pageSize = 15;
    /**
     * 排序字段
     */
    private String orderField = "";
    /**
     * 排序方式
     */
    private String orderType = "asc";
    /**
     * 查询出来的数据
     */
    private List<T> datas = Lists.newArrayList();
}
