package com.app.system.utils;

import java.util.Collection;
import java.util.Map;

/**
 * map、list集合是否为空判断
 * Created by wcf-pc on 2019/1/26.
 */
@SuppressWarnings("all")
public class CollectionUtils {

    /**
     * 判断list集合是否不为空
     *
     * @param collection
     * @return
     */
    public static boolean isNotEmpty(Collection collection) {
        if (collection != null && collection.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 判断list集合是否为空
     *
     * @param collection
     * @return
     */
    public static boolean isEmpty(Collection collection) {
        return !isNotEmpty(collection);
    }

    /**
     * 判断map集合是否不为空
     *
     * @param map
     * @return
     */
    public static boolean isNotEmpty(Map map) {
        if (map != null && map.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 判断map集合是否为空
     *
     * @param map
     * @return
     */
    public static boolean isEmpty(Map map) {
        return !isNotEmpty(map);
    }

}
