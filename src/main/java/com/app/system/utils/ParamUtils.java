package com.app.system.utils;

import org.springframework.util.ResourceUtils;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.io.FileNotFoundException;
import java.math.BigDecimal;

/**
 * Created by wcf-pc on 2018/4/16.
 */
@SuppressWarnings("all")
public class ParamUtils {
    private ParamUtils() {
    }

    public static String getString(ServletRequest request, String s, String defaultString) {
        String s1 = getStringValues(request, s);
        return s1 != null && !"".equals(s1) ? s1.trim() : defaultString;
    }

    public static int getInt(ServletRequest request, String s, int defaultInt) {
        boolean j = false;

        int j1;
        try {
            String e = getString(request, s);
            if (e == null) {
                j1 = defaultInt;
            } else if ("".equals(e.trim())) {
                j1 = defaultInt;
            } else {
                j1 = Integer.parseInt(e);
            }
        } catch (NumberFormatException var5) {
            j1 = 0;
        }

        return j1;
    }

    public static Integer getInteger(ServletRequest request, String s, int defaultInt) {
        int v = getInt(request, s, defaultInt);
        return v == defaultInt ? null : new Integer(v);
    }

    public static long getLong(ServletRequest request, String s, long defaultLong) {
        long l = 0L;

        try {
            String e = getString(request, s);
            if (e == null) {
                l = defaultLong;
            } else {
                l = Long.parseLong(e);
            }
        } catch (NumberFormatException var7) {
            l = 0L;
        }

        return l;
    }

    public static float getFloat(HttpServletRequest request, String s, float defaultFloat) {
        float l = 0.0F;

        try {
            String e = getString(request, s);
            if (e == null) {
                l = defaultFloat;
            } else {
                l = Float.parseFloat(e);
            }
        } catch (NumberFormatException var5) {
            l = 0.0F;
        }

        return l;
    }

    public static double getDouble(HttpServletRequest request, String s, double defaultDouble) {
        double l = 0.0D;

        try {
            String e = getString(request, s);
            if (e == null) {
                l = defaultDouble;
            } else {
                l = Double.parseDouble(e);
            }
        } catch (NumberFormatException var7) {
            l = 0.0D;
        }

        return l;
    }

    public static BigDecimal getDecimal(HttpServletRequest request, String s, double defaultDouble) {
        BigDecimal l = null;

        try {
            String e = getString(request, s);
            if (e == null) {
                l = null;
            } else {
                l = new BigDecimal(e);
            }
        } catch (NumberFormatException var6) {
            ;
        }

        return l;
    }

    public static String[] getStringParameters(ServletRequest request, String name) {
        String[] array = request.getParameterValues(name);
        return array == null ? new String[0] : array;
    }

    public static int[] getIntParameters(ServletRequest request, String name) {
        String[] array = getStringParameters(request, name);
        int[] intArray = new int[array.length];

        for (int i = 0; i < array.length; ++i) {
            intArray[i] = Integer.parseInt(array[i]);
        }

        return intArray;
    }

    private static String getString(ServletRequest request, String s) {
        String temp = null;

        try {
            temp = request.getParameter(s);
        } catch (Exception var4) {
            System.out.println(var4);
        }

        return temp;
    }

    private static String getStringValues(ServletRequest request, String s) {
        String[] temp = null;

        try {
            temp = request.getParameterValues(s);
        } catch (Exception var5) {
            System.out.println(var5);
        }

        String s1 = "";

        for (int i = 0; temp != null && i < temp.length; ++i) {
            s1 = s1 + temp[i] + ",";
        }
        String comma = ",";
        if (s1.endsWith(comma)) {
            s1 = s1.substring(0, s1.length() - 1);
        }

        return s1;
    }

    public static String getRootPath() {
        try {
            return ResourceUtils.getURL("classpath:").getPath();//开发时该目录定位到target/classes下
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
