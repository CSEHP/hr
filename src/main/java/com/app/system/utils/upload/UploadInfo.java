package com.app.system.utils.upload;

/**
 * Created by wcf-pc on 2018/4/16.
 */
@SuppressWarnings("all")
public class UploadInfo {
    private long fileSize;
    private String originalFilename;
    private String saveFilename;
    private String contentType;

    public UploadInfo() {
    }

    public long getFileSize() {
        return this.fileSize;
    }

    public void setFileSize(long fileSize) {
        this.fileSize = fileSize;
    }

    public String getOriginalFilename() {
        return this.originalFilename;
    }

    public void setOriginalFilename(String originalFilename) {
        this.originalFilename = originalFilename;
    }

    public String getSaveFilename() {
        return this.saveFilename;
    }

    public void setSaveFilename(String saveFilename) {
        this.saveFilename = saveFilename;
    }

    public String getContentType() {
        return this.contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }
}
