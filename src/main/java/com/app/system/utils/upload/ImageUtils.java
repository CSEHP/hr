package com.app.system.utils.upload;

import java.io.File;
import java.util.Arrays;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.name.Rename;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;

/**
 * Created by wcf-pc on 2018/4/16.
 *
 * @author wcf-pc
 */
@SuppressWarnings("all")
public class ImageUtils {
    private static final Logger log = LoggerFactory.getLogger(ImageUtils.class);

    private ImageUtils() {
    }

    public static void doThumb(File file, int width) {
        doThumb(file, file, width, 1.0F);
    }

    public static void doThumbNewFile(File file, int width, String prefix) {

        String types = Arrays.toString(ImageIO.getReaderFormatNames());
        String suffix = null;
        // 获取图片后缀
        String dot = ".";
        if (file.getName().indexOf(dot) > -1) {
            suffix = file.getName().substring(file.getName().lastIndexOf(".") + 1);
        }// 类型和图片后缀全部小写，然后判断后缀是否合法
        if (suffix == null || types.toLowerCase().indexOf(suffix.toLowerCase()) < 0) {
            //log.error("Sorry, the image suffix is illegal. the standard image suffix is {}." + types);
            return;
        }

        String fileName = file.getAbsolutePath();
        int pos = fileName.lastIndexOf(File.separator);
        fileName = fileName.substring(0, pos + 1) + prefix + fileName.substring(pos + 1);
        System.out.println(fileName);
        doThumb(file, new File(fileName), width, 1.0F);
    }

    public static void doThumb(File file, int width, int height) {
        doThumb(file, file, width, height, 1.0F);
    }

    public static void doThumb(File file, int width, float quality) {
        doThumb(file, file, width, quality);
    }

    public static void doThumb(File file, int width, int height, float quality) {
        doThumb(file, file, width, height, quality);
    }

    public static void doThumb(File originFile, File outFile, int width, float quality) {
        try {
            Thumbnails.of(new File[]{originFile}).width(width).outputQuality(quality).toFile(outFile);
        } catch (Exception var5) {
            var5.printStackTrace();
        }

    }

    public static void doThumb(File originFile, File outFile, int width, int height, float quality) {
        try {
            Thumbnails.of(new File[]{originFile}).size(width, height).outputQuality(quality).toFile(outFile);
        } catch (Exception var6) {
            var6.printStackTrace();
        }

    }

    public static void doThumbs(File path, int width, float quality) {
        try {
            Thumbnails.of(path.listFiles()).width(width).outputQuality(quality).toFiles(Rename.NO_CHANGE);
        } catch (Exception var4) {
            var4.printStackTrace();
        }

    }
}
