package com.app.system.utils.pdf;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * pdf页面显示格式化
 * Created by WCF on 2019/9/4.
 */
@SuppressWarnings("all")
public class PdfShowUtils {

    /**
     * 所有钱款显示调用
     *
     * @param value
     * @return
     */
    public static String getMoneyShow(BigDecimal value) {
        if (value == null || value.compareTo(new BigDecimal(0)) <= 0) {
            return "/";
        } else {
            DecimalFormat df2 = new DecimalFormat("###,###.00");
            String temp2 = df2.format(value);
            return temp2;
        }
    }

    /**
     * 所有面积显示调用
     *
     * @param value
     * @return
     */
    public static String getAreaShow(BigDecimal value, String defaultValue) {
        return value == null || value.compareTo(new BigDecimal("0")) == 0 ? defaultValue : value.toString();
    }

    public static String getAreaShow(BigDecimal value) {
        return getAreaShow(value, "/");
    }

    /**
     * 所有字符串显示调用
     *
     * @param s
     * @return
     */
    public static String getStrShow(String value, String defaultValue) {
        return (value == null || value.trim().equals("")) ? defaultValue : value;
    }

    public static String getStrShow(String value) {
        return getStrShow(value, "/");
    }

    /**
     * 所有整数显示调用
     *
     * @param integer
     * @return
     */
    public static String getIntShow(Integer value, String defaultValue) {
        return (value == null || value <= 0) ? defaultValue : value.toString();
    }

    public static String getIntShow(Integer value) {
        return getIntShow(value, "/");
    }
}
