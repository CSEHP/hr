package com.app.system.utils.pdf;

import com.app.system.utils.WebUtils;
import freemarker.template.Configuration;

import java.io.File;
import java.io.IOException;

/**
 * Created by ZYK on 2019/1/16.
 */
@SuppressWarnings("all")
public class FreemarkerConfiguration {
    private static Configuration config = null;

    private FreemarkerConfiguration() {
    }

    public static synchronized Configuration getConfiguration() {
        if (config == null) {
            setConfiguration();
        }

        return config;
    }

    private static void setConfiguration() {
        config = new Configuration();
        String path = null;
        try {
            path = WebUtils.getRootPath() + "templates/printPdf";
            config.setDirectoryForTemplateLoading(new File(path));
        } catch (IOException var2) {
            var2.printStackTrace();
        }

    }
}
