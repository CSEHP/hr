package com.app.system.utils.pdf;

import com.app.system.log4j2.LogUtils;
import org.slf4j.Logger;
import org.springframework.util.ResourceUtils;
import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

/**
 * PDF流加载
 * Created by ZYK on 2019/1/16.
 */
@SuppressWarnings("all")
public class PdfDocumentGenerator {
    private static Logger logger = LogUtils.getBusinessLogger();

    public boolean generate(String template, Map<String, Object> variables, OutputStream outputStream) throws Exception {
        try {
            HtmlGenerator e = new HtmlGenerator();
            String htmlContent = e.generate(template, variables);
            this.generate(htmlContent, outputStream);
        } catch (Exception var6) {
            logger.error(var6.toString());
        }

        return true;
    }

    public void generate(String htmlContent, OutputStream outputStream) throws Exception {
        Object out = null;

        try {
            DocumentBuilder e = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = e.parse(new ByteArrayInputStream(htmlContent.getBytes("UTF-8")));

            try {
                ITextRenderer e1 = this.createTextRenderer();
                e1.setDocument(doc, (String) null);
                e1.layout();
                e1.createPDF(outputStream);
            } catch (Exception var11) {
                logger.error(var11.toString());
            }
        } catch (Exception var12) {
            throw var12;
        } finally {
            if (out != null) {
                ((OutputStream) out).close();
            }

        }

    }

    public ITextRenderer createTextRenderer() {
        ITextRenderer renderer = new ITextRenderer();
        ITextFontResolver fontResolver = renderer.getFontResolver();
        addFonts(fontResolver);
        return renderer;
    }

    public static ITextFontResolver addFonts(ITextFontResolver fontResolver) {
        String fontPath = null;
        try {
            fontPath = ResourceUtils.getURL("classpath:").getPath() + "static/fonts";
        } catch (IOException var2) {
            var2.printStackTrace();
        }
        File fontsDir = new File(fontPath);
        logger.debug("fontPath=" + fontsDir.getAbsolutePath());
        if (fontsDir != null && fontsDir.isDirectory()) {
            File[] files = fontsDir.listFiles();
            File[] arr = files;
            int len = files.length;

            for (int i = 0; i < len; ++i) {
                File file = arr[i];
                if (!file.isDirectory()) {
                    try {
                        logger.debug("字体文件：" + file.getAbsolutePath());
                        fontResolver.addFont(file.getAbsolutePath(), "Identity-H", false);
                    } catch (Exception var9) {
                        logger.debug("字体加载：" + var9.getMessage());
                        var9.printStackTrace();
                    }
                }
            }
        }

        return fontResolver;
    }
}
