package com.app.system.utils.pdf;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

/**
 * Created by ZYK on 2019/1/16.
 */
@SuppressWarnings("all")
public class HtmlGenerator {
    public String generate(String template, Map<String, Object> variables) throws IOException, TemplateException {
        BufferedWriter writer = null;
        StringWriter stringWriter = null;
        String htmlContent = "";

        try {
            Configuration e = FreemarkerConfiguration.getConfiguration();
            e.setDefaultEncoding("utf-8");
            Template tp = e.getTemplate(template);
            stringWriter = new StringWriter();
            writer = new BufferedWriter(stringWriter);
            tp.process(variables, writer);
            htmlContent = stringWriter.toString();
            writer.flush();
        } finally {
            try {
                if(stringWriter != null) {
                    stringWriter.close();
                }
            } catch (Exception var13) {
                ;
            }

            if(writer != null) {
                writer.close();
            }

        }

        return htmlContent;
    }
}
