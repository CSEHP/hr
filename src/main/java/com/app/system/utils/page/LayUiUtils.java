package com.app.system.utils.page;

import com.alibaba.druid.support.json.JSONUtils;
import com.google.common.collect.Maps;

import java.util.List;
import java.util.Map;

/**
 * 前端ui分页公共方法
 * Created by wcf-pc on 2018/8/30.
 * @author wcf-pc
 */
@SuppressWarnings("all")
public class LayUiUtils {

    private static Map map(String code, String msg, int count, Object data) {
        Map<String, Object> result = Maps.newHashMap();
        result.put("code", code);
        result.put("msg", msg);
        result.put("count", count);
        result.put("data", data);
        return result;
    }

    public static Map page(int totalCount, List<Map> listData) {
        return map("", "", totalCount, listData);
    }

    public static Map list(String message, List<Map> listData) {
        return map("", message, 0, listData);
    }

    public static Map list(List<Map> listData) {
        return map("", "", 0, listData);
    }
}
