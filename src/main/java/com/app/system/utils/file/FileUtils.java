package com.app.system.utils.file;

import com.app.system.utils.dataType.BigDecimalUtils;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.net.URL;
import java.util.List;
import java.util.Vector;

/**
 * Created by wcf-pc on 2019/1/26.
 */
@SuppressWarnings("all")
public class FileUtils {

    /**
     * 读Excel内容
     *
     * @param inputStream    Excel文件流
     * @param sheetIndex     sheet索引，第一个sheet为0
     * @param ignoreFirstRow 是否忽略第一行(考虑到表头问题)
     * @return
     * @throws Exception
     */
    public static List read(InputStream inputStream, int sheetIndex,
                            boolean ignoreFirstRow) throws Exception {

        List result = new Vector();

        Workbook workbook = Workbook.getWorkbook(inputStream);
        Sheet sheet = workbook.getSheet(sheetIndex);
        int firstNum = 0;
        int rowNum = sheet.getRows();
        int cellNum = sheet.getColumns();

        if (ignoreFirstRow) {
            firstNum++;
        }

        //循环读取内容
        for (int i = firstNum; i < rowNum; i++) {
            Vector rowList = new Vector();

            for (int ii = 0; ii < cellNum; ii++) {
                Cell cell = sheet.getCell(ii, i); //得到工作表的单元格
                String content = cell.getContents(); //getContents()将Cell中的字符转为字符串
                rowList.add(content);
            }
            result.add(rowList);
        }
        workbook.close();
        inputStream.close();
        return result;
    }

    /**
     * 文件在线打开、下载
     *
     * @param filePath
     * @param response
     * @param isOnLine true：打开   false：下载
     * @throws Exception
     */
    public static void downOrOnLine(String filePath, HttpServletResponse response, boolean isOnLine) throws Exception {
        File f = new File(filePath);
        if (!f.exists()) {
            response.sendError(404, "File not found!");
            return;
        }
        BufferedInputStream br = new BufferedInputStream(new FileInputStream(f));
        byte[] buf = new byte[1024];
        int len = 0;

        response.reset(); // 非常重要
        if (isOnLine) { // 在线打开方式
            URL u = new URL("file:///" + filePath);
            response.setContentType(u.openConnection().getContentType());
            response.setHeader("Content-Disposition", "inline; filename=" + f.getName());
            // 文件名应该编码成UTF-8
        } else { // 纯下载方式
            response.setContentType("application/x-msdownload");
            response.setHeader("Content-Disposition", "attachment; filename=" + f.getName());
        }
        OutputStream out = response.getOutputStream();
        while ((len = br.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        br.close();
        out.close();
    }

    /**
     * 根据传入的文件大小显示KB或MB
     *
     * @param fleSize
     * @return
     */
    public static String getFileSizeStr(Long fleSize) {
        BigDecimal param = new BigDecimal("1000");
        BigDecimal bigFileSize = new BigDecimal(fleSize);
        if (bigFileSize.compareTo(param) >= 0) {
            BigDecimal sizeKB = BigDecimalUtils.getBigAndBigDiv(bigFileSize, param, 2);
            if (sizeKB.compareTo(param) > 0) {
                BigDecimal sizeMB = BigDecimalUtils.getBigAndBigDiv(sizeKB, param, 2);
                return sizeMB.toString() + "MB";
            } else {
                return sizeKB.toString() + "KB";
            }
        } else {
            return bigFileSize.toString() + "byte";
        }
    }

}
