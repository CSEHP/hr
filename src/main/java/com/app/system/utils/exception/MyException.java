package com.app.system.utils.exception;

/**
 * 自定义异常
 * Created by wcf-pc on 2018/4/17.
 */
@SuppressWarnings("all")
public class MyException extends RuntimeException {
    public MyException() {
    }

    public MyException(String message) {
        super(message);
    }
}
