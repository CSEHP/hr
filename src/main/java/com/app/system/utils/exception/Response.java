package com.app.system.utils.exception;

/**
 * Created by wcf-pc on 2018/4/17.
 */
@SuppressWarnings("all")
public class Response<T> {

    private String code;
    private String message;
    private String error;
    private T data;

    public Response() {

    }

    public Response(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public Response(String code, String message, String error, T data) {
        this.code = code;
        this.message = message;
        this.error = error;
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
