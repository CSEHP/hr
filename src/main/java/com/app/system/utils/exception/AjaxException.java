package com.app.system.utils.exception;

/**
 * Created by wcf-pc on 2018/4/17.
 *
 * @author wcf-pc
 */
@SuppressWarnings("all")
public class AjaxException extends RuntimeException {
    private String code;
    private String message;

    public AjaxException(String code, String message) {
        this.code = (code == null ? "0001" : code);
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "AjaxException{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
