package com.app.system.utils.exception;

/**
 * 用于记录用户HTTP请求超出设定的限制
 * Created by WCF on 2019/9/4.
 */
@SuppressWarnings("all")
public class RequestLimitException extends RuntimeException {
    public RequestLimitException() {
        super("HTTP请求超出设定的限制");
    }

    public RequestLimitException(String message) {
        super(message);
    }

    public RequestLimitException(String message, Throwable cause) {
        super(message, cause);
    }
}
