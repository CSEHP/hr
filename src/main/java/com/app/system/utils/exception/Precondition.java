package com.app.system.utils.exception;

import com.app.system.utils.exception.AjaxException;
import com.app.system.utils.exception.MyException;

/**
 * Created by wcf-pc on 2018/4/17.
 */
@SuppressWarnings("all")
public class Precondition {
    public static final void checkSqdsArguement(boolean expression, String msg) {
        if (!expression) {
            throw new MyException(msg);
        }
    }


    public static final void checkAjaxArguement(boolean expression, String code, String message) {
        if (!expression) {
            throw new AjaxException(code, message);
        }
    }
}
