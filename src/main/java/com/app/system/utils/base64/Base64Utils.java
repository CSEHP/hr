package com.app.system.utils.base64;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * Created by WCF on 2019/8/26.
 */
@SuppressWarnings("all")
public class Base64Utils {
    /**
     * 使用Base64加密字符串
     *
     * @return 加密之后的字符串
     */
    public static String encode(String data) {
        return Base64.getEncoder().encodeToString(data.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * 使用Base64解密
     *
     * @return 解密之后的字符串
     */
    public static String decode(String data) {
        return new String(Base64.getDecoder().decode(data.replace("\"", "")), StandardCharsets.UTF_8);
    }

    public static void main(String[] args) {
        String encode = encode("{\"key\":\"kiki123\",\"value\":\"hello lmbx\"}");
        System.out.println(encode);
        String decode = decode("eyJrZXkiOiIxMjMiLCJ2YWx1ZSI6ImFhYSJ9");
        System.out.println(decode);
    }

    //base64字符串转化成图片
    public static boolean GenerateImage(String imgStr, String imagePath, String imageName) {   //对字节数组字符串进行Base64解码并生成图片
        if (imgStr == null) //图像数据为空
            return false;
        BASE64Decoder decoder = new BASE64Decoder();
        try {
            //Base64解码
            byte[] b = decoder.decodeBuffer(imgStr);
            for (int i = 0; i < b.length; ++i) {
                if (b[i] < 0) {//调整异常数据
                    b[i] += 256;
                }
            }
            File file = new File(imagePath);
            if (!file.exists()) {
                file.mkdirs();
            }
            //生成jpeg图片
            String imgFilePath = imagePath + imageName;//新生成的图片
            OutputStream out = new FileOutputStream(imgFilePath);
            out.write(b);
            out.flush();
            out.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String getImageStr(String path){
        if(path==null || path.trim().length()<=0){return "";}

        File picFile = new File(path);
        if(picFile.isDirectory() || (!picFile.exists())) return "";

        try {
            BufferedImage image = ImageIO.read(picFile);
            if (image == null) {
                return "";
            }
        } catch(IOException ex) {
            ex.printStackTrace();
            return "";
        }

        String imageStr = "";
        try {
            byte[] data = null;
            InputStream in = new FileInputStream(path);
            data = new byte[in.available()];
            in.read(data);
            BASE64Encoder encoder = new BASE64Encoder();
            imageStr = encoder.encode(data);
        } catch (Exception e) {
            imageStr="";
            e.printStackTrace();
        }

        return imageStr;
    }
}
