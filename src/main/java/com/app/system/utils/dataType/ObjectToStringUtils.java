package com.app.system.utils.dataType;

/**
 * @author wmy
 * @date 2020/07/16
 */
@SuppressWarnings("all")
public class ObjectToStringUtils {
    public static String getNotNull(Object o) {
            return o == null ? "" : o.toString();
    }
}
