//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.app.system.utils.dataType;

import com.app.system.permission.model.DataDict;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SuppressWarnings("all")
public class StringUtils {

    public static String getNull2Default(String s) {
        return (s == null || s.trim().equals("")) ? "/" : s;
    }
    public static String getNotNull(String s) {
        return s == null ? "" : s;
    }

    public static boolean isNotEmpty(String string) {
        return !isEmpty(string);
    }

    public static boolean isEmpty(String string) {
        return string == null || "".equals(string);
    }

    public static String dataDictShow(DataDict dataDict) {
        StringBuffer text = new StringBuffer("");
        if (dataDict != null) {
            text.append("<span style='color:" + dataDict.getColor() + ";'>");
            text.append(dataDict.getName());
            text.append("</span>");
        }
        return text.toString();
    }

    /**
     * bigDecimal非空处理，不足两位小数补0
     * freemarker统一用
     *
     * @param b
     * @return
     */
    public static String getBigDecimalNotNull(BigDecimal b) {
        String result = "";
        BigDecimal zero = new BigDecimal("0");
        if (b == null || b.compareTo(zero) == 0) {
            result = "0.00";
        } else {
            DecimalFormat decimalFormat = new DecimalFormat("0.00#");
            result = decimalFormat.format(b);
        }
        return result;
    }

    /**
     * bigDecimal非空处理
     * freemarker统一用
     *
     * @param b
     * @return
     */
    public static String getBigDecimalNotNullNoSmall(BigDecimal b) {
        String result = "";
        BigDecimal zero = new BigDecimal("0");
        if (b == null || b.compareTo(zero) == 0) {
            result = "";
        } else {
            result = b.toString();
        }
        return result;
    }

    /**
     * integer非空处理
     * freemarker统一用
     *
     * @param b
     * @return
     */
    public static String getIntegerNotNullForPdf(Integer b) {
        String result = "";
        BigDecimal zero = new BigDecimal("0");
        if (b == null) {
        } else if (b == 0) {
            result = "0";
        } else {
            result = b.toString();
        }
        return result;
    }

    /**
     * 通过正则查找子串出现的次数
     * @param srcText 源字符串
     * @param findText 要查找的字符串
     * @return count
     */
    public static int showNumber(String srcText, String findText) {
        int count = 0;
        Pattern p = Pattern.compile(findText);
        Matcher m = p.matcher(srcText);
        while (m.find()) {
            count++;
        }
        return count;
    }


    /**
     * 自动生成编号
     *
     * @param prefix 前缀
     * @param nowNum 当前值
     * @return
     */
    public static String getAutoNum(String prefix, int nowNum) {

        StringBuilder builder = new StringBuilder();
        StringBuilder num = new StringBuilder();
        AtomicInteger count = new AtomicInteger(nowNum);
        // 4位数字的采取编号处理。9999的情况下从001开始采取。
        if (count.get() > 9999) {
            count = new AtomicInteger(1);
        }

        // 采用4位数的数字进行序号处理。 incrementAndGet返回加1后的新值
        if (count.get() < 10) {
            num.append(count.incrementAndGet());
        } else if (count.get() >= 100) {
            num.append(count.incrementAndGet());
        } else {
            num.append(count.incrementAndGet());
        }

        // 组合。
        builder.append(prefix);
        builder.append(num);

        return builder.toString();
    }

    /**
     * 处理分隔字符串方法
     *
     * @param param
     * @return
     */
    public static List<String> getStringParamArray(String param) {
        if (param == null) {
            return new ArrayList<>();
        } else {
            if (param.contains(",")) {
                String[] array = param.split(",");
                return Arrays.asList(array);
            } else {
                return Collections.singletonList(param);
            }
        }
    }
}
