package com.app.system.utils.dataType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by ZYK on 2019/1/16.
 */
@SuppressWarnings("all")
public class IntegerUtils {
    /**
     * 空值处理
     *
     * @param integer
     * @return
     */
    public static Integer getNotNull(Integer integer) {
        return integer == null ? 0 : integer;
    }

    public static String formatStr(Integer value) {
        return value == null || value == 0 ? "" : value + "";
    }

    /**
     * String空值转换Integer
     * @param value
     * @return
     */
    public static Integer parseInt(String value) {
        if (value != null) {
            value = value.trim();
        }
        return value == null || value.equals("无") || value.equals("") ? 0 : Integer.parseInt(value);
    }

    /**
     * 处理分隔字符串方法
     *
     * @param param
     * @return
     */
    public static List<Integer> getIntegerParamArray(String param) {
        if (param == null) {
            return new ArrayList<>();
        } else {
            if (param.contains(",")) {
                String[] array = param.split(",");
                return Arrays.stream(array).map(Integer::parseInt).collect(Collectors.toList());
            } else {
                List<Integer> resultList = new ArrayList<>();
                resultList.add(Integer.valueOf(param));
                return resultList;
            }
        }
    }

    /**
     * object数值转化为int
     * @param object
     * @return
     */
    public static Integer objectToInteger(Object object) {
        if(object == null){
            return 0;
        }
        return Integer.valueOf(String.valueOf(object));
    }
}
