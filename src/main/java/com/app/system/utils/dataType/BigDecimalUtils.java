package com.app.system.utils.dataType;

import java.math.BigDecimal;

/**
 * Created by wsb on 2017/10/25.
 */
@SuppressWarnings("all")
public class BigDecimalUtils {

    /**
     * 处理空值(BigDecimal)
     *
     * @param value
     * @return
     */
    public static BigDecimal getNotNull(BigDecimal value) {
        return value == null ? BigDecimal.ZERO : value;
    }

    /**
     * 处理空值(BigDecimal)
     *
     * @param value
     * @return
     */
    public static String getNotNullStr(BigDecimal value) {
        return (value == null || value.compareTo(BigDecimal.ZERO) == 0) ? "" : value.toString();
    }

    /**
     * 计算求和
     *
     * @param values
     * @return
     */
    public static BigDecimal getSum(BigDecimal... values) {
        BigDecimal result = new BigDecimal("0");
        if (values != null && values.length > 0) {
            for (BigDecimal v : values) {
                result = result.add(getNotNull(v));
            }
        }
        return result;
    }

    /**
     * 计算差
     *
     * @param b1
     * @param b2
     * @return
     */
    public static BigDecimal getBigAndBigSub(BigDecimal b1, BigDecimal b2) {
        return getNotNull(b1).subtract(getNotNull(b2));
    }

    /**
     * 计算除法
     *
     * @param b1
     * @param b2
     * @return
     */
    public static BigDecimal getBigAndBigDiv(BigDecimal b1, BigDecimal b2, Integer scale) {
        BigDecimal result = new BigDecimal("0");
        b1 = getNotNull(b1);
        b2 = getNotNull(b2);
        if (b2.compareTo(BigDecimal.ZERO) > 0) {
            result = b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP);
        }
        return result;
    }

    /**
     * 计算bigDecimal*integer数值
     *
     * @param b
     * @param i
     * @return
     */
    public static BigDecimal getBigAndIntMul(BigDecimal b, Integer i) {
        return getNotNull(b).multiply(new BigDecimal(IntegerUtils.getNotNull(i)));
    }

    /**
     * 计算bigDecimal*bigDecimal数值
     *
     * @param b1
     * @param b2
     * @return
     */
    public static BigDecimal getBigAndBigMul(BigDecimal b1, BigDecimal b2) {
        return getNotNull(b1).multiply(getNotNull(b2));
    }

    /**
     * 比较两个值的大小，返回较大的那个值
     *
     * @param b1
     * @param b2
     * @return
     */
    public static BigDecimal getMaxBig(BigDecimal b1, BigDecimal b2) {
        return getNotNull(b1).compareTo(getNotNull(b2)) < 1 ? getNotNull(b2) : getNotNull(b1);
    }

    /**
     * 数值相除得到%
     *
     * @param numerator   分子
     * @param denominator 分母
     * @return
     */
    public static String getPercentBigDecimal(int numerator, int denominator) {
        if (numerator > 0 && denominator > 0) {
            return getBigAndBigDiv(getBigAndBigMul(new BigDecimal(numerator), new BigDecimal("100")), new BigDecimal(denominator), 2).toString() + "%";
        } else {
            return "";
        }
    }

    /**
     * 空值转换BigDecimal
     * @param value
     * @return
     */
    public static BigDecimal parseBig(String value) {
        BigDecimal values = null;
        if (value != null) {
            value = value.trim();
        }
        try {
            values = value == null || value.equals("无") || value.equals("") ? new BigDecimal(0) : new BigDecimal(value);
        } catch (Exception e){
            e.printStackTrace();
        }
        return values;
    }
}
