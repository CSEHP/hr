package com.app.business.utils;

import java.util.Random;

/**
 * 生成名称
 *
 * @author WMY
 */
public class GenerateNameUtils {

    // 字符串源数据
    private static final String SOURCE = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&";

    private static final Random RANDOM = new Random();

    /**
     * 生成固定位数字符串
     *
     * @param length 字符串长度
     * @return
     */
    public static String generateName(int length) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < length; i++) {
            result.append(SOURCE.charAt(RANDOM.nextInt(SOURCE.length())));
        }
        return result.toString();
    }
}
