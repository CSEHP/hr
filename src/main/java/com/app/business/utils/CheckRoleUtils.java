package com.app.business.utils;

import com.app.system.permission.model.SysRole;
import com.app.system.permission.model.UserInfo;

import java.util.Set;

/**
 * 检测角色
 *
 * @author wmy
 * @date 2021/05/26
 */
public class CheckRoleUtils {

    /**
     * 检测登录角色是否有管理员或审核专员
     *
     * @return
     */
    public static boolean checkLoginForAdmin(UserInfo userInfo) {
        boolean result = false;
        Set<SysRole> roleList = userInfo.getRoleList();
        for (SysRole sysRole : roleList) {
            if ("作品审核管理".equals(sysRole.getRole()) || "admin".equals(sysRole.getRole())) {
                result = true;
                break;
            }
        }
        return result;
    }
}
