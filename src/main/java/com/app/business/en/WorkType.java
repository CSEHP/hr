package com.app.business.en;

/**
 * 职工类型枚举
 *
 * @author WMY
 */
public enum WorkType {
    /**
     * 未达标职工
     */
    NO_STANDARD("未达标职工", 1),

    /**
     * 新增未达标职工
     */
    NEW_NO_STANDARD("新增未达标职工", 2),

    /**
     * 级差职工
     */
    GRADATIONS("级差职工", 1);

    private String name;
    private int index;

    WorkType(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
