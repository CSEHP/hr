package com.app.business.en;

/**
 * 是否枚举
 *
 * @author WMY
 */
public enum IsType {
    /**
     * 是
     */
    IS("是", 1),

    /**
     * 否
     */
    NO("否", 2);

    private String name;
    private int index;

    IsType(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
