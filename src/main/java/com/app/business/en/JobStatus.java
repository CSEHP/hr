package com.app.business.en;

/**
 * 情况类型枚举
 *
 * @author WMY
 */
public enum JobStatus {
    /**
     * 在职
     */
    ON_JOB("在职", 1),

    /**
     * 离退休
     */
    RETIRE("离退休", 2),

    /**
     * 其他
     */
    OTHER("其他", 1);

    private String name;
    private int index;

    JobStatus(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
