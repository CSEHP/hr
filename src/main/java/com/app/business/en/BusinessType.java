package com.app.business.en;

/**
 * 操作业务表类别枚举
 *
 * @author WMY
 */
public enum BusinessType {
    /**
     * 员工信息表
     */
    WORKER_INFO("新增", 1),

    /**
     * 无房职工申请表
     */
    NO_HOUSE_WORKER_APPLY("修改", 2),

    /**
     * 未达标职工申请表
     */
    NO_STANDARD_WORKER_APPLY("删除", 1);

    private String name;
    private int index;

    BusinessType(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
