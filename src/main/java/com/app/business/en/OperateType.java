package com.app.business.en;

/**
 * 操作类型枚举
 *
 * @author WMY
 */
public enum OperateType {
    /**
     * 新增
     */
    NEW("新增", 1),

    /**
     * 修改
     */
    EDIT("修改", 2),

    /**
     * 删除
     */
    DEL("删除", 3);

    private String name;
    private int index;

    OperateType(String name, int index) {
        this.name = name;
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
