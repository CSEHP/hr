package com.app.business.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * 无房职工信息申报表
 *
 * @author wmy
 * @date 2021/06/23
 */
@EqualsAndHashCode(callSuper = true)
@DynamicInsert()
@DynamicUpdate()
@EntityListeners(AuditingEntityListener.class)
@Entity
@Data
public class NoHouseWorkerApply extends BaseModel {
    /**
     * 申请人姓名
     */
    private String name;

    /**
     * 申请人身份证号
     */
    private String idCard;

    /**
     * 是否正在发放住房补贴
     */
    private Integer isPayingSubsidies=1;

    /**
     * 原补贴系数
     */
    private BigDecimal lastSubsidyCoefficient;

    /**
     * 现补贴系数（系统自动计算）
     */
    private BigDecimal subsidyCoefficient;

    /**
     * 补贴总额（结果）
     */
    private BigDecimal totalSubsidy;

    /**
     * 职工信息id
     */
    @JSONField(serialize = false)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "workerInfoId")
    private WorkerInfo workerInfo;
}
