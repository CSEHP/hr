package com.app.business.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by WSJ
 */
@EqualsAndHashCode(callSuper = true)
@DynamicInsert()
@DynamicUpdate()
@EntityListeners(AuditingEntityListener.class)
@Entity
@Data
public class EmployeeInfo extends BaseModel {
    /*姓名*/
    private String name;
    /*身份证号*/
    private String idCard;
    /*联系方式*/
    private String phone;
    /*性别*/
    private Integer sex;//1.男 2，女
    private Integer age;//年龄
    private String email;//邮箱
    private String depName;//部门名称
    private String depCode;//部门编号
    private String address;//住址
    private String picPath;//头像

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date rzDate;//入职时间

    private BigDecimal money;//月工资（基础薪资）


}
