package com.app.business.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 未达标职工信息申报表
 *
 * @author wmy
 * @date 2021/06/23
 */
@EqualsAndHashCode(callSuper = true)
@DynamicInsert()
@DynamicUpdate()
@EntityListeners(AuditingEntityListener.class)
@Entity
@Data
public class NoStandardWorkerApply extends BaseModel {
    /**
     * 申请人姓名
     */
    private String name;

    /**
     * 申请人身份证号
     */
    private String idCard;

    /**
     * 联系电话
     */
    private String phone;

    /**
     * 情况类型 1、在职，2、离退休，3、其他
     */
    private Integer jobStatus;

    /**
     * 职工类型 1、未达标职工，2、新增未达标职工，3、级差职工
     */
    private Integer workerType;

    /**
     * 参加工作时间
     *
     */
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date startWorkingDate;

    /**
     * 调入本单位时间
     */
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date joinWorkingDate;

    /**
     * 退休时间
     */
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date retireDate;

    /**
     * 配偶姓名
     */
    private String spouseName;

    /**
     * 配偶身份证号
     */
    private String spouseIdCard;

    /**
     * 配偶工作单位
     */
    private String spouseUnit;

    /**
     * 配偶是否享受过补贴
     */
   private Integer spouseIsEnjoyed=1; //1是 2否
    /**
     * 配偶补贴面积
     */
   private BigDecimal spouseEnjoyedArea;

    /**
     * 住房地址
     */
    private String livingAddress;

    /**
     * 申请人购房时单位
     */
    private String boughtUnit;

    /**
     * 申请人购房时补贴额    已享受(已发补贴额)
     */
    private BigDecimal boughtUnitSubsidy;

    /**
     * 军队转业干部时部队
     */
    private String boughtTroops;

    /**
     * 军队转业干部时补贴额
     */
    private BigDecimal boughtTroopsSubsidy;

    /**
     * 是否享受过含有政府补贴性质的住房
     */
    private Integer isEnjoyed; //1未享受2已享受、

    /**
     * 部门编号
     */
    private String departmentCode;

    /**
     * 享受过含有政府补贴性质的住房面积
     */
    private BigDecimal enjoyedBuildArea;

    /**
     * 住房补贴建筑面积标准
     */
    private BigDecimal buildAreaStandard;

    /**
     * 核定后住房总面积
     */
    private BigDecimal buildAreaSureStandard;

    /**
     * 差额面积
     */
    private BigDecimal differenceBuildArea;

    /**
     * 建立住房公积金前的工龄
     */
    private BigDecimal workedYears;

    /**
     * 差额补贴额
     */
    private BigDecimal differenceSubsidy;

    /**
     * 补贴总额
     */
    private BigDecimal totalSubsidy;

    private String recordNo; //编号  SYSCJ
    private Integer num; //顺序号 0001

    /**
     * 备注
     */
    @Type(type = "text")
    private String memo;

    /**
     * 原职务职称Id
     */
    @JSONField(serialize = false)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "lastPositionId")
    private Position lastPosition;

    /**
     * 现职务职称Id
     */
    @JSONField(serialize = false)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "positionId")
    private Position position;

    /**
     * 退休时职务职称Id
     */
    @JSONField(serialize = false)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "retirePositionId")
    private Position retirePosition;
}
