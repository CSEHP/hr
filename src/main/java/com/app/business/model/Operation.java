package com.app.business.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;

/**
 * 操作记录表
 *
 * @author wmy
 * @date 2021/06/23
 */
@EqualsAndHashCode(callSuper = true)
@DynamicInsert()
@DynamicUpdate()
@EntityListeners(AuditingEntityListener.class)
@Entity
@Data
public class Operation extends BaseModel {
    /**
     * 操作内容
     */
    @Type(type = "text")
    private String operateContent;

    /**
     * 操作类别
     */
    private Integer operateType; //1新增 2.修改 3删除 

    /**
     * 操作业务表Id
     */
    private Integer businessId;

    /**
     * 操作业务表类别 1、员工表，2、无房职工申报表，3、未达标职工申报表 4工资表
     */
    private Integer businessType;
    /**
     * 操作人
     */
    private String name;

    /**
     * JSON_DATA
     */
    @Type(type = "text")
    private String jsonData;
}
