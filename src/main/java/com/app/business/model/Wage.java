package com.app.business.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * 无房职工历史月度工资表
 *
 * @author wmy
 * @date 2021/06/23
 */
@EqualsAndHashCode(callSuper = true)
@DynamicInsert()
@DynamicUpdate()
@EntityListeners(AuditingEntityListener.class)
@Entity
@Data
public class Wage extends BaseModel {
    /**
     * 申请人姓名
     */
    private String name;

    /**
     * 申请人身份证号
     */
    private String idCard;

    /**
     * 工资年度
     */
    private Integer year;

    /**
     * 工资月度
     */
    private Integer month;

    /**
     * 工资金额
     */
    private BigDecimal money;

    /**
     * 职工信息id
     */
    @JSONField(serialize = false)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "workerInfoId")
    private WorkerInfo workerInfo;
}
