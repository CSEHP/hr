package com.app.business.model;


import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * 上传文件
 *
 * @author 22522
 */
/**
 * Created by WSJ
 */
@EqualsAndHashCode(callSuper = true)
@DynamicInsert()
@DynamicUpdate()
@EntityListeners(AuditingEntityListener.class)
@Entity
@Data
public class FileInfo extends BaseModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String fileName;    //文件名称
    private String filePath;    //文件路径
    private String suffixes;    //文件后缀名
    private Long size;    //文件大小

    private Integer categoryId;    //类型 1.个人简历 2.合同 ，3
    private Integer subCategoryId;//合同附件类型 1.个人简历：个人简历
                                             // 2.合同：1.身份信息 2.劳务合同

    private Date uploadTime;    //上传时间
    private String uploadPerson;    //上传人员
    private Integer dataId;  //关联表id //dataId

    /**
     * 获取缩略图的路径
     * 缩略图在原图同文件夹下，路径s_+filePath
     *
     * @return
     */
    @Transient
    public String getSmallPic() {
        if (filePath != null) {
            int index = filePath.lastIndexOf("/");
            return filePath.substring(0, index) + "/s_" + filePath.substring(index + 1);
        }
        return filePath;
    }
}
