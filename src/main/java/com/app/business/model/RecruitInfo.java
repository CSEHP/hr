package com.app.business.model;

/**
 * Created by M·JJ on 2022-02-01
 */

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.math.BigDecimal;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@DynamicInsert()
@DynamicUpdate()
@EntityListeners(AuditingEntityListener.class)
@Entity
@Data
public class RecruitInfo extends BaseModel {
    private String firmName;//公司名称
    private String jobName;//职位名称
    private String jobDesc;//职位详情
    private String degree;//学历要求
    private String jobYear;//工作年限
    private BigDecimal salary;//薪资待遇
    private String area;//办公地点
    private String mobile;//联系电话
    private String email;//电子邮箱
    private String memo;//备注
    private String person;//发布人

    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date fbDate;//发布时间
}
