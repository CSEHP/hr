package com.app.business.model;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by Wsj
 */
@EqualsAndHashCode(callSuper = true)
@DynamicInsert()
@DynamicUpdate()
@EntityListeners(AuditingEntityListener.class)
@Entity
@Data
public class SalaryInfo extends BaseModel {
    private Integer year;//年份
    private Integer month;//月份
    private BigDecimal money;//基础薪资
    private BigDecimal bonusMoney;//奖金
    private Integer days;//本月应出勤天数
    private Integer cqDay;//出勤天数
    private Integer sjDay;//事假天数

    /**
     * 员工外键
     */
    @JSONField(serialize = false)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employeeInfoId")
    private EmployeeInfo employeeInfo;
}
