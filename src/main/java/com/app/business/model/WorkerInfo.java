package com.app.business.model;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;

/**
 * 职工信息表
 *
 * @author wmy
 * @date 2021/06/23
 */
@EqualsAndHashCode(callSuper = true)
@DynamicInsert()
@DynamicUpdate()
@EntityListeners(AuditingEntityListener.class)
@Entity
@Data
public class WorkerInfo extends BaseModel {
    /**
     * 申请人姓名
     */
    private String name;

    /**
     * 申请人身份证号
     */
    private String idCard;

    /**
     * 建立公积金前工龄
     */
    private BigDecimal workedYears;

    /**
     * 参加工作时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    @Temporal(TemporalType.DATE)
    private Date startWorkingDate;

    /**
     * 调入本单位时间
     */
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date joinWorkingDate;

    /**
     * 退休时间
     */
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date retireDate;

    /**
     * 配偶姓名
     */
    private String spouseName;

    /**
     * 配偶身份证号
     */
    private String spouseIdCard;

    /**
     * 配偶工作单位
     */
    private String spouseUnit;

    /**
     * 部门编号
     */
    private String departmentCode;

    /**
     * 住房地址
     */
    private String livingAddress;

    /**
     * 月基本工资（修改职工信息时，本字段值放入25个月工资中让其修改）
     */
    private BigDecimal wages;

    private String recordNo; //编号  SYSCJ
    private Integer num; //顺序号 0001

    /**
     * 备注
     */
    @Type(type = "text")
    private String memo;

    /**
     * 现职务职称Id
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "positionId")
    private Position position;

    /**
     * 退休时职务职称Id
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "retirePositionId")
    private Position retirePosition;
}
