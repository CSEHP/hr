package com.app.business.service;

import com.app.business.model.NoStandardWorkerApply;
import com.app.business.model.Wage;
import com.app.business.model.WorkerInfo;
import com.app.system.jpa.JpaBaseDao;
import com.app.system.utils.dataType.DateUtils;
import com.google.common.collect.Maps;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zt on 2021-06-24
 */
@Service
public class WageService  extends JpaBaseDao<Wage,Integer> {

    /**
     * 批量存储补贴工资
     * @param wageList
     */
    @Transactional
    public void saveWageList(List<Wage> wageList, WorkerInfo workerInfo) {
        for (Wage wage : wageList) {
            wage.setName(workerInfo.getName());
            wage.setIdCard(workerInfo.getIdCard());
            wage.setMoney(workerInfo.getWages());
            wage.setWorkerInfo(workerInfo);
            save(wage);
        }
    }

    /**
     * 根据年月计算工资条数
     * @param startYear
     * @param startMonth
     * @return
     */
    public List<Wage> getWageListByYearAndMonth(int startYear,int startMonth) {
        List<Wage> wageList = new ArrayList<>();
        //根据补贴时间段算出总月数
        int countMonth = Integer.valueOf(DateUtils.month(DateUtils.stringToDateDefault("2019-09-01"), DateUtils.stringToDateDefault("2021-09-30")));
        for (int i = 0; i < countMonth; i++) {
            if (startMonth == 12 && i != 0) {
                startYear += 1;
                startMonth = 1;
            } else if (i != 0) {
                startMonth += 1;
            }
            Wage wage = new Wage();
            wage.setYear(startYear);
            wage.setMonth(startMonth);
            wageList.add(wage);
        }
        return wageList;
    }

    /**
     * 根据workInfoId获取工资基本信息
     * @param workInfoId
     * @return
     */
    public List<Wage> getWageListByWorkInfoId(int workInfoId){
        String hql="from Wage w where w.workerInfo.id=:workInfoId order by w.id asc";
        Map<String, Object> map = Maps.newLinkedHashMap();
        map.put("workInfoId", workInfoId);
        return this.findHql(hql, map);
    }

    /**
     * 根据workerInfoId获取工资总额
     * @param workerInfoId
     * @return
     */
    public Map<String,Object> getMoneyByWorkInfoId(int workerInfoId){
        String sql="select worker_info_id as workerId,sum(money) as moneyMonthCount from wage where worker_info_id=:workerInfoId";
        Map<String, Object> map = Maps.newLinkedHashMap();
        map.put("workerInfoId",workerInfoId);
        List<Map<String, Object>> mapList= this.findSql(sql, map);

        Map<String,Object> resultMap=new HashMap<>();
        for (Map<String,Object> map1:mapList){
            resultMap.put(map1.get("workerId").toString(),map1.get("moneyMonthCount").toString());
        }
       return resultMap;
    }

    /**
     * 删除工资表数据
     * @param workerId
     * @return
     */
    @Transactional
    public void deleteWageByWorkerInfoId(int workerId) {
        List<Wage> wageList=this.getWageListByWorkInfoId(workerId);
        for (Wage wage:wageList){
            this.delete(wage.getId());
        }
    }
}
