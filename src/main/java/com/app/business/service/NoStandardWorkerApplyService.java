package com.app.business.service;

import com.app.business.model.NoHouseWorkerApply;
import com.app.business.model.NoStandardWorkerApply;
import com.app.business.model.WorkerInfo;
import com.app.system.jpa.JpaBaseDao;
import com.app.system.jpa.PageBean;
import com.app.system.jpa.Wrapper;
import com.app.system.permission.model.UserInfo;
import com.app.system.utils.CollectionUtils;
import com.app.system.utils.WebUtils;
import com.app.system.utils.dataType.BigDecimalUtils;
import com.app.system.utils.dataType.DateUtils;
import com.app.system.utils.dataType.StringUtils;
import com.google.common.collect.Maps;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zt on 2021-06-24
 */
@Service
public class NoStandardWorkerApplyService extends JpaBaseDao<NoStandardWorkerApply,Integer> {
    /**
     * 分页数据（基础数据列表）
     */
    public PageBean<NoStandardWorkerApply> getNoStandardWorkerApplyPage(PageBean pageBean) {
        UserInfo userInfo=WebUtils.getLoginUserInfo();
        String departmentCode=userInfo.getDepartment().getCode();
        String retireDate_ge = (String) WebUtils.getRequest().getParameter("retireDate_ge");
        String retireDate_le = (String) WebUtils.getRequest().getParameter("retireDate_le");
        String depCode = (String) WebUtils.getRequest().getParameter("depCode");
        //查询语句
        StringBuffer query = new StringBuffer(" n from NoStandardWorkerApply n ");
        //默认排序(PageBean指定排序字段类型的话，则默认排序不使用)
        StringBuffer orderBy = new StringBuffer("n.id desc");

        Wrapper wrapper = new Wrapper(pageBean,query,null, orderBy);
        wrapper.andLike("n.departmentCode",departmentCode);
        if (StringUtils.isNotEmpty(retireDate_ge)) {
            Date date = DateUtils.stringToDateFormat(retireDate_ge, "yyyy-MM-dd");
            wrapper.andGe("n.retireDate", date);
        }
        if (StringUtils.isNotEmpty(retireDate_le)) {
            Date date = DateUtils.stringToDateFormat(retireDate_le, "yyyy-MM-dd");
            wrapper.andLe("n.retireDate", date);
        }
        if (StringUtils.isNotEmpty(depCode)) {
            wrapper.andLike("n.departmentCode", depCode);
        }
        wrapper.initSearchParams();
        return this.pageHql(wrapper);
    }
    /**
     * 分页数据（基础数据列表(在职)）
     */
    public PageBean<NoStandardWorkerApply> getNoStandardWorkerApplyZzPage(PageBean pageBean) {
        UserInfo userInfo=WebUtils.getLoginUserInfo();
        String departmentCode=userInfo.getDepartment().getCode();
        String retireDate_ge = (String) WebUtils.getRequest().getParameter("retireDate_ge");
        String retireDate_le = (String) WebUtils.getRequest().getParameter("retireDate_le");
        String depCode = (String) WebUtils.getRequest().getParameter("depCode");
        //查询语句
        StringBuffer query = new StringBuffer(" n from NoStandardWorkerApply n");
        //默认排序(PageBean指定排序字段类型的话，则默认排序不使用)
        StringBuffer orderBy = new StringBuffer("n.id desc");

        Wrapper wrapper = new Wrapper(pageBean,query,null, orderBy);
        wrapper.andLike("n.departmentCode",departmentCode);
        wrapper.andEq("n.jobStatus",1);
        if (StringUtils.isNotEmpty(retireDate_ge)) {
            Date date = DateUtils.stringToDateFormat(retireDate_ge, "yyyy-MM-dd");
            wrapper.andGe("n.retireDate", date);
        }
        if (StringUtils.isNotEmpty(retireDate_le)) {
            Date date = DateUtils.stringToDateFormat(retireDate_le, "yyyy-MM-dd");
            wrapper.andLe("n.retireDate", date);
        }
        if (StringUtils.isNotEmpty(depCode)) {
            wrapper.andLike("n.departmentCode", depCode);
        }
        wrapper.initSearchParams();
        return this.pageHql(wrapper);
    }
    /**
     * 分页数据（基础数据列表(在职)）
     */
    public PageBean<NoStandardWorkerApply> getNoStandardWorkerApplyTxPage(PageBean pageBean) {
        UserInfo userInfo=WebUtils.getLoginUserInfo();
        String departmentCode=userInfo.getDepartment().getCode();
        String retireDate_ge = (String) WebUtils.getRequest().getParameter("retireDate_ge");
        String retireDate_le = (String) WebUtils.getRequest().getParameter("retireDate_le");
        String depCode = (String) WebUtils.getRequest().getParameter("depCode");
        //查询语句
        StringBuffer query = new StringBuffer(" n from NoStandardWorkerApply n ");
        //默认排序(PageBean指定排序字段类型的话，则默认排序不使用)
        StringBuffer orderBy = new StringBuffer("n.id desc");
        Wrapper wrapper = new Wrapper(pageBean,query,null, orderBy);
        wrapper.andLike("n.departmentCode",departmentCode);
        wrapper.andEq("n.jobStatus",2);
        if (StringUtils.isNotEmpty(retireDate_ge)) {
            Date date = DateUtils.stringToDateFormat(retireDate_ge, "yyyy-MM-dd");
            wrapper.andGe("n.retireDate", date);
        }
        if (StringUtils.isNotEmpty(retireDate_le)) {
            Date date = DateUtils.stringToDateFormat(retireDate_le, "yyyy-MM-dd");
            wrapper.andLe("n.retireDate", date);
        }
        if (StringUtils.isNotEmpty(depCode)) {
            wrapper.andLike("n.departmentCode", depCode);
        }
        wrapper.initSearchParams();
        return this.pageHql(wrapper);
    }
    /**
     * 分页数据（基础数据列表(在职)）
     */
    public PageBean<NoStandardWorkerApply> getNoStandardWorkerApplyOtherPage(PageBean pageBean) {
        UserInfo userInfo=WebUtils.getLoginUserInfo();
        String departmentCode=userInfo.getDepartment().getCode();
        String retireDate_ge = (String) WebUtils.getRequest().getParameter("retireDate_ge");
        String retireDate_le = (String) WebUtils.getRequest().getParameter("retireDate_le");
        String depCode = (String) WebUtils.getRequest().getParameter("depCode");
        //查询语句
        StringBuffer query = new StringBuffer(" n from NoStandardWorkerApply n ");
        //默认排序(PageBean指定排序字段类型的话，则默认排序不使用)
        StringBuffer orderBy = new StringBuffer("n.id desc");
        Wrapper wrapper = new Wrapper(pageBean,query,null, orderBy);
        wrapper.andLike("n.departmentCode",departmentCode);
        wrapper.andEq("n.jobStatus",3);
        if (StringUtils.isNotEmpty(retireDate_ge)) {
            Date date = DateUtils.stringToDateFormat(retireDate_ge, "yyyy-MM-dd");
            wrapper.andGe("n.retireDate", date);
        }
        if (StringUtils.isNotEmpty(retireDate_le)) {
            Date date = DateUtils.stringToDateFormat(retireDate_le, "yyyy-MM-dd");
            wrapper.andLe("n.retireDate", date);
        }
        if (StringUtils.isNotEmpty(depCode)) {
            wrapper.andLike("n.departmentCode", depCode);
        }
        wrapper.initSearchParams();
        return this.pageHql(wrapper);
    }
    /**
     *  根据编号获取信息
     */
    public NoStandardWorkerApply getWorkerInfoByNum(int num,String recordNo) {
        String hql = " from NoStandardWorkerApply n left join fetch n.position p left join fetch n.retirePosition r where n.num=:num and n.recordNo=:recordNo";
        Map<String, Object> map = Maps.newLinkedHashMap();
        map.put("num", num);
        map.put("recordNo", recordNo);
        List<NoStandardWorkerApply> noStandardWorkerApplyList = this.findHql(hql, map);
        NoStandardWorkerApply noStandardWorkerApply = null;
        if (CollectionUtils.isNotEmpty(noStandardWorkerApplyList)) {
            noStandardWorkerApply = noStandardWorkerApplyList.get(0);
        }
        return noStandardWorkerApply;

    }
    /**
     *  根据id获取信息
     */
    public NoStandardWorkerApply getWorkerInfoById(int noStandardId) {
        String hql = " from NoStandardWorkerApply n where n.id=:noStandardId";
        Map<String, Object> map = Maps.newLinkedHashMap();
        map.put("noStandardId", noStandardId);
        List<NoStandardWorkerApply> noStandardWorkerApplyList = this.findHql(hql, map);
        NoStandardWorkerApply noStandardWorkerApply = null;
        if (CollectionUtils.isNotEmpty(noStandardWorkerApplyList)) {
            noStandardWorkerApply = noStandardWorkerApplyList.get(0);
        }
        return noStandardWorkerApply;

    }

    /**
     * 生成编号
     * @return
     */
    public int getRecordNum(String recordNo) {
        String sql = " select max(num) as num from no_standard_worker_apply where record_no=:recordNo ";
        Map<String, Object> map = Maps.newLinkedHashMap();
        map.put("recordNo",recordNo);
        List<Map<String, Object>> mapList = this.findSql(sql, map);
        String num=String.valueOf(mapList.get(0).get("num"));
        if (num.equals("null")){
            num="0";
        }
        return Integer.valueOf(num);
    }
    /**
     * 通过idCard联合查询
     *
     * @param idCard
     * @return
     */
    public NoStandardWorkerApply getWorkerInfoByIdCard(String idCard) {
        String hql = " from NoStandardWorkerApply w left join fetch w.position p left join fetch w.retirePosition r where w.idCard=:idCard";
        Map<String, Object> map = Maps.newLinkedHashMap();
        map.put("idCard", idCard);
        List<NoStandardWorkerApply> noStandardWorkerApplyList = this.findHql(hql, map);

        NoStandardWorkerApply noStandardWorkerApply = null;
        if (CollectionUtils.isNotEmpty(noStandardWorkerApplyList)) {
            noStandardWorkerApply = noStandardWorkerApplyList.get(0);
        }
        return noStandardWorkerApply;
    }
    /**
     * 统计
     */
    public  Map<String,Map<String,String>> getNumAndMoneyByRecord(){
        String sql="select n.record_no as recordNo," +
                "sum(case when n.id and n.is_enjoyed=2 then 1 else 0 end) as personCountTwo," +
                "sum(case when n.id and n.is_enjoyed=1 then 1 else 0 end) as personCountOne," +
                "sum(case when n.total_subsidy and n.is_enjoyed=2 then n.total_subsidy else 0 end) as moneyTwo," +
                "sum(case when n.total_subsidy and n.is_enjoyed=1 then n.total_subsidy else 0 end) as moneyOne " +
                "from no_standard_worker_apply n group by n.record_no";
        Map map=new HashMap();
        List<Map> mapList= this.findSql(sql,map);
        Map<String,Map<String,String>> resultMap=new HashMap<>();
        int personNumTwo=0;
        int personNumOne=0;
        BigDecimal moneyTwo=new BigDecimal("0");
        BigDecimal moneyOne=new BigDecimal("0");
        for (Map map1:mapList){
            personNumTwo+=Integer.valueOf(map1.get("personCountTwo").toString());
            personNumOne+=Integer.valueOf(map1.get("personCountOne").toString());
            moneyTwo= BigDecimalUtils.getSum(moneyTwo,new BigDecimal(map1.get("moneyTwo").toString()));
            moneyOne= BigDecimalUtils.getSum(moneyOne,new BigDecimal(map1.get("moneyOne").toString()));
            String recordNo=map1.get("recordNo").toString();
            Map map2=new HashMap();
            map2.put("personCountTwo",map1.get("personCountTwo").toString());
            map2.put("personCountOne",map1.get("personCountOne").toString());
            map2.put("moneyTwo",map1.get("moneyTwo").toString());
            map2.put("moneyOne",map1.get("moneyOne").toString());
            resultMap.put(recordNo,map2);
        }
        Map<String, String> map3=new HashMap<>();
        map3.put("personCountOne",String.valueOf(personNumOne));
        map3.put("moneyOne",String.valueOf(moneyOne));
        resultMap.put("hjOne",map3);
        Map<String, String> map4=new HashMap<>();
        map4.put("personCountTwo",String.valueOf(personNumTwo));
        map4.put("moneyTwo",String.valueOf(moneyTwo));
        resultMap.put("hjTwo",map4);
        Map<String, String> map5=new HashMap<>();
        map5.put("personCountThree",String.valueOf(personNumTwo+personNumOne));
        map5.put("moneyThree",String.valueOf(BigDecimalUtils.getSum(moneyTwo,moneyOne)));
        resultMap.put("hjThree",map5);
        return resultMap;
    }
}
