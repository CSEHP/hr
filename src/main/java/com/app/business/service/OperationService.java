package com.app.business.service;

import com.alibaba.fastjson.JSON;
import com.app.business.model.Operation;
import com.app.system.jpa.JpaBaseDao;
import com.app.system.jpa.PageBean;
import com.app.system.jpa.Wrapper;
import com.app.system.permission.service.DataDictService;
import com.app.system.permission.service.UserInfoService;
import com.app.system.utils.CollectionUtils;
import com.app.system.utils.WebUtils;
import com.app.system.utils.dataType.DateUtils;
import com.app.system.utils.dataType.StringUtils;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by zt on 2021-06-24
 */
@Service
public class OperationService extends JpaBaseDao<Operation, Integer> {
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private DataDictService dataDictService;
    /**
     * 记录操作内容
     * @param operateType
     * @param businessType
     */

    //content操作内容 operateType操作类别   操作业务表类别businessType   //object 对象
    @Transactional
    public void getOperationHisLog(String name,int id,int operateType, int businessType, Object object) {

        Map<String,String> operateTypeMap=dataDictService.getMapByParentNameForValue_Name("操作类别");
        String jsonData="";
        if (object!=null){
            jsonData=JSON.toJSONString(object);
        }
        String content="用户："+WebUtils.getLoginUserInfo().getName()+"【"+operateTypeMap.get(String.valueOf(operateType))+"】"+"了"+"【"+name+"】"+"的数据";
        Operation operation=new Operation();
        operation.setName(WebUtils.getLoginUserInfo().getName());
        operation.setOperateContent(content);
        operation.setOperateType(operateType);
        operation.setBusinessType(businessType);
        operation.setJsonData(jsonData);
        operation.setBusinessId(id);
        this.save(operation);

    }

    /**
     * 分页数据（基础数据列表）
     */
    public PageBean<Operation> getOperationPage() {
        String createDate_ge = (String) WebUtils.getRequest().getParameter("createDate_ge");
        String createDate_le = (String) WebUtils.getRequest().getParameter("createDate_le");
        //查询语句
        StringBuffer query = new StringBuffer(" o from Operation o");
        //默认排序(PageBean指定排序字段类型的话，则默认排序不使用)
        StringBuffer orderBy = new StringBuffer("o.id desc");

        Wrapper wrapper = new Wrapper(query, orderBy);
        if (StringUtils.isNotEmpty(createDate_ge)) {
            Date date = DateUtils.stringToDateFormat(createDate_ge, "yyyy-MM-dd");
            wrapper.andGe("o.createDate", date);
        }
        if (StringUtils.isNotEmpty(createDate_le)) {
            Date date = DateUtils.stringToDateFormat(createDate_le, "yyyy-MM-dd");
            wrapper.andLe("o.createDate", date);
        }
        wrapper.initSearchParams();
        return this.pageHql(wrapper);
    }
    /**
     * 查询上一天历史记录
     * */
    public Operation getHistoryLog(int businessId,int businessType,int id){
        StringBuffer hql = new StringBuffer(" from Operation o where businessId=:businessId and businessType=:businessType and o.id!=:id and o.id<=:id order by o.id desc");
        LinkedHashMap<String,Object> map= Maps.newLinkedHashMap();
        map.put("businessId", businessId);
        map.put("businessType", businessType);
        map.put("id", id);
        List<Operation> operationList=this.findHql(hql.toString(), map);
        if (CollectionUtils.isNotEmpty(operationList)) {
            return operationList.get(0);
        }
        return null;
    }

}
