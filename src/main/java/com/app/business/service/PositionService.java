package com.app.business.service;

import com.app.business.model.NoStandardWorkerApply;
import com.app.business.model.Position;
import com.app.system.jpa.JpaBaseDao;
import com.app.system.utils.CollectionUtils;
import com.google.common.collect.Maps;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by zt on 2021-06-24
 */
@Service
public class PositionService extends JpaBaseDao<Position,Integer> {

    /**
     * 根据职务编号获取对象
     * @param code
     * @return
     */
    public Position getPositionByCode(String code){
        StringBuffer hql = new StringBuffer(" from Position p where p.code=:code ");
        Map<String, Object> map = Maps.newLinkedHashMap();
        map.put("code", code);
        List<Position> positionList = this.findHql(hql.toString(), map);
        Position position = null;
        if (CollectionUtils.isNotEmpty(positionList)) {
            position = positionList.get(0);
        }
        return position;
    }

    /**
     * 查询所有职务职称
     *
     */
    public List<Position> getPositionList(){
        String hql = " from Position p order by p.id";
        Map<String, Object> map = Maps.newLinkedHashMap();
        return this.findHql(hql, map);
    }
}
