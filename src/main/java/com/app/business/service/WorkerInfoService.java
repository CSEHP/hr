package com.app.business.service;

import com.app.business.model.WorkerInfo;
import com.app.system.jpa.JpaBaseDao;
import com.app.system.jpa.PageBean;
import com.app.system.jpa.Wrapper;
import com.app.system.jpa.en.Operator;
import com.app.system.permission.model.UserInfo;
import com.app.system.utils.CollectionUtils;
import com.app.system.utils.WebUtils;
import com.app.system.utils.dataType.DateUtils;
import com.app.system.utils.dataType.StringUtils;
import com.google.common.collect.Maps;
import org.apache.commons.collections4.Put;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zt on 2021-06-24
 */
@Service
public class WorkerInfoService extends JpaBaseDao<WorkerInfo, Integer> {
    /**
     * 分页数据（基础数据列表）
     */
    public PageBean<WorkerInfo> getWorkerInfoPage() {
        UserInfo userInfo=WebUtils.getLoginUserInfo();
        String departmentCode=userInfo.getDepartment().getCode();
        String joinWorkingDate_ge = (String) WebUtils.getRequest().getParameter("joinWorkingDate_ge");
        String joinWorkingDate_le = (String) WebUtils.getRequest().getParameter("joinWorkingDate_le");
        String retireDate_ge = (String) WebUtils.getRequest().getParameter("retireDate_ge");
        String retireDate_le = (String) WebUtils.getRequest().getParameter("retireDate_le");
        String depCode = (String) WebUtils.getRequest().getParameter("depCode");
        String sbFlag = (String) WebUtils.getRequest().getParameter("sbFlag");
        //查询语句
        StringBuffer query = new StringBuffer("w from WorkerInfo w left join fetch w.position p ");

        //默认排序(PageBean指定排序字段类型的话，则默认排序不使用)
        StringBuffer orderBy = new StringBuffer("w.id desc");

        Wrapper wrapper = new Wrapper(query, orderBy);
        wrapper.andLike("w.departmentCode",departmentCode);
        if (StringUtils.isNotEmpty(joinWorkingDate_ge)) {
            Date date = DateUtils.stringToDateFormat(joinWorkingDate_ge, "yyyy-MM-dd");
            wrapper.andGe("w.joinWorkingDate", date);
        }
        if (StringUtils.isNotEmpty(joinWorkingDate_le)) {
            Date date = DateUtils.stringToDateFormat(joinWorkingDate_le, "yyyy-MM-dd");
            wrapper.andLe("w.joinWorkingDate", date);
        }
        if (StringUtils.isNotEmpty(retireDate_ge)) {
            Date date = DateUtils.stringToDateFormat(retireDate_ge, "yyyy-MM-dd");
            wrapper.andGe("w.retireDate", date);
        }
        if (StringUtils.isNotEmpty(retireDate_le)) {
            Date date = DateUtils.stringToDateFormat(retireDate_le, "yyyy-MM-dd");
            wrapper.andLe("w.retireDate", date);
        }
        if (StringUtils.isNotEmpty(depCode)) {
            wrapper.andLike("w.departmentCode", depCode);
        }

        wrapper.initSearchParams();
        return this.pageHql(wrapper);
    }

    /**
     * 通过id联合查询
     *
     * @param workerId
     * @return
     */
    public WorkerInfo getWorkerInfoById(int workerId) {
        String hql = " from WorkerInfo w left join fetch w.position p left join fetch w.retirePosition r where w.id=:workerId";
        Map<String, Object> map = Maps.newLinkedHashMap();
        map.put("workerId", workerId);
        List<WorkerInfo> workerInfoList = this.findHql(hql.toString(), map);

        WorkerInfo workerInfo = null;
        if (CollectionUtils.isNotEmpty(workerInfoList)) {
            workerInfo = workerInfoList.get(0);
        }
        return workerInfo;
    }

    /**
     * 通过idCard联合查询
     *
     * @param idCard
     * @return
     */
    public WorkerInfo getWorkerInfoByIdCard(String idCard) {
        String hql = " from WorkerInfo w left join fetch w.position p left join fetch w.retirePosition r where w.idCard=:idCard";
        Map<String, Object> map = Maps.newLinkedHashMap();
        map.put("idCard", idCard);
        List<WorkerInfo> workerInfoList = this.findHql(hql, map);

        WorkerInfo workerInfo = null;
        if (CollectionUtils.isNotEmpty(workerInfoList)) {
            workerInfo = workerInfoList.get(0);
        }
        return workerInfo;
    }
    /**
     *  根据编号获取信息
     */
    public WorkerInfo getWorkerInfoByNum(int num,String recordNo) {
        String hql = " from WorkerInfo w left join fetch w.position p left join fetch w.retirePosition r where w.num=:num and w.recordNo=:recordNo";
        Map<String, Object> map = Maps.newLinkedHashMap();
        map.put("num", num);
        map.put("recordNo", recordNo);
        List<WorkerInfo> workerInfoList = this.findHql(hql, map);

        WorkerInfo workerInfo = null;
        if (CollectionUtils.isNotEmpty(workerInfoList)) {
            workerInfo = workerInfoList.get(0);
        }
        return workerInfo;

    }

    /**
     * 生成编号
     * @return
     */
    public int getRecordNum(String recordNo) {
        String sql = " select max(num) as num from worker_info where record_no =:recordNo ";
        Map<String, Object> map = Maps.newLinkedHashMap();
        map.put("recordNo",recordNo);
        List<Map<String, Object>> mapList = this.findSql(sql, map);
        String num="0";
        if (mapList.size()>0){
             num=String.valueOf(mapList.get(0).get("num"));
            if (num.equals("null")){
                num="0";
            }
        }
        return Integer.valueOf(num);
    }

}
