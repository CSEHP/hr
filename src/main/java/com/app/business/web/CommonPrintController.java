package com.app.business.web;

/**
 * Created by zt on 2021-06-26
 */

import com.app.business.model.NoHouseWorkerApply;
import com.app.business.model.NoStandardWorkerApply;
import com.app.business.model.Wage;
import com.app.business.model.WorkerInfo;
import com.app.business.service.NoHouseWorkerApplyService;
import com.app.business.service.NoStandardWorkerApplyService;
import com.app.business.service.WageService;
import com.app.business.service.WorkerInfoService;
import com.app.system.permission.service.DataDictService;
import com.app.system.utils.ParamUtils;
import com.app.system.utils.dataType.DateUtils;
import com.app.system.utils.dataType.StringUtils;
import com.app.system.utils.exception.Precondition;
import com.app.system.utils.dataType.BigDecimalUtils;
import com.app.system.utils.dataType.StringUtils;
import com.app.system.utils.pdf.PdfDocumentGenerator;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.List;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/pdf/print/*")
public class CommonPrintController {
    @Autowired
    private DataDictService dataDictService;
    @Autowired
    private NoHouseWorkerApplyService noHouseWorkerApplyService;
    @Autowired
    private WageService wageService;
    @Autowired
    private NoStandardWorkerApplyService noStandardWorkerApplyService;
    /**
     * 无房职工补贴打印
     */
    @RequestMapping("noHouseApplyPdf")
    public void noHouseApplyPdf(HttpServletRequest request, HttpServletResponse response) throws Exception {
        Map<String, String> isMap = dataDictService.getMapByParentNameForValue_Name("是否");
        int noHouseWorkerApplyId = ParamUtils.getInt(request, "noHouseWorkerApplyId", 0);
        int flag = ParamUtils.getInt(request, "flag", 0);
        Precondition.checkAjaxArguement(noHouseWorkerApplyId != 0, "1111", "数据错误！");
        NoHouseWorkerApply noHouseWorkerApply = noHouseWorkerApplyService.getNoHouseWorkerApplyByApplyId(noHouseWorkerApplyId);
        WorkerInfo workerInfo = noHouseWorkerApply.getWorkerInfo();
        // 是否正在发放住房补贴
        String isPayingSubsidiesStr = isMap.get(noHouseWorkerApply.getIsPayingSubsidies().toString());
        // 现职务
        String positionName = workerInfo.getPosition().getName();
        // 参加工作时间
        String startWorkingDate = DateUtils.dateToStringFormat(workerInfo.getStartWorkingDate(), "yyyy年MM月dd日");
        //调入本单位时间
        String joinWorkingDate = DateUtils.dateToStringFormat(workerInfo.getJoinWorkingDate(), "yyyy年MM月dd日");
        //退休时间
        String retireDate="";
        if (workerInfo.getRetireDate()!=null){
             retireDate = DateUtils.dateToStringFormat(workerInfo.getRetireDate(), "yyyy年MM月dd日");
        }

        // 所属工资表
        List<Wage> wageList = wageService.getWageListByWorkInfoId(workerInfo.getId());

        Map map = Maps.newHashMap();
        map.put("workerInfo", workerInfo);
        map.put("noHouseWorkerApply", noHouseWorkerApply);
        map.put("positionName", positionName);
        map.put("startWorkingDate", startWorkingDate);
        map.put("joinWorkingDate", joinWorkingDate);
        map.put("retireDate", retireDate);
        map.put("isPayingSubsidiesStr", isPayingSubsidiesStr);
        map.put("wageList", wageList);
        map.put("flag", flag);
        map.put("strUtils", new StringUtils());
        String template = "noHouseApplyPdf.html";
        this.commonPrintPDF(map, template, response);
    }

    /**
     * 公共打印方法
     *
     * @param map
     * @param template
     * @param response
     * @throws Exception
     * @throws InvocationTargetException
     * @throws NoSuchMethodException
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     */
    private void commonPrintPDF(Map map, String template, HttpServletResponse response) throws Exception, InvocationTargetException, NoSuchMethodException, NoSuchFieldException, IllegalAccessException {
        PdfDocumentGenerator pdfDocumentGenerator = new PdfDocumentGenerator();
        OutputStream outputStream = null;
        try {
            response.setHeader("Expires", "0");
            response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
            outputStream = response.getOutputStream();
            pdfDocumentGenerator.generate(template, map, outputStream);
        } catch (IOException e) {

        } finally {

        }
    }


    /**
     * 无房职工补贴打印
     */
    @RequestMapping("noStandPdf")
    @ResponseBody
    public void noStandPdf(HttpServletRequest request, HttpServletResponse response) throws Exception, InvocationTargetException, NoSuchMethodException, NoSuchFieldException, IllegalAccessException {
        int noStandId = ParamUtils.getInt(request, "noStandId", 0);
        //flag 1确认 2申报
        int flag = ParamUtils.getInt(request, "flag", 0);
        Map<String, String> jobStatusMap = dataDictService.getMapByParentNameForValue_Name("情况类型");
        Map<String, String> isEnjoyedMap = dataDictService.getMapByParentNameForValue_Name("是否享受过政府补贴");
//        Map<String,String> zwMap=dataDictService.getMapByParentNameForValue_Name("职务名称");
        NoStandardWorkerApply noStandardWorkerApply = noStandardWorkerApplyService.findOne(noStandId);
        Map map = Maps.newHashMap();
        map.put("noStandardWorkerApply", noStandardWorkerApply);
        map.put("jobStatus", jobStatusMap.get(noStandardWorkerApply.getJobStatus().toString()));
        //现在职务
        map.put("nowZw", noStandardWorkerApply.getPosition().getName());
        //申请人情况类型
        map.put("isEnjoyedToSting", isEnjoyedMap.get(noStandardWorkerApply.getIsEnjoyed().toString()));

        map.put("name", noStandardWorkerApply.getName());
        map.put("idCard", noStandardWorkerApply.getIdCard());
        if(noStandardWorkerApply.getRetireDate()!=null){
            map.put("retireDate", DateUtils.dateToStringFormat(noStandardWorkerApply.getRetireDate(),"yyyy年MM月dd日"));
        }else{
            map.put("retireDate","");
        }
        if (noStandardWorkerApply.getStartWorkingDate()!=null){
            map.put("startWorkingDate", DateUtils.dateToStringFormat(noStandardWorkerApply.getStartWorkingDate(),"yyyy年MM月dd日"));
        }else{
            map.put("startWorkingDate","");
        }
        if (noStandardWorkerApply.getJoinWorkingDate()!=null){
            map.put("joinWorkingDate", DateUtils.dateToStringFormat(noStandardWorkerApply.getJoinWorkingDate(),"yyyy年MM月dd日"));
        }else{
            map.put("joinWorkingDate","");
        }
        map.put("spouseName", noStandardWorkerApply.getSpouseName());
        map.put("spouseIdCard", noStandardWorkerApply.getSpouseIdCard());
        map.put("spouseUnit", noStandardWorkerApply.getSpouseUnit());
        map.put("livingAddress", noStandardWorkerApply.getLivingAddress());
        map.put("enjoyedBuildArea", noStandardWorkerApply.getEnjoyedBuildArea());
        map.put("phone", noStandardWorkerApply.getPhone());
        map.put("buildAreaStandard", noStandardWorkerApply.getBuildAreaStandard());
        map.put("buildAreaSureStandard", noStandardWorkerApply.getBuildAreaSureStandard());
        map.put("differenceBuildArea", noStandardWorkerApply.getDifferenceBuildArea());
        map.put("workedYears", noStandardWorkerApply.getWorkedYears());
        map.put("boughtUnitSubsidy", noStandardWorkerApply.getBoughtUnitSubsidy());
        map.put("differenceSubsidy", noStandardWorkerApply.getDifferenceSubsidy());
        map.put("totalSubsidy", noStandardWorkerApply.getTotalSubsidy());

        map.put("boughtUnit", noStandardWorkerApply.getBoughtUnit());
        map.put("boughtTroops", noStandardWorkerApply.getBoughtTroops());
        map.put("boughtTroopsSubsidy", noStandardWorkerApply.getBoughtTroopsSubsidy());
        map.put("isEnjoyed", noStandardWorkerApply.getIsEnjoyed());

        map.put("flag", Integer.valueOf(flag));
        String template = "noStandPdf.html";
        this.commonPrintPDF(map, template, response);
    }
}
