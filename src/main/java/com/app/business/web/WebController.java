package com.app.business.web;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 多数据源查询
 * Created by wcf-pc on 2019/1/23.
 */
@Controller
public class WebController {
    @RequestMapping("/c/d")
    @ResponseBody
    public List cc() {
        DataSource dataSource = new DataSource();
        dataSource.setUrl("jdbc:mysql://118.178.237.205:3306/hr?characterEncoding=UTF-8&autoReconnect=true&failOverReadOnly=false&useSSL=true");
        dataSource.setUsername("root");
        dataSource.setPassword("Wan@qwer123");
        dataSource.setDriverClassName("com.mysql.jdbc.Driver");
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        List list = jdbcTemplate.queryForList("select attributeName from datadict");

        return list;
    }
}
