package com.app.business.web;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.params.ExcelExportEntity;
import com.app.business.map.FamilyExcelMap;
import com.app.business.model.NoHouseWorkerApply;
import com.app.business.model.Wage;
import com.app.business.model.WorkerInfo;
import com.app.business.service.*;
import com.app.business.utils.ExcelStyleUtil;
//import com.app.business.utils.ExportParams;
import com.app.system.jpa.PageBean;
import com.app.system.permission.service.DataDictService;
import com.app.system.permission.service.DepartmentService;
import com.app.system.utils.ParamUtils;
import com.app.system.utils.dataType.BigDecimalUtils;
import com.app.system.utils.dataType.MoneyUtils1;
import com.app.system.utils.dataType.StringUtils;
import com.app.system.utils.exception.Precondition;
import com.app.system.utils.exception.Response;
import com.app.system.utils.page.LayUiUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created by zt on 2021-06-24
 */
@Controller
@RequestMapping("/business/noHouse/*")
public class NoHouseWorkerApplyController {

    @Autowired
    private WorkerInfoService workerInfoService;
    @Autowired
    private NoHouseWorkerApplyService noHouseWorkerApplyService;
    @Autowired
    private WageService wageService;
    @Autowired
    private DataDictService dataDictService;
    @Autowired
    private PositionService positionService;
    @Autowired
    private StatisticsService statisticsService;
    @Autowired
    private OperationService operationService;
    @Autowired
    private DepartmentService departmentService;


    /**
     * 无房职工申报列表
     */
    @RequestMapping("noHouseApplyList")
    public void noHouseApplyList() {
    }

    /**
     * 无房职工申报列表数据
     */
    @RequestMapping("loadNoHouseWorkerApplyList")
    @ResponseBody
    @RequiresPermissions("noHouse:noHouseApplyList")
    public Map loadNoHouseWorkerApplyList() {
        List<Map> listData = Lists.newArrayList();
        Map<String, String> codeMap = departmentService.getAllNameBycode();
        PageBean pageBean1=new PageBean();
        PageBean<NoHouseWorkerApply> noHouseWorkerApplyPageBean = this.noHouseWorkerApplyService.getWorkerInfoPage(pageBean1);
        List<NoHouseWorkerApply> noHouseWorkerApplyList = noHouseWorkerApplyPageBean.getDatas();
        if (CollectionUtils.isNotEmpty(noHouseWorkerApplyList)) {
            for (NoHouseWorkerApply noHouseWorkerApply : noHouseWorkerApplyList) {
                WorkerInfo workerInfo = noHouseWorkerApply.getWorkerInfo();

                Map<String, Object> item = Maps.newHashMap();
                item.put("id", noHouseWorkerApply.getId());
                item.put("name", StringUtils.getNotNull(noHouseWorkerApply.getName()));
                item.put("idCard", StringUtils.getNotNull(noHouseWorkerApply.getIdCard()));
                item.put("livingAddress", StringUtils.getNotNull(workerInfo.getLivingAddress()));
                item.put("workedYears", BigDecimalUtils.getNotNull(workerInfo.getWorkedYears()));
                item.put("startWorkingDate", String.valueOf(workerInfo.getStartWorkingDate()));
                item.put("joinWorkingDate", String.valueOf(workerInfo.getJoinWorkingDate()));
                item.put("retireDate", String.valueOf(workerInfo.getRetireDate()));
                item.put("totalSubsidy", noHouseWorkerApply.getTotalSubsidy());
                item.put("subsidyCoefficient", noHouseWorkerApply.getSubsidyCoefficient());
                item.put("lastSubsidyCoefficient", noHouseWorkerApply.getLastSubsidyCoefficient());
                if (noHouseWorkerApply.getWorkerInfo().getNum()!=null){
                    item.put("recordNo",workerInfo.getRecordNo()+new DecimalFormat("0000").format(workerInfo.getNum()));
                }else{
                    item.put("recordNo","");
                }
                item.put("depName",codeMap.get(noHouseWorkerApply.getWorkerInfo().getDepartmentCode()));

                listData.add(item);
            }
        }
        return LayUiUtils.page(noHouseWorkerApplyPageBean.getTotalCount().intValue(), listData);

    }

    /**
     * 通过身份证号提取申报表数据
     */
    @RequestMapping("loadWorkerInfoForApply")
    @ResponseBody
    public Response loadWorkerInfoForApply(HttpServletRequest request, ModelMap modelMap) {
        int workerId = ParamUtils.getInt(request, "workerId", 0);
        NoHouseWorkerApply noHouseWorkerApply1=noHouseWorkerApplyService.getNoHouseWorkerApplyByWorkId(workerId);

        String idCard = ParamUtils.getString(request, "idCard", "");
        Precondition.checkAjaxArguement(StringUtils.isNotEmpty(idCard), "1111", "请填写身份证号");
        WorkerInfo workerInfo = workerInfoService.getWorkerInfoByIdCard(idCard);
        Precondition.checkAjaxArguement(workerInfo != null, "1111", "查无此号");
        NoHouseWorkerApply noHouseWorkerApply=noHouseWorkerApplyService.getNoHouseWorkerInfoByIdCard(idCard);
        if (noHouseWorkerApply!=null && noHouseWorkerApply1==null){
            Response response = new Response<>();
            response.setCode("1111");
            response.setMessage("该条数据已录入请刷新列表");
            return response;
        }else{
            List<Wage> wageList =wageService.getWageListByWorkInfoId(workerInfo.getId());
            Map data=new HashMap();
            data.put("workerInfo",workerInfo);
            data.put("wageList",wageList);

            Response response = new Response<>();
            response.setCode("0001");
            response.setData(data);
            response.setMessage("提取成功");
            return response;
        }

    }

    /**
     * 无房职信息申报信息登记
     */
    @RequestMapping("noHouseApplyInfo")
    public void noHouseApplyInfo(HttpServletRequest request, ModelMap modelMap) {
        int noHouseWorkerApplyId = ParamUtils.getInt(request, "noHouseWorkerApplyId", 0);
        NoHouseWorkerApply noHouseWorkerApply = noHouseWorkerApplyService.findOne(noHouseWorkerApplyId);
        List<Wage> wageList=null;
        WorkerInfo workerInfo = new WorkerInfo();
        if (noHouseWorkerApply == null) {
            noHouseWorkerApply = new NoHouseWorkerApply();
        } else {
            workerInfo = noHouseWorkerApply.getWorkerInfo();
            wageList = wageService.getWageListByWorkInfoId(noHouseWorkerApply.getWorkerInfo().getId());
        }
        modelMap.addAttribute("noHouseWorkerApply", noHouseWorkerApply);
        modelMap.addAttribute("workerInfo", workerInfo);
        modelMap.addAttribute("wageList", wageList);
    }

    @RequestMapping("loadDataWageList")
    @ResponseBody
    public Map loadDataWageList(HttpServletRequest request) {
        int workInfoId = ParamUtils.getInt(request, "workInfoId", 0);
        List<Wage> wageList = wageService.getWageListByWorkInfoId(workInfoId);
        List<Map> listData = Lists.newArrayList();
        for (Wage wage : wageList) {
            Map map = new HashMap();
            map.put("name", wage.getName());
            map.put("idCard", wage.getIdCard());
            map.put("id", wage.getId());
            map.put("year", wage.getYear());
            map.put("month", wage.getMonth());
            map.put("money", wage.getMoney());
            listData.add(map);
        }
        return LayUiUtils.list(listData);
    }

    /**
     * 修改工资保存
     *
     * @param request
     * @return
     */
    @RequestMapping("saveWage")
    @ResponseBody
    public Response saveWage(HttpServletRequest request) {
        int id = ParamUtils.getInt(request, "id", 0);
        String money = ParamUtils.getString(request, "money", "0");
        Wage wage = wageService.findOne(id);
        wage.setMoney(new BigDecimal(money));
        this.wageService.save(wage);
        //操作记录
        operationService.getOperationHisLog(wage.getName(),wage.getId(),2,4,wage);

        int workerId = ParamUtils.getInt(request, "workerId", 0);
        String lastSubsidyCoefficient = ParamUtils.getString(request, "lastSubsidyCoefficient", "0");
        //        （现补贴系数*工资-原补贴系数*工资）累加四舍五入取整
        BigDecimal moneyMoney=new BigDecimal("0");
        List<Wage> wageList=wageService.getWageListByWorkInfoId(workerId);
        for (Wage wage1 :wageList){
            BigDecimal nowAndLogMoney=new BigDecimal("0");
            nowAndLogMoney=BigDecimalUtils.getBigAndBigSub(BigDecimalUtils.getBigAndBigMul(new BigDecimal("0.6"),wage1.getMoney()),BigDecimalUtils.getBigAndBigMul(new BigDecimal(lastSubsidyCoefficient),wage1.getMoney()));
            moneyMoney=moneyMoney.add(nowAndLogMoney);
        }
        moneyMoney=MoneyUtils1.sswr2w(moneyMoney);

//        //无房计算补贴合计
//        //补贴比例差*总钱数 四舍五入保留两位小数
//        Map<String, Object> countMoneyMap = wageService.getMoneyByWorkInfoId(workerId);
//        Object money = countMoneyMap.get(String.valueOf(workerId));
//        BigDecimal countMoney = new BigDecimal("0");
//        BigDecimal moneyMoneyCount = new BigDecimal(String.valueOf(money)); //月工资合计
//        BigDecimal nowScale = new BigDecimal("0.6"); //补贴比例系数
//        BigDecimal formerScale = new BigDecimal(lastSubsidyCoefficient);//原补贴比例系数
//        BigDecimal scale = BigDecimalUtils.getBigAndBigSub(nowScale, formerScale); //补贴比例系数差值
//        countMoney = MoneyUtils1.sswr2w(BigDecimalUtils.getBigAndBigMul(moneyMoneyCount, scale));//补贴总额
        return new Response("0001", "更新成功", "", moneyMoney);
    }

    /**
     * 计算无房职工补贴总额
     *
     * @param request
     * @return
     */
    @RequestMapping("getCountMoney")
    @ResponseBody
    public Response getCountMoney(HttpServletRequest request) {
        Integer workInfoId = ParamUtils.getInt(request, "workInfoId", 0);
        String lastSubsidyCoefficient = ParamUtils.getString(request, "lastSubsidyCoefficient", "0");

//        （现补贴系数*工资-原补贴系数*工资）累加
        BigDecimal moneyMoney=new BigDecimal("0");
        List<Wage> wageList=wageService.getWageListByWorkInfoId(workInfoId);
        for (Wage wage :wageList){
            BigDecimal nowAndLogMoney=new BigDecimal("0");
            nowAndLogMoney=BigDecimalUtils.getBigAndBigSub(BigDecimalUtils.getBigAndBigMul(new BigDecimal("0.6"),wage.getMoney()),BigDecimalUtils.getBigAndBigMul(new BigDecimal(lastSubsidyCoefficient),wage.getMoney()));
            moneyMoney=moneyMoney.add(nowAndLogMoney);
        }
        moneyMoney=MoneyUtils1.sswr2w(moneyMoney);


//         //补贴比例差*总钱数 四舍五入保留两位小数
//        Map<String, Object> countMoneyMap = wageService.getMoneyByWorkInfoId(workInfoId);
//        Object money = countMoneyMap.get(String.valueOf(workInfoId));
//        BigDecimal countMoney = new BigDecimal("0");
//        BigDecimal moneyMoneyCount = new BigDecimal(String.valueOf(money)); //月工资合计
//            BigDecimal nowScale = new BigDecimal("0.6"); //补贴比例系数
//            BigDecimal formerScale = new BigDecimal(lastSubsidyCoefficient);//原补贴比例系数
//            BigDecimal scale = BigDecimalUtils.getBigAndBigSub(nowScale, formerScale); //补贴比例系数差值
//            countMoney = MoneyUtils1.sswr2w(BigDecimalUtils.getBigAndBigMul(moneyMoneyCount, scale));//补贴总额

        return new Response("0001", "更新成功", "", moneyMoney);
    }

    /**
     * 修改工资保存
     *
     * @param request
     * @return
     */
    @RequestMapping("saveNoHouseApply")
    @ResponseBody
    public Response saveNoHouseApply(HttpServletRequest request) {
        return noHouseWorkerApplyService.saveData(request);
    }

    /**
     * 无房职信息查看
     */
    @RequestMapping("noHouseApplyView")
    public void noHouseApplyView(HttpServletRequest request, ModelMap modelMap) {
        int noHouseWorkerApplyId = ParamUtils.getInt(request, "noHouseWorkerApplyId", 0);
        NoHouseWorkerApply noHouseWorkerApply = noHouseWorkerApplyService.findOne(noHouseWorkerApplyId);
        List<Wage> wageList=wageService.getWageListByWorkInfoId(noHouseWorkerApply.getWorkerInfo().getId());
        modelMap.addAttribute("workerInfo", noHouseWorkerApply.getWorkerInfo());
        modelMap.addAttribute("noHouseWorkerApply", noHouseWorkerApply);
        modelMap.addAttribute("wageList", wageList);
    }


    /**
     * 无房职工补贴打印
     */
    @RequestMapping("noHousePrint")
    public void noHousePrint(HttpServletRequest request, ModelMap modelMap) {
        int noHouseWorkerApplyId = ParamUtils.getInt(request, "noHouseWorkerApplyId", 0);
        int flag = ParamUtils.getInt(request, "flag", 0);
        NoHouseWorkerApply noHouseWorkerApply = noHouseWorkerApplyService.findOne(noHouseWorkerApplyId);
        WorkerInfo workerInfo = noHouseWorkerApply.getWorkerInfo();
        modelMap.addAttribute("workerInfo", workerInfo);
        modelMap.addAttribute("noHouseWorkerApply", noHouseWorkerApply);
        modelMap.addAttribute("flag", flag);

    }

    /**
     * 无房职工申报列表
     */
    @RequestMapping("noHousePublicityList")
    public void noHousePublicityList() {

    }
    /**
     * 无房职工申报列表
     */
    @RequestMapping("noHouseApplySbList")
    public void noHouseApplySbList() {

    }

    /**
     * 无房职工汇总列表
     */
    @RequestMapping("noHouseSummaryList")
    public void noHouseSummaryList() {

    }

    /**
     * 无房职工清册列表
     */
    @RequestMapping("noHouseInventoryList")
    public void noHouseInventoryList() {

    }

    /**
     * 无房职工列表数据
     */
    @RequestMapping("noHousePublicityListInfo")
    @ResponseBody
    public Map noHousePublicityListInfo() {
        List<Map> listData = Lists.newArrayList();
        Map<String, String> btMap = this.dataDictService.getMapByParentNameForValue_Name("是否正在发放住房补贴");
        PageBean pageBean1=new PageBean();
        PageBean<NoHouseWorkerApply> PageBean = this.noHouseWorkerApplyService.getWorkerInfoPage(pageBean1);
        List<NoHouseWorkerApply> workerInfoList = PageBean.getDatas();
        if (CollectionUtils.isNotEmpty(workerInfoList)) {
            Map<String, Object> item;
            for (NoHouseWorkerApply noHouseWorkerApply : workerInfoList) {
                item = Maps.newHashMap();
                item.put("id", noHouseWorkerApply.getId());
                item.put("name", StringUtils.getNotNull(noHouseWorkerApply.getName()));
                item.put("idCard", StringUtils.getNotNull(noHouseWorkerApply.getIdCard()));
                item.put("livingAddress", StringUtils.getNotNull(noHouseWorkerApply.getWorkerInfo().getLivingAddress()));
                item.put("retireDate", String.valueOf(noHouseWorkerApply.getWorkerInfo().getRetireDate()));
                item.put("workedYears", BigDecimalUtils.getNotNull(noHouseWorkerApply.getWorkerInfo().getWorkedYears()));
                item.put("isPayingSubsidies", btMap.get(noHouseWorkerApply.getIsPayingSubsidies().toString()));
                item.put("lastSubsidyCoefficient", noHouseWorkerApply.getLastSubsidyCoefficient());
                item.put("subsidyCoefficient", noHouseWorkerApply.getSubsidyCoefficient());
                item.put("totalSubsidy", noHouseWorkerApply.getTotalSubsidy());
                listData.add(item);
            }
        }
        return LayUiUtils.page(PageBean.getTotalCount().intValue(), listData);

    }

    /**
     * 无房职工公示查看
     *
     * @param request
     * @param modelMap
     */
    @RequestMapping("noHousePublicityView")
    public void noHousePublicityView(HttpServletRequest request, ModelMap modelMap) {
        int noHouseApplyId = ParamUtils.getInt(request, "noHouseApplyId", 0);
        NoHouseWorkerApply noHouseWorkerApply = noHouseWorkerApplyService.findOne(noHouseApplyId);
        modelMap.addAttribute("noHouseWorkerApply", noHouseWorkerApply);
        modelMap.addAttribute("workerInfo", noHouseWorkerApply.getWorkerInfo());
    }

    /**
     * 无房公示信息导出
     *
     * @param request
     * @param modelMap
     */
    @RequestMapping("exportExcel")
    public void exportExcel(HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
        int flag = ParamUtils.getInt(request, "flag", 0);
        PageBean pageBean1=new PageBean();
        pageBean1.setPageSize(Integer.MAX_VALUE);
        PageBean<NoHouseWorkerApply> pageBean = this.noHouseWorkerApplyService.getWorkerInfoPage(pageBean1);
        pageBean.setPageSize(Integer.MAX_VALUE);
        List<NoHouseWorkerApply> noHouseWorkerApplyList = pageBean.getDatas();
        List<Map> listData = new ArrayList<>();
        int personCount=noHouseWorkerApplyList.size();
        BigDecimal moneyCount=new BigDecimal("0");
        for (NoHouseWorkerApply noHouseWorkerApply : noHouseWorkerApplyList) {
            moneyCount=BigDecimalUtils.getSum(moneyCount,noHouseWorkerApply.getTotalSubsidy());
        }
        this.getExportImportLoadData(noHouseWorkerApplyList, listData,flag);
        this.exportImportExcel1(response, listData, flag,personCount,moneyCount);
    }

    public void getExportImportLoadData(List<NoHouseWorkerApply> noHouseWorkerApplyList, List<Map> listData, int flag ) {
        Map<String, String> btMap = this.dataDictService.getMapByParentNameForValue_Name("是否正在发放住房补贴");
        if (noHouseWorkerApplyList.size() > 0) {
            Map<String, Object> item;
            //基础数据导入
            for (NoHouseWorkerApply noHouseWorkerApply : noHouseWorkerApplyList) {
                item = new LinkedHashMap<>();//有序
                //1.公示表 2.汇总 3.补贴清册
                if (flag == 1) {
                    item.put("name", noHouseWorkerApply.getName());
                    item.put("totalSubsidy", noHouseWorkerApply.getTotalSubsidy());
                    item.put("memo", noHouseWorkerApply.getWorkerInfo().getMemo());
                    listData.add(item);
                } else if (flag == 2) {
                    item.put("name", noHouseWorkerApply.getName());
                    item.put("idCard", noHouseWorkerApply.getIdCard());
                    item.put("zw", noHouseWorkerApply.getWorkerInfo().getPosition().getName());
                    item.put("totalSubsidy", noHouseWorkerApply.getTotalSubsidy());
                    item.put("memo", noHouseWorkerApply.getWorkerInfo().getMemo());
                    listData.add(item);
                } else if (flag == 3) {
                    item.put("name", noHouseWorkerApply.getName());
                    item.put("idCard", noHouseWorkerApply.getIdCard());
                    item.put("zw", noHouseWorkerApply.getWorkerInfo().getPosition().getName());
                    item.put("startWorkingDate", String.valueOf(noHouseWorkerApply.getWorkerInfo().getStartWorkingDate()));
                    item.put("isPayingSubsidies", btMap.get(noHouseWorkerApply.getIsPayingSubsidies().toString()));
                    item.put("lastSubsidyCoefficient", noHouseWorkerApply.getLastSubsidyCoefficient());
                    item.put("subsidyCoefficient", noHouseWorkerApply.getSubsidyCoefficient());
                    item.put("totalSubsidy", noHouseWorkerApply.getTotalSubsidy());
                    listData.add(item);
                }
            }
        }
    }

    /**
     * 导出信息
     *
     * @param response
     * @param listData
     */
    public void exportImportExcel1(HttpServletResponse response, List<Map> listData, int flag,int personCount,BigDecimal moneyCount) {
        String title = "";
        String title2="";
        String title3="";
        String title5="";
        if (flag == 1) {
            title = "无房职工住房补贴公示表";
            title3="单位全称（公章）:北京市顺义区市场监督管理局";
        } else if (flag == 2) {
            title = "无房职工住房补贴汇总表";
            title3="单位全称（公章）:北京市顺义区市场监督管理局";
        } else if (flag == 3) {
            title = "北京市顺义区市场监督管理局无房职工住房补贴清册";
            title2 = "二级单位公章:                                                                              一级单位公章：                                                                           备案单位公章：                                           ";
            title5 = "无房职工人数：" + personCount + "   无房职工补贴总额合计：" + moneyCount + " ";
        }
        //标题行
        List<ExcelExportEntity> colList = new ArrayList<>();
        ExcelExportEntity colEntity = new ExcelExportEntity("序号", "序号");
        colEntity.setNeedMerge(true);
        colList.add(colEntity);
        //档案信息跨越表头
        ExcelExportEntity itemColGroup = new ExcelExportEntity(title3, "item");
        //档案信息数值及表头列
        List<ExcelExportEntity> itemColList = new ArrayList<>();

        //封装数据
        List<Map<String, Object>> resultList = new ArrayList<>();
        Integer orderNumber = 0;
        if (CollectionUtils.isNotEmpty(listData)) {
            //封装数据及表头
            for (Map map : listData) {
                //档案信息列封装容器声明
                Map<String, Object> valMap = new HashMap<>();
                List<Map<String, Object>> detailList = new ArrayList<>();
                Map<String, Object> deliValMap = new HashMap<>();

                orderNumber++;
                valMap.put("序号", orderNumber);
                Set<Map.Entry> mapSet = map.entrySet();
                for (Map.Entry entry : mapSet) {
                    String key = entry.getKey().toString();
                    Object value = entry.getValue();
                    if (orderNumber == 1) {
                        //标题
                        String head = FamilyExcelMap.exportNoHousePubicityExcelMap().get(key).toString();
                        ExcelExportEntity excelExportEntity = new ExcelExportEntity(head, key);
                        excelExportEntity.setWidth(20);
                        itemColList.add(excelExportEntity);
                    }
                    //数据
                    deliValMap.put(key, value);
                }
                detailList.add(deliValMap);
                valMap.put("item", detailList);
                resultList.add(valMap);
            }
            if (flag == 3) {
                Map<String, Object> map1 = new HashMap<>();
                map1.put("name", "填表人:");
                map1.put("key", "签字");
                map1.put("zw", "联系电话:");
                map1.put("isPayingSubsidies", "行政一把手签字:");
                map1.put("subsidyCoefficient", "填表日期:");
                Map<String, Object> map2 = new HashMap<>();
                List<Map<String, Object>> detailListCopy = new ArrayList<>();
                detailListCopy.add(map1);
                map2.put("item", detailListCopy);
                map2.put("序号", "签字");

                Map<String, Object> map3 = new HashMap<>();
                map3.put("name", "无房职工人数:");
                map3.put("idCard", personCount);
                map3.put("subsidyCoefficient", "无房职工补贴总额合计:");
                map3.put("totalSubsidy", moneyCount);
                Map<String, Object> map4 = new HashMap<>();
                List<Map<String, Object>> detailListCopy1 = new ArrayList<>();
                detailListCopy1.add(map3);
                map4.put("item",detailListCopy1);
                map4.put("序号","合计");
                resultList.add(map4);
                resultList.add(map2);
            }
            itemColGroup.setList(itemColList);
            colList.add(itemColGroup);
        }

        ExportParams entity = new ExportParams(title,title);
        if (flag==3){
            entity.setSecondTitle(title2);
            entity.setCreateHeadRows(true);
        }
        entity.setStyle(ExcelStyleUtil.class);
        Workbook workbook = ExcelExportUtil.exportExcel(entity, colList, resultList);
        statisticsService.down(title, response, workbook);
    }

}
