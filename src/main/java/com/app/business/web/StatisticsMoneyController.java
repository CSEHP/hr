package com.app.business.web;

import com.app.business.service.NoHouseWorkerApplyService;
import com.app.business.service.NoStandardWorkerApplyService;
import com.app.system.utils.dataType.BigDecimalUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zt on 2021-08-13
 */
@Controller
@RequestMapping("/business/statistics/*")
public class StatisticsMoneyController {
    @Autowired
    private NoHouseWorkerApplyService noHouseWorkerApplyService;
    @Autowired
    private NoStandardWorkerApplyService noStandardWorkerApplyService;

    /**
     * 统计
     */
    @RequestMapping("statisticsMoney")
    public void statisticsMoney(ModelMap modelMap) {
        Map<String,Map<String,String>> noHouseMap=noHouseWorkerApplyService.getNumAndMoneyByRecord();
        Map<String,Map<String,String>> noStandMap=noStandardWorkerApplyService.getNumAndMoneyByRecord();
        Map map=new HashMap();
        map.put("count",Integer.valueOf(noHouseMap.get("hj").get("personCount"))+Integer.valueOf(noStandMap.get("hjThree").get("personCountThree")));
        map.put("money", String.valueOf(BigDecimalUtils.getSum(new BigDecimal(noHouseMap.get("hj").get("money")),new BigDecimal(noStandMap.get("hjThree").get("moneyThree")))));
        noStandMap.put("zhj",map);
        modelMap.addAttribute("noHouseMap",noHouseMap);
        modelMap.addAttribute("noStandMap",noStandMap);
    }

    @RequestMapping("loadStatisticsMoney")
    public void loadStatisticsMoney(ModelMap modelMap) {

    }
}
