package com.app.business.web;

import com.alibaba.fastjson.JSON;
import com.app.business.map.FamilyExcelMap;
import com.app.business.model.*;
import com.app.business.service.*;
import com.app.system.jpa.PageBean;
import com.app.system.permission.service.DataDictService;
import com.app.system.utils.GetDifference;
import com.app.system.utils.ParamUtils;
import com.app.system.utils.dataType.DateUtils;
import com.app.system.utils.page.LayUiUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.Reader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by zt on 2021-06-29
 */
@Controller
@RequestMapping("/business/operation/*")
public class OperateController {
    @Autowired
    private OperationService operationService;
    @Autowired
    private DataDictService dataDictService;
    @Autowired
    private WorkerInfoService workerInfoService;
    @Autowired
    private NoHouseWorkerApplyService noHouseWorkerApplyService;
    @Autowired
    private NoStandardWorkerApplyService noStandardWorkerApplyService;
    @Autowired
    private WageService wageService;

    @RequestMapping("operateLogList")
    public void operateLogList() {

    }
    @RequestMapping("view")
    public void view() {

    }

    /**
     * 操作记录列表数据
     */
    @RequestMapping("loadOperationList")
    @ResponseBody
    public Map loadOperationList() throws IllegalArgumentException, IllegalAccessException{
        Map<String, String> operateTypeMap = dataDictService.getMapByParentNameForValue_Name("操作类别");
        Map<String, String> businessTypeMap = dataDictService.getMapByParentNameForValue_Name("操作业务表类别");
        List<Map> listData = Lists.newArrayList();
        PageBean<Operation> pageBean = this.operationService.getOperationPage();
        List<Operation> operationList = pageBean.getDatas();
        if (CollectionUtils.isNotEmpty(operationList)) {
            Map<String, Object> item;
            for (Operation operation : operationList) {
                item = Maps.newHashMap();
                item.put("id", operation.getId());
                item.put("name", operation.getName());
                item.put("businessType", businessTypeMap.get(operation.getBusinessType().toString()));
                item.put("operateType", operateTypeMap.get(operation.getOperateType().toString()));
                item.put("content", operation.getOperateContent());
                item.put("create", DateUtils.dateToStringFormat(operation.getCreateTime(), "YYYY-MM-dd"));
                //操作内容处理
                Operation operationLog=operationService.getHistoryLog(operation.getBusinessId(),operation.getBusinessType(),operation.getId());
                WorkerInfo workerInfoLog=null;
                NoHouseWorkerApply noHouseWorkerApplyLog=null;
                NoStandardWorkerApply noStandardWorkerApplyLog=null;
                Wage wageLog=null;
                WorkerInfo workerInfo=null;
                NoHouseWorkerApply noHouseWorkerApply=null;
                NoStandardWorkerApply noStandardWorkerApply=null;
                Wage wage=null;
                String content1="";
                String content2="";
                try {
                    JSON json=JSON.parseObject(operation.getJsonData());
                    JSON jsonLog=null;
                    if (operationLog!=null){
                        jsonLog=JSON.parseObject(operationLog.getJsonData());
                    }
                    if (operation.getBusinessType()==1){
                        workerInfoLog = json.toJavaObject(jsonLog,WorkerInfo.class);
                        workerInfo = json.toJavaObject(json,WorkerInfo.class);
                        if (operation.getOperateType()==1){
                            workerInfoLog=null;
                        }
                        if (operation.getOperateType()==3){
                            workerInfo=null;
                        }
                        Map<String, String> map = GetDifference.getDifference(workerInfoLog, workerInfo);

                        if (map.size()>0){
                            for(Map.Entry<String, String> entry : map.entrySet()) {
                                if (entry.getKey()!=entry.getValue()){
                                    content1+=entry.getValue()+";";
                                }
                                List<String> list = Arrays.asList(content1.split(","));
                                for (int i=0;i<=list.size();i++){
                                    content2+= FamilyExcelMap.contentMap1().get(list.get(i).substring(0,list.get(i).indexOf(":")))+":"+list.get(i).substring(list.get(i).indexOf(":")+1)+";";
                                }
                            }
                        }
                    }else if (operation.getBusinessType()==2){
                        noHouseWorkerApplyLog = json.toJavaObject(jsonLog,NoHouseWorkerApply.class);
                        noHouseWorkerApply = json.toJavaObject(json,NoHouseWorkerApply.class);
                        if (operation.getOperateType()==1){
                            noHouseWorkerApplyLog=null;
                        }
                        if (operation.getOperateType()==3){
                            noHouseWorkerApply=null;
                        }
                        Map<String, String> map = GetDifference.getDifference(noHouseWorkerApplyLog, noHouseWorkerApply);
                        if (map.size()>0){
                        for(Map.Entry<String, String> entry : map.entrySet()) {
                            if (entry.getKey() != entry.getValue()) {
                                content1 += entry.getValue() + ";";
                            }
                            List<String> list = Arrays.asList(content1.split(","));
                            for (int i = 0; i <= list.size(); i++) {
                                content2 += FamilyExcelMap.contentMap2().get(list.get(i).substring(0, list.get(i).indexOf(":"))) + ":" + list.get(i).substring(list.get(i).indexOf(":") + 1) + ";";
                            }
                        }
                        }
                    }else if (operation.getBusinessType()==3){
                        noStandardWorkerApplyLog = json.toJavaObject(jsonLog,NoStandardWorkerApply.class);
                        noStandardWorkerApply = json.toJavaObject(json,NoStandardWorkerApply.class);
                        if (operation.getOperateType()==1){
                            noStandardWorkerApplyLog=null;
                        }
                        if (operation.getOperateType()==3){
                            noStandardWorkerApply=null;
                        }
                        Map<String, String> map = GetDifference.getDifference(noStandardWorkerApplyLog, noStandardWorkerApply);
                        if (map.size()>0) {
                            for (Map.Entry<String, String> entry : map.entrySet()) {
                                if (entry.getKey() != entry.getValue()) {
                                    content1 += entry.getValue() + ";";
                                }
                                List<String> list = Arrays.asList(content1.split(","));
                                for (int i = 0; i <= list.size(); i++) {
                                    content2 += FamilyExcelMap.contentMap3().get(list.get(i).substring(0, list.get(i).indexOf(":"))) + ":" + list.get(i).substring(list.get(i).indexOf(":") + 1) + ";";
                                }
                            }
                        }
                    }else if (operation.getBusinessType()==4){
                        wageLog = json.toJavaObject(jsonLog,Wage.class);
                        wage = json.toJavaObject(json,Wage.class);
                        if (operation.getOperateType()==1){
                            wageLog=null;
                        }
                        if (operation.getOperateType()==3){
                            wage=null;
                        }
                        Map<String, String> map = GetDifference.getDifference(wageLog, wage);
                        if (map.size()>0) {
                            for (Map.Entry<String, String> entry : map.entrySet()) {
                                if (entry.getKey() != entry.getValue()) {
                                    content1 += entry.getValue() + ";";
                                }
                                List<String> list = Arrays.asList(content1.split(","));
                                for (int i = 0; i <= list.size(); i++) {
                                    content2 += FamilyExcelMap.contentMap4().get(list.get(i).substring(0, list.get(i).indexOf(":"))) + ":" + list.get(i).substring(list.get(i).indexOf(":") + 1) + ";";
                                }
                            }

                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                item.put("content2", content2);
                listData.add(item);
            }
        }
        return LayUiUtils.page(pageBean.getTotalCount().intValue(), listData);
    }


    @RequestMapping("operationView")
    public void operationView(HttpServletRequest request, ModelMap modelMap) {
        int operationId = ParamUtils.getInt(request, "operationId", 0);
        Operation operation=operationService.findOne(operationId);
        Operation operationLog=operationService.getHistoryLog(operation.getBusinessId(),operation.getBusinessType(),operation.getId());

        WorkerInfo workerInfo=null;
        NoHouseWorkerApply noHouseWorkerApply=null;
        NoStandardWorkerApply noStandardWorkerApply=null;
        Wage wage=null;
        WorkerInfo workerInfoLog=null;
        NoHouseWorkerApply noHouseWorkerApplyLog=null;
        NoStandardWorkerApply noStandardWorkerApplyLog=null;
        Wage wageLog=null;
        try {
            JSON json=JSON.parseObject(operation.getJsonData());
            JSON jsonLog=null;
            if (operationLog!=null){
                jsonLog=JSON.parseObject(operationLog.getJsonData());
            }
            if (operation.getBusinessType()==1){
                workerInfoLog = json.toJavaObject(jsonLog,WorkerInfo.class);
                workerInfo = json.toJavaObject(json,WorkerInfo.class);
                if (operation.getOperateType()==1){
                    workerInfoLog=null;
                }
                if (operation.getOperateType()==3){
                    workerInfo=null;
                    workerInfoLog = json.toJavaObject(json,WorkerInfo.class);
                }
            }else if (operation.getBusinessType()==2){
                noHouseWorkerApplyLog = json.toJavaObject(jsonLog,NoHouseWorkerApply.class);
                noHouseWorkerApply = json.toJavaObject(json,NoHouseWorkerApply.class);
                if (operation.getOperateType()==1){
                    noHouseWorkerApplyLog=null;
                }
                if (operation.getOperateType()==3){
                    noHouseWorkerApply=null;
                    noHouseWorkerApplyLog = json.toJavaObject(json,NoHouseWorkerApply.class);
                }
            }else if (operation.getBusinessType()==3){
                noStandardWorkerApplyLog = json.toJavaObject(jsonLog,NoStandardWorkerApply.class);
                noStandardWorkerApply = json.toJavaObject(json,NoStandardWorkerApply.class);
                if (operation.getOperateType()==1){
                    noStandardWorkerApplyLog=null;
                }
                if (operation.getOperateType()==3){
                    noStandardWorkerApply=null;
                    noStandardWorkerApplyLog = json.toJavaObject(json,NoStandardWorkerApply.class);
                }
            }else if (operation.getBusinessType()==4){
                wageLog = json.toJavaObject(jsonLog,Wage.class);
                wage = json.toJavaObject(json,Wage.class);
                if (operation.getOperateType()==1){
                    wageLog=null;
                }
                if (operation.getOperateType()==3){
                    wage=null;
                    wageLog = json.toJavaObject(json,Wage.class);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        modelMap.addAttribute("operation",operation);

        modelMap.addAttribute("workerInfo",workerInfo);
        modelMap.addAttribute("noHouseWorkerApply",noHouseWorkerApply);
        modelMap.addAttribute("noStandardWorkerApply",noStandardWorkerApply);
        modelMap.addAttribute("wage",wage);
        modelMap.addAttribute("workerInfoLog",workerInfoLog);
        modelMap.addAttribute("noHouseWorkerApplyLog",noHouseWorkerApplyLog);
        modelMap.addAttribute("noStandardWorkerApplyLog",noStandardWorkerApplyLog);
        modelMap.addAttribute("wageLog",wageLog);
    }


}
