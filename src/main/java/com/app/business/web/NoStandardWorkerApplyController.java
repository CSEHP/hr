package com.app.business.web;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.params.ExcelExportEntity;
import com.app.business.map.FamilyExcelMap;
import com.app.business.model.*;
import com.app.business.service.*;
import com.app.business.utils.ExcelStyleUtil;
import com.app.system.jpa.PageBean;
import com.app.system.permission.model.UserInfo;
import com.app.system.permission.service.DataDictService;
import com.app.system.permission.service.DepartmentService;
import com.app.system.utils.ParamUtils;
import com.app.system.utils.WebUtils;
import com.app.system.utils.dataType.*;
import com.app.system.utils.exception.Response;
import com.app.system.utils.page.LayUiUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

/**
 * Created by zt on 2021-06-24
 */
@Controller
@RequestMapping("/business/noStandard/*")
public class NoStandardWorkerApplyController {
    @Autowired
    private NoStandardWorkerApplyService noStandardWorkerApplyService;
    @Autowired
    private DataDictService dataDictService;
    @Autowired
    private PositionService positionService;
    @Autowired
    private StatisticsService statisticsService;
    @Autowired
    private OperationService operationService;
    @Autowired
    private WorkerInfoService workerInfoService;
    @Autowired
    private DepartmentService departmentService;

    /**
     * 未达标职工申报列表
     */
    @RequestMapping("noStandInfoList")
    public void noStandInfoList() {

    }

    /**
     * 未达标职工列表数据
     */
    @RequestMapping("loadNoStandInfoList")
    @ResponseBody
    public Map loadNoStandInfoList() {
        Map<String, String> codeMap = departmentService.getAllNameBycode();
        Map<String, String> jobStatusMap = dataDictService.getMapByParentNameForValue_Name("情况类型");
        Map<String, String> workerTypeMap = dataDictService.getMapByParentNameForValue_Name("职工类型");
        Map<String, String> zfbtMap = dataDictService.getMapByParentNameForValue_Name("是否享受过政府补贴");
        List<Map> listData = Lists.newArrayList();
        PageBean pageBean1=new PageBean();
        PageBean<NoStandardWorkerApply> pageBean = this.noStandardWorkerApplyService.getNoStandardWorkerApplyPage(pageBean1);
        List<NoStandardWorkerApply> noStandardWorkerApplyList = pageBean.getDatas();
        if (CollectionUtils.isNotEmpty(noStandardWorkerApplyList)) {
            Map<String, Object> item;
            for (NoStandardWorkerApply noStandardWorkerApply : noStandardWorkerApplyList) {
                item = Maps.newHashMap();
                item.put("id", noStandardWorkerApply.getId());
                item.put("name", StringUtils.getNotNull(noStandardWorkerApply.getName()));
                item.put("idCard", StringUtils.getNotNull(noStandardWorkerApply.getIdCard()));
                item.put("boughtUnit", StringUtils.getNotNull(noStandardWorkerApply.getBoughtUnit()));
                item.put("livingAddress", StringUtils.getNotNull(noStandardWorkerApply.getLivingAddress()));
                item.put("phone", StringUtils.getNotNull(noStandardWorkerApply.getPhone()));
                item.put("retireDate", DateUtils.dateToStringDefault(noStandardWorkerApply.getRetireDate()));
                item.put("joinWorkingDate", StringUtils.getNotNull(noStandardWorkerApply.getJoinWorkingDate().toString()));
                item.put("jobStatus", jobStatusMap.get(noStandardWorkerApply.getJobStatus().toString()));
                item.put("isEnjoyed", zfbtMap.get(noStandardWorkerApply.getIsEnjoyed().toString()));
                item.put("workerType", workerTypeMap.get(noStandardWorkerApply.getWorkerType().toString()));
                item.put("differenceSubsidy",BigDecimalUtils.getNotNullStr(noStandardWorkerApply.getDifferenceSubsidy()));
                item.put("totalSubsidy",BigDecimalUtils.getNotNullStr(noStandardWorkerApply.getTotalSubsidy()));
                if (noStandardWorkerApply.getNum()!=null){
                    item.put("recordNo",noStandardWorkerApply.getRecordNo()+new DecimalFormat("0000").format(noStandardWorkerApply.getNum()));
                }else{
                    item.put("recordNo","");
                }
                item.put("depName",codeMap.get(noStandardWorkerApply.getDepartmentCode()));
                listData.add(item);
            }
        }
        return LayUiUtils.page(pageBean.getTotalCount().intValue(), listData);
    }

    /**
     * 未达标职工获取数据
     */
    @RequestMapping("noStandInfoInfo")
    public void noStandInfoInfo(HttpServletRequest request, ModelMap modelMap) {
        UserInfo userInfo=WebUtils.getLoginUserInfo();
        String departmentCode=userInfo.getDepartment().getCode();
        Map<String,String> depByNumMap=dataDictService.getMapByParentNameForValue_Name("编号规则");
        String record="JGBJ-";
        if (departmentCode.length()>3){
            record=depByNumMap.get(departmentCode.substring(0,4));
        }
        List<Position> positionList=positionService.getPositionList();
        int noStandId = ParamUtils.getInt(request, "noStandId", 0);
        String nowPosition = "";
        String txPosition = "";
        String ylPosition = "";
        String num="";
        NoStandardWorkerApply noStandardWorkerApply = noStandardWorkerApplyService.findOne(noStandId);
        if (noStandardWorkerApply == null) {
            noStandardWorkerApply = new NoStandardWorkerApply();
            num=this.getRecordNoAndNum(record);
        }else{
            if (noStandardWorkerApply.getNum()==null){
                num="";
            }else{
                num=new DecimalFormat("0000").format(noStandardWorkerApply.getNum());
            }
            if (noStandardWorkerApply.getPosition() != null) {
                nowPosition = noStandardWorkerApply.getPosition().getCode();
            }
            if (noStandardWorkerApply.getRetirePosition() != null) {
                txPosition = noStandardWorkerApply.getRetirePosition().getCode();
            }
            if (noStandardWorkerApply.getLastPosition() != null) {
                ylPosition = noStandardWorkerApply.getLastPosition().getCode();
            }
        }
        modelMap.addAttribute("noStandardWorkerApply", noStandardWorkerApply);
        modelMap.addAttribute("nowPosition", nowPosition);
        modelMap.addAttribute("txPosition", txPosition);
        modelMap.addAttribute("ylPosition", ylPosition);
        modelMap.addAttribute("positionList", positionList);
        modelMap.addAttribute("num", num);
        modelMap.addAttribute("record", record);
    }
    /**
     * 获取最新编号
     * @return
     */
    public String getRecordNoAndNum(String recordNo){
        DecimalFormat decimalFormat = new DecimalFormat("0000");
        String num="";
        int number1=Integer.valueOf(workerInfoService.getRecordNum(recordNo));
        int number2=Integer.valueOf(noStandardWorkerApplyService.getRecordNum(recordNo));
        if (number1>number2){
            num=(decimalFormat.format(number1+1));
        }else{
            num=(decimalFormat.format(number2+1));
        }
        return num;
    }
    /**
     * 未达标职工获取数据
     */
    @RequestMapping("noStandInfoInfoCopy")
    public void noStandInfoInfoCopy(HttpServletRequest request, ModelMap modelMap) {
        UserInfo userInfo=WebUtils.getLoginUserInfo();
        String departmentCode=userInfo.getDepartment().getCode();
        Map<String,String> depByNumMap=dataDictService.getMapByParentNameForValue_Name("编号规则");
        String record="JGBJ-";
        if (departmentCode.length()>3){
            record=depByNumMap.get(departmentCode.substring(0,4));
        }
        int noStandId = ParamUtils.getInt(request, "noStandId", 0);
        List<Position> positionList=positionService.getPositionList();
        String nowPosition = "";
        String txPosition = "";
        String ylPosition = "";
        String num="";
        NoStandardWorkerApply noStandardWorkerApply = noStandardWorkerApplyService.findOne(noStandId);
        if (noStandardWorkerApply == null) {
            noStandardWorkerApply = new NoStandardWorkerApply();
            num=this.getRecordNoAndNum(record);
        }else{
            if (noStandardWorkerApply.getNum()==null){
                num="";
            }else{
                num=new DecimalFormat("0000").format(noStandardWorkerApply.getNum());
            }
            if (noStandardWorkerApply.getPosition() != null) {
                nowPosition = noStandardWorkerApply.getPosition().getCode();
            }
            if (noStandardWorkerApply.getRetirePosition() != null) {
                txPosition = noStandardWorkerApply.getRetirePosition().getCode();
            }
            if (noStandardWorkerApply.getLastPosition() != null) {
                ylPosition = noStandardWorkerApply.getLastPosition().getCode();
            }
        }
        modelMap.addAttribute("noStandardWorkerApply", noStandardWorkerApply);
        modelMap.addAttribute("nowPosition", nowPosition);
        modelMap.addAttribute("txPosition", txPosition);
        modelMap.addAttribute("ylPosition", ylPosition);
        modelMap.addAttribute("positionList", positionList);
        modelMap.addAttribute("num", num);
        modelMap.addAttribute("record", record);
    }

    /**
     *
     * @param request
     * @return
     */
    @RequestMapping("getNoStandMoney")
    @ResponseBody
    public Response getNoStandMoney(HttpServletRequest request) {
        //是否享受过政府补贴 1未享受 2已享受
        int isEnjoyed=ParamUtils.getInt(request,"isEnjoyed",0);
        //购房补贴金额
        String boughtUnitSubsidy=ParamUtils.getString(request,"boughtUnitSubsidy","0");
        //军队补贴金额
        String boughtTroopsSubsidy=ParamUtils.getString(request,"boughtTroopsSubsidy","0");
        //住房补贴建筑面积标准
        String buildAreaStandard=ParamUtils.getString(request,"buildAreaStandard","0");
        //核定后住房总面积
        String buildAreaSureStandard = ParamUtils.getString(request, "buildAreaSureStandard", "0");
        //公积金前的工龄
        String workedYears = ParamUtils.getString(request, "workedYears", "0");
        if (isEnjoyed==0){
            return new Response("0002", "请选择是否享受过政府补贴");
        }else if (isEnjoyed==1){  //1未享受
            //差额面积
            BigDecimal area= BigDecimalUtils.getBigAndBigSub(new BigDecimal(buildAreaStandard),new BigDecimal(buildAreaSureStandard));
            //差额补贴额=（421+10.9×职工建立住房公积金前的工龄）×差额面积
//            BigDecimal money1= BigDecimalUtils.getBigAndBigMul(BigDecimalUtils.getSum(BigDecimalUtils.getBigAndBigMul(new BigDecimal("10.9"),new BigDecimal(workedYears)),new BigDecimal("421")),area);
            BigDecimal money= MoneyUtils1.sswr2w(BigDecimalUtils.getBigAndBigMul(BigDecimalUtils.getSum(BigDecimalUtils.getBigAndBigMul(new BigDecimal("10.9"),new BigDecimal(workedYears)),new BigDecimal("421")),area));
            //补贴总额度 补贴总额=差额补贴额—单位补贴额—部队住房补贴额     四舍五入保留两位小数sswrz 四舍五入保留整数sswrzs
            BigDecimal moneyCount=MoneyUtils1.sswr2w( BigDecimalUtils.getBigAndBigSub(money,BigDecimalUtils.getSum(new BigDecimal(boughtUnitSubsidy),new BigDecimal(boughtTroopsSubsidy))));
           Map map=new HashMap();
           map.put("area",area);
           map.put("money",money);
           map.put("moneyCount",moneyCount);
           return new Response("0001", "更新成功", "", map);
        }else{
            //2已享受
            //差额面积
            BigDecimal area= BigDecimalUtils.getBigAndBigSub(new BigDecimal(buildAreaStandard),new BigDecimal(buildAreaSureStandard));
            //差额补贴额=（421+10.9×职工建立住房公积金前的工龄）×差额面积  四舍五入保留两位小数sswr2w 四舍五入保留整数sswrzs
            BigDecimal money= MoneyUtils1.sswr2w(BigDecimalUtils.getBigAndBigMul(BigDecimalUtils.getSum(BigDecimalUtils.getBigAndBigMul(new BigDecimal("10.9"),new BigDecimal(workedYears)),new BigDecimal("421")),area));
            BigDecimal moneyCount=MoneyUtils1.sswr2w(BigDecimalUtils.getBigAndBigSub(money,BigDecimalUtils.getSum(new BigDecimal(boughtUnitSubsidy),new BigDecimal(boughtTroopsSubsidy))));
            Map map=new HashMap();
            map.put("area",area);
            map.put("money",money);
            map.put("moneyCount",moneyCount);
            return new Response("0001", "更新成功","", map);
        }

    }

    /**
     * 无房职信息保存
     */
    @RequestMapping("noStandInfoSave")
    @ResponseBody
    public synchronized Response noStandInfoSave(HttpServletRequest request) {
        int noStandId = ParamUtils.getInt(request, "noStandId", 0);
        String nowPosition = ParamUtils.getString(request, "nowPosition", "");
        String txPosition = ParamUtils.getString(request, "txPosition", "");
            String ylPosition = ParamUtils.getString(request, "ylPosition", "");
        //获取职务对象
        Position position = positionService.getPositionByCode(nowPosition);
        Position retirePosition = positionService.getPositionByCode(txPosition);
        Position lastPositionInfo = positionService.getPositionByCode(ylPosition);
        NoStandardWorkerApply noStandardWorkerApply=noStandardWorkerApplyService.findOne(noStandId);
        int operateType=2;
        if (noStandardWorkerApply==null){
            operateType=1;
            noStandardWorkerApply=new NoStandardWorkerApply();
            noStandardWorkerApply.setPosition(position);
            noStandardWorkerApply.setRetirePosition(retirePosition);
            noStandardWorkerApply.setLastPosition(lastPositionInfo);
            UserInfo userInfo=WebUtils.getLoginUserInfo();
            noStandardWorkerApply.setDepartmentCode(userInfo.getDepartment().getCode());
        }else{
            if (position!=null){
                noStandardWorkerApply.setPosition(position);
            }
            if (retirePosition!=null){
                noStandardWorkerApply.setRetirePosition(retirePosition);
            }
            if (lastPositionInfo!=null){
                noStandardWorkerApply.setLastPosition(lastPositionInfo);
            }
        }
        WebUtils.bind(request, noStandardWorkerApply);
        if (noStandardWorkerApply.getSpouseIsEnjoyed()==2){
            noStandardWorkerApply.setSpouseEnjoyedArea(new BigDecimal("0"));
        }
        noStandardWorkerApplyService.save(noStandardWorkerApply);
         //操作记录
        operationService.getOperationHisLog(noStandardWorkerApply.getName(),noStandardWorkerApply.getId(),operateType,3,noStandardWorkerApply);

        return new Response("0001", "保存成功");
    }

    /**
     * 未达标职工数据删除
     * @param request
     * @return
     */
    @RequestMapping("delNoStand")
    @ResponseBody
    public Response delNoStand(HttpServletRequest request) {
        int noStandId=ParamUtils.getInt(request,"noStandId",0);
        if (noStandId!=0){
            NoStandardWorkerApply noStandardWorkerApply=noStandardWorkerApplyService.findOne(noStandId);
            //操作记录
            operationService.getOperationHisLog(noStandardWorkerApply.getName(),noStandardWorkerApply.getId(),3,3,noStandardWorkerApply);
            noStandardWorkerApplyService.delete(noStandId);
            return new Response("0001", "删除成功");
        } else {
            return new Response("0002", "删除失败");
        }
    }

    /**
     * 未达标职工数据查看除
     */
    @RequestMapping("noStandView")
    public void noStandView(HttpServletRequest request, ModelMap modelMap) {
        int noStandId = ParamUtils.getInt(request, "noStandId", 0);
        NoStandardWorkerApply noStandardWorkerApply = noStandardWorkerApplyService.findOne(noStandId);
        modelMap.addAttribute("noStandardWorkerApply", noStandardWorkerApply);
    }

    /**
     * 无房信息确认列表
     * @param request
     * @param modelMap
     */
    @RequestMapping("noStandVerifyList")
    public void noStandVerifyList(HttpServletRequest request, ModelMap modelMap) {

    }
    /**
     * 无房信息数据公示列表
     * @param request
     * @param modelMap
     */
    @RequestMapping("noStandPublicList")
    public void noStandPublicList(HttpServletRequest request, ModelMap modelMap) {

    }
    /**
     * 无房公示信息导出
     * @param request
     * @param modelMap
     */
    @RequestMapping("exportExcel")
    public void exportExcel(HttpServletRequest request, ModelMap modelMap, HttpServletResponse response) {
        int flag=ParamUtils.getInt(request,"flag",0);
        PageBean<NoStandardWorkerApply> PageBean=null;
        PageBean pageBean1=new PageBean();
        pageBean1.setPageSize(Integer.MAX_VALUE);
        if (flag==3){
             PageBean = noStandardWorkerApplyService.getNoStandardWorkerApplyZzPage(pageBean1);
        }else if (flag==4){
             PageBean = noStandardWorkerApplyService.getNoStandardWorkerApplyTxPage(pageBean1);
        }else if (flag==5){
            PageBean = noStandardWorkerApplyService.getNoStandardWorkerApplyOtherPage(pageBean1);
        }else{
             PageBean = noStandardWorkerApplyService.getNoStandardWorkerApplyPage(pageBean1);
        }
        List<NoStandardWorkerApply> noStandardWorkerApplyList=PageBean.getDatas();
        int personCount=noStandardWorkerApplyList.size();
        BigDecimal moneyCount=new BigDecimal("0");
        for (NoStandardWorkerApply noStandardWorkerApply:noStandardWorkerApplyList){
            moneyCount=BigDecimalUtils.getSum(moneyCount,noStandardWorkerApply.getTotalSubsidy());
        }
        List<Map> listData = new ArrayList<>();
        this.getExportImportLoadData(noStandardWorkerApplyList, listData,flag);
        this.exportImportExcel1(response, listData,flag,personCount,moneyCount);
    }

    public void getExportImportLoadData(List<NoStandardWorkerApply> noStandardWorkerApplyList, List<Map> listData, int flag) {
        Map<String, String> btMap = this.dataDictService.getMapByParentNameForValue_Name("是否正在发放住房补贴");
        if (noStandardWorkerApplyList.size() > 0) {
            Map<String, Object> item;
            //基础数据导入
            for (NoStandardWorkerApply noStandardWorkerApply : noStandardWorkerApplyList) {
                item = new LinkedHashMap<>();//有序
                //1.公示表 2.汇总 3.补贴清册(在职) 4.补贴清册(离职) 5补贴清册（其他）
                if (flag==1){
                    item.put("name", noStandardWorkerApply.getName());
                    item.put("differenceSubsidy", noStandardWorkerApply.getDifferenceSubsidy());
                    item.put("totalSubsidy", noStandardWorkerApply.getTotalSubsidy());
                    item.put("memo", noStandardWorkerApply.getMemo());
                    listData.add(item);
                }else if (flag==2){
                    item.put("name", noStandardWorkerApply.getName());
                    item.put("idCard", noStandardWorkerApply.getIdCard());
                    item.put("zw", noStandardWorkerApply.getPosition().getName());
                    item.put("totalSubsidy", noStandardWorkerApply.getTotalSubsidy());
                    item.put("memo", noStandardWorkerApply.getMemo());
                    listData.add(item);
                }else if (flag==3 || flag==5){
                    item.put("name", noStandardWorkerApply.getName());
                    item.put("idCard", noStandardWorkerApply.getIdCard());
                    item.put("startWorkingDate", noStandardWorkerApply.getStartWorkingDate());
                    item.put("zw", noStandardWorkerApply.getPosition().getName());
                    item.put("buildAreaStandard", noStandardWorkerApply.getBuildAreaStandard());
                    item.put("workedYears", noStandardWorkerApply.getWorkedYears());
                    item.put("buildAreaSureStandard", noStandardWorkerApply.getBuildAreaSureStandard());
                    item.put("differenceBuildArea", noStandardWorkerApply.getDifferenceBuildArea());
                    item.put("boughtUnitSubsidyDw", noStandardWorkerApply.getBoughtUnitSubsidy());
                    item.put("boughtTroopsSubsidy", noStandardWorkerApply.getBoughtTroopsSubsidy());
                    item.put("totalSubsidy", noStandardWorkerApply.getTotalSubsidy());
                    listData.add(item);
                }else if (flag==4){
                    item.put("name", noStandardWorkerApply.getName());
                    item.put("idCard", noStandardWorkerApply.getIdCard());
                    item.put("startWorkingDate", noStandardWorkerApply.getStartWorkingDate());
                    item.put("retireDate", noStandardWorkerApply.getRetireDate());
                    String tx = "";
                    if (noStandardWorkerApply.getRetirePosition() != null) {
                        tx = noStandardWorkerApply.getRetirePosition().getName();
                    }
                    item.put("tx", tx);
                    item.put("buildAreaStandard", noStandardWorkerApply.getBuildAreaStandard());
                    item.put("workedYears", noStandardWorkerApply.getWorkedYears());
                    item.put("buildAreaSureStandard", noStandardWorkerApply.getBuildAreaSureStandard());
                    item.put("differenceBuildArea", noStandardWorkerApply.getDifferenceBuildArea());
                    item.put("boughtUnitSubsidyDw", noStandardWorkerApply.getBoughtUnitSubsidy());
                    item.put("boughtTroopsSubsidy", noStandardWorkerApply.getBoughtTroopsSubsidy());
                    item.put("totalSubsidy", noStandardWorkerApply.getTotalSubsidy());
                    listData.add(item);
                }
            }
        }
    }

    /**
     * 导出信息
     *
     * @param response
     * @param listData
     */
    public void exportImportExcel1(HttpServletResponse response, List<Map> listData,int flag,int personCount,BigDecimal moneyCount) {
        Map<String, String> dictMap=dataDictService.getMapByParentNameForValue_Name("情况类型");
        String title="";
        String title2="";
        String title3="";
        String type="";
        if (flag==1){
            title = "未达标职工住房补贴公示表";
            title3="单位全称（公章）:北京市顺义区市场监督管理局";
        }else if (flag==2){
            title = "未达标职工住房补贴汇总表";
            title3="单位全称（公章）:北京市顺义区市场监督管理局";
        }else if (flag==3){
            type="在职";
            title = "北京市顺义区市场监督管理局未达标职工住房补贴清册（在职）";
            title2 = "二级单位公章:                                                                                                  一级单位公章：                                                                                                              备案单位公章：                                                                                          ";
        }else if (flag==4){
            type="离退休";
            title = "北京市顺义区市场监督管理局未达标职工住房补贴清册（离退休）";
            title2 = "二级单位公章:                                   一级单位公章：                                 备案单位公章： ";
        }else if (flag==5){
            type="其他";
            title = "北京市顺义区市场监督管理局未达标职工住房补贴清册（其他）";
            title2 = "二级单位公章:                                   一级单位公章：                                 备案单位公章： ";
        }
        //标题行
        List<ExcelExportEntity> colList = new ArrayList<>();
        ExcelExportEntity colEntity = new ExcelExportEntity("序号", "序号");
        colEntity.setNeedMerge(true);
        colList.add(colEntity);
        //档案信息跨越表头
        ExcelExportEntity itemColGroup = new ExcelExportEntity(title3, "item");
        //档案信息数值及表头列
        List<ExcelExportEntity> itemColList = new ArrayList<>();

        //封装数据
        List<Map<String, Object>> resultList = new ArrayList<>();
        Integer orderNumber = 0;
        if (CollectionUtils.isNotEmpty(listData)) {
            //封装数据及表头
            for (Map map : listData) {
                //档案信息列封装容器声明
                Map<String, Object> valMap = new HashMap<>();
                List<Map<String, Object>> detailList = new ArrayList<>();
                Map<String, Object> deliValMap = new HashMap<>();

                orderNumber++;
                valMap.put("序号", orderNumber);
                Set<Map.Entry> mapSet = map.entrySet();
                for (Map.Entry entry : mapSet) {
                    String key = entry.getKey().toString();
                    Object value = entry.getValue();
                    if (orderNumber == 1) {
                        //标题
                        String head = FamilyExcelMap.exportNoStandPubicityExcelMap().get(key).toString();
                        ExcelExportEntity excelExportEntity = new ExcelExportEntity(head, key);
                        excelExportEntity.setWidth(20);
                        itemColList.add(excelExportEntity);
                    }
                    //数据
                    deliValMap.put(key, value);
                }
                detailList.add(deliValMap);
                valMap.put("item", detailList);
                resultList.add(valMap);
            }
            if (flag == 3 || flag == 4 ||flag==5) {
                Map<String, Object> map1 = new HashMap<>();
                map1.put("name", "填表人:");
                map1.put("key", "签字");
                map1.put("startWorkingDate", "联系电话:");
                map1.put("buildAreaStandard", "行政一把手签字:");
                map1.put("buildAreaSureStandard", "填表日期:");
                Map<String, Object> map2 = new HashMap<>();
                List<Map<String, Object>> detailListCopy = new ArrayList<>();
                detailListCopy.add(map1);
                map2.put("item", detailListCopy);
                map2.put("序号", "签字");

                Map<String, Object> map3 = new HashMap<>();
                map3.put("name", type+"未达标职工人数：");
                map3.put("idCard", personCount);
                map3.put("boughtTroopsSubsidy", type+"未达标职工补贴总额合计：");
                map3.put("totalSubsidy", moneyCount);
                Map<String, Object> map4 = new HashMap<>();
                List<Map<String, Object>> detailListCopy1 = new ArrayList<>();
                detailListCopy1.add(map3);
                map4.put("item",detailListCopy1);
                map4.put("序号","合计");
                resultList.add(map4);
                resultList.add(map2);
            }
            itemColGroup.setList(itemColList);
            colList.add(itemColGroup);
        }

        ExportParams entity = new ExportParams(title,title);
        if (flag==3 || flag==4 || flag==5){
            entity.setSecondTitle(title2);
        }
        entity.setStyle(ExcelStyleUtil.class);
        Workbook workbook = ExcelExportUtil.exportExcel(entity, colList, resultList);
        statisticsService.down(title, response, workbook);
    }

    /**
     *未达标汇总列表
     */
    @RequestMapping("noStandSummaryList")
    public void noStandSummaryList(){
    }
    /**
     *未达标清册列表（在职）
     */
    @RequestMapping("noStandQczzList")
    public void noStandQczzList(){
    }
    /**
     *未达标清册列表（退休）
     */
    @RequestMapping("noStandTxList")
    public void noStandTxList(){
    }
    /**
     *未达标清册列表（其他）
     */
    @RequestMapping("otherList")
    public void otherList(){
    }



    /**
     * 未达标职工列表数据(在职)
     */
    @RequestMapping("loadNoStandInfoZzList")
    @ResponseBody
    public Map loadNoStandInfoZzList() {
        Map<String, String> codeMap = departmentService.getAllNameBycode();
        Map<String, String> jobStatusMap = dataDictService.getMapByParentNameForValue_Name("情况类型");
        Map<String, String> workerTypeMap = dataDictService.getMapByParentNameForValue_Name("职工类型");
        Map<String, String> zfbtMap = dataDictService.getMapByParentNameForValue_Name("是否享受过政府补贴");
        List<Map> listData = Lists.newArrayList();
        PageBean pageBean1=new PageBean();
        PageBean<NoStandardWorkerApply> pageBean = this.noStandardWorkerApplyService.getNoStandardWorkerApplyZzPage(pageBean1);
        List<NoStandardWorkerApply> noStandardWorkerApplyList = pageBean.getDatas();
        if (CollectionUtils.isNotEmpty(noStandardWorkerApplyList)) {
            Map<String, Object> item;
            for (NoStandardWorkerApply noStandardWorkerApply : noStandardWorkerApplyList) {
                item = Maps.newHashMap();
                item.put("id", noStandardWorkerApply.getId());
                item.put("name", StringUtils.getNotNull(noStandardWorkerApply.getName()));
                item.put("idCard", StringUtils.getNotNull(noStandardWorkerApply.getIdCard()));
                item.put("boughtUnit", StringUtils.getNotNull(noStandardWorkerApply.getBoughtUnit()));
                item.put("livingAddress", StringUtils.getNotNull(noStandardWorkerApply.getLivingAddress()));
                item.put("phone", StringUtils.getNotNull(noStandardWorkerApply.getPhone()));
                item.put("retireDate",DateUtils.dateToStringDefault(noStandardWorkerApply.getRetireDate()));
                item.put("jobStatus", jobStatusMap.get(noStandardWorkerApply.getJobStatus().toString()));
                item.put("isEnjoyed", zfbtMap.get(noStandardWorkerApply.getIsEnjoyed().toString()));
                item.put("workerType", workerTypeMap.get(noStandardWorkerApply.getWorkerType().toString()));
                item.put("differenceSubsidy",BigDecimalUtils.getNotNullStr(noStandardWorkerApply.getDifferenceSubsidy()));
                item.put("totalSubsidy",BigDecimalUtils.getNotNullStr(noStandardWorkerApply.getTotalSubsidy()));
                if (noStandardWorkerApply.getNum()!=null){
                    item.put("recordNo",noStandardWorkerApply.getRecordNo()+new DecimalFormat("0000").format(noStandardWorkerApply.getNum()));
                }else{
                    item.put("recordNo","");
                }
                item.put("depName",codeMap.get(noStandardWorkerApply.getDepartmentCode()));
                listData.add(item);
            }
        }
        return LayUiUtils.page(pageBean.getTotalCount().intValue(), listData);
    }
    /**
     * 未达标职工列表数据(在职)
     */
    @RequestMapping("loadNoStandInfoTxList")
    @ResponseBody
    public Map loadNoStandInfoTxList() {
        Map<String, String> codeMap = departmentService.getAllNameBycode();
        Map<String, String> jobStatusMap = dataDictService.getMapByParentNameForValue_Name("情况类型");
        Map<String, String> workerTypeMap = dataDictService.getMapByParentNameForValue_Name("职工类型");
        Map<String, String> zfbtMap = dataDictService.getMapByParentNameForValue_Name("是否享受过政府补贴");
        List<Map> listData = Lists.newArrayList();
        PageBean pageBean1=new PageBean();
        PageBean<NoStandardWorkerApply> pageBean = this.noStandardWorkerApplyService.getNoStandardWorkerApplyTxPage(pageBean1);
        List<NoStandardWorkerApply> noStandardWorkerApplyList = pageBean.getDatas();
        if (CollectionUtils.isNotEmpty(noStandardWorkerApplyList)) {
            Map<String, Object> item;
            for (NoStandardWorkerApply noStandardWorkerApply : noStandardWorkerApplyList) {
                item = Maps.newHashMap();
                item.put("id", noStandardWorkerApply.getId());
                item.put("name", StringUtils.getNotNull(noStandardWorkerApply.getName()));
                item.put("idCard", StringUtils.getNotNull(noStandardWorkerApply.getIdCard()));
                item.put("boughtUnit", StringUtils.getNotNull(noStandardWorkerApply.getBoughtUnit()));
                item.put("livingAddress", StringUtils.getNotNull(noStandardWorkerApply.getLivingAddress()));
                item.put("phone", StringUtils.getNotNull(noStandardWorkerApply.getPhone()));
                item.put("retireDate", DateUtils.dateToStringDefault(noStandardWorkerApply.getRetireDate()));
                item.put("jobStatus", jobStatusMap.get(noStandardWorkerApply.getJobStatus().toString()));
                item.put("isEnjoyed", zfbtMap.get(noStandardWorkerApply.getIsEnjoyed().toString()));
                item.put("workerType", workerTypeMap.get(noStandardWorkerApply.getWorkerType().toString()));
                item.put("differenceSubsidy",BigDecimalUtils.getNotNullStr(noStandardWorkerApply.getDifferenceSubsidy()));
                item.put("totalSubsidy",BigDecimalUtils.getNotNullStr(noStandardWorkerApply.getTotalSubsidy()));
                if (noStandardWorkerApply.getNum()!=null){
                    item.put("recordNo",noStandardWorkerApply.getRecordNo()+new DecimalFormat("0000").format(noStandardWorkerApply.getNum()));
                }else{
                    item.put("recordNo","");
                }
                item.put("depName",codeMap.get(noStandardWorkerApply.getDepartmentCode()));
                listData.add(item);
            }
        }
        return LayUiUtils.page(pageBean.getTotalCount().intValue(), listData);
    }
    /**
     * 未达标职工列表数据(在职)
     */
    @RequestMapping("loadNoStandInfoOtherList")
    @ResponseBody
    public Map loadNoStandInfoOtherList() {
        Map<String, String> codeMap = departmentService.getAllNameBycode();
        Map<String, String> jobStatusMap = dataDictService.getMapByParentNameForValue_Name("情况类型");
        Map<String, String> workerTypeMap = dataDictService.getMapByParentNameForValue_Name("职工类型");
        Map<String, String> zfbtMap = dataDictService.getMapByParentNameForValue_Name("是否享受过政府补贴");
        List<Map> listData = Lists.newArrayList();
        PageBean pageBean1=new PageBean();
        PageBean<NoStandardWorkerApply> pageBean = this.noStandardWorkerApplyService.getNoStandardWorkerApplyOtherPage(pageBean1);
        List<NoStandardWorkerApply> noStandardWorkerApplyList = pageBean.getDatas();
        if (CollectionUtils.isNotEmpty(noStandardWorkerApplyList)) {
            Map<String, Object> item;
            for (NoStandardWorkerApply noStandardWorkerApply : noStandardWorkerApplyList) {
                item = Maps.newHashMap();
                item.put("id", noStandardWorkerApply.getId());
                item.put("name", StringUtils.getNotNull(noStandardWorkerApply.getName()));
                item.put("idCard", StringUtils.getNotNull(noStandardWorkerApply.getIdCard()));
                item.put("boughtUnit", StringUtils.getNotNull(noStandardWorkerApply.getBoughtUnit()));
                item.put("livingAddress", StringUtils.getNotNull(noStandardWorkerApply.getLivingAddress()));
                item.put("phone", StringUtils.getNotNull(noStandardWorkerApply.getPhone()));
                item.put("retireDate", DateUtils.dateToStringDefault(noStandardWorkerApply.getRetireDate()));
                item.put("jobStatus", jobStatusMap.get(noStandardWorkerApply.getJobStatus().toString()));
                item.put("isEnjoyed", zfbtMap.get(noStandardWorkerApply.getIsEnjoyed().toString()));
                item.put("workerType", workerTypeMap.get(noStandardWorkerApply.getWorkerType().toString()));
                item.put("differenceSubsidy",BigDecimalUtils.getNotNullStr(noStandardWorkerApply.getDifferenceSubsidy()));
                item.put("totalSubsidy",BigDecimalUtils.getNotNullStr(noStandardWorkerApply.getTotalSubsidy()));
                if (noStandardWorkerApply.getNum()!=null){
                    item.put("recordNo",noStandardWorkerApply.getRecordNo()+new DecimalFormat("0000").format(noStandardWorkerApply.getNum()));
                }else{
                    item.put("recordNo","");
                }
                item.put("depName",codeMap.get(noStandardWorkerApply.getDepartmentCode()));
                listData.add(item);
            }
        }
        return LayUiUtils.page(pageBean.getTotalCount().intValue(), listData);
    }


    /**
     * 未达标（确认）职工补贴打印
     */
    @RequestMapping("noStandQrPrint")
    public void noStandQrPrint(HttpServletRequest request, ModelMap modelMap) {
        int noStandId = ParamUtils.getInt(request, "noStandId", 0);
        NoStandardWorkerApply noStandardWorkerApply=noStandardWorkerApplyService.findOne(noStandId);
        modelMap.addAttribute("noStandardWorkerApply", noStandardWorkerApply);
    }
    /**
     * 未达标（申报）职工补贴打印
     */
    @RequestMapping("noStandSbPrint")
    public void noStandSbPrint(HttpServletRequest request, ModelMap modelMap) {
        int noStandId = ParamUtils.getInt(request, "noStandId", 0);
        int flag = ParamUtils.getInt(request, "flag", 0);
        NoStandardWorkerApply noStandardWorkerApply=noStandardWorkerApplyService.findOne(noStandId);
        modelMap.addAttribute("noStandardWorkerApply", noStandardWorkerApply);
        modelMap.addAttribute("flag", flag);
    }
    /**
     * 无房职工申报列表
     */
    @RequestMapping("noStandDeclareList")
    public void noStandDeclareList(HttpServletRequest request, ModelMap modelMap) {
    }
    /**
     * 身份证号码验证
     */
    @RequestMapping("loadWorkerInfoForApply")
    @ResponseBody
    public Response loadWorkerInfoForApply(HttpServletRequest request, ModelMap modelMap) {
        String idCard = ParamUtils.getString(request, "idCard", "");
        NoStandardWorkerApply noStandardWorkerApply = noStandardWorkerApplyService.getWorkerInfoByIdCard(idCard);
        if (noStandardWorkerApply!=null){
            return new Response("0002","身份证号重复请重新填写");
        }else{
            Response response = new Response<>();
            response.setCode("0001");
            return response;

        }
    }
    /**
     * 根据编号获取面积
     * @param request
     * @return
     */
    @RequestMapping("getAreaByCode")
    @ResponseBody
    public Response getAreaByCode(HttpServletRequest request){
        String code=ParamUtils.getString(request,"code","");
        if (code==""){
            return new Response("0002", "请选择职务职称");
        }
        BigDecimal area=positionService.getPositionByCode(code).getArea();
        return new Response("0001", "","",area);
    }
    @RequestMapping("checkRecordNo")
    @ResponseBody
    public Response checkRecordNo(HttpServletRequest request) {
        int num = ParamUtils.getInt(request, "num", 0);
        String recordNo = ParamUtils.getString(request, "recordNo", "");
        int noStandId = ParamUtils.getInt(request, "noStandId", 0);
        NoStandardWorkerApply noStandardWorkerApply1=noStandardWorkerApplyService.getWorkerInfoById(noStandId);
        WorkerInfo workerInfo = workerInfoService.getWorkerInfoByNum(num,recordNo);
        NoStandardWorkerApply noStandardWorkerApply = noStandardWorkerApplyService.getWorkerInfoByNum(num,recordNo);
        if (workerInfo==null && noStandardWorkerApply==null){
            Response response = new Response<>();
            response.setCode("0001");
            return response;
        }else if (noStandardWorkerApply1.getNum()==num){
            Response response = new Response<>();
            response.setCode("0001");
            return response;
        }else{
            return new Response("0002","备案编号重复请重新填写");
        }
    }
}
