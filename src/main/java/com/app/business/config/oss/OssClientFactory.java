package com.app.business.config.oss;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * OSSClient单例化工厂
 * 注：别的地方不能shutdown连接
 *
 * @author WMY
 * @date 2021/05/24
 */
//@Component
public class OssClientFactory {

    private OssClientFactory() {}

    private static String endpoint;

    private static String accessKeyId;

    private static String accessKeySecret;

    @Value("${Oos.endpoint}")
    public void setEndpoint(String endpoint) {
        OssClientFactory.endpoint = endpoint;
    }

    @Value("${Oos.accessKeyId}")
    public void setAccessKeyId(String accessKeyId) {
        OssClientFactory.accessKeyId = accessKeyId;
    }

    @Value("${Oos.accessKeySecret}")
    public void setAccessKeySecret(String accessKeySecret) {
        OssClientFactory.accessKeySecret = accessKeySecret;
    }

    private static class InnerClass {
        // 连接客户端实体
        private static final OSS OSS_CLIENT = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
    }

    /**
     * 获取ossClient连接
     *
     * @return ossClient
     */
    public static OSS getOssClient() {
        return InnerClass.OSS_CLIENT;
    }
}
