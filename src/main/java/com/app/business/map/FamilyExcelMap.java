package com.app.business.map;

import com.app.system.permission.service.DataDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author wmy
 * @date 2020/5/6
 */
@SuppressWarnings("all")
public class FamilyExcelMap {
    @Autowired
    private static DataDictService dataDictService;

    /**
     * 加载成员变量
     * @param request
     */
    public FamilyExcelMap(HttpServletRequest request) {
        WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
        dataDictService = (DataDictService) webApplicationContext.getBean(DataDictService.class);
    }

    /**
     * 导出信息Excel Map
     *
     * @return
     */
    public static Map exportNoHousePubicityExcelMap() {
        Map map = new HashMap();
        map.put("name", "姓名");
        map.put("idCard", "身份证号码");
        map.put("zw", "职务 （职称）");
        map.put("startWorkingDate", "参加工作时间(年月)");
        map.put("isPayingSubsidies", "是否正在发放住房补贴");
        map.put("lastSubsidyCoefficient", "原补贴系数");
        map.put("subsidyCoefficient", "现补贴系数");
        map.put("totalSubsidy", "补贴总额(元)");
        map.put("memo", "备注");
        return map;
    }

    /**
     * 导出信息Excel Map
     *
     * @return
     */
    public static Map exportNoStandPubicityExcelMap() {
        Map map = new HashMap();
        map.put("name", "姓名");
        map.put("idCard", "身份证号码");
        map.put("zw", "职务 （职称）");
        map.put("differenceSubsidy", "差额补贴额");
        map.put("totalSubsidy", "补贴总额(元)");
        map.put("memo", "备注");


        map.put("startWorkingDate", "参加工作时间");
        map.put("buildAreaStandard", "住房补贴建筑面积标准(㎡)");
        map.put("workedYears", "建立住房公积金前的工龄（年）");
        map.put("buildAreaSureStandard", "核定后住房总面积（㎡）");
        map.put("differenceBuildArea", "差额面积（㎡）");
        map.put("boughtUnitSubsidy", "已发补贴额（元）");
        map.put("boughtUnitSubsidyDw", "单位补贴额（元）");
        map.put("boughtTroopsSubsidy", "部队补贴额(元)");
        map.put("totalSubsidy", "补贴总额（元）");
        map.put("differenceSubsidy", "差额补贴额（元）");

        map.put("retireDate", "退休时间");
        map.put("tx", "退休时职务(岗位)");

        return map;
    }

    /**
     * 导出信息Excel Map
     *
     * @return
     */
    public static Map contentMap1() {
        Map map = new HashMap();
        map.put("name", "姓名");
        map.put("idCard", "身份证号码");
        map.put("workedYears", "建立公积金前工龄");
        map.put("startWorkingDate", "参加工作时间");
        map.put("joinWorkingDate", "调入本单位时间");
        map.put("retireDate", "退休时间");
        map.put("spouseName", "配偶姓名");
        map.put("spouseIdCard", "配偶身份证号");
        map.put("spouseUnit", "配偶工作单位");
        map.put("livingAddress", "住房地址");
        map.put("wages", "月基本工资");
        map.put("memo", "备注");
        return map;
    }
    public static Map contentMap2() {
        Map map = new HashMap();
        map.put("name", "姓名");
        map.put("idCard", "身份证号码");
        map.put("isPayingSubsidies", "是否正在发放住房补贴");
        map.put("lastSubsidyCoefficient", "原补贴系数");
        map.put("subsidyCoefficient", "现补贴系数");
        map.put("totalSubsidy", "补贴总额");
        return map;
    }
    public static Map contentMap3() {
        Map map = new HashMap();
        map.put("name", "姓名");
        map.put("idCard", "身份证号码");
        map.put("phone", "联系电话");
        map.put("jobStatus", "情况类型");
        map.put("workerType", "职工类型");
        map.put("startWorkingDate", "参加工作时间");
        map.put("retireDate", "退休时间");
        map.put("retireDate", "调入本单位时间");
        map.put("spouseName", "配偶姓名");
        map.put("spouseIdCard", "配偶身份证号");
        map.put("spouseUnit", "配偶工作单位");
        map.put("spouseIsEnjoyed", "配偶是否享受过补贴");
        map.put("spouseEnjoyedArea", "配偶补贴面积");
        map.put("livingAddress", "住房地址");
        map.put("boughtUnit", "申请人购房时单位");
        map.put("boughtUnitSubsidy", "申请人购房时补贴额");
        map.put("boughtTroops", "军队转业干部时部队");
        map.put("boughtTroopsSubsidy", "军队转业干部时补贴额");
        map.put("isEnjoyed", "是否享受过含有政府补贴性质的住房");
        map.put("enjoyedBuildArea", "享受过含有政府补贴性质的住房面积");
        map.put("buildAreaStandard", "住房补贴建筑面积标准");
        map.put("buildAreaSureStandard", "核定后住房总面积");
        map.put("differenceBuildArea", "差额面积");
        map.put("workedYears", "建立住房公积金前的工龄");
        map.put("differenceSubsidy", "差额补贴额");
        map.put("totalSubsidy", "补贴总额");
        map.put("memo", "备注");
        return map;
    }
    public static Map contentMap4() {
        Map map = new HashMap();
        map.put("name", "姓名");
        map.put("idCard", "身份证号码");
        map.put("year", "工资年度");
        map.put("month", "工资月度");
        map.put("money", "工资金额");
        return map;
    }
}
