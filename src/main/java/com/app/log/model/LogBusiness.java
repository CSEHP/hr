package com.app.log.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 业务办理日志
 * Created by WCF on 2019/8/28.
 */
@Data
@Entity
@Table(name = "log_business")
@SuppressWarnings("all")
public class LogBusiness implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    private String username;//用户名
    private String operation;//行为
    private String classMethod;//方法
    private String url;//访问路径
    @Column(columnDefinition = "text")
    private String uri;//访问参数
    private String ip;//ip地址
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;//操作时间
    private Long createDateLong;//操作时间long

}
