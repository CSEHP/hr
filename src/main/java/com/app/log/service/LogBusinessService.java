package com.app.log.service;

import com.app.log.model.LogBusiness;
import com.app.system.jpa.JpaBaseDao;
import com.app.system.jpa.PageBean;
import com.app.system.jpa.Wrapper;
import com.app.system.utils.WebUtils;
import com.app.system.utils.dataType.DateUtils;
import com.app.system.utils.dataType.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * Created by WCF on 2019/8/28.
 */
@Service
@SuppressWarnings("all")
public class LogBusinessService extends JpaBaseDao<LogBusiness, Long> {

    public PageBean<LogBusiness> pageList() {
        //查询内容项
        StringBuffer query = new StringBuffer("l from LogBusiness l");
        //排序
        StringBuffer orderBy = new StringBuffer("l.createDateLong desc");
        Wrapper wrapper = new Wrapper(query, orderBy);
        String key = (String) WebUtils.getRequest().getParameter("search_key");
        String column = (String) WebUtils.getRequest().getParameter("search_column");
        String date_ge = (String) WebUtils.getRequest().getParameter("date_ge");
        String date_le = (String) WebUtils.getRequest().getParameter("date_le");
        if (StringUtils.isNotEmpty(key)) {
            wrapper.andLike("l." + column, key);
        }
        if (StringUtils.isNotEmpty(date_ge)) {
            Date date = DateUtils.stringToDateFormat(date_ge, "yyyy-MM-dd HH:mm:ss");
            wrapper.andGe("l.createDate", date);
        }
        if (StringUtils.isNotEmpty(date_le)) {
            Date date = DateUtils.stringToDateFormat(date_le, "yyyy-MM-dd HH:mm:ss");
            wrapper.andLe("l.createDate", date);
        }
        wrapper.initSearchParams();//查询参数赋值
        return this.pageHql(wrapper);
    }

    @Transactional
    public void addLogBusiness(LogBusiness logBusiness) {
        this.save(logBusiness);
    }
}
