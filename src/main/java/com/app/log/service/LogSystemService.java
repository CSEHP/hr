package com.app.log.service;

import com.app.log.model.LogSystem;
import com.app.system.jpa.JpaBaseDao;
import com.app.system.jpa.PageBean;
import com.app.system.jpa.Wrapper;
import com.app.system.utils.CollectionUtils;
import com.app.system.utils.ParamUtils;
import com.app.system.utils.WebUtils;
import com.app.system.utils.dataType.DateUtils;
import com.app.system.utils.dataType.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Enumeration;

/**
 * Created by WCF on 2019/8/28.
 */
@Service
@SuppressWarnings("all")
public class LogSystemService extends JpaBaseDao<LogSystem, Long> {

    public PageBean<LogSystem> pageList() {
        //查询内容项
        StringBuffer query = new StringBuffer("l from LogSystem l");
        //排序
        StringBuffer orderBy = new StringBuffer("l.createDateLong desc");
        Wrapper wrapper = new Wrapper(query, orderBy);
        String key = (String) WebUtils.getRequest().getParameter("search_key");
        String column = (String) WebUtils.getRequest().getParameter("search_column");
        String dateGe = (String) WebUtils.getRequest().getParameter("date_ge");
        String dateLe = (String) WebUtils.getRequest().getParameter("date_le");
        if (StringUtils.isNotEmpty(key)) {
            wrapper.andLike("l." + column, key);
        }
        if (StringUtils.isNotEmpty(dateGe)) {
            Date date = DateUtils.stringToDateFormat(dateGe, "yyyy-MM-dd HH:mm:ss");
            wrapper.andGe("l.createDate", date);
        }
        if (StringUtils.isNotEmpty(dateLe)) {
            Date date = DateUtils.stringToDateFormat(dateLe, "yyyy-MM-dd HH:mm:ss");
            wrapper.andLe("l.createDate", date);
        }
        wrapper.initSearchParams();//查询参数赋值
        return this.pageHql(wrapper);
    }

    /**
     * 自定义添加日志
     *
     * @param request
     * @param value
     * @param classMethod
     */
    @Transactional
    public void addLog(HttpServletRequest request, String username, String value, String classMethod) {
        LogSystem log = new LogSystem();
        Date date = new Date();
        log.setOperation(value);//保存获取的操作
        log.setClassMethod(classMethod);
        log.setUrl(request.getRequestURI());
        Enumeration info = request.getParameterNames();
        StringBuffer param = new StringBuffer("");
        String myUri = null;
        while (info.hasMoreElements()) {
            myUri = (String) info.nextElement();
            param.append(myUri);
            param.append("=");
            param.append(ParamUtils.getString(request, myUri, ""));
            param.append("&");
        }
        log.setUri(param.toString());
        log.setCreateDate(date);
        log.setCreateDateLong(date.getTime());
        log.setUsername(username);
        String ip = WebUtils.getIpAddr(request);
        log.setIp(ip);
        this.save(log);
    }
    @Transactional
    public void addLogSystem(LogSystem logSystem){
        this.save(logSystem);
    }
}
