package com.app.log.web;

import com.app.log.annotation.MyLog;
import com.app.log.model.LogBusiness;
import com.app.log.model.LogSystem;
import com.app.log.service.LogBusinessService;
import com.app.log.service.LogSystemService;
import com.app.system.jpa.PageBean;
import com.app.system.permission.model.DataDict;
import com.app.system.utils.CollectionUtils;
import com.app.system.utils.dataType.DateUtils;
import com.app.system.utils.dataType.StringUtils;
import com.app.system.utils.page.LayUiUtils;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * Created by WCF on 2019/8/28.
 */
@Controller
@RequestMapping("/logs/**")
@SuppressWarnings("all")
public class LogInfoController {
    @Resource
    private LogSystemService logSystemService;
    @Resource
    private LogBusinessService logBusinessService;

    @RequestMapping("list")
    @RequiresPermissions("logs:list")
    public void list() {

    }

    @RequestMapping("loadData")
    @ResponseBody
    @RequiresPermissions("logs:loadData")
    public Map loadData(String type) {
        PageBean pageBean = null;
        List<Map> listData = Lists.newArrayList();
        List dataList = null;
        type = StringUtils.isEmpty(type) ? "system" : type;
        if ("system".equals(type)) {
            pageBean = this.logSystemService.pageList();
            dataList = pageBean.getDatas();
            loadLogSystem(dataList, listData);
        } else {
            pageBean = this.logBusinessService.pageList();
            dataList = pageBean.getDatas();
            loadLogBusiness(dataList, listData);
        }
        return LayUiUtils.page(pageBean.getTotalCount().intValue(), listData);
    }

    /**
     * 系统权限日志
     *
     * @param dataList
     * @param listData
     */
    private void loadLogSystem(List<LogSystem> dataList, List<Map> listData) {
        if (!CollectionUtils.isEmpty(dataList)) {
            for (LogSystem log : dataList) {
                Map<String, Object> item = Maps.newHashMap();
                item.put("id", log.getId());
                item.put("username", log.getUsername());
                item.put("operation", log.getOperation());
                item.put("classMethod", log.getClassMethod());
                item.put("url", log.getUrl());
                item.put("uri", log.getUri());
                item.put("ip", log.getIp());
                item.put("createDate", DateUtils.dateToStringFormat(log.getCreateDateLong(), "yyyy-MM-dd HH:mm:ss:SSS"));
                listData.add(item);
            }
        }
    }

    /**
     * 业务办理日志
     *
     * @param dataList
     * @param listData
     */
    private void loadLogBusiness(List<LogBusiness> dataList, List<Map> listData) {
        if (!CollectionUtils.isEmpty(dataList)) {
            for (LogBusiness log : dataList) {
                Map<String, Object> item = Maps.newHashMap();
                item.put("id", log.getId());
                item.put("username", log.getUsername());
                item.put("operation", log.getOperation());
                item.put("classMethod", log.getClassMethod());
                item.put("url", log.getUrl());
                item.put("uri", log.getUri());
                item.put("ip", log.getIp());
                item.put("createDate", DateUtils.dateToStringFormat(log.getCreateDateLong(), "yyyy-MM-dd HH:mm:ss:SSS"));
                listData.add(item);
            }
        }
    }
}
