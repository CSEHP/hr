package com.app.log.annotation;


import java.lang.annotation.*;

/**
 * Created by WCF on 2019/8/28.
 */
@Target(ElementType.METHOD)//注解放置的目标位置，METHOD是可注解再方法级别上
@Retention(RetentionPolicy.RUNTIME)//注解在哪个阶段执行
@Documented//生成文档
public @interface MyLog {
    String value() default "";
}
