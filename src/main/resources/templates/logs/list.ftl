<!DOCTYPE html>
<html>
<head>
    <title>系统日志查看</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row  layui-col-space10">
        <div class="layui-col-xs12">
            <div class="layui-card">
                <div class="layui-tab layui-tab-brief" lay-filter="tab">
                    <ul class="layui-tab-title">
                        <li class="layui-this" lay-type="system">系统权限日志</li>
                        <li lay-type="business">业务办理日志</li>
                    </ul>
                    <div class="layui-tab-content">
                        <div class="layui-tab-item layui-show">
                            <form class="layui-form searchForm" id="searchForm">
                                <input type="hidden" name="type" id="type" value="system"/>
                                <div class="layui-inline" style="width: 100px;">
                                    <select name="search_column" id="search_column">
                                        <option value="username">用户名</option>
                                        <option value="operation">用户行为</option>
                                        <option value="classMethod">实体方法</option>
                                        <option value="url">访问路径</option>
                                        <option value="uri">访问参数</option>
                                        <option value="ip">ip地址</option>
                                    </select>
                                </div>
                                <div class="layui-inline layui-hide-xs">
                                    <input class="layui-input" name="search_key" id="search_key"
                                           autocomplete="off" style="width: 150px;" placeholder="请输入关键字">
                                </div>
                                <div class="layui-inline">日期：</div>
                                <div class="layui-inline">
                                    <input class="layui-input" name="date_ge" id="date_ge"
                                           autocomplete="off" style="width: 150px;">
                                </div>
                                <div class="layui-inline">-</div>
                                <div class="layui-inline">
                                    <input class="layui-input" name="date_le" id="date_le"
                                           autocomplete="off" style="width: 150px;">
                                </div>
                                <button class="layui-btn" type="button" id="searchButton"><i
                                        class="layui-icon layui-icon-search"></i> 搜索
                                </button>
                            </form>
                            <table id="dataTable" class="layui-table" lay-filter="layFilter"></table>
                            <!--序号-->
                            <script type="text/html" id="indexTpl">
                                {{d.LAY_TABLE_INDEX+1}}
                            </script><!--菜单-->
                            <script type="text/html" id="dataTable_toolbar">
                                <a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="dataTable_view"><i
                                        class="layui-icon">&#xe615;</i>查看</a>
                            </script>
                            <!-- 表格头部工具栏 -->
                            <script type="text/html" id="toolbar">
                                <div class="layui-btn-container right">
                                    <button lay-event="load" class="layui-btn layui-btn-sm"><i class="layui-icon">
                                        &#xe669;</i>重载
                                    </button>
                                </div>
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.config({
        base: '/modules/' //静态资源所在路径
    }).use(['table', 'form', 'jquery', 'jmdf', 'page', 'element', 'laydate'], function () {
        var table = layui.table, form = layui.form, $ = layui.jquery, jmdf = layui.jmdf, page = layui.page, element = layui.element, laydate = layui.laydate;
        //监听Tab切换，以改变地址hash值
        element.on('tab(tab)', function () {
            var type = this.getAttribute('lay-type');
            $("#type").val(type);
            page.search("searchForm", tableRenderId);
        });
        laydate.render({
            elem: '#date_ge'
            , type: 'datetime'
        });
        laydate.render({
            elem: '#date_le'
            , type: 'datetime'
        });
        var tableRenderId = "renderId";
        var tableId = "dataTable";
        var table_layFilter = "layFilter";
        var tableUrl = "loadData";

        var cols = [[
            {type: "numbers", title: "序号", fixed: true, toolbar: '#indexTpl', align: 'center', width: 60}
            , {field: "createDate", title: "操作时间", align: 'center', width: 200}
            , {field: "ip", title: "ip地址", align: 'left', width: 120}
            , {field: "username", title: "操作者", align: 'left', width: 100}
            , {field: "operation", title: "用户行为", align: 'left', width: 160}
            , {field: "classMethod", title: "实体方法", align: 'left', minWidth: 500}
            , {field: "url", title: "url", align: 'left', minWidth: 220}
            , {title: '参数', fixed: 'right', align: 'center', toolbar: '#dataTable_toolbar', width: 100}
        ]];
        //table设置项
        var options = {
            elem: '#' + tableId//table列表id值
            , even: false
            , url: tableUrl
            , toolbar: '#toolbar'//自定义工具栏
            , cols: cols
            , id: tableRenderId
            ,height:'full-160'
            , defaultToolbar: ['filter', 'print']//显示列、打印
            , autoSort: false //禁用前端自动排序。
        };
        //初始表格（含分页设置）
        page.render(options);
        //表格排序
        page.sort(tableId, tableRenderId);
        //刷新当前页方法封装
        page.reload();

        //分页查询（查询条件）
        $("#searchButton").click(function () {
            page.search("searchForm", tableRenderId);
        });
        //table表格操作项
        table.on("tool(" + table_layFilter + ")", function (obj) {
            var data = obj.data;///获得当前行数据
            var layEvent = obj.event;//获得lay-event对应的值
            var id = data["id"];
            if (layEvent == "dataTable_view") {
                layer.msg(data.uri, {
                    time: 120000, //120s后自动关闭
                    btn: ['知道了']
                });
            }
        });
        //table表格头工具栏事件
        table.on('toolbar(' + table_layFilter + ')', function (obj) {
            switch (obj.event) {
                case 'load':
                    loadData();
                    break;
            }
        });
    });
</script>
</body>
</html>