<!DOCTYPE html>
<html>
<head>
    <title>${(dataDictMap['系统名称'].value)!}</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
    <link rel="stylesheet" href="/style/defaultTheme.css" media="all">
</head>
<body class="layui-layout-body">
<div id="LAY_app"<#-- class="layadmin-side-shrink"-->>
    <div class="layui-layout layui-layout-admin">
        <div class="layui-header">
            <!-- 头部区域 -->
            <ul class="layui-nav layui-layout-left">
                <li class="layui-nav-item layadmin-flexible" lay-unselect>
                    <a href="javascript:;" layadmin-event="flexible" title="侧边伸缩">
                        <i class="layui-icon layui-icon-shrink-right <#--layui-icon-spread-left-->" id="LAY_app_flexible"></i>
                    </a>
                </li>
                <li class="layui-nav-item" lay-unselect>
                    <a href="javascript:;" layadmin-event="refresh" title="刷新">
                        <i class="layui-icon layui-icon-refresh-3"></i>
                    </a>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;" style="font-size: 25px;font-weight: bold;margin-left: 0px;">
                    ${(dataDictMap['系统名称'].value)!}
                    </a>
                </li>
            <#--<li class="layui-nav-item layui-hide-xs" lay-unselect>-->
            <#--<a href="/main/console.html" target="_blank" title="主界面">-->
            <#--<i class="layui-icon layui-icon-website"></i>-->
            <#--</a>-->
            <#--</li>-->
            </ul>
            <ul class="layui-nav layui-layout-right" lay-filter="layadmin-layout-right" style="margin-right: 20px;">
            <#--<li class="layui-nav-item" lay-unselect>-->
            <#--<a lay-href="app/message/index.html" layadmin-event="message" lay-text="消息中心">-->
            <#--<i class="layui-icon layui-icon-notice"></i>-->
            <#--<!-- 如果有新消息，则显示小圆点 &ndash;&gt;-->
            <#--<span class="layui-badge-dot"></span>-->
            <#--</a>-->
            <#--</li>-->
            <#--<li class="layui-nav-item layui-hide-xs" lay-unselect>-->
            <#--<a href="javascript:;" layadmin-event="theme">-->
            <#--<i class="layui-icon layui-icon-theme"></i>-->
            <#--</a>-->
            <#--</li>-->
                <li class="layui-nav-item layui-hide-xs" lay-unselect>
                    <a href="javascript:;" onclick="lockScreen()" title="锁屏">
                        <i class="layui-icon">&#xe673;</i>
                    </a>
                </li>
                <li class="layui-nav-item layui-hide-xs" lay-unselect>
                    <a href="javascript:;" layadmin-event="note" title="便签">
                        <i class="layui-icon layui-icon-note"></i>
                    </a>
                </li>
                <li class="layui-nav-item layui-hide-xs" lay-unselect>
                    <a href="javascript:;" layadmin-event="fullscreen" title="全屏">
                        <i class="layui-icon layui-icon-screen-full"></i>
                    </a>
                </li>
                <li class="layui-nav-item" lay-unselect>
                    <a href="javascript:;">
                        <i class="layui-icon  layui-icon-username"></i>
                    <#--<i class="layui-icon layui-icon-username"></i>&nbsp;<cite>${(userInfo.name)!}</cite>-->
                    </a>
                    <dl class="layui-nav-child">
                        <dd><a lay-href="/main/baseInfo">基本资料</a></dd>
                    <#--<dd><a lay-href="set/user/password.html">修改密码</a></dd>-->
                        <hr>
                        <dd style="text-align: center;"><a href="/login/logout">退出</a></dd>
                    </dl>
                </li>
            </ul>
        </div>
        <!-- 侧边菜单 -->
        <div class="layui-side layui-side-menu">
            <div class="layui-side-scroll">
            <#--<div class="layui-logo">-->
            <#--<span>您好！<span class="userName">${(userInfo.name)!}</span>，欢迎登录</p></span>-->
            <#--</div>-->
                <div class="user-photo">
                    <a class="img" title="我的头像"><img src="${(userInfo.pic)!}" class="layui-circle" id="myLogo"
                                                     onerror="this.src='/images/系统图标.png';this.onerror = null;"></a>
                    <p>您好！<span class="userName">${(userInfo.name)!}</span>，欢迎登录</p>
                </div>
                <ul class="layui-nav layui-nav-tree" lay-shrink="all" id="LAY-system-side-menu"
                    lay-filter="layadmin-system-side-menu">
                <#--<li data-name="home" class="layui-nav-item layui-this">-->
                <#--<a href="javascript:;" lay-href="console" lay-tips="主页" lay-direction="2">-->
                <#--<i class="layui-icon layui-icon-home"></i>-->
                <#--<cite>主页</cite>-->
                <#--</a>-->
                <#--</li>-->
                <#if (sysPermissionList?? && sysPermissionList?size > 0)>
                    <#list sysPermissionList as p>
                        <#if (p.resourceType == 'menu' && p.enabled == 0)>
                            <li class="layui-nav-item<#if p_index == 0> layui-nav-itemed</#if>">
                                <a href="javascript:;" lay-tips="${p.name}" lay-direction="2">
                                    <i class="layui-icon ${(p.icon)?html}"></i>
                                    <cite>${p.name}</cite>
                                </a>
                                <#list p.sysPermissionList as item>
                                    <#if (item.resourceType == 'menu' && item.enabled == 0)>
                                        <dl class="layui-nav-child">
                                            <dd><a lay-href="${item.url}"
                                                        <#if item.name = '电子选房入口'>
                                                            onclick="eventShrink()"
                                                        </#if>
                                                ><i class="layui-icon ${(item.icon?default('layui-icon-right'))?html}"></i>${item.name}
                                            </a></dd>
                                        </dl>
                                    </#if>
                                </#list>
                            </li>
                        </#if>
                    </#list>
                </#if>
                </ul>
            </div>
        </div>
        <!-- 页面标签 -->
        <div class="layadmin-pagetabs" id="LAY_app_tabs">
            <div class="layui-icon layadmin-tabs-control layui-icon-prev" layadmin-event="leftPage"></div>
            <div class="layui-icon layadmin-tabs-control layui-icon-next" layadmin-event="rightPage"></div>
            <div class="layui-icon layadmin-tabs-control layui-icon-down">
                <ul class="layui-nav layadmin-tabs-select" lay-filter="layadmin-pagetabs-nav">
                    <li class="layui-nav-item" lay-unselect>
                        <a href="javascript:;"></a>
                        <dl class="layui-nav-child layui-anim-fadein">
                            <dd layadmin-event="closeThisTabs"><a href="javascript:;">关闭当前标签页</a></dd>
                            <dd layadmin-event="closeOtherTabs"><a href="javascript:;">关闭其它标签页</a></dd>
                            <dd layadmin-event="closeAllTabs"><a href="javascript:;">关闭全部标签页</a></dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <div class="layui-tab layui-tab-brief" lay-unauto lay-allowClose="true" lay-filter="layadmin-layout-tabs">
                <ul class="layui-tab-title" id="LAY_app_tabsheader">
                    <li class="layui-this"><i class="layui-icon layui-icon-home"></i> 首页</li>
                </ul>
            </div>
        </div>
        <!-- 主体内容 -->
        <div class="layui-body" id="LAY_app_body">
            <div class="layadmin-tabsbody-item layui-show">
                <iframe src="/main/console" frameborder="0" class="layadmin-iframe"></iframe>
            </div>
        </div>
        <!-- 辅助元素，一般用于移动设备下遮罩 -->
        <div class="layadmin-body-shade" layadmin-event="shade"></div>
    </div>
    <object id="mTokenPlugin" type="application/x-mtokenplugin" width="0" height="0">
        <param value="pluginLoaded"/>
    </object>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script src="/plugins/ukey/mToken_K1.js" type="text/javascript"></script>
<script src="/plugins/ukey/mToken_K1Plugin.js" type="text/javascript"></script>
<script src="/plugins/ukey/base64.js" type="text/javascript"></script>
<script>
    //JavaScript代码区域
    layui.config({
        base: '/' //静态资源所在路径
        , version: true
    }).extend({
        index: 'lib/index' //主入口模块
        , jmdf: 'jmdf' //主入口模块
    }).use(['index', 'jmdf', 'jquery'], function () {
        var ts = 5000;
        var jmdf = layui.jmdf,$ = layui.jquery;

        function loadUser() {
            K1mToken.LoadLibrary();
            var keyNumber = 0;
            keyNumber = K1mToken.K1_mTokenFindDevice();
            if (keyNumber < 1) {
                layer.alert("检测到您未插入钥匙盘，系统将要退出！",{ icon: 1, closeBtn: 0 },function () {
                    top.location.href = "../login/logout";
                });
            }

        }

        $(function () {
        <#if keyUsed>
            if ("${(departmentName)!}" != "管理部门") {
                setInterval(function () {
                    loadUser();
                }, ts);
            }
        </#if>
        });


        function validateIsLogin() {
            $.ajax({
                type: "get"
                , url: "/login/validateIsLogin"
                , success: function (result) {
                    if (result == "login") {//已登录，无需其他操作
                    } else {//未登录
                        clearInterval(timer);
                        jmdf.login();
                    }
                }, error: function () {
                    console.log("========error")
                }
            });
        }

        validateIsLogin();
        var timer = setInterval(function () {
            validateIsLogin();
        }, ts);

        function reStart() {
            timer = setInterval(function () {
                validateIsLogin();
            }, ts);
        }

        window.reStart = reStart;
    });
</script>
</body>
</html>