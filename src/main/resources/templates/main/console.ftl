<!DOCTYPE html>
<html>
<head>
    <title>系统桌面</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://wdxlh-img.xcls.com.cn/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
    <link rel="stylesheet" href="/style/jmdf.css" media="all">
    <link rel="stylesheet" href="/modules/formSelects/formSelects-v4.css" media="all">
    <#setting number_format="#0.####">
    <style>
        .titleMsg {
            font-size: 18px;
            font-weight: bolder;
        }
        .quickBtn{
            width: 160px;
        }
    </style>
</head>
<body>
<div class="layui-fluid">
</div>
<script src="https://wdxlh-img.xcls.com.cn/layui/layui.js" charset="utf-8"></script>
<script src="https://wdxlh-img.xcls.com.cn/plugins/media/echarts.min.js"></script>
<script>
    layui.config({
        jmdf: '/modules/jmdf',
        base: '/modules/' //静态资源所在路径
    }).use(['table', 'form', 'jquery', 'jmdf', 'page', 'dataDict', "laydate", 'upload', 'element'], function () {
        var $ = layui.jquery, jmdf = layui.jmdf, page = layui.page, table = layui.table, laydate = layui.laydate,
            form = layui.form, dataDict = layui.dataDict, upload = layui.upload, element = layui.element;
    });
</script>
</body>
</html>