<!DOCTYPE html>
<html>
<head>
    <title>用户基本信息</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
    <link rel="stylesheet" href="/modules/formSelects/formSelects-v4.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row  layui-col-space10">
        <div class="layui-col-xs12">
            <div class="layui-card">
                <div class="layui-tab layui-tab-brief">
                    <ul class="layui-tab-title">
                        <li class="layui-this">用户基本信息</li>
                        <li>修改密码</li>
                    </ul>
                    <div class="layui-tab-content">
                        <div class="layui-tab-item layui-show">
                            <form class="layui-form" id="baseInfoForm" action="">
                                <input type="hidden" name="id" value="${(userInfo.id)!}"/>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">用户名：</label>
                                    <div class="layui-form-mid">${(userInfo.username)!}</div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">真实姓名：</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="name" id="name" lay-verify="required" value="${(userInfo.name)!}" autocomplete="off" placeholder="请输入真实姓名" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">用户头像：</label>
                                    <div class="layui-input-inline">
                                        <input type="hidden" name="pic" id="pic" value="${(userInfo.pic)!}" class="layui-input">
                                        <div class="layui-upload-list" style="margin:0">
                                            <img src="${(userInfo.pic)!}" id="srcimgurl" class="layui-upload-img">
                                        </div>
                                    </div>
                                    <div class="layui-input-inline">
                                        <button type="button" class="layui-btn layui-btn-danger" id="editPic">修改图片</button>
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">联系电话：</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="telPhone" id="telPhone" value="${(userInfo.telPhone)!}" autocomplete="off" placeholder="" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">描述：</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="description" id="description" value="${(userInfo.description)!}" autocomplete="off" placeholder="" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <div class="layui-input-block">
                                        <button class="layui-btn" id="submitBtn" name="submitBtn" lay-submit="" lay-filter="submitBaseInfo">立即提交</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="layui-tab-item">
                            <form class="layui-form" id="pwdForm" action="">
                                <input type="hidden" name="id" value="${(userInfo.id)!}"/>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">用户名：</label>
                                    <div class="layui-form-mid">${(userInfo.username)!}</div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">原始密码：</label>
                                    <div class="layui-input-block">
                                        <input type="password" name="oldPwd" id="oldPwd" lay-verify="required" value="" autocomplete="off" placeholder="请输入原始密码" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">新密码：</label>
                                    <div class="layui-input-block">
                                        <input type="password" name="newPwd" id="newPwd" lay-verify="required" value="" autocomplete="off" placeholder="请输入新密码" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">确认密码：</label>
                                    <div class="layui-input-block">
                                        <input type="password" name="confirmPwd" id="confirmPwd" lay-verify="required|confirmPwd" value="" autocomplete="off" placeholder="再次输入" class="layui-input">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <div class="layui-input-block">
                                        <button class="layui-btn" id="submitPwdBtn" name="submitBtn" lay-submit="" lay-filter="submitPwd">立即提交</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.config({version: true}).extend({
        croppers: '/modules/cropper/croppers' //主入口模块
    }).use(['form', 'jquery', 'croppers', 'element'], function () {
        var form = layui.form;
        var element = layui.element;
        var $ = layui.jquery;
        var croppers = layui.croppers;

        form.on('submit(submitBaseInfo)', function (data) {
            $.ajax({
                url: "saveBaseInfo"
                , type: "post"
                , data: $("#baseInfoForm").serializeArray()
                , success: function (data) {
                    if (data.code == "0001") {
                        layer.msg(data.message, {anim: 0, icon: 1});
                    } else {
                        layer.msg(data.message, {anim: 6, icon: 2});
                    }
                }
            });
            return false;
        });
        //创建一个头像上传组件
        croppers.render({
            elem: '#editPic'
            , saveW: 200     //保存宽度
            , saveH: 200
            , mark: 1 / 1    //选取比例
            , area: '900px'  //弹窗宽度
            , url: "/upload/uploadPic"  //图片上传接口返回和（layui 的upload 模块）返回的JOSN一样
            , done: function (data) { //上传完毕回调
                var staticAccessPath = data.staticAccessPath;
                var uploadPath = data.uploadPath;
                var url = staticAccessPath + uploadPath;
                $("#pic").val(url);
                $("#srcimgurl").attr('src', url);
            }
        });
        form.verify({
            confirmPwd: function (value, item) {
                var newPwd = $("#newPwd").val();
                if (newPwd != value) {
                    return "两次密码输入不一致";
                }
            }
        });
        form.on('submit(submitPwd)', function (data) {
            $.ajax({
                url: "savePwd"
                , type: "post"
                , data: $("#pwdForm").serializeArray()
                , success: function (data) {
                    if (data.code == "0001") {
                        layer.msg(data.message, {anim: 0, icon: 1, time: 1000}, function () {
                            top.location.href = "/login/logout";
                        });
                    } else {
                        layer.msg(data.message, {anim: 6, icon: 2});
                    }
                }
            });
            return false;
        });
    });
</script>
</body>
</html>