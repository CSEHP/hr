<!DOCTYPE html>
<html>
<head>
    <title>预约搬家信息登记</title>
    <#include "../common/common.ftl"/>
    <#setting number_format="#.##">
</head>
<body>
<div class="layui-fluid">
    <form class="layui-form layui-form-pane" id="familyForm">
        <input type="hidden" name="type" value="cp"/>
        <div class="layui-col-md6">
            <blockquote class="layui-elem-quote layui-bg-white">家庭信息<span
                        class="jmdf-red-notice">【保存前：${(operation.createTime?string('yyyy年MM月dd日 HH:mm:ss'))!}  操作人：${(operation.name)!}】</span>
            </blockquote>
        <#if operation.businessType==1>
        <div class="layui-card">
            <div class="layui-card-header">一、原历史数据</div>
            <div class="layui-card-body">
                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        申请人姓名：${(workerInfoLog.name)!}
                    </div>
                    <div class="layui-col-xs6">
                        申请人身份证号：${(workerInfoLog.idCard)!}
                    </div>
                </div>

                <div class="layui-form-item" style="line-height: 40px;" >
                    <div class="layui-col-xs6">
                        建立公积金前工龄：${(workerInfoLog.workedYears)!}
                    </div>
                    <div class="layui-col-xs6">
                        参加工作时间：
                        ${(workerInfoLog.startWorkingDate?string("yyyy-MM-dd"))!}
                    </div>
                </div>

                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        调入本单位时间：
                        ${(workerInfoLog.joinWorkingDate?string("yyyy-MM-dd"))!}
                    </div>
                    <div class="layui-col-xs6">
                        退休时间：
                        ${(workerInfoLog.retireDate?string("yyyy-MM-dd"))!}
                    </div>
                </div>
                <div class="layui-form-item" style="line-height: 40px;">

                    <div class="layui-col-xs6">
                        月基本工资：${(workerInfoLog.wages)!}
                    </div>
                    <div class="layui-col-xs6">
                        住房地址:${(workerInfoLog.livingAddress)!}
                    </div>
                </div>
                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        配偶姓名：${(workerInfoLog.spouseName)!}
                    </div>
                    <div class="layui-col-xs6">
                        配偶身份证号：${(workerInfoLog.spouseIdCard)!}
                    </div>
                </div>
                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        配偶工作单位：
                    </div>
                    <div class="layui-col-xs6">
                        备注：${(workerInfoLog.memo)!}
                    </div>
                </div>
            </div>
        </div>
</#if>
        <#if operation.businessType==2>
            <div class="layui-card">
                <div class="layui-card-header">一、原历史数据</div>
                <div class="layui-card-body">
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            姓名：${(noHouseWorkerApplyLog.name)!}
                            <#--                            <#if noHouseWorkerApply.isPayingSubsidies=="1" >是</#if>-->
                            <#--                            <#if noHouseWorkerApply.isPayingSubsidies=="2" >否</#if>-->
                        </div>
                        <div class="layui-col-xs6">
                            原补贴系数：${(noHouseWorkerApplyLog.idCard)!}
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            是否正在发放住房补贴（1：是 2:否）：${(noHouseWorkerApplyLog.isPayingSubsidies)!}
                        </div>
                        <div class="layui-col-xs6">
                            原补贴系数：${(noHouseWorkerApplyLog.lastSubsidyCoefficient)!}
                        </div>
                    </div>

                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            现补贴系数：${(noHouseWorkerApplyLog.subsidyCoefficient)!}
                        </div>
                        <div class="layui-col-xs6">
                            补贴总额：${(noHouseWorkerApplyLog.totalSubsidy)!}
                        </div>
                    </div>

                </div>

            </div>
</#if>
        <#if operation.businessType==3>
            <div class="layui-card">
                <div class="layui-card-header">一、原来历史数据</div>
                <div class="layui-card-body">
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            申请人姓名：${(noStandardWorkerApplyLog.name)!}
                        </div>
                        <div class="layui-col-xs6">
                            申请人身份证号：${(noStandardWorkerApplyLog.idCard)!}
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            联系方式   ：${(noStandardWorkerApplyLog.phone)!}
                        </div>
                        <div class="layui-col-xs6">
                            退休时职务职称：${(noStandardWorkerApplyLog.retirePosition.name)!}
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            原职务职称   ：${(noStandardWorkerApplyLog.lastPosition.name)!}
                        </div>
                        <div class="layui-col-xs6">
                            现职务职称：${(noStandardWorkerApplyLog.position.name)!}
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            情况类型   ：
                            <#if noStandardWorkerApply.jobStatus==1>在职</#if>
                            <#if noStandardWorkerApply.jobStatus==2>离退休</#if>
                            <#if noStandardWorkerApply.jobStatus==3>其他</#if>
                        </div>
                        <div class="layui-col-xs6">
                            职工类型：
                            <#if noStandardWorkerApply.jobStatus==1>未达标职工</#if>
                            <#if noStandardWorkerApply.jobStatus==2>新增未达标职工</#if>
                            <#if noStandardWorkerApply.jobStatus==3>级差职工</#if>
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;" >
                        <div class="layui-col-xs6">
                            建立公积金前工龄：${(noStandardWorkerApplyLog.workedYears)!}
                        </div>
                        <div class="layui-col-xs6">
                            参加工作时间：${(noStandardWorkerApplyLog.startWorkingDate?string("yyyy-MM-dd"))!}
                        </div>
                    </div>

                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            调入本单位时间：${(noStandardWorkerApplyLog.joinWorkingDate?string("yyyy-MM-dd"))!}
                        </div>
                        <div class="layui-col-xs6">
                            退休时间：${(noStandardWorkerApplyLog.retireDate?string("yyyy-MM-dd"))!}
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            配偶姓名：${(noStandardWorkerApplyLog.spouseName)!}
                        </div>
                        <div class="layui-col-xs6">
                            配偶身份证号：${(noStandardWorkerApplyLog.spouseIdCard)!}
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            配偶工作单位:${(noStandardWorkerApplyLog.spouseUnit)!}
                        </div>
                        <div class="layui-col-xs6">
                            住房地址:${(noStandardWorkernoStandardWorkerApplyLogApply.livingAddress)!}
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            申请人购房时单位:${(noStandardWorkerApplyLog.boughtUnit)!}
                        </div>
                        <div class="layui-col-xs6">
                            申请人购房时补贴额:${(noStandardWorkerApplyLog.boughtUnitSubsidy)!}
                        </div>
                    </div>

                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            军队转业干部时部队:${(noStandardWorkerApplyLog.boughtTroops)!}
                        </div>
                        <div class="layui-col-xs6">
                            军队转业干部时补贴额:${(noStandardWorkerApplyLog.boughtTroopsSubsidy)!}
                        </div>
                    </div>

                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            是否享受过含有政府补贴性质的住房（1：未享受 2：已享受）:
                            ${(noStandardWorkerApplyLog.isEnjoyed)!}
<#--                            <#if noStandardWorkerApplyLog.isEnjoyed==''>-->
<#--                            <#elseif noStandardWorkerApplyLog.isEnjoyed==1>未享受-->
<#--                            <#elseif noStandardWorkerApplyLog.isEnjoyed==2>已享受</#if>-->
                        </div>
                        <div class="layui-col-xs6">
                            享受过含有政府补贴性质的住房面积:${(noStandardWorkerApplyLog.enjoyedBuildArea)!}
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            住房补贴建筑面积标准:${(noStandardWorkerApplyLog.buildAreaStandard)!}
                        </div>
                        <div class="layui-col-xs6">
                            核定后住房总面积:${(noStandardWorkerApplyLog.buildAreaSureStandard)!}
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            差额面积:${(noStandardWorkerApplyLog.differenceBuildArea)!}
                        </div>
                        <div class="layui-col-xs6">
                            差额补贴额:${(noStandardWorkerApplyLog.differenceSubsidy)!}
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            补贴总额:${(noStandardWorkerApplyLog.totalSubsidy)!}
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            备注：${(noStandardWorkerApplyLog.memo)!}
                        </div>
                    </div>

                </div>
            </div>
</#if>
        <#if operation.businessType==4>
            <div class="layui-card">
        <div class="layui-card-header">一、原历史数据</div>
        <div class="layui-card-body">
            <div class="layui-form-item" style="line-height: 40px;">
                <div class="layui-col-xs6">
                    姓名：${(wageLog.name)!}
                </div>
                <div class="layui-col-xs6">
                    身份证号：${(wageLog.idCard)!}
                </div>
            </div>
            <div class="layui-form-item" style="line-height: 40px;">
                <div class="layui-col-xs6">
                    年度：${(wageLog.year)!}
                </div>
                <div class="layui-col-xs6">
                    月份：${(wageLog.month)!}
                </div>
            </div>
            <div class="layui-form-item" style="line-height: 40px;">
                <div class="layui-col-xs6">
                    年度：${(wageLog.money)!}
                </div>
            </div>
        </div>
        </div>
        </#if>
        </div>

        <div class="layui-col-md6" style="padding-left: 10px">
            <blockquote class="layui-elem-quote layui-bg-white">家庭信息<span
                        class="jmdf-red-notice">【保存前：${(operation.createTime?string('yyyy年MM月dd日 HH:mm:ss'))!}  操作人：${(operation.name)!}】</span>
            </blockquote>
        <#if operation.businessType==1>
        <div class="layui-card">
            <div class="layui-card-header">一、现数据</div>
            <div class="layui-card-body">
                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        申请人姓名：${(workerInfo.name)!}
                        <span class="contrast"
                        before="${(workerInfo.name)!}"
                        after="${(workerInfoLog.name)!}"></span>
                    </div>
                    <div class="layui-col-xs6">
                        申请人身份证号：${(workerInfo.idCard)!}
                        <span class="contrast"
                                before="${(workerInfo.idCard)!}"
                                after="${(workerInfoLog.idCard)!}"></span>
                    </div>
                </div>

                <div class="layui-form-item" style="line-height: 40px;" >
                    <div class="layui-col-xs6">
                        建立公积金前工龄：${(workerInfo.workedYears)!}
                        <span class="contrast"
                                before="${(workerInfo.workedYears)!}"
                                after="${(workerInfoLog.workedYears)!}"></span>
                    </div>
                    <div class="layui-col-xs6">
                        参加工作时间：
<#--                        ${(workerInfo.startWorkingDate?string("yyyy-MM-dd"))!}-->
<#--                        <span class="contrast"-->
<#--                                before="${(workerInfo.startWorkingDate?string("yyyy-MM-dd"))}"-->
<#--                                after="${(workerInfoLog.startWorkingDate?string("yyyy-MM-dd"))!}"></span>-->
                    </div>
                </div>

                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        调入本单位时间：
                        ${(workerInfo.joinWorkingDate?string("yyyy-MM-dd"))!}
                        <span class="contrast"
                                before="${(workerInfo.joinWorkingDate?string("yyyy-MM-dd"))!}"
                                after="${(workerInfoLog.joinWorkingDate?string("yyyy-MM-dd"))!}"></span>
                    </div>
                    <div class="layui-col-xs6">
                        退休时间：
                        ${(workerInfo.retireDate?string("yyyy-MM-dd"))!}
                        <span class="contrast"
                                before="${(workerInfo.retireDate?string("yyyy-MM-dd"))!}"
                                after="${(workerInfoLog.retireDate?string("yyyy-MM-dd"))!}"></span>
                    </div>
                </div>
                <div class="layui-form-item" style="line-height: 40px;">

                    <div class="layui-col-xs6">
                        月基本工资：${(workerInfo.wages)!}
                        <span class="contrast"
                                before="${(workerInfo.wages)!}"
                                after="${(workerInfoLog.wages)!}"></span>
                    </div>
                    <div class="layui-col-xs6">
                        住房地址:${(workerInfo.livingAddress)!}
                        <span class="contrast"
                                before="${(workerInfo.livingAddress)!}"
                                after="${(workerInfoLog.livingAddress)!}"></span>
                    </div>
                </div>
                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        配偶姓名：${(workerInfo.spouseName)!}
                        <span class="contrast"
                                before="${(workerInfo.spouseName)!}"
                                after="${(workerInfoLog.spouseName)!}"></span>
                    </div>
                    <div class="layui-col-xs6">
                        配偶身份证号：${(workerInfo.spouseIdCard)!}
                        <span class="contrast"
                                before="${(workerInfo.spouseIdCard)!}"
                                after="${(workerInfoLog.spouseIdCard)!}"></span>
                    </div>
                </div>
                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        配偶工作单位：${(workerInfo.spouseUnit)!}
                        <span class="contrast"
                                before="${(workerInfo.spouseUnit)!}"
                                after="${(workerInfoLog.spouseUnit)!}"></span>
                    </div>
                    <div class="layui-col-xs6">
                        备注：${(workerInfo.memo)!}
                        <span class="contrast"
                                before="${(workerInfo.memo)!}"
                                after="${(workerInfoLog.memo)!}"></span>
                    </div>
                </div>
            </div>
        </div>
</#if>
        <#if operation.businessType==2>
            <div class="layui-card">
                <div class="layui-card-header">二、现数据</div>
                <div class="layui-card-body">
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            姓名：${(noHouseWorkerApply.name)!}
                            <span class="contrast"
                                  before="${(noHouseWorkerApply.name)!}"
                                  after="${(noHouseWorkerApplyLog.name)!}"></span>
                        </div>
                        <div class="layui-col-xs6">
                            原补贴系数：${(noHouseWorkerApply.idCard)!}
                            <span class="contrast"
                                  before="${(noHouseWorkerApply.idCard)!}"
                                  after="${(noHouseWorkerApplyLog.idCard)!}"></span>
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            是否正在发放住房补贴：
                            <#if noHouseWorkerApply.isPayingSubsidies==1 >是</#if>
                            <#if noHouseWorkerApply.isPayingSubsidies==2 >否</#if>
                            <span class="contrast"
                                  before="${(noHouseWorkerApply.isPayingSubsidies)!}"
                                  after="${(noHouseWorkerApplyLog.isPayingSubsidies)!}"></span>
                        </div>
                        <div class="layui-col-xs6">
                            原补贴系数：${(noHouseWorkerApply.lastSubsidyCoefficient)!}
                            <span class="contrast"
                                  before="${(noHouseWorkerApply.lastSubsidyCoefficient)!}"
                                  after="${(noHouseWorkerApplyLog.lastSubsidyCoefficient)!}"></span>
                        </div>
                    </div>

                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            现补贴系数：${(noHouseWorkerApply.subsidyCoefficient)!}
                            <span class="contrast"
                                  before="${(noHouseWorkerApply.subsidyCoefficient)!}"
                                  after="${(noHouseWorkerApplyLog.subsidyCoefficient)!}"></span>
                        </div>
                        <div class="layui-col-xs6">
                            补贴总额：${(noHouseWorkerApply.totalSubsidy)!}
                            <span class="contrast"
                                  before="${(noHouseWorkerApply.totalSubsidy)!}"
                                  after="${(noHouseWorkerApplyLog.totalSubsidy)!}"></span>
                        </div>
                    </div>

                </div>
            </div>
</#if>
        <#if operation.businessType==3>
            <div class="layui-card">
                <div class="layui-card-header">一、现数据</div>
                <div class="layui-card-body">
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            申请人姓名：${(noStandardWorkerApply.name)!}
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.name)!}"
                                  after="${(noStandardWorkerApplyLog.name)!}"></span>
                        </div>
                        <div class="layui-col-xs6">
                            申请人身份证号：${(noStandardWorkerApply.idCard)!}
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.idCard)!}"
                                  after="${(noStandardWorkerApplyLog.idCard)!}"></span>
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            联系方式   ：${(noStandardWorkerApply.phone)!}
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.phone)!}"
                                  after="${(noStandardWorkerApplyLog.phone)!}"></span>
                        </div>
                        <div class="layui-col-xs6">
                            退休时职务职称：${(noStandardWorkerApply.retirePosition.name)!}
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            原职务职称   ：${(noStandardWorkerApply.lastPosition.name)!}
                        </div>
                        <div class="layui-col-xs6">
                            现职务职称：${(noStandardWorkerApply.position.name)!}
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            情况类型   ：
                            <#if noStandardWorkerApply.jobStatus==1>在职</#if>
                            <#if noStandardWorkerApply.jobStatus==2>离退休</#if>
                            <#if noStandardWorkerApply.jobStatus==3>其他</#if>
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.jobStatus)!}"
                                  after="${(noStandardWorkerApplyLog.jobStatus)!}"></span>
                        </div>
                        <div class="layui-col-xs6">
                            职工类型：
                            <#if noStandardWorkerApply.workerType==1>未达标职工</#if>
                            <#if noStandardWorkerApply.workerType==2>新增未达标职工</#if>
                            <#if noStandardWorkerApply.jobStatus==3>级差职工</#if>
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.workerType)!}"
                                  after="${(noStandardWorkerApplyLog.workerType)!}"></span>
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;" >
                        <div class="layui-col-xs6">
                            建立公积金前工龄：${(noStandardWorkerApply.workedYears)!}
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.workedYears)!}"
                                  after="${(noStandardWorkerApplyLog.workedYears)!}"></span>
                        </div>
                        <div class="layui-col-xs6">
                            参加工作时间：${(noStandardWorkerApply.startWorkingDate?string("yyyy-MM-dd"))!}
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.startWorkingDate?string("yyyy-MM-dd"))!}"
                                  after="${(noStandardWorkerApplyLog.startWorkingDate?string("yyyy-MM-dd"))!}"></span>
                        </div>
                    </div>

                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            调入本单位时间：${(noStandardWorkerApply.joinWorkingDate?string("yyyy-MM-dd"))!}
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.joinWorkingDate?string("yyyy-MM-dd"))!}"
                                  after="${(noStandardWorkerApplyLog.joinWorkingDate?string("yyyy-MM-dd"))!}"></span>
                        </div>
                        <div class="layui-col-xs6">
                            退休时间：${(noStandardWorkerApply.retireDate?string("yyyy-MM-dd"))!}
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.retireDate?string("yyyy-MM-dd"))!}"
                                  after="${(noStandardWorkerApplyLog.retireDate?string("yyyy-MM-dd"))!}"></span>
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            配偶姓名：${(noStandardWorkerApply.spouseName)!}
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.spouseName)!}"
                                  after="${(noStandardWorkerApplyLog.spouseName)!}"></span>
                        </div>
                        <div class="layui-col-xs6">
                            配偶身份证号：${(noStandardWorkerApply.spouseIdCard)!}
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.spouseIdCard)!}"
                                  after="${(noStandardWorkerApplyLog.spouseIdCard)!}"></span>
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            配偶工作单位:${(noStandardWorkerApply.spouseUnit)!}
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.spouseUnit)!}"
                                  after="${(noStandardWorkerApplyLog.spouseUnit)!}"></span>
                        </div>
                        <div class="layui-col-xs6">
                            住房地址:${(noStandardWorkerApply.livingAddress)!}
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.livingAddress)!}"
                                  after="${(noStandardWorkerApplyLog.livingAddress)!}"></span>
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            申请人购房时单位:${(noStandardWorkerApply.boughtUnit)!}
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.boughtUnit)!}"
                                  after="${(noStandardWorkerApplyLog.boughtUnit)!}"></span>
                        </div>
                        <div class="layui-col-xs6">
                            申请人购房时补贴额:${(noStandardWorkerApply.boughtUnitSubsidy)!}
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.boughtUnitSubsidy)!}"
                                  after="${(noStandardWorkerApplyLog.boughtUnitSubsidy)!}"></span>
                        </div>
                    </div>

                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            军队转业干部时部队:${(noStandardWorkerApply.boughtTroops)!}
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.boughtTroops)!}"
                                  after="${(noStandardWorkerApplyLog.boughtTroops)!}"></span>
                        </div>
                        <div class="layui-col-xs6">
                            军队转业干部时补贴额:${(noStandardWorkerApply.boughtTroopsSubsidy)!}
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.boughtTroopsSubsidy)!}"
                                  after="${(noStandardWorkerApplyLog.boughtTroopsSubsidy)!}"></span>
                        </div>
                    </div>

                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            是否享受过含有政府补贴性质的住房:
                            <#if noStandardWorkerApply.isEnjoyed==1>未享受</#if>
                            <#if noStandardWorkerApply.isEnjoyed==2>已享受</#if>
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.isEnjoyed)!}"
                                  after="${(noStandardWorkerApplyLog.isEnjoyed)!}"></span>
                        </div>
                        <div class="layui-col-xs6">
                            享受过含有政府补贴性质的住房面积:${(noStandardWorkerApply.enjoyedBuildArea)!}
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.enjoyedBuildArea)!}"
                                  after="${(noStandardWorkerApplyLog.enjoyedBuildArea)!}"></span>
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            住房补贴建筑面积标准:${(noStandardWorkerApply.buildAreaStandard)!}
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.buildAreaStandard)!}"
                                  after="${(noStandardWorkerApplyLog.buildAreaStandard)!}"></span>
                        </div>
                        <div class="layui-col-xs6">
                            核定后住房总面积:${(noStandardWorkerApply.buildAreaSureStandard)!}
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.buildAreaSureStandard)!}"
                                  after="${(noStandardWorkerApplyLog.buildAreaSureStandard)!}"></span>
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            差额面积:${(noStandardWorkerApply.differenceBuildArea)!}
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.differenceBuildArea)!}"
                                  after="${(noStandardWorkerApplyLog.differenceBuildArea)!}"></span>
                        </div>
                        <div class="layui-col-xs6">
                            差额补贴额:${(noStandardWorkerApply.differenceSubsidy)!}
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.differenceSubsidy)!}"
                                  after="${(noStandardWorkerApplyLog.differenceSubsidy)!}"></span>
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            补贴总额:${(noStandardWorkerApply.totalSubsidy)!}
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.totalSubsidy)!}"
                                  after="${(noStandardWorkerApplyLog.totalSubsidy)!}"></span>
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            备注：${(noStandardWorkerApply.memo)!}
                            <span class="contrast"
                                  before="${(noStandardWorkerApply.memo)!}"
                                  after="${(noStandardWorkerApplyLog.memo)!}"></span>
                        </div>
                    </div>

                </div>
            </div>
</#if>
        <#if operation.businessType==4>
            <div class="layui-card">
                <div class="layui-card-header">一、现数据</div>
                <div class="layui-card-body">
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            姓名：${(wage.name)!}
                            <span class="contrast"
                                  before="${(wage.name)!}"
                                  after="${(wageLog.name)!}"></span>
                        </div>
                        <div class="layui-col-xs6">
                            身份证号：${(wage.idCard)!}
                            <span class="contrast"
                                  before="${(wage.idCard)!}"
                                  after="${(wageLog.idCard)!}"></span>
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            年度：${(wage.year)!}
                            <span class="contrast"
                                  before="${(wage.year)!}"
                                  after="${(wageLog.year)!}"></span>
                        </div>
                        <div class="layui-col-xs6">
                            月份：${(wage.month)!}
                            <span class="contrast"
                                  before="${(wage.month)!}"
                                  after="${(wageLog.month)!}"></span>
                        </div>
                    </div>
                    <div class="layui-form-item" style="line-height: 40px;">
                        <div class="layui-col-xs6">
                            年度：${(wage.money)!}
                            <span class="contrast"
                                  before="${(wage.money)!}"
                                  after="${(wageLog.money)!}"></span>
                        </div>
                    </div>
                </div>
            </div>
        </#if>
        </div>

        <button style="display: none;" class="layui-btn" id="submitBtn" name="submitBtn" lay-submit=""
                lay-filter="submit">保存
        </button>
    </form>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.config({
        base: '/modules/' //静态资源所在路径
    }).use(['form', 'jmdf', 'dataDict', 'element', 'laydate'], function () {
        var $ = layui.jquery, jmdf = layui.jmdf, page = layui.page, table = layui.table, form = layui.form;
        initContrast();

        function initContrast() {
            $(".contrast").each(function () {
                var beforeValue = $(this).attr("before");
                var afterValue = $(this).attr("after");
                if (beforeValue != afterValue) {
                    $(this).addClass("layui-badge layui-bg-red");
                    $(this).html("有改动");
                }
            })
        }
    });
</script>
</body>
</html>