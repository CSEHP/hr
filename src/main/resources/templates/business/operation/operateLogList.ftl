<!DOCTYPE html>
<html>
<head>
    <title>无房职工信息列表</title>
    <#include "../common/common.ftl"/></head>
<body>
<div class="layui-fluid">
    <div class="layui-row layui-col-space10">
        <div class="layui-col-xs12">
            <div class="layui-card">
                <div class="layui-card-header">
                    <span class="layui-breadcrumb" lay-separator="-">
                          <a href="">操作记录管理</a>
                          <a><cite>操作记录管理列表</cite></a>
                    </span>
                </div>
                <div class="layui-card-body">
                    <form class="layui-form" id="searchForm">
                        姓名：
                        <div class="layui-inline" style="width: 120px;">
                            <input class="layui-input" name="search_like_string_content" autocomplete="off">

                        </div>
                        <div class="layui-inline">操作时间：</div>
                        <div class="layui-inline">
                            <input class="layui-input" name="createTime_ge" id="createTime_ge"
                                   autocomplete="off" style="width: 150px;">
                        </div>
                        <div class="layui-inline">-</div>
                        <div class="layui-inline">
                            <input class="layui-input" name="createTime_le" id="createTime_le"
                                   autocomplete="off" style="width: 150px;">
                        </div>
                        <button class="layui-btn" type="button" id="searchButton"><i
                                    class="layui-icon layui-icon-search"></i>查询
                        </button>

                    </form>
                    <table id="dataTable" class="layui-table" lay-filter="layFilter"></table>
                    <!--序号-->
                    <script type="text/html" id="indexTpl">
                        {{# if(d.LAY_TABLE_INDEX == undefined){ }}
                        {{d.LAY_INDEX}}
                        {{#  } }}
                        {{# if(d.LAY_TABLE_INDEX != undefined){ }}
                        {{d.LAY_TABLE_INDEX + 1}}
                        {{#  } }}
                    </script>
                    <!--菜单-->
                    <script type="text/html" id="dataTable_toolbar">
                        <a class="layui-btn layui-btn-xs jmdf-btn-view" lay-event="dataTable_view"><i
                                    class="layui-icon layui-icon-friends"></i>查看</a>
                    </script>
                    <!-- 表格头部工具栏 -->
                    <script type="text/html" id="toolbar">
                        <div class="layui-btn-container right">
                            <button lay-event="reload" class="layui-btn layui-btn-sm"><i
                                        class="layui-icon layui-icon-refresh"></i>重载
                            </button>
                        </div>
                    </script>
                    <script type="text/html" id="titleTpl">
                        <a lay-event="dataTable_view" class="layui-table-link" style="cursor: pointer;">{{d.name}}</a>
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.config({
        base: '/modules/' //静态资源所在路径
    }).extend({
        formSelects: 'formSelects/formSelects-v4' //主入口模块
    }).use(['table', 'form', 'jquery', 'jmdf', 'page', 'dataDict', 'formSelects','laydate'], function () {
        var $ = layui.jquery, jmdf = layui.jmdf, page = layui.page, table = layui.table, form = layui.form, dataDict = layui.dataDict, formSelects = layui.formSelects,laydate = layui.laydate;
        /***********************数据字典表格相关操作 start*************************/
        dataDict.select("职务名称", "position", "");
        dataDict.select("职务名称", "retirePosition", "");
        laydate.render({
            elem: '#createTime_ge'
            , type: 'date'
        });
        laydate.render({
            elem: '#createTime_le'
            , type: 'date'
        });
        /***********************table表格相关操作 start*************************/
        var tableRenderId = "renderId";
        var tableId = "dataTable";
        var table_layFilter = "layFilter";
        var tableUrl = "loadOperationList";

        //列项内容
        var cols = [[
            // {title: "全选", type: 'checkbox'}
            , {title: "序号", toolbar: '#indexTpl', align: 'center', width: 60}
            , {field: "businessType", title: "操作业务表类别", align: 'center'}
            , {field: "operateType", title: "操作类别", align: 'center'}
            , {field: "content2", title: "变动内容", align: 'center',width: 500}
            , {field: "name", title: "操作人", align: 'center', }
            , {field: "create", title: "操作时间", align: 'center'}
            , {title: '操作', fixed: 'right', align: 'center', toolbar: '#dataTable_toolbar'}
        ]];

        // table设置项
        var options = {
            elem: '#' + tableId//table列表id值
            , url: tableUrl
            , toolbar: '#toolbar'//自定义工具栏
            , cols: cols
            , id: tableRenderId
            , defaultToolbar: ['filter', 'print']//显示列、打印
            , autoSort: false //禁用前端自动排序。
        };
        //初始表格（含分页设置）
        page.render(options);
        //表格排序
        page.sort(table_layFilter, tableRenderId);

        //刷新当前页方法封装
        page.reload();
        //分页查询（查询条件）
        $("#searchButton").click(function () {
            page.search("searchForm", tableRenderId);
        });
        form.render();
        //头工具栏事件
        table.on('toolbar(' + table_layFilter + ')', function (obj) {
            switch (obj.event) {
                case 'reload':
                    loadData();
                    break;
                case 'add':
                    jmdf.form("noHouseInfo",65)
                    break;
            }
        });

        table.on("tool(" + table_layFilter + ")", function (obj) {
            var data = obj.data;///获得当前行数据
            var layEvent = obj.event;//获得lay-event对应的值
            var id = data["id"];
            if (layEvent == "dataTable_edit") {
                jmdf.form('noHouseInfo?workerId=' + id, "65")
            } else if (layEvent == "dataTable_view") {
                jmdf.view("operationView?operationId=" + id, "100");
            } else if (layEvent == "del"){
                var url = "delNoHouse?workerId=" + data.id;
                jmdf.del(url);
            }
        });

        //批量审核通过
        $('#lockCount').click(function () {
            var checkStatus = table.checkStatus(tableRenderId);
            var ids = [];
            $(checkStatus.data).each(function (i, o) {//o即为表格中一行的数据
                ids.push(o.id);
            });
            if (ids.length < 1) {
                layer.msg('请选择要审核通过的数据!');
                return false;
            }
            var length = ids.length;
            ids = ids.join(",");
            layer.confirm('确定要审核通过这' + length + '个初评数据吗？审核通过之后初评数据将不能修改！', {
                anim: 1,
                icon: 3,
                title: '信息提示'
            }, function (index) {
                var load = "";
                $.ajax({
                    url: "checkSureData",
                    data: {
                        "familyIdList": ids,
                        "type": "checkCount",
                        "checkType": "cp"
                    },
                    type: "post",
                    traditional: true,
                    beforeSend: function () {
                        //加载层
                        load = layer.load(1, {shade: [0.8, '#393D49']});
                    },
                    success: function (data) {
                        layer.close(load);
                        if (data.code == "0001") {
                            layer.msg(data.message, {anim: 0, icon: 1});
                        } else {
                            layer.msg(data.message, {anim: 6, icon: 2});
                        }
                        loadData();
                        layer.close(index);
                    }
                });
            });
        });

        //全部审核通过
        $('#lockAll').click(function () {
            layer.confirm('确定要审核通过所有的初评数据吗？审核通过之后初评数据将不能修改！', {anim: 1, icon: 3, title: '信息提示'}, function (index) {
                var load = "";
                $.ajax({
                    url: "checkSureData",
                    data: {
                        "type": "checkAll",
                        "checkType": "cp"
                    },
                    type: "post",
                    traditional: true,
                    beforeSend: function () {
                        //加载层
                        load = layer.load(1, {shade: [0.8, '#393D49']});
                    },
                    success: function (data) {
                        layer.close(load);
                        if (data.code == "0001") {
                            layer.msg(data.message, {anim: 0, icon: 1});
                        } else {
                            layer.msg(data.message, {anim: 6, icon: 2});
                        }
                        loadData();
                    }
                });
            });
        });
    });
</script>
</body>
</html>