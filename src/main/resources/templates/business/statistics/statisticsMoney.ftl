<!DOCTYPE html>
<html>
<head>
    <title>无房职工信息列表</title>
    <#include "../common/common.ftl"/></head>
<body>
<div class="layui-fluid">
    <div class="layui-row layui-col-space10">
        <div class="layui-col-xs12">
            <div class="layui-card">
                <div class="layui-card-header">
                    <span class="layui-breadcrumb" lay-separator="-">
                          <a href="">数据统计分析</a>
                          <a><cite>统计分析</cite></a>
                    </span>
                </div>


                <div>
                    <table class="layui-table" style="text-align: center">
                        <tr>
                            <td style="background-color:#F2F2F2" colspan="2">类型</td>
                            <td style="background-color:#F2F2F2">部门</td>
                            <td style="background-color:#F2F2F2">人数</td>
                            <td style="background-color:#F2F2F2">总金额(元)</td>
                        </tr>
                        <tr>
                            <td colspan="2" rowspan="5" style="width: 20px">无房职工</td>
                            <td>机关本级</td>
                            <td style="color:#1890FF">${noHouseMap['JGBJ-']['personCount']}</td>
                            <td style="color:#1890FF">${noHouseMap['JGBJ-']['money']}</td>
                        </tr>
                        <tr>
                            <td>特检所
                            </td>
                            <td style="color:#1890FF">${noHouseMap['TJS-']['personCount']}</td>
                            <td style="color:#1890FF">${noHouseMap['TJS-']['money']}</td>
                        </tr>
                        <tr>
                            <td>检测所</td>
                            <td style="color:#1890FF">${noHouseMap['JCS-']['personCount']}</td>
                            <td style="color:#1890FF">${noHouseMap['JCS-']['money']}</td>
                        </tr>
                        <tr>
                            <td>监控中心</td>
                            <td style="color:#1890FF">${noHouseMap['JKZX-']['personCount']}</td>
                            <td style="color:#1890FF">${noHouseMap['JKZX-']['money']}</td>
                        </tr>
                        <tr>
                            <td >无房职工合计</td>
                            <td style="color:#1890FF">${noHouseMap['hj']['personCount']}</td>
                            <td style="color:#1890FF">${noHouseMap['hj']['money']}</td>
                        </tr>
                        <tr>
                            <td rowspan="11">未达标职工</td>
                            <td rowspan="5">已享受过住房补贴</td>
                            <td>机关本级</td>
                            <td style="color:#1890FF">${noStandMap['JGBJ-']['personCountTwo']}</td>
                            <td style="color:#1890FF">${noStandMap['JGBJ-']['moneyTwo']}</td>
                        </tr>
                        <tr>
                            <td>特检所</td>
                            <td style="color:#1890FF">${noStandMap['TJS-']['personCountTwo']}</td>
                            <td style="color:#1890FF">${noStandMap['TJS-']['moneyTwo']}</td>
                        </tr>
                        <tr>
                            <td>计量检测所</td>
                            <td style="color:#1890FF">${noStandMap['JCS-']['personCountTwo']}</td>
                            <td style="color:#1890FF">${noStandMap['JCS-']['moneyTwo']}</td>
                        </tr>
                        <tr>
                            <td>监控中心</td>
                            <td style="color:#1890FF">${noStandMap['JKZX-']['personCountTwo']}</td>
                            <td style="color:#1890FF">${noStandMap['JKZX-']['moneyTwo']}</td>
                        </tr>
                        <tr>
                            <td>已享受过住房补贴合计</td>
                            <td style="color:#1890FF">${noStandMap['hjTwo']['personCountTwo']}</td>
                            <td style="color:#1890FF">${noStandMap['hjTwo']['moneyTwo']}</td>
                        </tr>
                        <tr>
                            <td rowspan="5">未享受过住房补贴</td>
                            <td>机关本级</td>
                            <td style="color:#1890FF">${noStandMap['JGBJ-']['personCountOne']}</td>
                            <td style="color:#1890FF">${noStandMap['JGBJ-']['moneyOne']}</td>
                        </tr>
                        <tr>
                            <td>特检所</td>
                            <td style="color:#1890FF">${noStandMap['TJS-']['personCountOne']}</td>
                            <td style="color:#1890FF">${noStandMap['TJS-']['moneyOne']}</td>
                        </tr>
                        <tr>
                            <td>计量检测所</td>
                            <td style="color:#1890FF">${noStandMap['JCS-']['personCountOne']}</td>
                            <td style="color:#1890FF">${noStandMap['JCS-']['moneyOne']}</td>
                        </tr>
                        <tr>
                            <td>监控中心</td>
                            <td style="color:#1890FF">${noStandMap['JKZX-']['personCountOne']}</td>
                            <td style="color:#1890FF">${noStandMap['JKZX-']['moneyOne']}</td>
                        </tr>
                        <tr>
                            <td >未享受过住房补贴合计</td>
                            <td style="color:#1890FF">${noStandMap['hjOne']['personCountOne']}</td>
                            <td style="color:#1890FF">${noStandMap['hjOne']['moneyOne']}</td>
                        </tr>
                        <tr>
                            <td colspan="2">未达标职工合计</td>
                            <td style="color:#1890FF">${noStandMap['hjThree']['personCountThree']}</td>
                            <td style="color:#1890FF">${noStandMap['hjThree']['moneyThree']}</td>
                        </tr>
                        <tr>
                            <td colspan="3">总合计</td>
                            <td style="color:#1890FF">${noStandMap['zhj']['count']}</td>
                            <td style="color:#1890FF">${noStandMap['zhj']['money']}</td>
                        </tr>
                    </table>

                </div>
                <div class="layui-card-body">
                    <form class="layui-form" id="searchForm">
                    </form>
                    <table id="dataTable" class="layui-table" lay-filter="layFilter"></table>
                    <!--序号-->
                    <script type="text/html" id="indexTpl">
                        {{# if(d.LAY_TABLE_INDEX == undefined){ }}
                        {{d.LAY_INDEX}}
                        {{#  } }}
                        {{# if(d.LAY_TABLE_INDEX != undefined){ }}
                        {{d.LAY_TABLE_INDEX + 1}}
                        {{#  } }}
                    </script>

                </div>
            </div>
        </div>
    </div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.config({
        base: '/modules/' //静态资源所在路径
    }).extend({
        formSelects: 'formSelects/formSelects-v4' //主入口模块
    }).use(['table', 'form', 'jquery', 'jmdf', 'page', 'dataDict', 'formSelects','laydate'], function () {
        var $ = layui.jquery, jmdf = layui.jmdf, page = layui.page, table = layui.table, form = layui.form, dataDict = layui.dataDict, formSelects = layui.formSelects,laydate = layui.laydate;
        /***********************table表格相关操作 start*************************/
        var tableRenderId = "renderId";
        var tableId = "dataTable";
        var table_layFilter = "layFilter";
        var tableUrl = "loadStatisticsMoney";

        // table设置项
        var options = {
            elem: '#' + tableId//table列表id值
            , url: tableUrl
            , toolbar: '#toolbar'//自定义工具栏
            , cols: cols
            , id: tableRenderId
            , defaultToolbar: ['filter', 'print']//显示列、打印
            , autoSort: false //禁用前端自动排序。
        };
        //初始表格（含分页设置）
        page.render(options);
        //表格排序
        page.sort(table_layFilter, tableRenderId);

        //刷新当前页方法封装
        page.reload();
        //分页查询（查询条件）
        $("#searchButton").click(function () {
            page.search("searchForm", tableRenderId);
        });
        form.render();
        //头工具栏事件
        table.on('toolbar(' + table_layFilter + ')', function (obj) {
            switch (obj.event) {
                case 'reload':
                    loadData();
                    break;
                case 'add':
                    jmdf.form("noHouseInfo",65)
                    break;
            }
        });

        table.on("tool(" + table_layFilter + ")", function (obj) {
            var data = obj.data;///获得当前行数据
            var layEvent = obj.event;//获得lay-event对应的值
            var id = data["id"];
            if (layEvent == "dataTable_edit") {
                jmdf.form('noHouseInfo?workerId=' + id, "65")
            } else if (layEvent == "dataTable_view") {
                jmdf.view("noHouseView?workerId=" + id, "65");
            } else if (layEvent == "del"){
                var url = "delNoHouse?workerId=" + data.id;
                jmdf.del(url);
            }
        });

        //批量审核通过
        $('#lockCount').click(function () {
            var checkStatus = table.checkStatus(tableRenderId);
            var ids = [];
            $(checkStatus.data).each(function (i, o) {//o即为表格中一行的数据
                ids.push(o.id);
            });
            if (ids.length < 1) {
                layer.msg('请选择要审核通过的数据!');
                return false;
            }
            var length = ids.length;
            ids = ids.join(",");
            layer.confirm('确定要审核通过这' + length + '个初评数据吗？审核通过之后初评数据将不能修改！', {
                anim: 1,
                icon: 3,
                title: '信息提示'
            }, function (index) {
                var load = "";
                $.ajax({
                    url: "checkSureData",
                    data: {
                        "familyIdList": ids,
                        "type": "checkCount",
                        "checkType": "cp"
                    },
                    type: "post",
                    traditional: true,
                    beforeSend: function () {
                        //加载层
                        load = layer.load(1, {shade: [0.8, '#393D49']});
                    },
                    success: function (data) {
                        layer.close(load);
                        if (data.code == "0001") {
                            layer.msg(data.message, {anim: 0, icon: 1});
                        } else {
                            layer.msg(data.message, {anim: 6, icon: 2});
                        }
                        loadData();
                        layer.close(index);
                    }
                });
            });
        });

        //全部审核通过
        $('#lockAll').click(function () {
            layer.confirm('确定要审核通过所有的初评数据吗？审核通过之后初评数据将不能修改！', {anim: 1, icon: 3, title: '信息提示'}, function (index) {
                var load = "";
                $.ajax({
                    url: "checkSureData",
                    data: {
                        "type": "checkAll",
                        "checkType": "cp"
                    },
                    type: "post",
                    traditional: true,
                    beforeSend: function () {
                        //加载层
                        load = layer.load(1, {shade: [0.8, '#393D49']});
                    },
                    success: function (data) {
                        layer.close(load);
                        if (data.code == "0001") {
                            layer.msg(data.message, {anim: 0, icon: 1});
                        } else {
                            layer.msg(data.message, {anim: 6, icon: 2});
                        }
                        loadData();
                    }
                });
            });
        });
    });
</script>
<style>
    .dataTable tr {
        height: 30px;
    }

    tr.change:hover {
        background-color: #E6F7FF
    }

    .dataTable {
        width: 95%;
        border-top: 2px solid #FAFAFA;
        border-left: 2px solid #FAFAFA;
        border-right: 2px solid #FAFAFA;
        border-bottom: 2px solid #FAFAFA;
        border-spacing: inherit;
        line-height: 24px;
        -fs-table-paginate: paginate; /*处理分页断行问题*/
        margin-left: 40px;
        height: 250px;
    }

    .dataTable tr td {
        text-align: center;
        padding: 2px;
        border-right: 1px solid #FAFAFA;
        border-bottom: 1px solid #FAFAFA;
        border-top: 1px solid #FAFAFA;
    }
</style>
</body>
</html>