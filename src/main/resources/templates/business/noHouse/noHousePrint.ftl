<!DOCTYPE html>
<html>
<head>
    <title>房屋交房单打印</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <blockquote class="layui-elem-quote layui-bg-white">
        <span class="layui-breadcrumb" lay-separator="-">
            <a href="">无房职工信息管理</a>
            <a href="">无房职工补贴打印</a>
            <a><cite>打印补贴确认表</cite></a>
        </span>
    </blockquote>
    <div class="layui-card">
        <div class="layui-card-header">
            【${(noHouseWorkerApply.name)!}】无房职工住房补贴确认表打印
        </div>
        <div class="layui-card-body" style="height: 800px;">
            <input class="layui-hide" name="noHouseWorkerApplyId" value="${(noHouseWorkerApply.id)!}">
            <iframe id="printIframe"
                    src="/pdf/print/noHouseApplyPdf?noHouseWorkerApplyId=${(noHouseWorkerApply.id)!}&flag=${flag}#toolbar=0"
                    style="width: 100%;height: 99%;"></iframe>
        </div>
    </div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.config({
        base: '/modules/' //静态资源所在路径
    }).use(['table', 'form', 'jquery', 'jmdf', 'page', 'dataDict', 'upload', 'element'], function () {
        var $ = layui.jquery, jmdf = layui.jmdf, page = layui.page, table = layui.table,
            form = layui.form, dataDict = layui.dataDict, upload = layui.upload, element = layui.element;
    });
</script>
</body>
</html>
