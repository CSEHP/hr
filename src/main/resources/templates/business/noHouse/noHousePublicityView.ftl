<!DOCTYPE html>
<html>
<head>
    <title>预约搬家信息登记</title>
    <#include "../common/common.ftl"/>
    <#setting number_format="#.##">
</head>
<body>
<div class="layui-fluid">
    <form class="layui-form layui-form-pane" id="familyForm">
        <input type="hidden" name="type" value="cp"/>
        <blockquote class="layui-elem-quote layui-bg-white">
            <span class="layui-breadcrumb" lay-separator="-">
              <a href="">无房职工信息管理</a>
              <a><cite>查看</cite></a>
            </span>
        </blockquote>
        <div class="layui-card">
            <div class="layui-card-header">一、无房职工基本信息</div>
            <div class="layui-card-body">
                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        申请人姓名：${(workerInfo.name)!}
                    </div>
                    <div class="layui-col-xs6">
                        申请人身份证号：${(workerInfo.idCard)!}
                    </div>
                </div>

                <div class="layui-form-item" style="line-height: 40px;" >
                    <div class="layui-col-xs6">
                        建立公积金前工龄：${(workerInfo.workedYears)!}
                    </div>
                    <div class="layui-col-xs6">
                        参加工作时间：${(workerInfo.startWorkingDate)!}
                    </div>
                </div>

                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        调入本单位时间：${(workerInfo.joinWorkingDate)!}
                    </div>
                    <div class="layui-col-xs6">
                        退休时间：${(workerInfo.retireDate)!}
                    </div>
                </div>
                <div class="layui-form-item" style="line-height: 40px;">

                    <div class="layui-col-xs6">
                        月基本工资：${(workerInfo.wages)!}
                    </div>
                    <div class="layui-col-xs6">
                        住房地址:${(workerInfo.livingAddress)!}
                    </div>
                </div>
                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        配偶姓名：${(workerInfo.spouseName)!}
                    </div>
                    <div class="layui-col-xs6">
                        配偶身份证号：${(workerInfo.spouseIdCard)!}
                    </div>
                </div>
                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        配偶工作单位：
                    </div>
                    <div class="layui-col-xs6">
                        备注：${(workerInfo.memo)!}
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-card">
                <div class="layui-card-header">二、无房职工补贴信息</div>
            <div class="layui-card-body">
                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        是否正在发放住房补贴：
                        <#if noHouseWorkerApply.isPayingSubsidies==1 >是</#if>
                        <#if noHouseWorkerApply.isPayingSubsidies==2 >否</#if>
                    </div>
                    <div class="layui-col-xs6">
                        原补贴系数：${(noHouseWorkerApply.lastSubsidyCoefficient)!}
                    </div>
                </div>

                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        现补贴系数：${(noHouseWorkerApply.subsidyCoefficient)!}
                    </div>
                    <div class="layui-col-xs6">
                        补贴总额：${(noHouseWorkerApply.totalSubsidy)!}
                    </div>
                </div>

            </div>
        </div>
        <div class="layui-card">
            <div class="layui-card-body">
                <div class="layui-card-header">二、无房职工工资明细</div>
                <table id="dataTable" class="layui-table" lay-filter="layFilter"></table>
                <!--菜单-->
                <script type="text/html" id="indexTpl">
                    {{d.LAY_TABLE_INDEX+1}}
                </script>
            </div>
        </div>
        <button style="display: none;" class="layui-btn" id="submitBtn" name="submitBtn" lay-submit=""
                lay-filter="submit">保存
        </button>
    </form>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.config({
        base: '/modules/' //静态资源所在路径
    }).use(['table', 'form', 'jmdf', 'dataDict', 'element', 'laydate'], function () {
        var form = layui.form, $ = layui.jquery, dataDict = layui.dataDict, jmdf = layui.jmdf, laydate = layui.laydate,
            parentIndex = parent.layer.getFrameIndex(window.name), table = layui.table;
        // 是否显示工资输入卡片
        loadWageTable();
        function loadWageTable(){
            table.render({
                elem: "#dataTable",
                url: 'loadDataWageList?workInfoId=' + ${workerInfo.id},
                id: "dataID",
                page: false,
                height: "full-140",
                cols: [[
                    // {checkbox: true, fixed: true}
                    {title: "序号", toolbar: '#indexTpl', align: 'center', width: 60}
                    , {field: "name", title: "姓名", align: 'left', edit: 'text'}
                    , {field: "idCard", title: "身份证", align: 'center'}
                    , {field: "year", title: "年度", align: 'center'}
                    , {field: "month", title: "月份", align: 'center'}
                    , {field: "money", title: "基本工资", align: 'center'}
                ]]
            });

        }

    });
</script>
</body>
</html>