<!DOCTYPE html>
<html>
<head>
    <title>无房职工信息登记</title>
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
    <link rel="stylesheet" href="/modules/formSelects/formSelects-v4.css" media="all">
    <#include "../common/common.ftl"/>
    <#setting number_format="#.##">
</head>
<body>
<div class="layui-fluid">
    <form class="layui-form layui-form-pane" id="familyForm">
        <input type="hidden" name="workerId" value="${(workerInfo.id)!}"/>
        <input type="hidden" name="noHouseApplyId" value="${(noHouseWorkerApply.id)!}"/>
        <blockquote class="layui-elem-quote layui-bg-white">
            <span class="layui-breadcrumb" lay-separator="-">
              <a href="">无房职工信息管理</a>
              <a href="">无房职工信息申报</a>
              <a><cite>申报</cite></a>
            </span>
        </blockquote>
        <div class="layui-card">
            <div class="layui-card-header">一、职工补贴信息申报<span style="color: red"></span></div>
            <div class="layui-card-body">
                <div class="layui-form-item">
                    <div class="layui-col-xs6">
                        <div class="layui-inline">
                            <label class="layui-form-label" style="width: 150px">申请人身份证号：</label>
                            <div class="layui-input-inline">
                                <input class="layui-input" name="idCard" style="width: 200px;"
                                       value="${(workerInfo.idCard)!}"/>
                            </div>
                        </div>
                        <button class="layui-btn" type="button" id="loadWorkerInfo"><i
                                    class="layui-icon layui-icon-search"></i>提取职工信息
                        </button>
                    </div>
                    <div class="layui-col-xs6">
                        <div class="layui-inline">
                            <label class="layui-form-label" style="width: 150px">申请人姓名：</label>
                            <div class="layui-input-inline">
                                <input class="layui-input" name="name" readonly style="width: 200px;"
                                       value="${(workerInfo.name)!}"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-col-xs6">
                        <div class="layui-inline">
                            <label class="layui-form-label" style="width:150px">现职务（职称） ：</label>
                            <div class="layui-input-inline">
                                <input class="layui-input" readonly style="width: 200px;" name="position"
                                       value="${(workerInfo.position.name)!}"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-xs6">
                        <div class="layui-inline">
                            <label class="layui-form-label" style="width: 150px">参加工作时间 ：</label>
                            <div class="layui-input-inline">
                                <input class="layui-input" readonly style="width: 200px;" name="startWorkingDate"
                                       value="${(workerInfo.startWorkingDate)!}"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-col-xs6">
                        <div class="layui-inline">
                            <label class="layui-form-label" style="width: 200px">是否正在发放住房补贴：</label>
                            <div class="layui-input-inline">
                                    <input type="radio" name="isPayingSubsidies" value="1" title="是" <#if noHouseWorkerApply.isPayingSubsidies==1>checked</#if>>
                                    <input type="radio" name="isPayingSubsidies" value="2" title="否" <#if noHouseWorkerApply.isPayingSubsidies==2>checked</#if>>
                            </div>
                        </div>
                    </div>

                    <div class="layui-col-xs6">
                        <div class="layui-inline">
                            <label class="layui-form-label" style="width: 150px">原补贴系数：</label>
                            <div class="layui-input-inline">
                                <input class="layui-input" type="number" id="lastSubsidyCoefficient"
                                       name="lastSubsidyCoefficient" style="width: 200px;"
                                       value="${(noHouseWorkerApply.lastSubsidyCoefficient)!}" lay-verify="required"/>
                            </div>
                        </div>
                        <div class="layui-inline">
                            <div class="layui-form-mid jmdf-color-red">*</div>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-col-xs6">
                        <div class="layui-inline">
                            <label class="layui-form-label" style="width: 150px">现补贴系数：</label>
                            <div class="layui-input-inline">
                                <input class="layui-input" type="number" name="subsidyCoefficient" style="width: 200px;"
                                       value="0.6" readonly/>

                            </div>
                        </div>
                    </div>
                    <div class="layui-col-xs6">
                        <div class="layui-inline">
                            <label class="layui-form-label" style="width: 150px">补贴总额：</label>
                            <div class="layui-input-inline">
                                <input class="layui-input" type="number" readonly name="totalSubsidy" id="totalSubsidy"
                                       style="width: 200px;" value="${(noHouseWorkerApply.totalSubsidy)!}"
                                       lay-verify="required"/>
                            </div>
                        </div>
                        <div class="layui-inline">
                            <div class="layui-form-mid jmdf-color-red">*</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="layui-card">
            <div class="layui-card-header">二、基本工资情况<span style="color: red" style="font-size: 14px;margin-left: 15px">
       &nbsp; &nbsp; &nbsp; &nbsp;补贴总额：（月工资*现补贴系数-月工资*原补贴系数）累加求和，四舍五入保留两位小数。
                </span></div>
            <div class="layui-card-body" >
                <table>
                    <tbody id="body">
                    </tbody>
                </table>
            </div>
        </div>

<#--        <div class="layui-card layui-hide" id="wageCard">-->
<#--            <div class="layui-card-header">二、基本工资情况<span style="color: red">（点击基本工资可进行修改）</span></div>-->
<#--            <div class="layui-card-body">-->
<#--                <table id="dataTable" class="layui-table" lay-filter="layFilter"></table>-->
<#--                <!--菜单&ndash;&gt;-->
<#--                <script type="text/html" id="indexTpl">-->
<#--                    {{d.LAY_TABLE_INDEX+1}}-->
<#--                </script>-->
<#--            </div>-->
<#--        </div>-->
        <button style="display: none;" class="layui-btn" id="submitBtn" name="submitBtn" lay-submit=""
                lay-filter="submit">保存
        </button>
    </form>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.config({
        base: '/modules/' //静态资源所在路径
    }).use(['table', 'form', 'jmdf', 'dataDict', 'element', 'laydate'], function () {
        var form = layui.form, $ = layui.jquery, dataDict = layui.dataDict, jmdf = layui.jmdf, laydate = layui.laydate,
            parentIndex = parent.layer.getFrameIndex(window.name), table = layui.table;
        laydate.render({
            elem: '#startWorkingDate'
        });
        laydate.render({
            elem: '#joinWorkingDate'
        });
        laydate.render({
            elem: '#retireDate'
        });
        // var wageList=null;
        // console.log(wageList);

        // 是否显示工资输入卡片
        loadWageCard();
        function loadWageCard() {
            var workerId = $("input[name='workerId']").val();
            if (workerId === '') {
                $("#wageCard").addClass("layui-hide");
            } else {
                $("#wageCard").removeClass("layui-hide");
            }
        }

        // 加载工资表格相关
        loadWageTable();
        function loadWageTable(){
            var workerId = $("input[name='workerId']").val();
            if (workerId === '') {
                return false;
            }
            table.render({
                elem: "#dataTable",
                url: 'loadDataWageList?workInfoId=' + workerId,
                id: "dataID",
                page: false,
                height: "full-140",
                cols: [[
                    // {checkbox: true, fixed: true}
                    {title: "序号", toolbar: '#indexTpl', align: 'center', width: 60}
                    , {field: "name", title: "姓名", align: 'left', edit: 'text'}
                    , {field: "idCard", title: "身份证", align: 'center'}
                    , {field: "year", title: "年度", align: 'center'}
                    , {field: "month", title: "月份", align: 'center'}
                    , {field: "money", title: "基本工资", align: 'center', edit: 'text',templet:function (d) {
                            return "<p style='color: red'>"+d.money +"</p>";
                        }}
                ]]
            });

            //监听单元格编辑
            table.on('edit(layFilter)', function (obj) {
                var value = obj.value; //得到修改后的值
                var data = obj.data; //得到所在行所有键值
                var field = obj.field; //得到字段

                if (field === "money") {
                    var reg = /^\d+$/;
                    if (!reg.test(value)) {
                        layer.msg("请输入正整数", {anim: 6, icon: 2});
                        return;
                    }
                }
                var param = "id=" + data['id'] + "&" + field + "=" + value;
                // jmdf.saveOrUpdate("post", "saveWage", param);
                var workerId = $("input[name='workerId']").val();
                var lastSubsidyCoefficient = $("input[name='lastSubsidyCoefficient']").val();
                    $.ajax({
                        type: 'post',
                        url: 'saveWage',
                        data:{id:data['id'],money:value,workerId:workerId,lastSubsidyCoefficient:lastSubsidyCoefficient}
                        , success: function (data) {
                            if (data.code == "0001") {
                                layer.msg(data.message, {anim: 0, icon: 1, time: 1000});
                                $("#totalSubsidy").attr("value", data.data);
                            } else {
                                layer.msg(data.message, {anim: 6, icon: 2, time: 3000});
                            }
                            loadData();
                        }
                    });
            });
        }

        // 点击提取数据
        $("#loadWorkerInfo").click(function () {
            loadWagesList();
        });

        loadWagesList();
        // 通过身份证号提取数据
        function loadWagesList() {
            var idCard = $("input[name='idCard']").val();
            var workerId = $("input[name='workerId']").val();
            if (idCard === '') {
                layer.msg("请填写身份证号提取信息", {anim: 0, icon: 1, time: 1000});
                return false;
            } else {
                $("input[name='workerId']").val('');
            }
            $.ajax({
                url: 'loadWorkerInfoForApply',
                type: 'post',
                data: {idCard: idCard,workerId:workerId},
                success: function (data) {
                    if (data.code === "0001") {
                        $("input[name='workerId']").val(data.data.workerInfo.id);
                        $("input[name='name']").val(data.data.workerInfo.name);
                        $("input[name='position']").val(data.data.workerInfo.position.name);
                        $("input[name='startWorkingDate']").val(moment(data.data.workerInfo.startWorkingDate).format('YYYY-MM-DD'));
                        $("#body").html("");
                        var result = "<tr>";
                        for (var i=0;i<=data.data.wageList.length;i++){
                            var wage = data.data.wageList[i];
                            var year=wage.year;
                            var month=wage.month;
                            var money="0";
                            if (wage.money!==undefined){
                                money=wage.money;
                            }
                            var id=wage.id;
                            var tr1="";
                            if((i+1)%5===0){
                                tr1="</tr><tr>";
                            }
                            // onKeyUp="this.value=this.value.replace(/[^\.\d]/g,'');this.value=this.value.replace('.','');"
                            result += "<td><span class='span-zdy'>"+year+"年"+month+"月:</span><span><input class='inputZdy'  id='"+id+"' name='money"+id+"' value='"+money+"'/>"+"</span><td>" +tr1;
                            if (i === 24) {
                                break;
                            }
                        }
                        $("#body").append(result);
                        loadWageTable();
                        loadWageCard();
                        layer.msg(data.message, {anim: 0, icon: 1, time: 1000});
                    } else {
                        layer.msg(data.message, {anim: 6, icon: 2, time: 3000});
                    }
                }
            })
        }
        $("#body").on('change','.inputZdy',function(){
            var id=this.id
            var money = $("input[name='money"+id+"']").val();
            var workerId = $("input[name='workerId']").val();
            var lastSubsidyCoefficient = $("input[name='lastSubsidyCoefficient']").val();
            $.ajax({
                type: 'post',
                url: 'saveWage',
                data:{id:id,money:money,workerId:workerId,lastSubsidyCoefficient:lastSubsidyCoefficient}
                , success: function (data) {
                    if (data.code == "0001") {
                        layer.msg(data.message, {anim: 0, icon: 1, time: 1000});
                        $("#totalSubsidy").attr("value", data.data);
                    } else {
                        layer.msg(data.message, {anim: 6, icon: 2, time: 3000});
                    }
                    loadData();
                }
            });
        });

        // 原补贴系数更改
        $('input[name=lastSubsidyCoefficient]').change(function (e) {
            var workerId = $("input[name='workerId']").val();
            // 原补贴系数
            var lastSubsidyCoefficient = e.target.value;
            $.ajax({
                url: 'getCountMoney',
                type: 'post',
                data: {lastSubsidyCoefficient: lastSubsidyCoefficient, workInfoId: workerId},
                success: function (data) {
                    if (data.code === "0001") {
                        $("#totalSubsidy").attr("value", data.data);
                    } else {
                        layer.msg(data.message, {anim: 6, icon: 2, time: 3000});
                    }
                }
            })
        });

        /**
         * 监听提交
         */
        form.on('submit(submit)', function (data) {
            $.ajax({
                url: 'saveNoHouseApply',
                type: 'post',
                data: $("#familyForm").serialize(),
                success: function (data) {
                    if (data.code === "0001") {
                        layer.msg(data.message, {anim: 0, icon: 1, time: 1000}, function () {
                            parent.loadData();
                            parent.layer.close(parentIndex);
                        });
                    } else {
                        layer.msg(data.message, {anim: 6, icon: 2, time: 3000});
                    }
                }
            });
            return false;
        });
    });
</script>
<style>
    .inputZdy{
        width: 130px;
        height: 25px;
        border-width: 1px;
        border-style: solid;
        background-color: #ffffe0
    }
    .span-zdy{
        font-size: 16px;
        color: #666666;
    }
</style>
</body>
</html>
