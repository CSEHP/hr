<!DOCTYPE html>
<html>
<head>
    <title>无房职工信息登记</title>
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
    <link rel="stylesheet" href="/modules/formSelects/formSelects-v4.css" media="all">
    <#include "../common/common.ftl"/>
    <#setting number_format="#.##">
</head>
<body>
<div class="layui-fluid">
    <form class="layui-form layui-form-pane" id="familyForm">
        <input type="hidden" name="noStandId" value="${(noStandardWorkerApply.id)!}"/>
        <blockquote class="layui-elem-quote layui-bg-white">
            <span class="layui-breadcrumb" lay-separator="-">
              <a href="">未达标职工信息登记</a>
              <a href="">未达标职工信息登记</a>
              <a><cite>登记</cite></a>
            </span>
        </blockquote>
        <div class="layui-card">
            <div class="layui-card-header">一、职工基本信息<span style="color: red"></span></div>
            <div class="layui-card-body">
                <div class="layui-form-item">
                    <div class="layui-form-item">
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 180px">备案编号：</label>
                                <div class="layui-input-inline"><spanp style="padding-left: 10px;font-weight: bold;padding-top: 8px;display: inline-block;">${(record)!}</spanp>
                                    <input class="layui-input" name="num" lay-verify="required" style="width: 120px;float: right;text-align: left" value="${(num)!}"/>
                                    <input name="recordNo" value="${(record)!}" hidden/>
                                </div>
                            </div>
                            <div class="layui-inline">
                                <div class="layui-form-mid jmdf-color-red">*</div>
                            </div>

                        </div>
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 180px">申请人姓名：</label>
                                <div class="layui-input-inline">
                                    <input class="layui-input" name="name" lay-verify="required" style="width: 200px;" value="${(noStandardWorkerApply.name)!}"/>
                                </div>
                            </div>
                            <div class="layui-inline">
                                <div class="layui-form-mid jmdf-color-red">*</div>
                            </div>
                        </div>

                    </div>
                    <div class="layui-form-item">
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 180px">申请人身份证号：</label>
                                <div class="layui-input-inline">
                                    <input class="layui-input" name="idCard" lay-verify="required" style="width: 200px;" value="${(noStandardWorkerApply.idCard)!}"/>
                                </div>
                            </div>
                            <div class="layui-inline">
                                <div class="layui-form-mid jmdf-color-red">*</div>
                            </div>
                        </div>
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 180px">联系方式：</label>
                                <div class="layui-input-inline">
                                    <input class="layui-input" name="phone" lay-verify="required" style="width: 200px;" value="${(noStandardWorkerApply.phone)!}"/>
                                </div>
                            </div>
                            <div class="layui-inline">
                                <div class="layui-form-mid jmdf-color-red">*</div>
                            </div>
                        </div>

                    </div>
                    <div class="layui-form-item">
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width:180px">现职务职称：</label>
                                <div class="layui-input-inline">
                                    <#--                                    <select name="nowPosition" id="nowPosition" lay-verify="required" lay-filter="nowPosition">-->
                                    <#--                                        <option value="">--请选择--</option>-->
                                    <#--                                    </select>-->
                                    <select name="nowPosition" lay-verify="required" lay-filter="nowPosition">
                                        <option value="">--请选择--</option>
                                        <#list positionList as p>
                                            <option value="${p.code}" <#if nowPosition==p.code>selected</#if>>${p.name}</option>
                                        </#list>
                                    </select>
                                </div>
                            </div>
                            <div class="layui-inline">
                                <div class="layui-form-mid jmdf-color-red">*</div>
                            </div>
                        </div>
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width:180px">情况类型：</label>
                                <div class="layui-input-inline">
                                    <select name="jobStatus" id="jobStatus" lay-verify="required">
                                        <option value="">--请选择--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="layui-inline">
                                <div class="layui-form-mid jmdf-color-red">*</div>
                            </div>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width:180px">职工类型：</label>
                                <div class="layui-input-inline">
                                    <select name="workerType" id="workerType" lay-verify="required">
                                        <option value="">--请选择--</option>
                                    </select>
                                </div>
                            </div>
                            <div class="layui-inline">
                                <div class="layui-form-mid jmdf-color-red">*</div>
                            </div>
                        </div>
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 180px">参加工作时间：</label>
                                <div class="layui-input-inline">
                                    <input type="text" class="layui-input" autocomplete="off" style="width: 200px;"
                                           id="startWorkingDate" name="startWorkingDate" value="${(noStandardWorkerApply.startWorkingDate)!}" lay-verify="required" placeholder="yyyy-MM-dd"/>
                                </div>
                            </div>
                            <div class="layui-inline">
                                <div class="layui-form-mid jmdf-color-red">*</div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 180px">调入本单位时间：</label>
                                <div class="layui-input-inline">
                                    <input type="text" class="layui-input" autocomplete="off" style="width: 200px;"
                                           id="joinWorkingDate" name="joinWorkingDate" value="${(noStandardWorkerApply.joinWorkingDate)!}" lay-verify="required" placeholder="yyyy-MM-dd"/>
                                </div>
                            </div>
                            <div class="layui-inline">
                                <div class="layui-form-mid jmdf-color-red">*</div>
                            </div>
                        </div>
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 180px">退休时间：</label>
                                <div class="layui-input-inline">
                                    <input type="text" class="layui-input" autocomplete="off" style="width: 200px;"
                                           id="retireDate" name="retireDate" value="${(noStandardWorkerApply.retireDate)!}" placeholder="yyyy-MM-dd"/>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="layui-form-item">
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 180px">享受政府补贴面积：</label>
                                <div class="layui-input-inline">
                                    <input class="layui-input " type="number" name="enjoyedBuildArea" style="width: 200px;"
                                           value="${(noStandardWorkerApply.enjoyedBuildArea)!}" />
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width:180px">原职务职称：</label>
                                <div class="layui-input-inline">
<#--                                    <select name="ylPosition" id="ylPosition">-->
<#--                                        <option value="">--请选择--</option>-->
<#--                                    </select>-->
                                    <select name="ylPosition">
                                        <option value="">--请选择--</option>
                                        <#list positionList as p>
                                            <option value="${p.code}" <#if ylPosition==p.code>selected</#if>>${p.name}</option>
                                        </#list>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width:180px">退休时职务职称：</label>
                                <div class="layui-input-inline">
                                    <#--                                    <select name="txPosition" id="txPosition">-->
                                    <#--                                        <option value="">--请选择--</option>-->
                                    <#--                                    </select>-->
                                    <select name="txPosition">
                                        <option value="">--请选择--</option>
                                        <#list positionList as p>
                                            <option value="${p.code}" <#if txPosition==p.code>selected</#if>>${p.name}</option>
                                        </#list>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 180px">配偶是否享受过补贴：</label>
                                <div class="layui-input-inline"  style="width: 300px;">
                                    <input type="radio" name="spouseIsEnjoyed" lay-filter="sie" value="1"  title="是" <#if noStandardWorkerApply.spouseIsEnjoyed==1>checked</#if>>
                                    <input type="radio" name="spouseIsEnjoyed" lay-filter="sie"  value="2" title="否" <#if noStandardWorkerApply.spouseIsEnjoyed==2>checked</#if>>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-col-xs6" id="spouseEnjoyedArea">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 180px">配偶享受补贴面积：</label>
                                <div class="layui-input-inline">
                                    <input  class="layui-input"  name="spouseEnjoyedArea" type="number" value="${(noStandardWorkerApply.spouseEnjoyedArea)!}">
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 180px">配偶姓名：</label>
                                <div class="layui-input-inline">
                                    <input class="layui-input"  name="spouseName" style="width: 200px;" value="${(noStandardWorkerApply.spouseName)!}"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 180px">配偶身份证号：</label>
                                <div class="layui-input-inline">
                                    <input class="layui-input" name="spouseIdCard"  style="width: 200px;" value="${(noStandardWorkerApply.spouseIdCard)!}"/>
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 180px">配偶工作单位：</label>
                                <div class="layui-input-inline">
                                    <input class="layui-input"  name="spouseUnit" style="width: 200px;" value="${(noStandardWorkerApply.spouseUnit)!}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 180px">房屋坐落：</label>
                                <div class="layui-input-inline">
                                    <input class="layui-input" name="livingAddress" style="width: 200px;" value="${(noStandardWorkerApply.livingAddress)!}" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="layui-card">
            <div class="layui-card-header">二、职工补贴信息<span style="color: red"></span></div>
            <div class="layui-card-body">
                <div class="layui-form-item">
                    <div class="layui-col-xs6">
                        <div class="layui-inline">
                            <label class="layui-form-label" style="width: 180px">是否享受过政府补贴：</label>
                            <div class="layui-input-inline">
                                <input type="radio" name="isEnjoyed" value="2" title="已享受" checked>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-xs6">
                        <div class="layui-inline">
                            <label class="layui-form-label" style="width: 180px">建立公积金前工龄：</label>
                            <div class="layui-input-inline">
                                <input class="layui-input changeMoney" type="number" name="workedYears" style="width: 190px;" value="${(noStandardWorkerApply.workedYears)!}" lay-verify="required"/>
                            </div>

                        </div>
                        <div class="layui-inline">
                            <div class="layui-form-mid jmdf-color-red">*</div>
                        </div>
                    </div>
                </div>

                <div >
                    <div class="layui-form-item">
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 180px">申请人购房时单位：</label>
                                <div class="layui-input-inline">
                                    <input class="layui-input"  id="boughtUnit" name="boughtUnit" style="width: 200px;" value="${(noStandardWorkerApply.boughtUnit)!}"/>
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-xs6" >
                            <div class="layui-inline">
                                <#--                                <#if flag=2 >-->
                                <#--                                    <label class="layui-form-label" style="width: 180px">已发补贴额：</label>-->
                                <#--                                <#else>-->
                                <label class="layui-form-label" style="width: 180px">申请人购房时补贴额：</label>
                                <#--                                </#if>-->
                                <div class="layui-input-inline">
                                    <input class="layui-input changeMoney" type="number" id="boughtUnitSubsidy" name="boughtUnitSubsidy" style="width: 200px;" value="${(noStandardWorkerApply.boughtUnitSubsidy)!}"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 180px">军队转业干部时部队：</label>
                                <div class="layui-input-inline">
                                    <input class="layui-input" name="boughtTroops"id="boughtTroops" style="width: 200px;" value="${(noStandardWorkerApply.boughtTroops)!}" />
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 180px">军队转业干部补贴额：</label>
                                <div class="layui-input-inline">
                                    <input class="layui-input changeMoney"  type="number" id="boughtTroopsSubsidy" name="boughtTroopsSubsidy" style="width: 200px;" value="${(noStandardWorkerApply.boughtTroopsSubsidy)!}"/>
                                </div>
                            </div>
                        </div>
                    </div>
<#--                <div class="layui-form-item">-->

<#--                    <div class="layui-col-xs6">-->
<#--                        <div class="layui-inline">-->
<#--                            <label class="layui-form-label" style="width: 180px">已发补贴额 ：</label>-->
<#--                            <div class="layui-input-inline">-->
<#--                                <input class="layui-input changeMoney" type="number" id="boughtUnitSubsidy" name="boughtUnitSubsidy"-->
<#--                                       style="width: 200px;" value="${(noStandardWorkerApply.boughtUnitSubsidy)!}"-->
<#--                                       lay-verify="required"/>-->
<#--                            </div>-->
<#--                            <div class="layui-inline" style="padding-left: 15px">-->
<#--                                <div class="layui-form-mid jmdf-color-red">*</div>-->
<#--                            </div>-->
<#--                        </div>-->
<#--                    </div>-->

<#--                </div>-->
                <div class="layui-form-item">
                    <div class="layui-col-xs6">
                        <div class="layui-inline">
                            <label class="layui-form-label" style="width: 180px">住房补贴面积标准：</label>
                            <div class="layui-input-inline">
                                <input class="layui-input changeMoney" type="number" id="buildAreaStandard" readonly name="buildAreaStandard" style="width: 200px;"
                                       value="${(noStandardWorkerApply.buildAreaStandard)!}" lay-verify="required"/>
                            </div>
                            <div class="layui-inline" style="padding-left: 15px">
                                <div class="layui-form-mid jmdf-color-red">*</div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-xs6">
                        <div class="layui-inline">
                            <label class="layui-form-label" style="width: 180px">核定后住房总面积：</label>
                            <div class="layui-input-inline">
                                <input class="layui-input changeMoney" type="number" name="buildAreaSureStandard"
                                       style="width: 200px;" value="${(noStandardWorkerApply.buildAreaSureStandard)!}"
                                       lay-verify="required"/>
                            </div>
                            <div class="layui-inline" style="padding-left: 15px">
                                <div class="layui-form-mid jmdf-color-red">*</div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="layui-form-item">
                    <div class="layui-col-xs6">
                        <div class="layui-inline">
                            <label class="layui-form-label" style="width: 180px">差额面积：</label>
                            <div class="layui-input-inline">
                                <input class="layui-input" readonly type="number" id="differenceBuildArea"
                                       name="differenceBuildArea" style="width: 200px;"
                                       value="${(noStandardWorkerApply.differenceBuildArea)!}" lay-verify="required"/>
                            </div>
                            <div class="layui-inline" style="padding-left: 15px">
                                <div class="layui-form-mid jmdf-color-red">*</div>
                            </div>
                        </div>
                    </div>

                    <div class="layui-col-xs6">
                        <div class="layui-inline">
                            <label class="layui-form-label" style="width: 180px">差额补贴额：</label>
                            <div class="layui-input-inline">
                                <input class="layui-input" readonly type="number" id="differenceSubsidy"
                                       name="differenceSubsidy" style="width: 200px;"
                                       value="${(noStandardWorkerApply.differenceSubsidy)!}" lay-verify="required"/>
                            </div>
                            <div class="layui-inline" style="padding-left: 15px">
                                <div class="layui-form-mid jmdf-color-red">*</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-col-xs12">
                        <div class="layui-inline">
                            <label class="layui-form-label" style="width: 180px">补贴总额：</label>
                            <div class="layui-input-inline">
                                <input class="layui-input" readonly type="number" id="totalSubsidy" name="totalSubsidy"
                                       style="width: 200px;" value="${(noStandardWorkerApply.totalSubsidy)!}"
                                       lay-verify="required"/>
                            </div>
                            <div class="layui-inline" style="padding-left: 15px">
                                <div class="layui-form-mid jmdf-color-red">*</div>
                            </div>
                        </div>
                    </div>
                </div>
                    <p style="padding-bottom: 20px"> <span style="color: red">注：</span>差额补贴额=（421+10.9×职工建立住房公积金前的工龄）×差额面积  <span style="margin-left: 60px"></span>补贴总额=差额补贴额—单位补贴额—部队住房补贴额</p>
                <div class="layui-form-item">
                    <label style="font-size: 14px;padding-left: 15px">备注：</label>
                    <div class="layui-input-block" style="width: 75%">
                        <textarea class="layui-textarea" name="memo">${(noStandardWorkerApply.memo)!}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <button style="display: none;" class="layui-btn" id="submitBtn" name="submitBtn" lay-submit=""
                lay-filter="submit">保存
        </button>
    </form>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.config({
        base: '/modules/' //静态资源所在路径
    }).use(['form', 'jmdf', 'dataDict', 'element', 'laydate', 'jquery'], function () {
        var form = layui.form, $ = layui.jquery, dataDict = layui.dataDict, jmdf = layui.jmdf, laydate = layui.laydate,
            parentIndex = parent.layer.getFrameIndex(window.name);
        var laydate = layui.laydate;
        <#--dataDict.select("职务名称", "nowPosition", "${(nowPosition)!}");-->
        <#--dataDict.select("职务名称", "ylPosition", "${(ylPosition)!}");-->
        <#--dataDict.select("职务名称", "txPosition", "${(txPosition)!}");-->
        dataDict.select("情况类型", "jobStatus", "${(noStandardWorkerApply.jobStatus)!}");
        dataDict.select("职工类型", "workerType", "${(noStandardWorkerApply.workerType)!}");
        <#--dataDict.dataDictRadio("是否享受过政府补贴", "isEnjoyed", "${(noStandardWorkerApply.isEnjoyed)!}");-->
        laydate.render({
            elem: '#startWorkingDate'
        });
        laydate.render({
            elem: '#joinWorkingDate'
        });
        laydate.render({
            elem: '#retireDate'
        });

        form.on('radio(sie)', function (data) {
            if (this.value==2 ){
                document.getElementById("spouseEnjoyedArea").style.display="none";
            }else{
                document.getElementById("spouseEnjoyedArea").style.display="block";
            }
        })
        // 身份证验证
        $('input[name=idCard]').change(function (e) {
            var idCard = $("input[name='idCard']").val();
            $.ajax({
                url: 'loadWorkerInfoForApply',
                type: 'post',
                data: {idCard: idCard},
                success: function (data) {
                    if (data.code === "0001") {
                        // layer.msg(data.message, {anim: 0, icon: 1});
                    } else {
                        $("input[name='idCard']").val("");
                        layer.msg(data.message, {anim: 6, icon: 2, time: 3000});
                    }
                }
            })
        });
        // form.on('radio(isEnjoyed)', function (data) {
        //    var flag=this.value
        //     if (this.value==2 ){
        //         document.getElementById("yc").style.display="none";
        //         $("input[name='boughtUnit']").val("")
        //         $("input[name='boughtUnitSubsidy']").val("0")
        //         $("input[name='boughtTroops']").val("")
        //         $("input[name='boughtTroopsSubsidy']").val("0")
        //     }else{
        //         document.getElementById("yc").style.display="block";
        //         $("#boughtUnit").attr("value",1);
        //     }
        //
        // })
        // form.changeData('input(changeMoney)', function () {

         $(".changeMoney").change(function () {
             $.ajax({
                 url: 'getNoStandMoney',
                 type: 'post',
                 data: $("#familyForm").serialize(),
                 success: function (data) {
                     if (data.code == "0001") {
                         $("#differenceBuildArea").attr("value", data.data.area);
                         $("#differenceSubsidy").attr("value", data.data.money);
                         $("#totalSubsidy").attr("value", data.data.moneyCount);
                     } else {
                         layer.msg(data.message, {anim: 6, icon: 2, time: 3000});
                     }
                 }
             });
        })
        form.on('select(nowPosition)', function(data){
            $.ajax({
                url: 'getAreaByCode',
                type: 'post',
                data: {code:data.value},
                success: function (data) {
                    if (data.code == "0001") {
                        $("#buildAreaStandard").attr("value", data.data);
                    } else {
                        layer.msg(data.message, {anim: 6, icon: 2, time: 3000});
                    }
                }
            });
        });
        // form.on('radio(js)', function (data) {
        //     var workedYears = $("input[name='workedYears']").val();
        //     var boughtUnitSubsidy = $("input[name='boughtUnitSubsidy']").val();
        //     var buildAreaStandard = $("input[name='buildAreaStandard']").val();
        //     var buildAreaSureStandard = $("input[name='buildAreaSureStandard']").val();
        //     if (workedYears == '') {
        //         layer.msg("请填写公积金年龄", {anim: 6, icon: 2, time: 3000});
        //         return false;
        //     } else if (boughtUnitSubsidy === '') {
        //         layer.msg("请填写已发补贴额", {anim: 6, icon: 2, time: 3000});
        //         return false;
        //     } else if (buildAreaStandard === '') {
        //         layer.msg("请填写住房补贴面积标准", {anim: 6, icon: 2, time: 3000});
        //         return false;
        //     } else if (buildAreaSureStandard === '') {
        //         layer.msg("请填写核定后住房总面积", {anim: 6, icon: 2, time: 3000});
        //         return false;
        //     } else if (this.value == '2') {
        //         $.ajax({
        //             url: 'getNoStandMoney',
        //             type: 'post',
        //             data: $("#familyForm").serialize(),
        //             success: function (data) {
        //                 if (data.code == "0001") {
        //                     $("#differenceBuildArea").attr("value", data.data.area);
        //                     $("#differenceSubsidy").attr("value", data.data.money);
        //                     $("#totalSubsidy").attr("value", data.data.moneyCount);
        //                 } else {
        //                     layer.msg(data.message, {anim: 6, icon: 2, time: 3000});
        //                 }
        //             }
        //         });
        //     } else {
        //         return false;
        //     }
        // })
        form.on('submit(submit)', function (data) {
            var num = $("input[name='num']").val();
            var recordNo = $("input[name='recordNo']").val();
            var noStandId = $("input[name='noStandId']").val();
            $.ajax({
                url: 'checkRecordNo',
                type: 'post',
                data: {num: num,noStandId:noStandId,recordNo:recordNo},
                success: function (data) {
                    if (data.code === "0001") {
                        $.ajax({
                            url: 'noStandInfoSave',
                            type: 'post',
                            data:  $("#familyForm").serialize(),
                            success: function (data) {
                                if (data.code == "0001") {
                                    layer.msg(data.message, {anim: 0, icon: 1, time: 1000}, function () {
                                        parent.loadData();
                                        parent.layer.close(parentIndex);
                                    });
                                } else {
                                    layer.msg(data.message, {anim: 6, icon: 2, time: 3000});
                                }
                            }
                        })
                    } else {
                        layer.msg(data.message, {anim: 6, icon: 2, time: 3000});
                    }
                }
            })
            return false;
        });
        // 编号验证
        $('input[name=num]').change(function (e) {
            var num = $("input[name='num']").val();
            var noStandId = $("input[name='noStandId']").val();
            $.ajax({
                url: 'checkRecordNo',
                type: 'post',
                data: {num: num,noStandId:noStandId},
                success: function (data) {
                    if (data.code === "0001") {
                        // layer.msg(data.message, {anim: 0, icon: 1});
                    } else {
                        layer.msg(data.message, {anim: 6, icon: 2, time: 3000});
                    }
                }
            })
        });
    });
</script>
</body>
</html>