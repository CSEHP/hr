<!DOCTYPE html>
<html>
<head>
    <title>预约搬家信息登记</title>
    <#include "../common/common.ftl"/>
    <#setting number_format="#.##">
</head>
<body>
<div class="layui-fluid">
    <form class="layui-form layui-form-pane" id="familyForm">
        <input type="hidden" name="type" value="cp"/>
        <blockquote class="layui-elem-quote layui-bg-white">
            <span class="layui-breadcrumb" lay-separator="-">
              <a href="">未达标职工信息管理</a>
              <a href="">未达标职工信息管理</a>
              <a><cite>查看</cite></a>
            </span>
        </blockquote>
        <div class="layui-card">
            <div class="layui-card-header">一、未达标职工基本信息</div>
            <div class="layui-card-body">
                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        申请人姓名：${(noStandardWorkerApply.name)!}
                    </div>
                    <div class="layui-col-xs6">
                        申请人身份证号：${(noStandardWorkerApply.idCard)!}
                    </div>
                </div>
                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        联系方式   ：${(noStandardWorkerApply.phone)!}
                    </div>
                    <div class="layui-col-xs6">
                        退休时职务职称：${(noStandardWorkerApply.retirePosition.name)!}
                    </div>
                </div>
                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        原职务职称   ：${(noStandardWorkerApply.lastPosition.name)!}
                    </div>
                    <div class="layui-col-xs6">
                        现职务职称：${(noStandardWorkerApply.position.name)!}
                    </div>
                </div>
                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        情况类型   ：
                        <#if noStandardWorkerApply.jobStatus==1>在职</#if>
                        <#if noStandardWorkerApply.jobStatus==2>离退休</#if>
                        <#if noStandardWorkerApply.jobStatus==3>其他</#if>
                    </div>
                    <div class="layui-col-xs6">
                        职工类型：
                        <#if noStandardWorkerApply.jobStatus==1>未达标职工</#if>
                        <#if noStandardWorkerApply.jobStatus==2>新增未达标职工</#if>
                        <#if noStandardWorkerApply.jobStatus==3>级差职工</#if>
                    </div>
                </div>
                <div class="layui-form-item" style="line-height: 40px;" >
                    <div class="layui-col-xs6">
                        建立公积金前工龄：${(noStandardWorkerApply.workedYears)!}
                    </div>
                    <div class="layui-col-xs6">
                        参加工作时间：${(noStandardWorkerApply.startWorkingDate)!}
                    </div>
                </div>

                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        调入本单位时间：${(noStandardWorkerApply.joinWorkingDate)!}
                    </div>
                    <div class="layui-col-xs6">
                        退休时间：${(noStandardWorkerApply.retireDate)!}
                    </div>
                </div>
                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        配偶姓名：${(noStandardWorkerApply.spouseName)!}
                    </div>
                    <div class="layui-col-xs6">
                        配偶身份证号：${(noStandardWorkerApply.spouseIdCard)!}
                    </div>
                </div>
                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        配偶工作单位:${(noStandardWorkerApply.spouseUnit)!}
                    </div>
                    <div class="layui-col-xs6">
                        住房地址:${(noStandardWorkerApply.livingAddress)!}
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-card">
                <div class="layui-card-header">二、未达标职工补贴信息</div>
            <div class="layui-card-body">
                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        申请人购房时单位:${(noStandardWorkerApply.boughtUnit)!}
                    </div>
                    <div class="layui-col-xs6">
                        申请人购房时补贴额:${(noStandardWorkerApply.boughtUnitSubsidy)!}
                    </div>
                </div>

                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        军队转业干部时部队:${(noStandardWorkerApply.boughtTroops)!}
                    </div>
                    <div class="layui-col-xs6">
                        军队转业干部时补贴额:${(noStandardWorkerApply.boughtTroopsSubsidy)!}
                    </div>
                </div>

                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        是否享受过含有政府补贴性质的住房:
                        <#if noStandardWorkerApply.isEnjoyed==1>未享受</#if>
                        <#if noStandardWorkerApply.isEnjoyed==2>已享受</#if>
                    </div>
                    <div class="layui-col-xs6">
                        享受过含有政府补贴性质的住房面积:${(noStandardWorkerApply.enjoyedBuildArea)!}
                    </div>
                </div>
                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        住房补贴建筑面积标准:${(noStandardWorkerApply.buildAreaStandard)!}
                    </div>
                    <div class="layui-col-xs6">
                        核定后住房总面积:${(noStandardWorkerApply.buildAreaSureStandard)!}
                    </div>
                </div>
                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        差额面积:${(noStandardWorkerApply.differenceBuildArea)!}
                    </div>
                    <div class="layui-col-xs6">
                        差额补贴额:${(noStandardWorkerApply.differenceSubsidy)!}
                    </div>
                </div>
                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        补贴总额:${(noStandardWorkerApply.totalSubsidy)!}
                    </div>
                </div>
                <div class="layui-form-item" style="line-height: 40px;">
                    <div class="layui-col-xs6">
                        备注：${(noStandardWorkerApply.memo)!}
                    </div>
                </div>

            </div>
        </div>

        <button style="display: none;" class="layui-btn" id="submitBtn" name="submitBtn" lay-submit=""
                lay-filter="submit">保存
        </button>
    </form>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.config({
        base: '/modules/' //静态资源所在路径
    }).use(['form', 'jmdf', 'dataDict', 'element', 'laydate'], function () {
        var form = layui.form, $ = layui.jquery, dataDict = layui.dataDict, jmdf = layui.jmdf, laydate = layui.laydate,
            parentIndex = parent.layer.getFrameIndex(window.name);

    });
</script>
</body>
</html>