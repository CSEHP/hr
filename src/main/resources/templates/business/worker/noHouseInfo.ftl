<!DOCTYPE html>
<html>
<head>
    <title>无房职工信息登记</title>
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
    <link rel="stylesheet" href="/modules/formSelects/formSelects-v4.css" media="all">
    <#include "../common/common.ftl"/>
    <#setting number_format="#.##">
</head>
<body>
<div class="layui-fluid">
    <form class="layui-form layui-form-pane" id="familyForm">
        <input type="hidden" name="workerId" value="${(workerInfo.id)!}"/>
        <blockquote class="layui-elem-quote layui-bg-white">
            <span class="layui-breadcrumb" lay-separator="-">
              <a href="">无房职工信息登记</a>
              <a href="">无房职工信息登记</a>
              <a><cite>登记</cite></a>
            </span>
        </blockquote>
        <div class="layui-card">
            <div class="layui-card-header">一、职工基本信息<span style="color: red"></span></div>
            <div class="layui-card-body">
                <div class="layui-form-item">
                    <div class="layui-form-item">
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 150px">备案编号：</label>
                               <div class="layui-input-inline"><spanp style="padding-left: 10px;font-weight: bold;padding-top: 8px;display: inline-block;">${(record)!}</spanp>
                                  <input class="layui-input" name="num" lay-verify="required" style="width: 120px;float: right;text-align: left" value="${(num)!}"/>
                                  <input name="recordNo" value="${(record)!}" hidden/>
                                </div>
                            </div>
                            <div class="layui-inline">
                                <div class="layui-form-mid jmdf-color-red">*</div>
                            </div>
                        </div>
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 150px">申请人姓名：</label>
                                <div class="layui-input-inline">
                                    <input class="layui-input" name="name" lay-verify="required" style="width: 200px;" value="${(workerInfo.name)!}"/>
                                </div>
                            </div>
                            <div class="layui-inline">
                                <div class="layui-form-mid jmdf-color-red">*</div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 150px">申请人身份证号：</label>
                                <div class="layui-input-inline">
                                    <input class="layui-input" name="idCard" lay-verify="required" style="width: 200px;" value="${(workerInfo.idCard)!}"/>
                                </div>
                            </div>
                            <div class="layui-inline">
                                <div class="layui-form-mid jmdf-color-red">*</div>
                            </div>
                        </div>
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 150px">参加工作时间：</label>
                                <div class="layui-input-inline">
                                    <input type="text" class="layui-input" autocomplete="off" style="width: 200px;"
                                           id="startWorkingDate" name="startWorkingDate" value="${(workerInfo.startWorkingDate)!}" lay-verify="required" placeholder="yyyy-MM-dd"/>
                                </div>
                            </div>
                            <div class="layui-inline">
                                <div class="layui-form-mid jmdf-color-red">*</div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 150px">调入本单位时间：</label>
                                <div class="layui-input-inline">
                                    <input type="text" class="layui-input" autocomplete="off" style="width: 200px;"
                                           id="joinWorkingDate" name="joinWorkingDate" value="${(workerInfo.joinWorkingDate)!}" lay-verify="required" placeholder="yyyy-MM-dd"/>
                                </div>
                            </div>
                            <div class="layui-inline">
                                <div class="layui-form-mid jmdf-color-red">*</div>
                            </div>
                        </div>
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width:150px">现职务（职称） ：</label>
                                <div class="layui-input-inline">
<#--                                    <select name="nowPosition" id="nowPosition" lay-verify="required">-->
<#--                                        <option value="">--请选择--</option>-->
<#--                                    </select>         -->

                                    <select name="nowPosition" lay-verify="required">
                                        <option value="">--请选择--</option>
                                        <#list positionList as p>
                                            <option value="${p.code}"  <#if nowPosition==p.code>selected</#if>  >${p.name}</option>
                                        </#list>
                                    </select>

                                </div>
                                <div class="layui-inline">
                                    <div class="layui-form-mid jmdf-color-red">*</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width:150px">原职务（职称：</label>
                                <div class="layui-input-inline">
                                    <#--                                    <select name="txPosition" id="txPosition" >-->
                                    <#--                                        <option value="">--请选择--</option>-->
                                    <#--                                    </select>-->
                                    <select name="txPosition">
                                        <option value="">--请选择--</option>
                                        <#list positionList as p>
                                            <option value="${p.code}" <#if txPosition==p.code>selected</#if>>${p.name}</option>
                                        </#list>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 160px">建立公积金前工龄：</label>
                                <div class="layui-input-inline">
                                    <input class="layui-input" type="number" name="workedYears" style="width: 190px;" value="${(workerInfo.workedYears)!}"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 150px">月基本工资：</label>
                                <div class="layui-input-inline">
                                    <input class="layui-input" type="number" name="wages" style="width: 200px;" value="${(workerInfo.wages)!}" lay-verify="required"/>
                                    <#--                                    onKeyUp="this.value=this.value.replace(/[^\.\d]/g,'');this.value=this.value.replace('.','');"-->
                                </div>
                            </div>

                            <div class="layui-inline">
                                <div class="layui-form-mid jmdf-color-red">*
                                    <#--                                    (注:只能输入整数)-->
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 150px">退休时间：</label>
                                <div class="layui-input-inline">
                                    <input type="text" class="layui-input" autocomplete="off" style="width: 200px;"
                                           id="retireDate" name="retireDate" value="${(workerInfo.retireDate)!}"  placeholder="yyyy-MM-dd"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 150px">房屋坐落：</label>
                                <div class="layui-input-inline">
                                    <input class="layui-input" name="livingAddress" style="width: 200px;" value="${(workerInfo.livingAddress)!}" />
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 150px">配偶姓名：</label>
                                <div class="layui-input-inline">
                                    <input class="layui-input"  name="spouseName" style="width: 200px;" value="${(workerInfo.spouseName)!}"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 150px">配偶身份证号：</label>
                                <div class="layui-input-inline">
                                    <input class="layui-input" name="spouseIdCard"  style="width: 200px;" value="${(workerInfo.spouseIdCard)!}"/>
                                </div>
                            </div>
                        </div>
                        <div class="layui-col-xs6">
                            <div class="layui-inline">
                                <label class="layui-form-label" style="width: 150px">配偶工作单位：</label>
                                <div class="layui-input-inline">
                                    <input class="layui-input"  name="spouseUnit" style="width: 200px;" value="${(workerInfo.spouseUnit)!}"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label style="font-size: 14px;padding-left: 15px">备注：</label>
                        <div class="layui-input-block" style="width: 75%">
                            <textarea class="layui-textarea" name="memo">${(workerInfo.memo)!}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <button style="display: none;" class="layui-btn" id="submitBtn" name="submitBtn" lay-submit=""
                lay-filter="submit">保存
        </button>
    </form>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.config({
        base: '/modules/' //静态资源所在路径
    }).use(['form', 'jmdf', 'dataDict', 'element', 'laydate'], function () {
        var form = layui.form, $ = layui.jquery, dataDict = layui.dataDict, jmdf = layui.jmdf, laydate = layui.laydate,
            parentIndex = parent.layer.getFrameIndex(window.name);
        var laydate = layui.laydate;
        <#--dataDict.select("职务名称", "nowPosition", "${(nowPosition)!}");-->
        <#--dataDict.select("职务名称", "txPosition","${(txPosition)!}");-->
        laydate.render({
            elem: '#startWorkingDate'
        });
        laydate.render({
            elem: '#joinWorkingDate'
        });
        laydate.render({
            elem: '#retireDate'
        });

        // 身份证验证
        $('input[name=idCard]').change(function (e) {
            var idCard = $("input[name='idCard']").val();
            $.ajax({
                url: 'loadWorkerInfoForApply',
                type: 'post',
                data: {idCard: idCard},
                success: function (data) {
                    if (data.code === "0001") {
                        // layer.msg(data.message, {anim: 0, icon: 1});
                    } else {
                        $("input[name='idCard']").val("");
                        layer.msg(data.message, {anim: 6, icon: 2, time: 3000});
                    }
                }
            })
        });
        /**
         * 监听提交
         */
        form.on('submit(submit)', function (data) {
            var num = $("input[name='num']").val();
            var recordNo = $("input[name='recordNo']").val();
            var workerId = $("input[name='workerId']").val();
            $.ajax({
                url: 'checkRecordNo',
                type: 'post',
                data: {num: num,workerId:workerId,recordNo:recordNo},
                success: function (data) {
                    if (data.code === "0001") {
                        $.ajax({
                            url: 'workerInfoSave',
                            type: 'post',
                            data:  $("#familyForm").serialize(),
                            success: function (data) {
                                if (data.code == "0001") {
                                    layer.msg(data.message, {anim: 0, icon: 1, time: 1000}, function () {
                                        parent.loadData();
                                        parent.layer.close(parentIndex);
                                    });
                                } else {
                                    layer.msg(data.message, {anim: 6, icon: 2, time: 3000});
                                }
                            }
                        });
                        // layer.msg(data.message, {anim: 0, icon: 1});
                    } else {
                        layer.msg(data.message, {anim: 6, icon: 2, time: 3000});
                    }
                }
            })
            return false;
        });

        // 编号验证
        $('input[name=num]').change(function (e) {
            var num = $("input[name='num']").val();
            var recordNo = $("input[name='recordNo']").val();
            var workerId = $("input[name='workerId']").val();
            $.ajax({
                url: 'checkRecordNo',
                type: 'post',
                data: {num: num,workerId:workerId,recordNo:recordNo},
                success: function (data) {
                    if (data.code === "0001") {
                        // layer.msg(data.message, {anim: 0, icon: 1});
                    } else {
                        layer.msg(data.message, {anim: 6, icon: 2, time: 3000});
                    }
                }
            })
        });
    });
</script>
</body>
</html>