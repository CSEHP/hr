<meta charset="utf-8">
<link rel="stylesheet" href="/layui/css/layui.css" media="all">

<link rel="stylesheet" href="/style/admin.css" media="all">
<link rel="stylesheet" href="/style/jmdf.css" media="all">
<link rel="stylesheet" href="/modules/formSelects/formSelects-v4.css" media="all">
<script src="/plugins/moment/moment.js" charset="utf-8"></script>
<#setting number_format="#0.####">
