<!DOCTYPE html>
<html>
<head>
    <title>${(dataDictMap['系统名称'].value)!}</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
    <link rel="stylesheet" href="/style/login.css" media="all">
</head>
<body>
<div class="layadmin-user-login layadmin-user-display-show" id="LAY-user-login" style="display: none;">

    <div class="layadmin-user-login-main">
        <div class="layadmin-user-login-box layadmin-user-login-header">
            <h2>${(dataDictMap['系统名称'].value)!}</h2>
            <p>spring boot 1.5.9.RELEASE + shiro + jpa + layui</p>
        </div>
        <div class="layadmin-user-login-box layadmin-user-login-body layui-form">
            <div class="layui-form-item">
                <label class="layadmin-user-login-icon layui-icon layui-icon-username" for="LAY-user-login-username"></label>
                <input type="text" name="username" id="username" value="admin" lay-verify="required" placeholder="用户名" class="layui-input">
            </div>
            <div class="layui-form-item">
                <label class="layadmin-user-login-icon layui-icon layui-icon-password" for="LAY-user-login-password"></label>
                <input type="password" name="password" id="password" value="123456" lay-verify="required" placeholder="密码" class="layui-input">
            </div>
            <div class="layui-form-item">
                <div class="layui-row">
                    <div class="layui-col-xs7">
                        <label class="layadmin-user-login-icon layui-icon layui-icon-vercode" for="LAY-user-login-vercode"></label>
                        <input type="text" name="randomcode" id="randomcode" lay-verify="required" placeholder="验证码" class="layui-input">
                    </div>
                    <div class="layui-col-xs5">
                        <div style="margin-left: 10px;">
                            <img src="/validateCodeServlet" class="layadmin-user-login-codeimg" id="vercode_img">
                        </div>
                    </div>
                </div>
            </div>
            <div class="layui-form-item" style="margin-bottom: 20px;">
            <#--<input type="checkbox" name="rememberMe" id="rememberMe" lay-skin="primary" title="记住我">-->
                <#--<a href="forget.html" class="layadmin-user-jump-change layadmin-link" style="margin-top: 7px;">忘记密码？</a>-->
            </div>
            <div class="layui-form-item">
                <button class="layui-btn layui-btn-fluid" lay-submit lay-filter="LAY-user-login-submit">登 入</button>
            </div>
        </div>
    </div>
    <div class="layui-trans layadmin-user-login-footer">
    <#--<p>© 2018 <a href="http://www.layui.com/" target="_blank">layui.com</a></p>-->
        <p>
            <span>技术支持：Hr-Bs,wsj</span>
        </p>
    </div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.use(['form'], function () {
        var $ = layui.$
                , form = layui.form;
        form.render();
        form.on("submit(LAY-user-login-submit)", function (obj) {
            alert("ddddd");
            var username = $("#username").val();
            var password = $("#password").val();
            var randomcode = $("#randomcode").val();
//            var vcode = $("#vcode").val();
            var rememberMe = $('#rememberMe').is(':checked');
            $.post("/login/validate", {
                "username": username,
                "password": password,
                "randomcode": randomcode,
//                "vcode" : vcode,
                "rememberMe": rememberMe
            }, function (data) {
                if (data.status == '0') {
                    layer.msg('登录成功，进入系统', {
                        offset: '15px'
                        , icon: 1
                        , time: 500
                    }, function () {
                        top.location.href = '/';
                    });
                } else {
                    layer.msg(data.message, {offset: '15px', anim: 6, icon: 2, time: 2000});
                    reloadCode();
                }
            });
        });
        function reloadCode() {
            var vercode_img = $("#vercode_img").get(0);
            vercode_img.src = "/validateCodeServlet?t=" + new Date().getTime();
        }

        $("#vercode_img").click(function () {
            reloadCode();
        });
    });
</script>
</body>
</html>