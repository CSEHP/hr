<!DOCTYPE html>
<html class="loginHtml">
<head>
    <title>${(dataDictMap['系统名称'].value)!}</title>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <#--<link rel="stylesheet" href="/style/admin.css" media="all">-->
    <link rel="stylesheet" href="/style/public.css" media="all">
</head>
<body class="loginBody">
<div style="width: 100%;text-align: center;font-size: 35px;color: #ffffff;font-weight: bold;position: absolute;top:20%;">
    ${(dataDictMap['系统名称'].value)!}
</div>
<form class="layui-form">
    <div class="login_face"><img src="/images/face.jpg" class="userAvatar"></div>
    <div class="layui-form-item input-item">
        <label for="username">用户名</label>
        <input type="text" placeholder="请输入用户名" autocomplete="off" id="username" name="username" value="" class="layui-input" lay-verify="required">
    </div>
    <div class="layui-form-item input-item">
        <label for="password">密码</label>
        <input type="password" placeholder="请输入密码" autocomplete="off" id="password" name="password" value="" class="layui-input" lay-verify="required">
    </div>
    <div class="layui-form-item">
        <button type="button" id="submitBtn" class="layui-btn layui-btn-fluid" lay-submit lay-filter="LAY-user-login-submit">登 入</button>
    </div>
    <div class="layui-form-item layui-row">
        <a target="_blank">&nbsp;&nbsp;&nbsp;技术支持：Hr-Bs,wsj</a>
    </div>
</form>
<script src="/layui/layui.js" charset="utf-8"></script>
<script src="/lib/login.js" charset="utf-8"></script>
<script src="/plugins/jquery/jquery.min.js" charset="utf-8"></script>
<script src="/plugins/jquery/jquery.base64.js" charset="utf-8"></script>
<script>
    layui.use(['form'], function () {
        var $ = layui.$, form = layui.form;
        form.render();
        form.on("submit(LAY-user-login-submit)", function (obj) {
            var username = $.base64.encode($("#username").val());
            var password =  $.base64.encode($("#password").val());
            var randomcode =  $.base64.encode($("#randomcode").val());
//            var vcode = $("#vcode").val();
            var rememberMe = $('#rememberMe').is(':checked');
            $.post("/login/adminValidate", {
                "username": username,
                "password": password,
                // "randomcode": randomcode,
//                "vcode" : vcode,
                "rememberMe": rememberMe
            }, function (data) {
                if (data.status == '0') {
                    window.localStorage.setItem("lock", "");
                    layer.msg('登录成功，进入系统', {
                        offset: '15px'
                        , icon: 1
                        , time: 500
                    }, function () {
                        top.location.href = '/';
                    });
                } else {
                    layer.msg(data.message, {offset: '15px', anim: 6, icon: 2, time: 2000});
                    reloadCode();
                }
            });
        });
        function reloadCode() {
            var vercode_img = $("#vercode_img").get(0);
            vercode_img.src = "/validateCodeServlet?t=" + new Date().getTime();
        }

        $("#vercode_img").click(function () {
            reloadCode();
        });
        $("body").keydown(function () {
            if (event.keyCode == "13") {//keyCode=13是回车键
                $('#submitBtn').click();
            }
        });
    });
</script>
</body>
</html>