<!DOCTYPE html>
<html class="loginHtml">
<head>
    <title>${(dataDictMap['系统名称'].value)!}</title>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/jmdf.css" media="all">
    <link rel="stylesheet" href="/style/public.css" media="all">
    <script src="/plugins/jquery/jquery.min.js" charset="utf-8"></script>
    <script src="/plugins/jquery/jquery.base64.js" charset="utf-8"></script>
    <#if keyUsed>
        <script src="/plugins/ukey/mToken_K1.js" type="text/javascript"></script>
        <script src="/plugins/ukey/mToken_K1Plugin.js" type="text/javascript"></script>
        <script src="/plugins/ukey/base64.js" type="text/javascript"></script>
    </#if>
</head>
<body class="loginBody">
<#if keyUsed>
    <object id="mTokenPlugin" type="application/x-mtokenplugin" width="0" height="0">
        <param value="pluginLoaded"/>
    </object>
</#if>
<div style="width: 100%;text-align: center;font-size: 35px;color: white;font-weight: bold;position: absolute;top:20%;">
${(dataDictMap['系统名称'].value)!}
</div>
<form class="layui-form" style="height: 350px;">
    <div class="login_face"><img src="/images/face.jpg" class="userAvatar"></div>
    <#if keyUsed>
    <input type="hidden" name="clientDigest" id="clientDigest" value=""/>
    <input type="hidden" name="uuid" id="uuid" value=""/>
    </#if>
    <input type="text"  style="display: none;" disabled autocomplete = "off" />
    <input type="password"  style="display: none;" disabled autocomplete = "off" />

    <div class="layui-form-item layui-input-active">
        <label for="username">用户名</label>
        <input autocomplete="off" type="text" name="username" value="" class="layui-input" lay-verify="required"/>
    </div>
    <div class="layui-form-item">
        <label for="password">密码</label>
        <input autocomplete="off" type="text" onfocus="this.type='password'"  name="password" value="" class="layui-input" lay-verify="required" />
    </div>
    <#if (dataDictMap['验证码'].value == "1")>
    <div class="layui-form-item input-item" id="imgCode">
        <label for="code">验证码</label>
        <input type="text" placeholder="请输入验证码" autocomplete="off" id="randomcode" name="randomcode" class="layui-input" lay-verify="required">
        <img src="/validateCodeServlet" class="layadmin-user-login-codeimg" id="vercode_img" style="height: 100%;">
    </div>
    </#if>
     <span id="logining_info" style="height: 35px;display: inline-block;"></span>
    <div class="layui-form-item">
        <button type="button" id="submitBtn" class="layui-btn layui-btn-fluid" lay-submit lay-filter="LAY-user-login-submit">登 入</button>
    </div>
    <div class="layui-form-item layui-row">
        <a target="_blank">&nbsp;&nbsp;&nbsp;技术支持：Hr-Bs,wsj</a>
    </div>
</form>
<script src="/layui/layui.js" charset="utf-8"></script>
<script src="/lib/login.js" charset="utf-8"></script>

<script>
    layui.use(['form'], function () {
        var $ = layui.$, form = layui.form;
        form.render();
        form.on("submit(LAY-user-login-submit)", function (obj) {
            var username = $.base64.encode($("input[name='username']").val());
            var password =  $.base64.encode($("input[name='password']").val());
            var randomcode =  $.base64.encode($("#randomcode").val());
            var clientDigest =  $("#clientDigest").val();
            var rememberMe = $('#rememberMe').is(':checked');
            $.post("/login/validate", {
                "username": username,
                "password": password,
                "randomcode": randomcode,
                "clientDigest": clientDigest,
                "rememberMe": rememberMe
            }, function (data) {
                if (data.status == '0') {
                    window.localStorage.setItem("lock", "");
                    layer.msg('登录成功，进入系统', {
                        offset: '15px'
                        , icon: 1
                        , time: 500
                    }, function () {
                        top.location.href = '/main/main';
                    });
                } else {
                    layer.msg(data.message, {offset: '15px', anim: 6, icon: 2, time: 2000});
                    reloadCode();
                }
            });
        });
        function reloadCode() {
            var vercode_img = $("#vercode_img").get(0);
            vercode_img.src = "/validateCodeServlet?t=" + new Date().getTime();
        }

        $("#vercode_img").click(function () {
            reloadCode();
        });
        $("body").keydown(function () {
            if (event.keyCode == "13") {//keyCode=13是回车键
                $('#submitBtn').click();
            }
        });


        <#if keyUsed>
            $("input[name='username'").attr("readonly", "readonly");
            loadUser();
        </#if>

        //钥匙盘验证
        function loadUser() {
            K1mToken.LoadLibrary();
            var keyNumber = 0;
            keyNumber = K1mToken.K1_mTokenFindDevice();
            if (keyNumber < 1) {
                $("#logining_info").addClass("jmdf-red-notice");
                $("#logining_info").html("请插入钥匙盘");
                return false;
            }

            var keyUID = "";
            keyUID = K1mToken.K1_mTokenGetUID(1);
            if (keyUID == null || keyUID == "") {
                $("#logining_info").attr("class", "jmdf-red-notice");
                $("#logining_info").html("获取设备唯一硬件ID失败,错误码:" + K1mToken.K1_mTokenGetLastError());
                return;
            }

            document.getElementById("uuid").value = keyUID;


            $.get("loadUser", {guid: keyUID}, function (result) {
                if (result.status == "1") {
                    $("#logining_info").attr("class", "jmdf-color-green");
                    $("#logining_info").html("用户信息读取成功");
                    $("input[name='username']").val(result.username);

                    //进行客户端摘要的计算
                    var randomStr = result.randomStr;
                    var clientDigest = K1mToken.K1_mTokenSHA1WithSeed(keyUID, randomStr);
                    if (clientDigest == null || clientDigest == "") {
                        $("#logining_info").attr("class", "jmdf-red-notice");
                        $("#logining_info").html("钥匙盘错误，请联系技术支持");
                        return;
                    }
                    document.getElementById("clientDigest").value = clientDigest;

                    return true;
                } else {
                    $("#logining_info").attr("class", "jmdf-red-notice");
                    $("#logining_info").html("U-KEY不正确");
                    return false;
                }
            });
            return true;
        }
    });
</script>
</body>
</html>
