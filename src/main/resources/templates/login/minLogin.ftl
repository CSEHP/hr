<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
    <link rel="stylesheet" href="/style/login.css" media="all">
</head>
<body style="overflow: hidden;">
<#if keyUsed>
<object id="mTokenPlugin" type="application/x-mtokenplugin" width="0" height="0">
    <param value="pluginLoaded"/>
</object>
</#if>
<div class="layui-fluid">
    <div class="layui-row  layui-col-space10">
        <div class="layui-col-xs12">

        <#if keyUsed>
            <input type="hidden" name="clientDigest" id="clientDigest" value=""/>
            <input type="hidden" name="uuid" id="uuid" value=""/>
        </#if>
            <input type="text"  style="display: none;" disabled autocomplete = "off" />
            <input type="password"  style="display: none;" disabled autocomplete = "off" />

            <div class="layadmin-user-login-box layadmin-user-login-body layui-form">
                <div class="layui-form-item">
                    <label class="layadmin-user-login-icon layui-icon layui-icon-username"
                           for="LAY-user-login-username"></label>
                    <input type="text" name="username" id="username" value="admin" lay-verify="required"
                           placeholder="用户名" class="layui-input">
                </div>
                <div class="layui-form-item">
                    <label class="layadmin-user-login-icon layui-icon layui-icon-password"
                           for="LAY-user-login-password"></label>
                    <input type="password" name="password" id="password" value="123456" lay-verify="required"
                           placeholder="密码" class="layui-input">
                </div>
            <#if (dataDictMap['验证码'].value == "1")>
                <div class="layui-form-item">
                    <div class="layui-row">
                        <div class="layui-col-xs7">
                            <label class="layadmin-user-login-icon layui-icon layui-icon-vercode"
                                   for="LAY-user-login-vercode"></label>
                            <input type="text" name="randomcode" id="randomcode" lay-verify="required" placeholder="验证码"
                                   class="layui-input">
                        </div>
                        <div class="layui-col-xs5">
                            <div style="margin-left: 10px;">
                                <img src="/validateCodeServlet" class="layadmin-user-login-codeimg" id="vercode_img">
                            </div>
                        </div>
                    </div>
                </div>
            </#if>
                <span id="logining_info" style="height: 35px;display: inline-block;"></span>
                <div class="layui-form-item" style="margin-bottom: 20px;">
                <#--<input type="checkbox" name="rememberMe" id="rememberMe" lay-skin="primary" title="记住我">-->
                    <#--<a href="forget.html" class="layadmin-user-jump-change layadmin-link" style="margin-top: 7px;">忘记密码？</a>-->
                </div>
                <div class="layui-form-item">
                    <button id="submitBtn" class="layui-btn layui-btn-fluid" lay-submit
                            lay-filter="LAY-user-login-submit">登 入
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script src="/plugins/jquery/jquery.min.js" charset="utf-8"></script>
<script src="/plugins/jquery/jquery.base64.js" charset="utf-8"></script>
<script>
    layui.use(['form'], function () {
        var $ = layui.$
                , form = layui.form;
        form.render();
        var parentIndex = parent.layer.getFrameIndex(window.name);
        form.on("submit(LAY-user-login-submit)", function (obj) {
            var username = $.base64.encode($("#username").val());
            var password = $.base64.encode($("#password").val());
            var randomcode = $.base64.encode($("#randomcode").val());
            var clientDigest =  $("#clientDigest").val();
            var rememberMe = $('#rememberMe').is(':checked');
            $.post("/login/validate", {
                "username": username,
                "password": password,
                "randomcode": randomcode,
                "rememberMe": rememberMe,
                "clientDigest": clientDigest
            }, function (data) {
                if (data.status == '0') {
                    layer.msg("登录成功", {anim: 0, icon: 1});
                    parent.layer.close(parentIndex);
                } else {
                    layer.msg(data.message, {offset: '15px', anim: 6, icon: 2, time: 2000});
                    reloadCode();
                }
            });
        });
        function reloadCode() {
            var vercode_img = $("#vercode_img").get(0);
            vercode_img.src = "/validateCodeServlet?t=" + new Date().getTime();
        }

        $("#vercode_img").click(function () {
            reloadCode();
        });
        $("body").keydown(function () {
            if (event.keyCode == "13") {//keyCode=13是回车键
                $('#submitBtn').click();
            }
        });

    <#if keyUsed>
        $("input[name='username'").attr("readonly", "readonly");
        loadUser();
    </#if>

        //钥匙盘验证
        function loadUser() {
            K1mToken.LoadLibrary();
            var keyNumber = 0;
            keyNumber = K1mToken.K1_mTokenFindDevice();
            if (keyNumber < 1) {
                $("#logining_info").addClass("jmdf-red-notice");
                $("#logining_info").html("请插入钥匙盘");
                return false;
            }

            var keyUID = "";
            keyUID = K1mToken.K1_mTokenGetUID(1);
            if (keyUID == null || keyUID == "") {
                $("#logining_info").attr("class", "jmdf-red-notice");
                $("#logining_info").html("获取设备唯一硬件ID失败,错误码:" + K1mToken.K1_mTokenGetLastError());
                return;
            }

            document.getElementById("uuid").value = keyUID;


            $.get("loadUser", {guid: keyUID}, function (result) {
                if (result.status == "1") {
                    $("#logining_info").attr("class", "jmdf-color-green");
                    $("#logining_info").html("用户信息读取成功");
                    $("input[name='username']").val(result.username);

                    //进行客户端摘要的计算
                    var randomStr = result.randomStr;
                    var clientDigest = K1mToken.K1_mTokenSHA1WithSeed(keyUID, randomStr);
                    if (clientDigest == null || clientDigest == "") {
                        $("#logining_info").attr("class", "jmdf-red-notice");
                        $("#logining_info").html("钥匙盘错误，请联系技术支持");
                        return;
                    }
                    document.getElementById("clientDigest").value = clientDigest;

                    return true;
                } else {
                    $("#logining_info").attr("class", "jmdf-red-notice");
                    $("#logining_info").html("U-KEY不正确");
                    return false;
                }
            });
            return true;
        }
    });
</script>
</body>
</html>