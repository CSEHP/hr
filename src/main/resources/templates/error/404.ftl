<!DOCTYPE html>
<html>
<head>
    <title>404 错误提示</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layadmin-tips">
        <i class="layui-icon" face>&#xe664;</i>
        <div class="layui-text">
            <h1>
                <span class="layui-anim layui-anim-loop layui-anim-">4</span>
                <span class="layui-anim layui-anim-loop layui-anim-rotate">0</span>
                <span class="layui-anim layui-anim-loop layui-anim-">4</span>
            </h1>
            <h3>找不到页面</h3>
            <br/>
            <p>
                <button class="layui-btn layui-btn-danger" onclick="top.location.href='/login/mobileLogin';"><i class="layui-icon">&#xe65c;</i> 重新登录</button>
                <button class="layui-btn" onclick="history.back();"><i class="layui-icon">&#xe65c;</i> 返回</button>
            </p>
        </div>
    </div>
</div>
</body>
</html>
