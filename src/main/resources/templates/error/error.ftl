<!DOCTYPE html>
<html>
<head>
    <title>出错了</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layadmin-tips">
        <i class="layui-icon" face>&#xe664;</i>
        <div class="layui-text" style="font-size: 10px;">
            <#--<p>${url}</p>-->
            <h3>${message}</h3>
            <h3>${exception}</h3>
            <br/>
            <p>
                <button class="layui-btn layui-btn-danger" onclick="top.location.href='/login/logout';"><i class="layui-icon">&#xe65c;</i> 重新登录</button>
                <button class="layui-btn" onclick="history.back();"><i class="layui-icon">&#xe65c;</i> 返回</button>
            </p>
        </div>
    </div>
</div>
</body>
</html>