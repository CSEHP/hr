<!DOCTYPE html>
<html>
<head>
    <title>部门信息管理</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row  layui-col-space10">
        <div class="layui-col-xs12">
            <div class="layui-card">
                <div class="layui-card-header">部门信息管理</div>
                <div class="layui-card-body">
                    <form class="layui-form">
                        <input type="hidden" name="id" value="${(department.id)!}"/>
                        <input type="hidden" name="parentId" value="${(department.department.id)!}"/>
                        <div class="layui-form-item">
                            <label class="layui-form-label">父部门：</label>
                            <div class="layui-input-block">
                                <input type="text" disabled value="${(department.department.name)!}" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">部门名称：</label>
                            <div class="layui-input-block">
                                <input type="text" name="name" id="name" lay-verify="required" value="${(department.name)!}" autocomplete="off" placeholder="部门名称" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">部门编号：</label>
                            <div class="layui-input-block">
                                <input type="text" name="code" id="code" lay-verify="required|code" value="${(department.code)!}" autocomplete="off" placeholder="部门编号" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">排序：</label>
                            <div class="layui-input-block">
                                <input type="text" name="sortIndex" lay-verify="required|number" value="${(department.sortIndex)!}" autocomplete="off" placeholder="排序号" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">使用状态：</label>
                            <div class="layui-input-block">
                                <input type="radio" <#if department.state == 1>checked</#if> name="state" value="1" title="正常"/>
                                <input type="radio" <#if department.state == 2>checked</#if> name="state" value="2" title="锁定"/>
                            </div>
                        </div>
                        <button style="display: none;" class="layui-btn" id="submitBtn" name="submitBtn" lay-submit="" lay-filter="submit">立即提交</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.use(['form', 'jquery'], function () {
        var form = layui.form;
        var $ = layui.jquery;
        var parentIndex = parent.layer.getFrameIndex(window.name);
        //父页面通过submit按钮click方法，触发以下验证功能
        form.verify({
            code: function (value, item) {
                var msg = "";
                $.ajax({
                    url: "validateCode?code=" + value + "&id=${(department.id)!}"
                    , async: false
                    , success: function (data) {
                        if (data.code != "0001") {
                            msg = "部门编号重复";
                        }
                    }
                });
                return msg;
            }
        });
        form.on('submit(submit)', function (data) {
            $.ajax({
                url: "save"
                , type: "post"
                , data: $("form").serializeArray()
                , success: function (data) {
                    if (data.code == "0001") {
                        layer.alert("保存成功！")
                        parent.loadData();
                        parent.layer.close(parentIndex);
                    } else {
                        layer.msg(data.message, {anim: 6, icon: 2});
                    }
                }
            });
            return false;
        });
    });
</script>
</body>
</html>