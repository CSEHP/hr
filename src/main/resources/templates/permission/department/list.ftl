<!DOCTYPE html>
<html>
<head>
    <title>部门信息管理</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row  layui-col-space10">
        <div class="layui-col-xs12">
            <div class="layui-card">
                <div class="layui-card-header">部门信息管理</div>
                <div class="layui-card-body">
                    <button type="button" class="layui-btn layui-btn-sm refresh">刷新</button>
                    <button type="button" class="layui-btn layui-btn-sm open-all">展开</button>
                    <button type="button" class="layui-btn layui-btn-sm close-all">关闭</button>
                    <table class="layui-table layui-form" id="tree-table"></table>
                <#--<table id="dataTable" class="layui-table" lay-filter="layFilter"></table>-->
                    <!--序号-->
                    <script type="text/html" id="indexTpl">
                        {{d.LAY_TABLE_INDEX+1}}
                    </script>
                    <!--菜单-->
                    <script type="text/html" id="availableTpl">
                        <@shiro.hasPermission name="sysRole:setPermission">
                        <input type="checkbox" name="state" value="{{d.id}}" lay-skin="switch" lay-text="解锁|锁定"
                               lay-filter="availableFilter" {{# if(d.state== 1){ }} checked {{# } }}>
                        </@shiro.hasPermission>
                    </script>

                    <!-- 表格头部工具栏 -->
                    <script type="text/html" id="toolbar">
                        <div class="layui-btn-container right">
                            <button lay-event="reload" class="layui-btn layui-btn-sm"><i class="layui-icon">&#xe669;</i>重载
                            </button>
                        <@shiro.hasPermission name="department:form">
                            <button lay-event="add" class="layui-btn layui-btn-sm"><i class="layui-icon">&#xe654;</i>新增
                            </button>
                        </@shiro.hasPermission>
                        <@shiro.hasPermission name="sysRole:setPermission">
                            <button lay-event="del" class="layui-btn layui-btn-sm layui-btn-danger"><i
                                    class="layui-icon">&#xe640;</i>删除
                            </button>
                        </@shiro.hasPermission>
                        </div>
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.extend({
        jmdf: '/modules/jmdf'
        , treeTable: '/modules/treeTable/treeTable'
    }).use(['table', 'form', 'jquery', 'jmdf', 'treeTable'], function () {
        var $ = layui.jquery, treeTable = layui.treeTable, jmdf = layui.jmdf;
        /***********************table表格相关操作 start*************************/
        var tableRenderId = "renderId";
        var tableId = "tree-table";
        var tableUrl = "listJson";
        var rTable = null;
        //列项内容
        var cols = [
            {
                title: '操作',
                align: 'center',
                width: '120px',
                template: function (item) {
                    return '<a lay-filter="add" style="cursor: pointer;color: #01AAED;">添加</a> | <a lay-filter="edit" style="cursor: pointer;color: #00d20d;">修改</a> | <a lay-filter="del" style="cursor: pointer;color: #CF1900;">删除</a>';
                }
            },
            {
                key: 'code', title: '编号', width: '120px'
            },
            {
                key: 'title', title: '名称【排序】'
            }
        ];
        //table设置项
        var options = {
            elem: '#' + tableId//table列表id值
            , url: tableUrl
            , icon_key: 'title'
            , cols: cols
        };
        rTable = treeTable.render(options);
        treeTable.on('tree(add)', function (obj) {
            var data = obj.item;
            var pid = data.id;
            var url = 'form?pid=' + pid;
            jmdf.form(url, "80");
        });
        treeTable.on('tree(edit)', function (obj) {
            var data = obj.item;
            var id = data.id;
            var url = 'form?id=' + id;
            jmdf.form(url, "80");
        });
        treeTable.on('tree(del)', function (obj) {
            var data = obj.item;
            var id = data.id;
            var url = "del?id=" + id;
            jmdf.del(url);
        });
        $(".refresh").click(function () {
            loadData();
        });
        $(".open-all").click(function () {
            treeTable.openAll(rTable);
        });
        $(".close-all").click(function () {
            treeTable.closeAll(rTable);
        });
        function loadData() {
            treeTable.render(rTable);
        }

        window.loadData = loadData;
    });
</script>
</body>
</html>