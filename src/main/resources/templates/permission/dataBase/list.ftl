<!DOCTYPE html>
<html>
<head>
    <title>数据备份管理</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row  layui-col-space10">
        <div class="layui-col-xs12">
            <div class="layui-card">
                <div class="layui-card-header">数据备份管理
                    <span class="layui-badge layuiadmin-badge" id="statusSpan"></span>
                </div>
                <div class="layui-card-body">
                    <table id="dataTable" class="layui-table" lay-filter="layFilter"></table>
                    <!--序号-->
                    <script type="text/html" id="indexTpl">
                        {{d.LAY_TABLE_INDEX+1}}
                    </script>
                    <!--菜单-->
                    <script type="text/html" id="dataTable_toolbar">
                        <a class="layui-btn layui-btn-sm layui-btn-danger" lay-event="dataTable_download"><i class="layui-icon">&#xe601;</i>下载</a>
                    </script>
                    <!-- 表格头部工具栏 -->
                    <script type="text/html" id="toolbar">
                        <div class="layui-btn-container right">
                            <button lay-event="load" class="layui-btn layui-btn-sm"><i class="layui-icon">&#xe669;</i>重载</button>
                            <button lay-event="back" class="layui-btn layui-btn-sm"><i class="layui-icon">&#xe601;</i>手工备份</button>
                            <button lay-event="startCron" class="layui-btn layui-btn-normal layui-btn-sm"><i class="layui-icon">&#xe605;</i>备份任务启动</button>
                            <button lay-event="stopCron" class="layui-btn layui-btn-danger layui-btn-sm"><i class="layui-icon">&#x1006;</i>备份任务停止</button>
                        </div>
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.config({
        base: '/modules/' //静态资源所在路径
    }).use(['table', 'form', 'jquery', 'jmdf', 'page'], function () {
        var table = layui.table, form = layui.form, $ = layui.jquery, jmdf = layui.jmdf, page = layui.page;
        var tableRenderId = "renderId";
        var tableId = "dataTable";
        var table_layFilter = "layFilter";
        var tableUrl = "listJson";

        var cols = [[
            {type: "numbers", title: "序号", fixed: true, toolbar: '#indexTpl', align: 'center', width: 60}
            , {title: '操作', fixed: 'left', align: 'center', toolbar: '#dataTable_toolbar', width: 120}
            , {field: "fileDate", title: "时间", align: 'center', width: 220}
            , {field: "fileSize", title: "大小", align: 'left', width: 100}
            , {field: "fileName", title: "名称", align: 'left', width: 430}
            , {field: "filePath", title: "保存路径", align: 'left', width: 700}
        ]];
        //table设置项
        var options = {
            elem: '#' + tableId//table列表id值
            , even: false
            , url: tableUrl
            , toolbar: '#toolbar'//自定义工具栏
            , cols: cols
            , id: tableRenderId
//            ,height:'full-120'
            , defaultToolbar: ['filter', 'print']//显示列、打印
            , autoSort: false //禁用前端自动排序。
        };
        //初始表格（含分页设置）
        page.render(options);
        //表格排序
        page.sort(tableId, tableRenderId);
        //刷新当前页方法封装
        page.reload();

//        //分页查询（查询条件）
//        $("#searchButton").click(function () {
//            page.search("searchForm", tableRenderId);
//        });
        //table表格操作项
        table.on("tool(" + table_layFilter + ")", function (obj) {
            var data = obj.data;///获得当前行数据
            var layEvent = obj.event;//获得lay-event对应的值
            var id = data["id"];
            if (layEvent == "dataTable_download") {
                location.href = "down/" + id;
            }
        });
        //table表格头工具栏事件
        table.on('toolbar(' + table_layFilter + ')', function (obj) {
            switch (obj.event) {
                case 'load':
                    loadData();
                    break;
                case 'back':
                    jmdf.saveOrUpdate("get", "back", "");
                    break;
                case 'startCron':
                    jmdf.exec("/taskScheduler/startCron");
                    break;
                case 'stopCron':
                    jmdf.exec("/taskScheduler/stopCron");
                    break;
            }
        });

        function getStatus() {
            $.get("/taskScheduler/status", {}, function (data) {
                if (data.code == "0002") {
                    $("#statusSpan").removeClass("layui-bg-red").addClass("layui-bg-blue").html(data.message);
                } else {
                    $("#statusSpan").removeClass("layui-bg-blue").addClass("layui-bg-red").html(data.message);
                }
            });
        }

        getStatus();
        setInterval(getStatus, 2000);

        layer.open({
            type: 1
            , id: 'configTip'
            , title: "配置说明"
            , offset: "r"
            , shade: 0
            , closeBtn: 0
            , btn: ["关闭"]
            , btnAlign: 'c'
            , area: ['300px', '260px']
            , content: "<div style='padding: 5px;'><p>1.配置位置：</p><p>&nbsp;&nbsp;数据字典中，系统配置 > cronTrigger</p><p>2.cron表达式：</p><p>（1）每隔五秒执行一次：0/5 * * * * *</p><p>（2）每隔三十分钟执行一次：0 0/10 * * * *</p><p>（3）每隔一个小时执行一次：0 0 0/1 * * *</p></div>" //这里content是一个普通的String
        });
    });
</script>
</body>
</html>