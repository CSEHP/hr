<!DOCTYPE html>
<html>
<head>
    <title>数据字典配置</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row  layui-col-space10">
        <div class="layui-col-xs12">
            <div class="layui-card">
                <div class="layui-card-header">数据字典</div>
                <div class="layui-card-body">
                    <form class="layui-form" action="">
                        <input type="hidden" name="id" value="${(dataDict.id)!}"/>
                        <input type="hidden" name="parentId" value="${(dataDict.dataDict.id)!}"/>
                        <div class="layui-form-item">
                            <label class="layui-form-label">数据字典：</label>
                            <div class="layui-input-block"><input type="text" disabled value="${(dataDict.dataDict.name)!}" autocomplete="off" class="layui-input"></div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">名称：</label>
                            <div class="layui-input-block">
                                <input type="text" name="name" id="name" lay-verify="required" value="${(dataDict.name)!}" autocomplete="off" placeholder="请输入名称" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">颜色：</label>
                            <div class="layui-input-inline" style="width: 120px;">
                                <input type="text" value="${(dataDict.color)!}" placeholder="请选择颜色" class="layui-input" id="color" name="color">
                            </div>
                            <div class="layui-inline" style="left: -11px;">
                                <div id="test-form"></div>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">数值：</label>
                            <div class="layui-input-block">
                                <input type="text" name="value" id="value" lay-verify="required" value="${(dataDict.value)!}" autocomplete="off" placeholder="请输入数值" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">排序：</label>
                            <div class="layui-input-block">
                                <input type="text" name="sortIndex" lay-verify="required|number" value="${(dataDict.sortIndex)!}" autocomplete="off" placeholder="请输入排序号" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">是否可用：</label>
                            <div class="layui-input-block">
                                <input type="radio" <#if dataDict.enabled == 0>checked=""</#if> name="enabled" value="0" title="是"/>
                                <input type="radio" <#if dataDict.enabled == 1>checked=""</#if> name="enabled" value="1" title="否">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">备注：</label>
                            <div class="layui-input-block">
                                <textarea class="layui-textarea" name="memo">${(dataDict.memo)!}</textarea>
                            </div>
                        </div>
                        <button style="display: none;" class="layui-btn" id="submitBtn" name="submitBtn" lay-submit="" lay-filter="submit">立即提交</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.use(['form', 'jquery', 'colorpicker'], function () {
        var form = layui.form;
        var $ = layui.jquery;
        var colorpicker = layui.colorpicker;

        //表单赋值
        colorpicker.render({
            elem: '#test-form'
            , color: '#1c97f5'
            , done: function (color) {
                $('#color').val(color);
            }
        });


        var parentIndex = parent.layer.getFrameIndex(window.name);
        //父页面通过submit按钮click方法，触发以下验证功能
        form.on('submit(submit)', function (data) {
//            alert(JSON.stringify(data.field));
            $.ajax({
                url: "saveItem"
                , type: "post"
                , data: data.field
                , success: function (data) {
                    if (data.code == "0001") {
                        layer.alert("配置成功！")
                        parent.loadData();
                        parent.layer.close(parentIndex);
                    } else {
                        layer.msg(data.message, {anim: 6, icon: 2});
                    }

                }
            });
            return false;
        });
    });
</script>
</body>
</html>