<!DOCTYPE html>
<html>
<head>
    <title>数据字典配置</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row  layui-col-space10">
        <div class="layui-col-xs4">
            <div class="layui-card">
                <div class="layui-card-header">数据字典</div>
                <div class="layui-card-body">
                    <button id="dataLoad" class="layui-btn layui-btn-sm toolDataDict"><i class="layui-icon">&#xe669;</i>重载
                    </button>
                <@shiro.hasPermission name="dataDict:form">
                    <button id="dataAdd" class="layui-btn layui-btn-sm toolDataDict"><i class="layui-icon">&#xe654;</i>新增
                    </button>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="dataDict:deleteDataDict">
                    <button id="dataDel" class="layui-btn layui-btn-sm layui-btn-danger toolDataDict"><i
                            class="layui-icon">&#xe640;</i>删除
                    </button>
                </@shiro.hasPermission>
                    <table id="dataTable" class="layui-table" lay-filter="layFilter"></table>
                    <!--菜单-->
                    <script type="text/html" id="dataTable_toolbar">
                        <a class="layui-btn layui-btn-xs layui-btn-primary" lay-event="dataTable_view"><i
                                class="layui-icon">&#xe615;</i>管理</a>
                    </script>
                </div>
            </div>
        </div>
        <div class="layui-col-xs8">
            <!-- 填充内容 -->
            <div class="layui-card">
                <div class="layui-card-header" id="contentTitle">请选择左侧内容项</div>
                <div class="layui-card-body">
                <@shiro.hasPermission name="dataDict:formItem">
                    <button id="dataItemAdd" class="layui-btn layui-btn-sm toolDataDict"><i class="layui-icon">
                        &#xe654;</i>新增
                    </button>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="dataDict:deleteDataDict">
                    <button id="dataItemDel" class="layui-btn layui-btn-sm layui-btn-danger toolDataDict"><i
                            class="layui-icon">&#xe640;</i>删除
                    </button>
                </@shiro.hasPermission>
                    <table id="dataTableItem" class="layui-table" lay-filter="layFilterItem"></table>
                    <!--序号-->
                    <script type="text/html" id="indexTpl">
                        {{d.LAY_TABLE_INDEX+1}}
                    </script>
                    <script type="text/html" id="enabledTpl">
                        <@shiro.hasPermission name="dataDict:setEnabled">
                        <input type="checkbox" name="enabled" value="{{d.id}}" lay-skin="switch" lay-text="启用|禁用"
                               lay-filter="enabledFilter" {{# if(d.enabled== '0'){ }} checked {{#  } }}>
                        </@shiro.hasPermission>
                    </script>
                    <!--菜单-->
                    <script type="text/html" id="layFilterItem_toolbar">
                        <@shiro.hasPermission name="dataDict:formItem">
                        <a class="layui-btn layui-btn-xs layui-btn-primary" lay-event="dataTableItem_edit"><i
                                class="layui-icon layui-icon-edit"></i>编辑</a>
                        </@shiro.hasPermission>
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.config({
        base: '/modules/' //静态资源所在路径
    }).use(['table', 'form', 'jquery', 'jmdf'], function () {
        var table = layui.table, form = layui.form, $ = layui.jquery, jmdf = layui.jmdf;

        var dataID = "dataID", dataItemID = "dataItemID", selectDataDictId = null;

        table.render({
            elem: "#dataTable", url: "loadDataDict", id: dataID, page: false, height: "full-140", cellMinWidth: 80
            , cols: [[
                {checkbox: true, fixed: true}
                , {field: "name", title: "名称", align: 'left', edit: 'text'}
                , {field: "sortIndex", title: "排序", align: 'center', width: 80, edit: 'text'}
                , {title: '操作', fixed: 'right', align: 'center', toolbar: '#dataTable_toolbar', width: 90}
            ]]
        });
        table.render({
            elem: "#dataTableItem",
            url: "loadItemDataDict",
            id: dataItemID,
            page: false,
            height: "full-140",
            cellMinWidth: 80
            ,
            cols: [[
                {checkbox: true, fixed: true}
                , {title: "序号", toolbar: '#indexTpl', align: 'center', width: 60}
                , {
                    field: "name", title: "名称", align: 'left', edit: 'text', templet: function (d) {
                        return "<span style='color:" + d.color + "'>" + d.name + "</span>";
                    }
                }
                , {field: "value", title: "数值", align: 'left', edit: 'text'}
                , {title: "是否启用", align: 'center', toolbar: '#enabledTpl', width: 120}
                , {field: "sortIndex", title: "排序", align: 'center', width: 80, edit: 'text'}
                , {field: "memo", title: "备注", align: 'left', edit: 'text'}
                , {title: "操作", fixed: 'right', align: 'right', width: 100, toolbar: '#layFilterItem_toolbar'}
            ]]

        });

        //监听单元格编辑
        table.on('edit(layFilter)', function (obj) {
            editCell(obj);
        });
        //监听单元格编辑
        table.on('edit(layFilterItem)', function (obj) {
            editCell(obj);
        });
        function editCell(obj) {
            var value = obj.value; //得到修改后的值
            var data = obj.data; //得到所在行所有键值
            var field = obj.field; //得到字段

            if (field == "sortIndex") {
                var reg = /^\d+$/;
                if (!reg.test(value)) {
                    layer.msg("请输入正整数", {anim: 6, icon: 2});
                    return;
                }
            }

            var param = "id=" + data['id'] + "&" + field + "=" + value;
            jmdf.saveOrUpdate("post", "save", param);
        }

        table.on("tool(layFilter)", function (obj) {
            var data = obj.data;///获得当前行数据
            var layEvent = obj.event;//获得lay-event对应的值
            if (layEvent == "dataTable_view") {
                var id = data["id"];
                selectDataDictId = id;
                $("#contentTitle").html(data["name"]);
                table.reload(dataItemID, {
                    where: {
                        //结合后台框架，手工定义
                        parentId: id
                    }
                });
            }
        });
        table.on("tool(layFilterItem)", function (obj) {
            var data = obj.data;///获得当前行数据
            var layEvent = obj.event;//获得lay-event对应的值
            if (layEvent == "dataTableItem_edit") {
                var id = data["id"];
                var url = 'formItem?parentId=' + selectDataDictId + "&id=" + id;
                jmdf.form(url);
            }
        });
        form.on("switch(enabledFilter)", function (obj) {
//            layer.tips(this.value + ' ' + this.name + '：' + obj.elem.checked, obj.othis);
            var id = this.value;
            var value = obj.elem.checked ? 0 : 1;
            var url = "setEnabled?id=" + id + "&enabled=" + value;
            jmdf.saveOrUpdate("get", url, "");
        });

        $(".toolDataDict").on("click", function () {
            var id = $(this).attr("id");
            if (id == "dataLoad") {
                loadData();
            } else if (id == "dataAdd") {
                var url = 'form'
                jmdf.form(url);
            } else if (id == "dataDel") {
                deleteData(dataID);
            } else if (id == "dataItemAdd") {
                var url = 'formItem?parentId=' + selectDataDictId;
                jmdf.form(url);
            } else if (id == "dataItemDel") {
                deleteData(dataItemID);
            }
        });
        function loadData() {
            table.reload(dataID, {});//刷新
            table.reload(dataItemID, {});//刷新
        }

        window.loadData = loadData;

        function deleteData(ID) {
            var json = table.checkStatus(ID);
            if (json.data.length == 0) {
                layer.msg("请选择需要删除数据", {anim: 6, icon: 2});
                return;
            }
            var data = [];
            for (var i = 0; i < json.data.length; i++) {
                data.push(json.data[i].id);
            }
            var url = "deleteDataDict?ids=" + data.toString();
            jmdf.del(url);
        }
    });
</script>
</body>
</html>