<!DOCTYPE html>
<html>
<head>
    <title>角色信息配置</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row  layui-col-space10">
        <div class="layui-col-xs12">
            <div class="layui-card">
                <div class="layui-card-header">角色信息配置</div>
                <div class="layui-card-body">
                    <form class="layui-form" action="">
                        <input type="hidden" name="id" value="${(sysRole.id)!}"/>
                        <div class="layui-form-item">
                            <label class="layui-form-label">名称：</label>
                            <div class="layui-input-block">
                                <input type="text" name="role" id="role" lay-verify="required" value="${(sysRole.role)!}" autocomplete="off" placeholder="请输入名称" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">描述：</label>
                            <div class="layui-input-block">
                                <input type="text" name="description" id="description" lay-verify="required" value="${(sysRole.description)!}" autocomplete="off" placeholder="请输入名称" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">是否启用：</label>
                            <div class="layui-input-block">
                                <input type="radio" <#if sysRole.available == true>checked=""</#if> name="enabled" value="0" title="是"/>
                                <input type="radio" <#if sysRole.available == false>checked=""</#if> name="enabled" value="1" title="否">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">排序号：</label>
                            <div class="layui-input-block">
                                <input type="text" name="sortIndex" lay-verify="required|number" value="${(sysRole.sortIndex)!}" autocomplete="off" placeholder="请输入整数" class="layui-input">
                            </div>
                        </div>
                        <button style="display: none;" class="layui-btn" id="submitBtn" name="submitBtn" lay-submit="" lay-filter="submit">立即提交</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.use(['form', 'jquery'], function () {
        var form = layui.form;
        var $ = layui.jquery;
        var parentIndex = parent.layer.getFrameIndex(window.name);
        //父页面通过submit按钮click方法，触发以下验证功能
        form.on('submit(submit)', function (data) {
            $.ajax({
                url: "save"
                , type: "post"
                , data: data.field
                , success: function (data) {
                    if (data.code == "0001") {
                        layer.alert("配置成功！")
                        parent.loadData();
                        parent.layer.close(parentIndex);
                    } else {
                        layer.msg(data.message, {anim: 6, icon: 2});
                    }
                }
            });
            return false;
        });
    });
</script>
</body>
</html>