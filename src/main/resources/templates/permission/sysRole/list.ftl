<!DOCTYPE html>
<html>
<head>
    <title>角色信息管理</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row  layui-col-space10">
        <div class="layui-col-xs12">
            <div class="layui-card">
                <div class="layui-card-header">角色信息管理</div>
                <div class="layui-card-body">
                    <button id="load" class="layui-btn layui-btn-sm toolButton"><i class="layui-icon">&#xe669;</i>重载
                    </button>
                <@shiro.hasPermission name="sysRole:form">
                    <button id="add" class="layui-btn layui-btn-sm toolButton"><i class="layui-icon">&#xe654;</i>新增
                    </button>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="sysRole:deleteData">
                    <button id="del" class="layui-btn layui-btn-sm layui-btn-danger toolButton"><i class="layui-icon">
                        &#xe640;</i>删除
                    </button>
                </@shiro.hasPermission>
                    <table id="dataTable" class="layui-table" lay-filter="layFilter"></table>
                    <!--序号-->
                    <script type="text/html" id="indexTpl">
                        {{d.LAY_TABLE_INDEX+1}}
                    </script>
                    <!--菜单-->
                    <script type="text/html" id="dataTable_toolbar">
                        <@shiro.hasPermission name="sysRole:form">
                        <a class="layui-btn layui-btn-xs layui-btn-primary" lay-event="dataTable_edit"><i
                                class="layui-icon">&#xe642;</i>编辑</a>
                        </@shiro.hasPermission>
                        <@shiro.hasPermission name="sysRole:setPermission">
                        <a class="layui-btn layui-btn-xs layui-btn-primary" lay-event="dataTable_permission"><i
                                class="layui-icon">&#xe614;</i>分配权限</a>
                        </@shiro.hasPermission>
                    </script>
                    <script type="text/html" id="availableTpl">
                        <@shiro.hasPermission name="sysRole:setAvailable">
                        <input type="checkbox" name="enabled" value="{{d.id}}" lay-skin="switch" lay-text="启用|禁用"
                               lay-filter="availableFilter" {{# if(d.available){ }} checked {{# } }}>
                        </@shiro.hasPermission>
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.config({
        base: '/modules/' //静态资源所在路径
    }).use(['table', 'form', 'jquery', 'jmdf'], function () {
        var table = layui.table, form = layui.form, $ = layui.jquery, jmdf = layui.jmdf;
        var dataID = "dataID";

        table.render({
            elem: "#dataTable", url: "loadData", id: dataID, page: false, height: "full-140", cellMinWidth: 80
            , cols: [[
                {checkbox: true, fixed: true}
                , {title: "序号", toolbar: '#indexTpl', align: 'center', width: 60}
                , {field: "role", title: "名称", align: 'left', width: 220}
                , {field: "description", title: "描述", align: 'left'}
                , {title: "是否启用", align: 'center', toolbar: '#availableTpl', width: 120}
                , {field: "sortIndex", title: "排序", align: 'center', width: 80}
                , {title: '操作', fixed: 'right', align: 'center', toolbar: '#dataTable_toolbar', width: 200}
            ]]
        });

        table.on("tool(layFilter)", function (obj) {
            var data = obj.data;///获得当前行数据
            var layEvent = obj.event;//获得lay-event对应的值
            var id = data["id"];
            if (layEvent == "dataTable_edit") {
                formData(id);
            } else if (layEvent == "dataTable_permission") {
                var url = 'setPermission?id=' + id;
                jmdf.form(url);
            }
        });
        form.on("switch(availableFilter)", function (obj) {
            var id = this.value;
            var value = obj.elem.checked ? 0 : 1;
            var url = "setAvailable?id=" + id + "&available=" + value;
            jmdf.saveOrUpdate("get", url, "");
        });

        function loadData() {
            table.reload(dataID, {});//刷新
        }

        window.loadData = loadData;

        $(".toolButton").click(function () {
            var id = $(this).attr("id");
            if (id == "load") {
                loadData();
            } else if (id == "add") {
                formData(null);
            } else if (id == "del") {
                deleteData();
            }
        });
        function formData(id) {
            var url = 'form?id=' + id;
            jmdf.form(url);
        }

        function deleteData() {
            var json = table.checkStatus(dataID);
            if (json.data.length == 0) {
                layer.msg("请选择需要删除数据", {anim: 6, icon: 2});
                return;
            }
            var data = [];
            for (var i = 0; i < json.data.length; i++) {
                data.push(json.data[i].id);
            }
            var url = "deleteData?ids=" + data.toString();
            jmdf.del(url);
        }
    });
</script>
</body>
</html>