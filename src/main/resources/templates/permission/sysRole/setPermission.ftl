<!DOCTYPE html>
<html>
<head>
    <title>角色权限配置</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row  layui-col-space10">
        <div class="layui-col-xs12">
            <div class="layui-card">
                <div class="layui-card-header">角色权限配置</div>
                <div class="layui-card-body">
                    <form class="layui-form" action="">
                        <input type="hidden" name="id" value="${(sysRole.id)!}"/>
                        <div class="layui-form-item">
                            <label class="layui-form-label">名称：</label>
                            <div class="layui-input-block">${(sysRole.role)!}</div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">描述：</label>
                            <div class="layui-input-block">${(sysRole.description)!}</div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">权限分配：</label>
                            <div class="layui-input-block">
                                <div id="LAY-auth-tree-index"></div>
                            </div>
                        </div>
                        <button style="display: none;" class="layui-btn" id="submitBtn" name="submitBtn" lay-submit="" lay-filter="submit">立即提交</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.extend({
        authtree: '/layui/extend/authtree'
    }).use(['form', 'jquery', 'authtree'], function () {
        var form = layui.form;
        var $ = layui.jquery;
        var layer = layui.layer;
        var authtree = layui.authtree;
        var parentIndex = parent.layer.getFrameIndex(window.name);

        $.ajax({
            url: 'permissionJson?id=${sysRole.id}',
            dataType: 'json',
            success: function (data) {
                // 渲染时传入渲染目标ID，树形结构数据（具体结构看样例，checked表示默认选中），以及input表单的名字
                var list = data.list;
                var checkedIds = data.checkedIds;
                var trees = authtree.listConvert(list, {
                    primaryKey: 'id'
                    , startPid: 0
                    , parentKey: 'pid'
                    , nameKey: 'name'
                    , valueKey: 'id'
                    , checkedKey: checkedIds//自动匹配已选元素
                });
                authtree.render('#LAY-auth-tree-index', trees, {
                    inputname: 'authids[]'
                    , layfilter: 'lay-check-auth'
                    , dbltimeout: 120
                    , openall: true
                    , checkSkin: 'primary'
                    , autowidth: true
                });
                // 使用 authtree.on() 不会有冒泡延迟
                authtree.on('change(lay-check-auth)', function (data) {
                    // 获取所有已选中节点
                    var checked = authtree.getChecked('#LAY-auth-tree-index');
                    console.log('checked', checked);
                });
            },
            error: function (xml, errstr, err) {
                layer.alert(errstr + '，获取样例数据失败，请检查是否部署在本地服务器中！');
            }
        });
        //父页面通过submit按钮click方法，触发以下验证功能
        form.on('submit(submit)', function (data) {

            var authids = authtree.getChecked('#LAY-auth-tree-index');
            data.field.authids = authids.join("、");

            var roleId = "${sysRole.id}";
            data.field.roleId = roleId;

            $.ajax({
                url: "saveRolePermission"
                , type: "post"
                , data: data.field
                , success: function (data) {
                    if (data.code == "0001") {
                        layer.alert("配置成功！")
                        parent.loadData();
                        parent.layer.close(parentIndex);
                    } else {
                        layer.msg(data.message, {anim: 6, icon: 2});
                    }
                }
            });
            return false;
        });
    })
    ;
</script>
</body>
</html>