<!DOCTYPE html>
<html>
<head>
    <title>Shiro过滤链配置</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
</head>
<body>
<#setting number_format="#">
<div class="layui-fluid">
    <div class="layui-row layui-col-space10">
        <div class="layui-col-xs12">
            <!-- 填充内容 -->
            <div class="layui-card">
                <div class="layui-card-header">Shiro过滤链配置</div>
                <div class="layui-card-body layui-col-space1">
                    <form class="layui-form" action="">
                        <input type="hidden" name="id" value="${(sysPermissionInit.id)!}"/>
                        <div class="layui-form-item">
                            <label class="layui-form-label">链接地址：</label>
                            <div class="layui-input-block">
                                <input type="text" name="url" id="url" lay-verify="required" value="${(sysPermissionInit.url)!}" autocomplete="off" placeholder="请输入链接地址" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">具备的权限：</label>
                            <div class="layui-input-block">
                                <input type="text" name="permissionInit" lay-verify="required" value="${(sysPermissionInit.permissionInit)!}" autocomplete="off" placeholder="请输入具备的权限" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">排序号：</label>
                            <div class="layui-input-block">
                                <input type="text" name="sortIndex" lay-verify="required|number" value="${(sysPermissionInit.sortIndex)!}" autocomplete="off" placeholder="请输入整数" class="layui-input">
                            </div>
                        </div>
                        <!--<div class="layui-form-item">-->
                        <!--<div class="layui-input-block">-->
                        <button style="display: none;" class="layui-btn" id="submitBtn" name="submitBtn" lay-submit="" lay-filter="submit">立即提交</button>
                        <!--</div>-->
                        <!--</div>-->
                    </form>
                </div>
            </div>
            <div class="layui-card">
                <div class="layui-card-header">具备的权限</div>
                <div class="layui-card-body layui-col-space1">
                    <table class="layui-table">
                        <thead>
                        <tr>
                            <th>名称</th>
                            <th>说明</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>anon</td>
                            <td>匿名</td>
                        </tr>
                        <tr>
                            <td>authc</td>
                            <td>身份验证</td>
                        </tr>
                        <tr>
                            <td>authcBasic</td>
                            <td>http基本验证</td>
                        </tr>
                        <tr>
                            <td>logout</td>
                            <td>退出</td>
                        </tr>
                        <tr>
                            <td>noSessionCreation</td>
                            <td>不创建session</td>
                        </tr>
                        <tr>
                            <td>perms</td>
                            <td>许可验证</td>
                        </tr>
                        <tr>
                            <td>port</td>
                            <td>端口验证</td>
                        </tr>
                        <tr>
                            <td>rest</td>
                            <td>rest方面</td>
                        </tr>
                        <tr>
                            <td>roles</td>
                            <td>权限验证</td>
                        </tr>
                        <tr>
                            <td>ssl</td>
                            <td>ssl方面</td>
                        </tr>
                        <tr>
                            <td>user</td>
                            <td>用户方面</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.use(['form', 'jquery'], function () {
        var form = layui.form;
        var $ = layui.jquery;
        var parentIndex = parent.layer.getFrameIndex(window.name);
        //父页面通过submit按钮click方法，触发以下验证功能
        form.on('submit(submit)', function (data) {
            $.ajax({
                url: "save"
                , type: "post"
                , data: data.field
                , success: function (data) {
                    layer.alert("配置成功！")
                    parent.loadData();
                    parent.layer.close(parentIndex);
                }
            });
            return false;
        });
    });
</script>
</body>
</html>