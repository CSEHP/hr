<!DOCTYPE html>
<html>
<head>
    <title>Shiro过滤链配置</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row layui-col-space10">
        <div class="layui-col-xs12">
            <!-- 填充内容 -->
            <div class="layui-card">
                <div class="layui-card-header">Shiro过滤链配置</div>
                <div class="layui-card-body">
                <@shiro.hasPermission name="sysPermissionInit:updatePermission">
                    <button id="updatePermissionBtn" class="layui-btn layui-btn-sm"><i class="layui-icon">&#xe669;</i>
                        更新过滤链配置
                    </button>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="sysPermissionInit:form">
                    <button id="addPermissionBtn" class="layui-btn layui-btn-sm"><i class="layui-icon">&#xe654;</i>
                        新增过滤链
                    </button>
                </@shiro.hasPermission>
                    <table id="dataTable" class="layui-table" lay-filter="layFilter"></table>
                    <!--菜单-->
                    <script type="text/html" id="dataTable_toolbar">
                        <@shiro.hasPermission name="sysPermissionInit:form">
                        <a class="layui-btn layui-btn-xs" lay-event="edit"><i class="layui-icon">&#xe642;</i>编辑</a>
                        </@shiro.hasPermission>
                        <@shiro.hasPermission name="sysPermissionInit:del">
                        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del"><i class="layui-icon">
                            &#xe640;</i>删除</a>
                        </@shiro.hasPermission>
                    </script>
                    <!--序号-->
                    <script type="text/html" id="indexTpl">
                        {{d.LAY_TABLE_INDEX+1}}
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.config({
        base: '/modules/' //静态资源所在路径
    }).use(['table', 'form', 'jquery', "jmdf"], function () {
        var table = layui.table;
        var form = layui.form;
        var $ = layui.jquery;
        var jmdf = layui.jmdf;

        table.render({
            elem: "#dataTable"
            , url: "loadData"
            , id: 'dataID'
            , cols: [[
//                {checkbox: true, fixed: true, rowspan: 2},
                {title: "序号", templet: '#indexTpl', align: 'center', width: 80}
                , {field: "url", title: "链接地址", align: 'left'}
                , {field: "permissionInit", title: "权限", align: 'left', width: 180}
                , {field: "sortIndex", title: "排序", align: 'left', width: 80}
                , {title: '操作', fixed: 'right', toolbar: '#dataTable_toolbar', width: 150}
            ]]
            , page: false
            , height: "full-150"
            , cellMinWidth: 80
        });
        function loadData() {
            table.reload("dataID", {});//刷新
        }

        window.loadData = loadData;

        function editForm(id) {
            var url = 'form?id=' + id;
            jmdf.form(url);
        }

        table.on("tool(layFilter)", function (obj) {
            var data = obj.data;///获得当前行数据
            var layEvent = obj.event;//获得lay-event对应的值
            if (layEvent == "edit") {
                editForm(data.id);
            } else if (layEvent == "del") {
                var url = "del?id=" + data.id;
                jmdf.del(url);
            }
        });

        $("#updatePermissionBtn").click(function () {
            jmdf.saveOrUpdate("get", "updatePermission", "");
        });
        $("#addPermissionBtn").click(function () {
            editForm();
        });
    });
</script>
</body>
</html>