<!DOCTYPE html>
<html>
<head>
    <title>功能信息</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row  layui-col-space10">
        <div class="layui-col-xs12">
            <div class="layui-card">
                <div class="layui-card-header">功能信息</div>
                <div class="layui-card-body">
                    <form class="layui-form" action="">
                        <input type="hidden" name="id" value="${(sysPermission.id)!}"/>
                        <input type="hidden" name="parentId" value="${(sysPermission.sysPermission.id)!}"/>
                        <div class="layui-form-item">
                            <label class="layui-form-label">分配角色：</label>
                            <div class="layui-input-block">
                            <#list sysRoleList as sysRole>
                                <input type="checkbox" name="role" value="${sysRole.id}" lay-skin="primary" title="${sysRole.role}" <#if roleIds?seq_contains(sysRole.id)>checked</#if>/>
                            </#list>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">上级菜单名称：</label>
                            <div class="layui-input-block"><input type="text" disabled value="${(sysPermission.sysPermission.name)!}" autocomplete="off" class="layui-input"></div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">菜单名称：</label>
                            <div class="layui-input-block">
                                <input type="text" name="name" id="name" lay-verify="required" value="${(sysPermission.name)!}" autocomplete="off" placeholder="请输入" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">菜单图标：</label>
                            <div class="layui-input-block">
                                <input type="text" name="icon" id="iconPicker" lay-filter="iconPicker" lay-verify="required" value="${(sysPermission.icon)!}" autocomplete="off" placeholder="" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">权限字符串：</label>
                            <div class="layui-input-block">
                                <input type="text" name="permission" id="permission" value="${(sysPermission.permission)!}" autocomplete="off" placeholder="请输入" class="layui-input">
                                <div class="layui-form-mid layui-word-aux">示例：userInfo:add、userInfo:del等,多个之间用引文“;”分隔</div>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">资源路径：</label>
                            <div class="layui-input-block">
                                <input type="text" name="url" id="url" value="${(sysPermission.url)!}" autocomplete="url" placeholder="请输入" class="layui-input">
                                <div class="layui-form-mid layui-word-aux">示例：userInfo/add、userInfo/del等</div>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">排序：</label>
                            <div class="layui-input-block">
                                <input type="text" name="sortIndex" lay-verify="required|number" value="${(sysPermission.sortIndex)!}" autocomplete="off" placeholder="请输入" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">是否可用：</label>
                            <div class="layui-input-block">
                                <input type="radio" <#if sysPermission.enabled == 0>checked=""</#if> name="enabled" value="0" title="是"/>
                                <input type="radio" <#if sysPermission.enabled == 1>checked=""</#if> name="enabled" value="1" title="否">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">备注：</label>
                            <div class="layui-input-block">
                                <textarea class="layui-textarea" name="memo">${(sysPermission.memo)!}</textarea>
                            </div>
                        </div>
                        <button style="display: none;" class="layui-btn" id="submitBtn" name="submitBtn" lay-submit="" lay-filter="submit">立即提交</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.extend({
        iconPicker: '/modules/iconPicker/iconPicker'
    }).use(['form', 'jquery', 'iconPicker'], function () {
        var form = layui.form;
        var $ = layui.jquery;
        var iconPicker = layui.iconPicker;
        var parentIndex = parent.layer.getFrameIndex(window.name);
        //父页面通过submit按钮click方法，触发以下验证功能
        form.on('submit(submit)', function (data) {
//            alert(JSON.stringify(data.field));
            $.ajax({
                url: "saveMenu"
                , type: "post"
                , data: $("form").serializeArray()
                , success: function (data) {
                    if (data.code == "0001") {
                        layer.alert("配置成功！")
                        parent.loadData();
                        parent.layer.close(parentIndex);
                    } else {
                        layer.msg(data.message, {anim: 6, icon: 2});
                    }

                }
            });
            return false;
        });

        iconPicker.render({
            // 选择器，推荐使用input
            elem: '#iconPicker',
            // 数据类型：fontClass/unicode，推荐使用fontClass
            type: 'fontClass',
            // 是否开启搜索：true/false
            search: true,
            // 是否开启分页
            page: false,
//            // 每页显示数量，默认12
//            limit: 12,
            // 点击回调
            click: function (data) {
//                console.log(data);
            }
        });
        iconPicker.checkIcon('iconPicker', '${(sysPermission.icon)!}');


    });
</script>
</body>
</html>