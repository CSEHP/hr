<!DOCTYPE html>
<html>
<head>
    <title>菜单功能管理</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
    <link rel="stylesheet" href="/plugins/ztree/css/zTreeStyle/zTreeStyle.css" media="all">
    <link rel="stylesheet" href="/style/rMenu.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row layui-col-space10">
        <div class="layui-col-md3">
            <div class="layui-card">
                <div class="layui-card-header">菜单管理</div>
                <div class="layui-card-body">
                    <div class="layui-row">
                    <@shiro.hasPermission name="sysPermission:clearShiroCatch">
                        <button id="button_clearCatch" class="layui-btn layui-btn-sm buttonTool"><i class="layui-icon">
                            &#xe9aa;</i>更新权限
                        </button>
                    </@shiro.hasPermission>
                        <ul id="treeMenu" class="ztree fsTree" method="get" isRoot="1" url="loadTreeData"
                            clickCallbackId="fsDatagrid" clickCallbackInputs="menuId:$id" treeIdKey="id"
                            treePIdKey="pId" treeName="name"></ul>
                    </div>
                </div>
            </div>
            <div id="rMenu">
                <div class="layui-btn-group">
                <@shiro.hasPermission name="sysPermission:formMenu">
                    <button id="menu_add" class="layui-btn layui-btn-primary layui-btn-sm buttonTool"><i
                            class="layui-icon"></i>添加
                    </button>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="sysPermission:formMenu">
                    <button id="menu_update" class="layui-btn layui-btn-primary layui-btn-sm buttonTool"><i
                            class="layui-icon"></i>修改
                    </button>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="sysPermission:deleteMenuData">
                    <button id="menu_delete" class="layui-btn layui-btn-primary layui-btn-sm buttonTool"><i
                            class="layui-icon"></i>删除
                    </button>
                </@shiro.hasPermission>
                </div>
            </div>
        </div>
        <div class="layui-col-md9">
            <div class="layui-card">
                <div class="layui-card-header">功能列表</div>
                <div class="layui-card-body">
                <@shiro.hasPermission name="sysPermission:formButton">
                    <button id="button_add" class="layui-btn layui-btn-sm buttonTool"><i class="layui-icon">&#xe654;</i>新增
                    </button>
                </@shiro.hasPermission>
                <@shiro.hasPermission name="sysPermission:deleteMenuData">
                    <button id="button_delete" class="layui-btn layui-btn-sm layui-btn-danger buttonTool"><i
                            class="layui-icon">&#xe640;</i>删除
                    </button>
                </@shiro.hasPermission>
                    <table class="layui-table" id="dataTable" lay-filter="layFilter">
                    </table>
                    <!--序号-->
                    <script type="text/html" id="indexTpl">
                        {{d.LAY_TABLE_INDEX+1}}
                    </script>
                    <!--菜单-->
                    <script type="text/html" id="dataTable_toolbar">
                        <@shiro.hasPermission name="sysPermission:formButton">
                        <a class="layui-btn layui-btn-xs layui-btn-primary" lay-event="dataTable_edit"><i
                                class="layui-icon">&#xe642;</i>编辑</a>
                        </@shiro.hasPermission>
                    </script>
                    <script type="text/html" id="enabledTpl">
                        <@shiro.hasPermission name="sysPermission:setEnabled">
                        <input type="checkbox" name="enabled" value="{{d.id}}" lay-skin="switch" lay-text="启用|禁用"
                               lay-filter="enabledFilter" {{# if(d.enabled== '0'){ }} checked {{#  } }}>
                        </@shiro.hasPermission>
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script src="/plugins/jquery/jquery.min.js" charset="utf-8"></script>
<script src="/plugins/ztree/js/jquery.ztree.all.min.js" charset="utf-8"></script>
<script>
    layui.config({
        base: '/modules/' //静态资源所在路径
    }).use(["table", "jmdf"], function () {
                var table = layui.table, form = layui.form, $ = layui.jquery, jmdf = layui.jmdf;

                var dataID = "dataID", refreshIndex = 0;
                var zTree, rMenu;

                table.render({
                    elem: "#dataTable", url: "loadButtonData", id: dataID, page: false, height: "full-140", cellMinWidth: 80
                    , cols: [[
                        {checkbox: true, fixed: true}
                        , {title: "序号", toolbar: '#indexTpl', align: 'center', width: 60}
                        , {field: "name", title: "功能名称", align: 'left', edit: 'text', width: 120}
                        , {field: "permission", title: "权限字符串", align: 'left', edit: 'text'}
                        , {field: "url", title: "资源路径.", align: 'left', edit: 'text'}
                        , {title: "是否启用", align: 'center', toolbar: '#enabledTpl', width: 100}
                        , {field: "sortIndex", title: "排序", align: 'center', width: 80, edit: 'text'}
                        , {field: "memo", title: "备注", align: 'left', edit: 'text', width: 120}
                        , {title: '操作', fixed: 'right', align: 'center', toolbar: '#dataTable_toolbar', width: 90}
                    ]]
                });
                function loadData() {
                    if (refreshIndex == 1) {
                        zTree.reAsyncChildNodes(null, "refresh", true);
                    }
                    table.reload(dataID, {});
                    refreshIndex = 0;
                }

                window.loadData = loadData;

                //监听单元格编辑
                table.on('edit(layFilter)', function (obj) {
                    editCell(obj);
                });

                function editCell(obj) {
                    var value = obj.value; //得到修改后的值
                    var data = obj.data; //得到所在行所有键值
                    var field = obj.field; //得到字段

                    if (field == "sortIndex") {
                        var reg = /^\d+$/;
                        if (!reg.test(value)) {
                            layer.msg("请输入正整数", {anim: 6, icon: 2});
                            return;
                        }
                    }
                    var nodes = zTree.getSelectedNodes();
                    if (nodes == "" || nodes == null) {
                        layer.msg("请选择功能所对应的菜单", {anim: 6, icon: 2});
                        loadData();
                        return;
                    }
                    var parentId = nodes[0].id;//id
                    var param = "id=" + data['id'] + "&parentId=" + parentId + "&" + field + "=" + value;
                    jmdf.saveOrUpdate("post", "saveCellEditButton", param);
                }

                form.on("switch(enabledFilter)", function (obj) {
                    var id = this.value;
                    var value = obj.elem.checked ? 0 : 1;
                    var url = "setEnabled?id=" + id + "&enabled=" + value;
                    jmdf.saveOrUpdate("get", url, "");
                });
                table.on("tool(layFilter)", function (obj) {
                    var data = obj.data;///获得当前行数据
                    var layEvent = obj.event;//获得lay-event对应的值
                    var id = data["id"];
                    var parentId = data["parentId"];
                    if (layEvent == "dataTable_edit") {
                        var url = 'formButton?parentId=' + parentId + "&id=" + id;
                        jmdf.form(url);
                    }
                });
                $(".buttonTool").click(function () {
                    var id = $(this).attr("id");
                    refreshIndex = 0;
                    if (id.indexOf("menu_") >= 0) {
                        refreshIndex = 1;
                    }
                    if (id == "menu_add") {
                        addOrUpdateMenuData(1);
                    } else if (id == "menu_update") {
                        addOrUpdateMenuData(2);
                    } else if (id == "menu_delete") {
                        deleteMenu();
                    } else if (id == "button_add") {
                        addButtonData();
                    } else if (id == "button_delete") {
                        deleteButtonData(dataID);
                    } else if (id == "button_clearCatch") {
                        clearCatch();
                    }
                });
                function clearCatch() {
                    jmdf.saveOrUpdate("post", "clearShiroCatch", "");
                }

                function addOrUpdateMenuData(type) {
                    var nodes = zTree.getSelectedNodes();
                    var id = null;
                    if (nodes.length > 0) {
                        id = nodes[0].id;//id
                    }
                    var url = 'formMenu';
                    if (type == 1) {//新增
                        url += '?parentId=' + id;
                    } else if (type == 2) {//修改
                        url += '?id=' + id;
                    }
                    jmdf.form(url);
                }

                function deleteMenu() {
                    var nodes = zTree.getSelectedNodes();
                    var id = nodes[0].id;//id
                    if (id == null) {
                        layer.msg("根目录不允许删除", {anim: 6, icon: 2});
                    } else {
                        var url = "deleteMenuData?id=" + id;
                        jmdf.del(url);
                    }
                }

                function addButtonData() {
                    var nodes = zTree.getSelectedNodes();
                    if (nodes.length > 0) {
                        var id = nodes[0].id;//id
                        if (id == null) {
                            layer.msg("请选择需要添加功能的菜单", {anim: 6, icon: 2});
                        } else {
                            var url = 'formButton?parentId=' + id;
                            jmdf.form(url);
                        }
                    } else {
                        layer.msg("请选择需要添加功能的菜单", {anim: 6, icon: 2});
                    }
                }

                function deleteButtonData(ID) {
                    var json = table.checkStatus(ID);
                    if (json.data.length == 0) {
                        layer.msg("请选择需要删除数据", {anim: 6, icon: 2});
                        return;
                    }
                    var data = [];
                    for (var i = 0; i < json.data.length; i++) {
                        data.push(json.data[i].id);
                    }
                    var url = "deleteButtonData?ids=" + data.toString();
                    jmdf.del(url);
                }

                // zTree 的参数配置，深入使用请参考 API 文档（setting 配置详解）
                var setting = {
                    view: {
                        dblClickExpand: false
                        , nameIsHTML: true
                    },
                    check: {
                        enable: false
                    },
                    callback: {
                        onRightClick: OnRightClick
                        , onClick: onClick,
                    }
                    , async: {
                        enable: true
                        , type: "get"
                        , url: "loadTreeData"
                        , autoParam: ["id", "name"]
                    }
                };

                function OnRightClick(event, treeId, treeNode) {
//            if (!treeNode && event.target.tagName.toLowerCase() != "button" && $(event.target).parents("a").length == 0) {
//                zTree.cancelSelectedNode();
//                showRMenu("root", event.clientX, event.clientY);
//            } else if (treeNode && !treeNode.noR) {
                    zTree.selectNode(treeNode);
                    onClick();
                    showRMenu(event.clientX, event.clientY);
//            }
                }

                function onClick() {
                    var nodes = zTree.getSelectedNodes();
                    if (nodes.length > 0) {
                        var id = nodes[0].id;//id
                        table.reload(dataID, {
                            where: {
                                //结合后台框架，手工定义
                                parentId: id
                            }
                        });
                    }
                }

                function onBodyMouseDown(event) {
                    if (!(event.target.id == "rMenu" || $(event.target).parents("#rMenu").length > 0)) {
                        rMenu.css({"visibility": "hidden"});
                    }
                }

                function showRMenu(x, y) {
                    $("#rMenu").show();

                    y += document.body.scrollTop;
                    x += document.body.scrollLeft;
                    rMenu.css({"top": y + "px", "left": x + "px", "visibility": "visible"});

                    $("body").bind("mousedown", onBodyMouseDown);
                }

                function hideRMenu() {
                    if (rMenu) rMenu.css({"visibility": "hidden"});
                    $("body").unbind("mousedown", onBodyMouseDown);
                }

                function resetTree() {
                    hideRMenu();
                    $.fn.zTree.init($("#treeMenu"), setting);
                }

                zTree = $.fn.zTree.init($("#treeMenu"), setting);
                rMenu = $("#rMenu");
            }
    );
</script>
</body>
</html>