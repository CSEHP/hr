<!DOCTYPE html>
<html>
<head>
    <title>用户信息管理</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
    <link rel="stylesheet" href="/modules/formSelects/formSelects-v4.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row  layui-col-space10">
        <div class="layui-col-xs12">
            <div class="layui-card">
                <div class="layui-card-header">用户信息管理</div>
                <div class="layui-card-body">
                    <form class="layui-form searchForm" id="searchForm">
                        <div class="layui-inline layui-hide-xs">所属部门：</div>
                        <div class="layui-inline layui-hide-xs">
                            <select name="search_eq_integer_d.id" xm-select="departmentSelect" xm-select-radio="true">
                                <option value="">--</option>
                            </select>
                        </div>
                        <div class="layui-inline layui-hide-xs">使用角色：</div>
                        <div class="layui-inline layui-hide-xs" style="width: 100px;">
                            <select name="search_eq_integer_r.id">
                                <option value="">--</option>
                            <#list sysRoleList as sysRole>
                                <option value="${sysRole.id}">${sysRole.role}</option>
                            </#list>
                            </select>
                        </div>
                        <div class="layui-inline layui-hide-xs">真实姓名：</div>
                        <div class="layui-inline layui-hide-xs">
                            <input class="layui-input" name="search_like_string_u.name" id="demoReload"
                                   autocomplete="off" style="width: 150px;">
                        </div>
                        <div class="layui-inline">用户名：</div>
                        <div class="layui-inline">
                            <input class="layui-input" name="search_like_string_u.username" id="username"
                                   autocomplete="off" style="width: 150px;">
                        </div>
                        <div class="layui-inline layui-hide-xs">有效状态：</div>
                        <div class="layui-inline layui-hide-xs" style="width: 100px;">
                            <select name="search_eq_integer_u.state" id="state">
                                <option value="">--</option>
                            </select>
                        </div>
                        <button class="layui-btn" type="button" id="searchButton"><i
                                class="layui-icon layui-icon-search"></i> 搜索
                        </button>
                    </form>
                    <table id="dataTable" class="layui-table" lay-filter="layFilter"></table>
                    <!--序号-->
                    <script type="text/html" id="indexTpl">
                        {{d.LAY_TABLE_INDEX+1}}
                    </script>
                    <!--菜单-->
                    <script type="text/html" id="dataTable_toolbar">
                        <@shiro.hasPermission name="userInfo:form">
                        <a class="layui-btn layui-btn-xs layui-btn-primary" lay-event="dataTable_edit"><i
                                class="layui-icon">&#xe642;</i>修改</a>
                        </@shiro.hasPermission>
                        <@shiro.hasPermission name="userInfo:pwdForm">
                        <a class="layui-btn layui-btn-xs layui-btn-primary" lay-event="dataTable_pwd"><i
                                class="layui-icon">&#xe642;</i>密码</a>
                        </@shiro.hasPermission>
                    </script>
                    <script type="text/html" id="availableTpl">
                        <@shiro.hasPermission name="userInfo:setState">
                        <input type="checkbox" name="state" value="{{d.id}}" lay-skin="switch" lay-text="解锁|锁定"
                               lay-filter="availableFilter" {{# if(d.state== 1){ }} checked {{# } }}>
                        </@shiro.hasPermission>
                    </script>

                    <!-- 表格头部工具栏 -->
                    <script type="text/html" id="toolbar">
                        <div class="layui-btn-container right">
                            <button lay-event="reload" class="layui-btn layui-btn-sm"><i class="layui-icon">&#xe669;</i>重载
                            </button>
                        <@shiro.hasPermission name="userInfo:form">
                            <button lay-event="add" class="layui-btn layui-btn-sm"><i class="layui-icon">&#xe654;</i>新增
                            </button>
                        </@shiro.hasPermission>
                        <@shiro.hasPermission name="userInfo:delById">
                            <button lay-event="del" class="layui-btn layui-btn-sm layui-btn-danger"><i
                                    class="layui-icon">&#xe640;</i>删除
                            </button>
                        </@shiro.hasPermission>
                        </div>
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.config({
        base: '/modules/' //静态资源所在路径
    }).extend({
        formSelects: 'formSelects/formSelects-v4' //主入口模块
    }).use(['table', 'form', 'jquery', 'jmdf', 'page', 'dataDict', 'formSelects'], function () {
        var $ = layui.jquery, jmdf = layui.jmdf, page = layui.page, table = layui.table, form = layui.form, dataDict = layui.dataDict, formSelects = layui.formSelects;
        /***********************table表格相关操作 start*************************/
        var tableRenderId = "renderId";
        var tableId = "dataTable";
        var table_layFilter = "layFilter";
        var tableUrl = "loadData";

        formSelects.data('departmentSelect', 'service', {
            url: "/permission/department/getDepartmentTreeJson"
        });

        //列项内容
        var cols = [[
            {checkbox: true, fixed: true}
            , {title: "序号", toolbar: '#indexTpl', align: 'center', width: 60}
            , {field: "departmentName", title: "所属部门", align: 'left', minWidth: 220}
            , {field: "name", title: "真实姓名", align: 'left'}
            , {field: "username", title: "账号", align: 'left', width: 220}
            , {field: "roles", title: "角色", align: 'left'}
            , {title: "用户状态", align: 'center', toolbar: '#availableTpl', width: 120}
            , {title: '操作', fixed: 'right', align: 'left', toolbar: '#dataTable_toolbar', width: 160}
        ]];
        //table设置项
        var options = {
            elem: '#' + tableId//table列表id值
            , url: tableUrl
            , toolbar: '#toolbar'//自定义工具栏
            , cols: cols
            , id: tableRenderId
//            ,height:'full-120'
            , defaultToolbar: ['filter', 'print']//显示列、打印
            , autoSort: false //禁用前端自动排序。
        };

        //初始表格（含分页设置）
        page.render(options);
        //表格排序
        page.sort(tableId, tableRenderId);
        //刷新当前页方法封装
        page.reload();

        //分页查询（查询条件）
        $("#searchButton").click(function () {
            page.search("searchForm", tableRenderId);
        });
        dataDict.select("用户状态", "state", 1);
        form.render();
        //头工具栏事件
        table.on('toolbar(' + table_layFilter + ')', function (obj) {
            switch (obj.event) {
                case 'reload':
                    loadData();
                    break;
                case 'add':
                    formData(null);
                    break;
                case 'del':
                    deleteData();
                    break;
            }
        });

        table.on("tool(" + table_layFilter + ")", function (obj) {
            var data = obj.data;///获得当前行数据
            var layEvent = obj.event;//获得lay-event对应的值
            var id = data["id"];
            if (layEvent == "dataTable_edit") {
                formData(id);
            } else if (layEvent == "dataTable_pwd") {
                jmdf.form("pwdForm?id=" + id);
            }
        });
        form.on("switch(availableFilter)", function (obj) {
//            layer.tips(this.value + ' ' + this.name + '：' + obj.elem.checked, obj.othis);
            var id = this.value;
            var value = obj.elem.checked ? 1 : 2;
            var url = "setState?id=" + id + "&state=" + value;
            jmdf.saveOrUpdate("get", url, "");
        });
        function formData(id) {
            var url = 'form?id=' + id;
            jmdf.form(url, "80");
        }

        function deleteData() {
            var json = table.checkStatus(tableRenderId);
            if (json.data.length == 0) {
                layer.msg("请选择需要删除数据", {anim: 6, icon: 2});
                return;
            }
            var data = [];
            for (var i = 0; i < json.data.length; i++) {
                data.push(json.data[i].id);
            }
            var url = "delById?ids=" + data.toString();
            jmdf.del(url);
        }
    });
</script>
</body>
</html>