<!DOCTYPE html>
<html>
<head>
    <title>密码设置</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row  layui-col-space10">
        <div class="layui-col-xs12">
            <div class="layui-card">
                <div class="layui-card-header">密码设置</div>
                <div class="layui-card-body">
                    <form class="layui-form" action="">
                        <div class="layui-form-item">
                            <label class="layui-form-label">真实姓名：</label>
                            <div class="layui-form-mid">${(userInfo.name)!}</div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">用户名：</label>
                            <div class="layui-form-mid">${(userInfo.username)!}
                                <input type="hidden" name="username" value="${(userInfo.username)!}"/>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">密码：</label>
                            <div class="layui-input-block">
                                <input type="password" name="pwd" id="pwd" lay-verify="required" value=""
                                       autocomplete="off" placeholder="请输入密码" class="layui-input">
                            </div>
                        </div>
                        <button style="display: none;" class="layui-btn" id="submitBtn" name="submitBtn" lay-submit=""
                                lay-filter="submit">立即提交
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script src="/plugins/jquery/jquery.min.js" charset="utf-8"></script>
<script src="/plugins/jquery/jquery.base64.js" charset="utf-8"></script>
<script>
    layui.use(['form', 'jquery'], function () {
        var form = layui.form;
        var $ = layui.jquery;
        var parentIndex = parent.layer.getFrameIndex(window.name);
        //父页面通过submit按钮click方法，触发以下验证功能
        form.on('submit(submit)', function (data) {
            var pwd = $.base64.encode($("#pwd").val());
            var data = data.field;
            data['pwd'] = pwd;
            $.ajax({
                url: "savePwd"
                , type: "POST"
                , data: data
                , success: function (data) {
                    if (data.code == "0001") {
                        layer.alert("保存成功！")
                        parent.loadData();
                        parent.layer.close(parentIndex);
                    } else {
                        layer.msg(data.message, {anim: 6, icon: 2});
                    }
                }
            });
            return false;
        });
    });
</script>
</body>
</html>