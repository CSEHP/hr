<!DOCTYPE html>
<html>
<head>
    <title>用户信息管理</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
    <link rel="stylesheet" href="/modules/formSelects/formSelects-v4.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row  layui-col-space10">
        <div class="layui-col-xs12">
            <blockquote class="layui-elem-quote layui-bg-white">
                <p style="color: red;">用户名一旦发生变化，请立即初始化密码，否则该用户将无法正常登录</p>
            </blockquote>
            <div class="layui-card">
                <div class="layui-card-header">用户信息管理</div>
                <div class="layui-card-body">
                    <form class="layui-form" action="">
                        <input type="hidden" name="id" value="${(userInfo.id)!}"/>
                        <input type="hidden" name="signedStatus" value="${(userInfo.department.signedStatus)!1}"/>
                        <input type="hidden" name="throwCountDown" value="${(userInfo.department.throwCountDown)!1}"/>
                        <div class="layui-form-item">
                            <label class="layui-form-label">分配角色：</label>
                            <div class="layui-input-block">
                            <#list sysRoleList as sysRole>
                                <input type="checkbox" name="role" value="${sysRole.id}" lay-skin="primary" title="${sysRole.role}" <#if roleIds?seq_contains(sysRole.id)>checked</#if>/>
                            </#list>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">所属部门：</label>
                            <div class="layui-input-block">
                                <select name="departmentId" xm-select="departmentSelect" xm-select-radio="true">
                                    <option value="">请选择</option>
                                </select>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">真实姓名：</label>
                            <div class="layui-input-block">
                                <input type="text" name="name" id="name" lay-verify="required" value="${(userInfo.name)!}" autocomplete="off" placeholder="请输入真实姓名" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">用户名：</label>
                            <div class="layui-input-block">
                                <input type="text" name="username" id="username" lay-verify="required" value="${(userInfo.username)!}" autocomplete="off" placeholder="请输入用户名" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">用户头像：</label>
                            <div class="layui-input-inline">
                                <input type="hidden" name="pic" id="pic" value="${(userInfo.pic)!'/images/defaultUser.jpg'}" class="layui-input">
                                <div class="layui-upload-list" style="margin:0">
                                    <img src="${(userInfo.pic)!'/images/defaultUser.jpg'}" id="srcimgurl" class="layui-upload-img">
                                </div>
                            </div>
                            <div class="layui-input-inline">
                                <button type="button" class="layui-btn" id="editPic">修改图片</button>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">联系电话：</label>
                            <div class="layui-input-block">
                                <input type="text" name="telPhone" id="telPhone" value="${(userInfo.telPhone)!}" autocomplete="off" placeholder="" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">描述：</label>
                            <div class="layui-input-block">
                                <input type="text" name="description" id="description" value="${(userInfo.description)!}" autocomplete="off" placeholder="" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">编号(PIN)：</label>
                            <div class="layui-input-block">
                                <input type="text" name="pin" id="pin" value="${(userInfo.pin)!}" autocomplete="off" placeholder="" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">标示号码(GUID)：</label>
                            <div class="layui-input-block">
                                <input type="text" name="guid" id="guid" value="${(userInfo.guid)!}" autocomplete="off" placeholder="" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">密钥(UKEY)：</label>
                            <div class="layui-input-block">
                                <input type="text" name="ukey" id="ukey" value="${(userInfo.ukey)!}" autocomplete="off" placeholder="" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">用户状态：</label>
                            <div class="layui-input-block">
                                <input type="radio" <#if userInfo.state == 1>checked</#if> name="state" value="1" title="正常"/>
                                <input type="radio" <#if userInfo.state == 2>checked</#if> name="state" value="2" title="锁定"/>
                            </div>
                        </div>
                        <button style="display: none;" class="layui-btn" id="submitBtn" name="submitBtn" lay-submit="" lay-filter="submit">立即提交</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.config({version: true}).extend({
        croppers: '/modules/cropper/croppers' //主入口模块
        , formSelects: '/modules/formSelects/formSelects-v4' //主入口模块
    }).use(['form', 'jquery', 'formSelects', 'croppers'], function () {
        var form = layui.form;
        var $ = layui.jquery;
        var croppers = layui.croppers;
        var formSelects = layui.formSelects;
        var parentCroppersIndex = parent.layer.getFrameIndex(window.name);

        formSelects.data('departmentSelect', 'service', {
            url: "/permission/department/getDepartmentTreeJson",
            keyword: "${(userInfo.department.id)!}"
        });
        //父页面通过submit按钮click方法，触发以下验证功能
        form.on('submit(submit)', function (data) {
            $.ajax({
                url: "save"
                , type: "post"
                , data: $("form").serializeArray()
                , success: function (data) {
                    if (data.code == "0001") {
                        layer.alert("保存成功！")
                        parent.loadData();
                        parent.layer.close(parentCroppersIndex);
                    } else {
                        layer.msg(data.message, {anim: 6, icon: 2});
                    }
                }
            });
            return false;
        });
        //创建一个头像上传组件
        croppers.render({
            elem: '#editPic'
            , saveW: 200     //保存宽度
            , saveH: 200
            , mark: 1 / 1    //选取比例
            , area: '900px'  //弹窗宽度
            , url: "/upload/uploadPic"  //图片上传接口返回和（layui 的upload 模块）返回的JOSN一样
            , done: function (data) { //上传完毕回调
                var staticAccessPath = data.staticAccessPath;
                var uploadPath = data.uploadPath;
                var url = staticAccessPath + uploadPath;
                $("#pic").val(url);
                $("#srcimgurl").attr('src', url);
            }
        });
    });
</script>
</body>
</html>