<!DOCTYPE html>
<html>
<head>
    <title>当前在线用户</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/style/admin.css" media="all">
</head>
<body>
<div class="layui-fluid">
    <div class="layui-row  layui-col-space10">
        <div class="layui-col-xs12">
            <div class="layui-card">
                <div class="layui-card-header">当前在线用户</div>
                <div class="layui-card-body">
                    <button id="load" class="layui-btn layui-btn-sm toolButton"><i class="layui-icon">&#xe669;</i>重载</button>
                    <table id="dataTable" class="layui-table" lay-filter="layFilter"></table>
                    <!--序号-->
                    <script type="text/html" id="indexTpl">
                        {{d.LAY_TABLE_INDEX+1}}
                    </script>
                    <!--菜单-->
                    <script type="text/html" id="dataTable_toolbar">
                        <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="logout"><i class="layui-icon">&#xe642;</i>踢出</a>
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/layui/layui.js" charset="utf-8"></script>
<script>
    layui.config({
        base: '/modules/' //静态资源所在路径
    }).use(['table', 'form', 'jquery', 'jmdf'], function () {
        var table = layui.table, form = layui.form, $ = layui.jquery, jmdf = layui.jmdf;
        var dataID = "dataID";

        table.render({
            elem: "#dataTable", url: "onlineUserInfoJson", id: dataID, page: false, height: "full-140", cellMinWidth: 80
            , cols: [[
                {title: "序号", toolbar: '#indexTpl', align: 'center', width: 60}
                , {field: "sessionId", title: "sessionId", align: 'left', width: 320}
                , {field: "username", title: "username", align: 'left', width: 220}
                , {field: "host", title: "host", align: 'left'}
                , {field: "lastAccessTime", title: "lastAccessTime", align: 'left'}
                , {field: "startTimestamp", title: "startTimestamp", align: 'left'}
                , {field: "timeout", title: "timeout", align: 'left'}
                , {title: '操作', fixed: 'right', align: 'center', toolbar: '#dataTable_toolbar', width: 100}
            ]]
        });

        function loadData() {
            table.reload(dataID, {});//刷新
        }

        $("#load").click(function () {
            loadData();
        });
        table.on("tool(layFilter)", function (obj) {
            var data = obj.data;///获得当前行数据
            var layEvent = obj.event;//获得lay-event对应的值
            var id = data["sessionId"];
            if (layEvent == "logout") {
                logoutUser(id);
            }
        });
        function logoutUser(sessionId) {
            layer.open({
                title: false, content: '确定踢出吗？', btn: ['确定', '取消'], closeBtn: 0, btnAlign: 'c'
                , yes: function (index) {
                    var url = "logoutOnlineUser";
                    $.get(url, {sessionId: sessionId, _t: new Date().getTime()}, function (data) {
                        if (data.code == "0001") {//成功
                            layer.msg(data.message, {anim: 0, icon: 1});
                        } else {//失败
                            layer.msg(data.message, {anim: 6, icon: 2});
                        }
                        loadData();
                        layer.close(index);
                    });
                }
            });
        }
    });
</script>
</body>
</html>