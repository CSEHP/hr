function initPersonTable (dataDict, $){
    //添加新行
    $("#addRow").click(function () {
        /*克隆操作*/
        //获取最后一行tr索引
        var rowIndex = $("#personTable tr").length - 1;
        //克隆最后一行到最后一行的下一行
        $("#personTable tr:last").clone().insertAfter($("#personTable tr:eq(" + rowIndex + ")"));
        //清空新加的最后一行内容值
        $("#personTable tr:last input[type != hidden]").each(function () {
            $(this).val("");
        });

        /*下拉菜单操作*/
        //获取最后一行下拉菜单id的索引+1
        //截取id后面动态的i的值+1
        var accountTypeId = parseInt($("#personTable tr:last").find("select[name='person_accountType']").attr("id").substring(10)) + 1;
        //截取id后面动态的i的值+1
        var isMixedId = parseInt($("#personTable tr:last").find("select[name='person_isMixed']").attr("id").substring(19)) + 1;
        //将最后一行下拉菜单id的索引+1放入刚克隆的下拉菜单中
        $("#personTable tr:last").find("select[name='person_accountType']").attr("id", "person_accountType" + accountTypeId);
        $("#personTable tr:last").find("select[name='person_isMixed']").attr("id", "person_isMixed" + isMixedId);
        // dataDict.select()中直接在option后面append的option，没有清空原下拉菜单，所以本操作清空下拉菜单
        $("#person_accountType" + accountTypeId).html("<option value=''>--请选择--</option>");
        $("#person_isMixed" + isMixedId).html("<option value=''>--请选择--</option>");
        //根据数据构建本行下拉菜单
        dataDict.select("户口性质", "person_accountType" + accountTypeId, "");
        dataDict.select("是否", "person_isMixed" + isMixedId, "");
    });

    //构建下拉菜单
    $("#personTable tbody tr").each(function (obj) {
        var personed_accountType = $(this).find("input[name='personed_accountType']").val();
        var personed_isMixed = $(this).find("input[name='personed_isMixed']").val();
        //取每行下拉菜单的id
        var accountTypeId = $(this).find("select[name='person_accountType']").attr("id");
        var isMixedId = $(this).find("select[name='person_isMixed']").attr("id");
        //构建菜单
        dataDict.select("户口性质", accountTypeId, personed_accountType);
        dataDict.select("是否", isMixedId, personed_isMixed);
    });

    //动态删除
    $('body').on('click', '.del', function () {
        var familyid = $("input[name='familyId']").val();
        //取当前行认定人口name
        var thisName = $(this).parent().parent().find("input[name='person_name']").val();
        //取当前行认定人口Id
        var personId = $(this).parent().parent().find("input[name='person_id']").val();
        if ($('#personTable tbody tr').length === 2) {
            layer.msg('只有一条不允许删除。', {
                time: 2000
            });
        } else if (personId == "") {
            //删除当前按钮所在的tr
            $(this).closest('tr').remove();
        } else {
            layer.open({
                title: false,
                shade: "0.1",
                content: "<span style='color:red'>" + "确认删除“" + thisName + "”吗？删除后数据无法找回！</span>",
                btn: ['<i class="layui-icon layui-icon-ok"></i>&nbsp;确定', '<i class="layui-icon layui-icon-close"></i>&nbsp;取消'],
                closeBtn: 0,
                btnAlign: 'c',
                yes: function (index) {
                    $.ajax({
                        url: 'delPersonById',
                        type: 'post',
                        data: {
                            personId: personId,
                            familyId: familyid
                        },
                        success: function (data) {
                            if (data.code == "0001") {//成功
                                layer.msg(data.message, {anim: 0, icon: 1, time: 500}, function () {
                                    //此处可以优化为局部刷新重载家庭人口表格
                                    location.reload()
                                });
                            } else {//失败
                                layer.msg(data.message, {anim: 6, icon: 2}, function () {
                                });
                            }
                        }
                    });
                }
            });
        }
    });
}