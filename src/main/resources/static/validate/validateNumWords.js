/**
 * 是否仅包含数字及字母
 * @param input 字符串
 * @returns {*|Promise<Response | undefined>|RegExpMatchArray}
 */
function done(input) {
    var pattern = '^[a-z0-9A-Z]+$';
    var regex = new RegExp(pattern);
    return input.match(regex);
}
