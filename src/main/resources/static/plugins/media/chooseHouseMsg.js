// 获取日期集合
function getDateArr(day) {
    dateArr = [];
    for (i = 0; i < day; i++) {
        dateArr.unshift(getDay(-i))
    }
    return dateArr;
}

// 获取日期
function getDay(day) {
    var today = new Date();
    var targetday_milliseconds = today.getTime() + 1000 * 60 * 60 * 24 * day;
    today.setTime(targetday_milliseconds); //注意，这行是关键代码
    var tYear = today.getFullYear();
    var tMonth = today.getMonth();
    var tDate = today.getDate();
    tMonth = doHandleMonth(tMonth + 1);
    tDate = doHandleMonth(tDate);
    return tMonth + "-" + tDate;
}

// 日期补0
function doHandleMonth(month) {
    var m = month;
    if (month.toString().length == 1) {
        m = "0" + month;
    }
    return m;
}

//总体选房情况
var housePie = null;//饼状图
var houseTypeChart = null;//各居室柱状图
var houseType_json_xf = [];//各居室柱状图已选房源json数据
var houseType_json_noXf = [];//各居室柱状图未选房源json数据

//面积户型柱状图
// var xfHousePie = null;//饼状图
var houseAreaTypeChart = null;//各面积户型柱状图
var houseAreaType_json_xf = [];//各面积户型已选房源json数据
var houseAreaType_json_noXf = [];//各面积户型未选房源json数据

//楼号柱状图
// var qfHousePie = null;//饼状图
var buildNumChart = null;//各楼号柱状图
var buildNum_json_xf = [];//各楼号已选房源json数据
var buildNum_json_noXf = [];//各楼号未选房源json数据

var xfText = "已选";
var noXfText = "未选";
var arrayColor = ["#ff7575", "#01B468"];
var timeout = 15000;

function initChart() {
    //总体选房饼状图
    housePie = echarts.init(document.getElementById('housePie'));
    var optionHousePie = {
        color: [arrayColor[0], arrayColor[1]],
        tooltip: {
            trigger: 'item',
            formatter: "{b}({c})<br/>{d}%"
        },
        series: [
            {
                type: 'pie',
                center: ['50%', '50%'],
                radius: [90, 125],
                x: '0%', // for funnel
                itemStyle: {
                    normal: {
                        label: {
                            position: 'center',
                            fontSize: '20',
                            formatter: function (params) {
                                if (params.name == xfText) {
                                    return xfText + ":" + (params.percent + '%')
                                } else {
                                    return noXfText + ":" + (params.percent + '%')
                                }
                            }
                        },
                        labelLine: {
                            show: false
                        }
                    }
                },
                data: [
                    {name: xfText, value: 0},
                    {name: noXfText, value: 0}
                ]
            }
        ]
    };
    housePie.setOption(optionHousePie);

    //居室柱状图
    houseTypeChart = echarts.init(document.getElementById('houseTypeChart'));
    var optionHouseTypeChart = {
        color: [arrayColor[0], arrayColor[1]],
        grid: {
            left: '5%',
            right: '5%',
            bottom: '5%',
            containLabel: true
        },
        legend: {
            data: [
                {name: xfText, textStyle: {color: arrayColor[0]}}
                , {name: noXfText, textStyle: {color: arrayColor[1]}}
            ],
            top: '5%',
            right: '30px'
        },
        tooltip: {
            trigger: 'item'
            //,formatter: "{a}<br/>{b}<br/>{c}人"
        },
        xAxis: [
            {
                type: 'category',
                data: [],
                axisLine: {
                    lineStyle: {
                        color: '#5bc0de'
                    }
                },
                axisLabel: {
                    interval: 0,
                    rotate: 0,
                    textStyle: {
                        color: '#fff',
                        fontSize: "20"
                    },
                    formatter: '{value}'
                }
            }
        ],
        yAxis: [
            {
                type: 'value',
                axisLine: {
                    lineStyle: {
                        color: '#5bc0de'
                    }
                },
                splitLine: {
                    show: false
                },
                axisLabel: {
                    textStyle: {
                        color: '#fff'
                    },
                    formatter: function (value) {
                        return value + "";
                    }
                }
            }
        ],
        series: [
            {
                name: xfText,
                type: 'bar',
                barWidth: 80,
                stack: '总量',
                zlevel: 1,
                label: {
                    show: true,
                    position: 'inside',
                    fontSize: '20',
                    color: '#ffffff',
                    formatter: function (data) {
                        var i = data.dataIndex;//坐标
                        var v = data.value;//数值
                        if (v == 0) {
                            return "";
                        }
                        return houseType_json_xf[i];
                    }
                },
                data: houseType_json_xf
            },
            {
                name: noXfText,
                type: 'bar',
                barWidth: 80,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'top',
                        fontSize: '20',
                        color: '#ffffff',
                        formatter: function (data) {
                            var i = data.dataIndex;//坐标
                            //var v = data.value;//数值
                            var total = houseType_json_xf[i] + houseType_json_noXf[i];
                            if (total > 0) {
                                return total;
                            } else {
                                return "";
                            }
                        }
                    }
                },

                data: houseType_json_noXf
            }
        ]
    };
    houseTypeChart.setOption(optionHouseTypeChart);

    //现房选房饼状图
    // xfHousePie = echarts.init(document.getElementById('xfHousePie'));
    // var optionXfHousePie = {
    //     color: [arrayColor[0], arrayColor[1]],
    //     tooltip: {
    //         trigger: 'item',
    //         formatter: "{b}({c})<br/>{d}%"
    //     },
    //     series: [
    //         {
    //             type: 'pie',
    //             center: ['50%', '50%'],
    //             radius: [90, 125],
    //             x: '0%', // for funnel
    //             itemStyle: {
    //                 normal: {
    //                     label: {
    //                         position: 'center',
    //                         fontSize: '20',
    //                         formatter: function (params) {
    //                             if (params.name == xfText) {
    //                                 return xfText + ":" + (params.percent + '%')
    //                             } else {
    //                                 return noXfText + ":" + (params.percent + '%')
    //                             }
    //                         }
    //                     },
    //                     labelLine: {
    //                         show: false
    //                     }
    //                 }
    //             },
    //             data: [
    //                 {name: xfText, value: 0},
    //                 {name: noXfText, value: 0}
    //             ]
    //         }
    //     ]
    // };
    // xfHousePie.setOption(optionXfHousePie);

    //面积户型柱状图
    houseAreaTypeChart = echarts.init(document.getElementById('houseAreaTypeChart'));
    var optionHouseAreaTypeChart = {
        color: [arrayColor[0], arrayColor[1]],
        grid: {
            left: '5%',
            right: '5%',
            bottom: '5%',
            containLabel: true
        },
        legend: {
            data: [
                {name: xfText, textStyle: {color: arrayColor[0]}}
                , {name: noXfText, textStyle: {color: arrayColor[1]}}
            ],
            top: '5%',
            right: '30px'
        },
        tooltip: {
            trigger: 'item'
            //,formatter: "{a}<br/>{b}<br/>{c}人"
        },
        xAxis: [
            {
                type: 'category',
                data: [],
                axisLine: {
                    lineStyle: {
                        color: '#5bc0de'
                    }
                },
                axisLabel: {
                    interval: 0,
                    rotate: 0,
                    textStyle: {
                        color: '#fff',
                        fontSize: "18"
                    },
                    formatter: '{value}'
                }
            }
        ],
        yAxis: [
            {
                type: 'value',
                axisLine: {
                    lineStyle: {
                        color: '#5bc0de'
                    }
                },
                splitLine: {
                    show: false
                },
                axisLabel: {
                    textStyle: {
                        color: '#fff'
                    },
                    formatter: function (value) {
                        return value + "";
                    }
                }
            }
        ],
        series: [
            {
                name: xfText,
                type: 'bar',
                barWidth: 26,
                stack: '总量',
                zlevel: 1,
                label: {
                    show: true,
                    position: 'inside',
                    fontSize: '18',
                    color: '#ffffff',
                    formatter: function (data) {
                        var i = data.dataIndex;//坐标
                        var v = data.value;//数值
                        if (v == 0) {
                            return "";
                        }
                        return houseAreaType_json_xf[i];
                    }
                },
                data: houseAreaType_json_xf
            },
            {
                name: noXfText,
                type: 'bar',
                barWidth: 25,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'top',
                        fontSize: '16',
                        color: '#ffffff',
                        formatter: function (data) {
                            var i = data.dataIndex;//坐标
                            //var v = data.value;//数值
                            var total = houseAreaType_json_xf[i] + houseAreaType_json_noXf[i];
                            if (total > 0) {
                                return total;
                            } else {
                                return "";
                            }
                        }
                    }
                },

                data: houseAreaType_json_noXf
            }
        ]
    };
    houseAreaTypeChart.setOption(optionHouseAreaTypeChart);

    // //现房选房饼状图
    // qfHousePie = echarts.init(document.getElementById('qfHousePie'));
    // var optionQfHousePie = {
    //     color: [arrayColor[0], arrayColor[1]],
    //     tooltip: {
    //         trigger: 'item',
    //         formatter: "{b}({c})<br/>{d}%"
    //     },
    //     series: [
    //         {
    //             type: 'pie',
    //             center: ['50%', '50%'],
    //             radius: [90, 125],
    //             x: '0%', // for funnel
    //             itemStyle: {
    //                 normal: {
    //                     label: {
    //                         position: 'center',
    //                         fontSize: '20',
    //                         formatter: function (params) {
    //                             if (params.name == xfText) {
    //                                 return xfText + ":" + (params.percent + '%')
    //                             } else {
    //                                 return noXfText + ":" + (params.percent + '%')
    //                             }
    //                         }
    //                     },
    //                     labelLine: {
    //                         show: false
    //                     }
    //                 }
    //             },
    //             data: [
    //                 {name: xfText, value: 0},
    //                 {name: noXfText, value: 0}
    //             ]
    //         }
    //     ]
    // };
    // qfHousePie.setOption(optionQfHousePie);

    //楼号柱状图
    buildNumChart = echarts.init(document.getElementById('buildNumChart'));
    var optionBuildNumChart = {
        color: [arrayColor[0], arrayColor[1]],
        grid: {
            left: '5%',
            right: '5%',
            bottom: '5%',
            containLabel: true
        },
        legend: {
            data: [
                {name: xfText, textStyle: {color: arrayColor[0]}}
                , {name: noXfText, textStyle: {color: arrayColor[1]}}
            ],
            top: '5%',
            right: '30px'
        },
        tooltip: {
            trigger: 'item'
            //,formatter: "{a}<br/>{b}<br/>{c}人"
        },
        xAxis: [
            {
                type: 'category',
                data: [],
                axisLine: {
                    lineStyle: {
                        color: '#5bc0de'
                    }
                },
                axisLabel: {
                    interval: 0,
                    rotate: 0,
                    textStyle: {
                        color: '#fff',
                        fontSize: "18"
                    },
                    formatter: '{value}'
                }
            }
        ],
        yAxis: [
            {
                type: 'value',
                axisLine: {
                    lineStyle: {
                        color: '#5bc0de'
                    }
                },
                splitLine: {
                    show: false
                },
                axisLabel: {
                    textStyle: {
                        color: '#fff'
                    },
                    formatter: function (value) {
                        return value + "";
                    }
                }
            }
        ],
        series: [
            {
                name: xfText,
                type: 'bar',
                barWidth: 25,
                stack: '总量',
                zlevel: 1,
                label: {
                    show: true,
                    position: 'inside',
                    fontSize: '18',
                    color: '#ffffff',
                    formatter: function (data) {
                        var i = data.dataIndex;//坐标
                        var v = data.value;//数值
                        if (v == 0) {
                            return "";
                        }
                        return buildNum_json_xf[i];
                    }
                },
                data: buildNum_json_xf
            },
            {
                name: noXfText,
                type: 'bar',
                barWidth: 30,
                stack: '总量',
                label: {
                    normal: {
                        show: true,
                        position: 'top',
                        fontSize: '20',
                        color: '#ffffff',
                        formatter: function (data) {
                            var i = data.dataIndex;//坐标
                            //var v = data.value;//数值
                            var total = buildNum_json_xf[i] + buildNum_json_noXf[i];
                            if (total > 0) {
                                return total;
                            } else {
                                return "";
                            }
                        }
                    }
                },

                data: buildNum_json_noXf
            }
        ]
    };
    buildNumChart.setOption(optionBuildNumChart);
}

function getIndexMap() {
    var objPie = housePie.getOption();//总体饼图
    var typeChart = houseTypeChart.getOption();//总体居室柱图
    // var objXfPie = xfHousePie.getOption();//现房饼图
    var areaTypeChart = houseAreaTypeChart.getOption();//面积户型柱图
    // var objQfPie = qfHousePie.getOption();//期房饼图
    var numChart = buildNumChart.getOption();//楼号柱图
    $.ajax({
        url: "chooseHouseMsgDataMap",
        type: "get",
        async: false,   //先执行此处，再执行其它js方法
        dataType: "json",
        success: function (data) {
            document.getElementById("xfFamilyCnt").innerHTML = data.xfFamilyCnt;
            document.getElementById("xfFamilyCntCur").innerHTML = data.xfFamilyCntCur;
            document.getElementById("yjHouseCnt").innerHTML = data.yjHouseCnt;
            document.getElementById("yjHouseCntCur").innerHTML = data.yjHouseCntCur;
            document.getElementById("ejHouseCnt").innerHTML = data.ejHouseCnt;
            document.getElementById("ejHouseCntCur").innerHTML = data.ejHouseCntCur;
            document.getElementById("sjHouseCnt").innerHTML = data.sjHouseCnt;
            document.getElementById("sjHouseCntCur").innerHTML = data.sjHouseCntCur;

            //饼图
            objPie.series[0].data[0].value = data.housePie[0];
            objPie.series[0].data[1].value = data.housePie[1];
            housePie.setOption(objPie);

            //居室柱状图
            typeChart.xAxis[0].data = data.houseType;
            houseType_json_xf = data.houseType_json_xf;
            houseType_json_noXf = data.houseType_json_noXf;
            typeChart.series[0].data = houseType_json_xf;
            typeChart.series[1].data = houseType_json_noXf;
            houseTypeChart.setOption(typeChart);

            // //现房饼图
            // objXfPie.series[0].data[0].value = data.xfHousePie[0];
            // objXfPie.series[0].data[1].value = data.xfHousePie[1];
            // xfHousePie.setOption(objXfPie);

            //面积户型情况柱状图
            areaTypeChart.xAxis[0].data = data.xfHouseAreaType;
            houseAreaType_json_xf = data.xfHouseAreaType_json_xf;
            houseAreaType_json_noXf = data.xfHouseAreaType_json_noXf;
            areaTypeChart.series[0].data = houseAreaType_json_xf;
            areaTypeChart.series[1].data = houseAreaType_json_noXf;
            houseAreaTypeChart.setOption(areaTypeChart);

            // //期房饼图
            // objQfPie.series[0].data[0].value = data.qfHousePie[0];
            // objQfPie.series[0].data[1].value = data.qfHousePie[1];
            // qfHousePie.setOption(objQfPie);

            //期房户型情况柱状图
            numChart.xAxis[0].data = data.qfBuildNum;
            buildNum_json_xf = data.qfBuildNum_json_xf;
            buildNum_json_noXf = data.qfBuildNum_json_noXf;
            numChart.series[0].data = buildNum_json_xf;
            numChart.series[1].data = buildNum_json_noXf;
            buildNumChart.setOption(numChart);
        }
    });
}

$(function () {
    initChart();
    getIndexMap();
    setInterval(getIndexMap, timeout);
});