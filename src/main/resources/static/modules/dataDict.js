/**
 扩展模块
 @author:wcf
 @date:2018.12.10
 @version:1.0
 **/
layui.define(["layer", "jquery", "form", 'jmdf'], function (exports) {
    var $ = layui.jquery;
    var form = layui.form;
    var obj = {
        //dataDict：数据字典名称 selectId：下拉框Id selectValue：选中的数值
        select: function (dataDictName, selectId, selectValue) {//数据字典
            var ajax = $.ajax({
                type: "GET",
                timeout: 500, //超时时间设置，单位毫秒
                async: false,// true：同步请求  false：异步请求
                url: "/dataDict/" + dataDictName,
                success: function (data) {
                    if (data != null && data.length > 0) {
                        $("#" + selectId + " option:gt(0)").remove();
                        var sel = $("#" + selectId);
                        for (var i in data) {
                            var name = data[i].name;
                            var value = data[i].value;
                            //var color = data[i].color;
                            var selected = "";
                            if (value == selectValue) {
                                selected = "selected";
                            }
                            sel.append("<option value='" + value + "' " + selected + ">" + name + "</option>");
                        }
                        form.render("select");//重新渲染select适应form样式
                    }
                },
                complete: function (XMLHttpRequest, status) { //求完成后最终执行参数
                    // 设置timeout的时间，通过检测complete时status的值判断请求是否超时，如果超时执行响应的操作
                    if (status == 'timeout') { //超时,status还有success,error等值的情况
                        ajax.abort();
                        //alert("请求超时");
                        layer.msg("请求超时", {anim: 0, icon: 1, time: 500});
                    }
                }
            });
        }
        //dataDictName：数据字典名称 radioId：divId checkValue：选中的数值
        , dataDictRadio: function (dataDictName, name, checkValue) {//数据字典
            var ajax = $.ajax({
                type: "GET",
                timeout: 500, //超时时间设置，单位毫秒
                async: false,// true：同步请求  false：异步请求
                url: "/dataDict/" + dataDictName,
                success: function (data) {
                    if (data != null && data.length > 0) {
                        var radio = $("#" + name);
                        for (var i in data) {
                            var title = data[i].name;
                            var value = data[i].value;

                            var checked = "";
                            if (value == checkValue) {
                                checked = "checked";
                            }
                            radio.append("<input type='radio' lay-filter='" + name + "' name='" + name + "' value='" + value + "' title='" + title + "'" + checked + "/>");
                        }
                        form.render("radio");//重新渲染radio样式
                    }
                },
                complete: function (XMLHttpRequest, status) { //求完成后最终执行参数
                    // 设置timeout的时间，通过检测complete时status的值判断请求是否超时，如果超时执行响应的操作
                    if (status == 'timeout') { //超时,status还有success,error等值的情况
                        ajax.abort();
                        //alert("请求超时");
                        layer.msg("请求超时", {anim: 0, icon: 1, time: 500});
                    }
                }
            });
        }
    };

    //输出接口
    exports('dataDict', obj);
});