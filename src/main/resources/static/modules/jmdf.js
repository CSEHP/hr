/**
 扩展模块
 @author:wcf
 @date:2018.05.26
 @version:1.0
 **/

layui.define(["layer", "jquery"], function (exports) {
    var $ = layui.jquery;
    var shade = 0.5;
    var i_w = 65;
    var obj = {

        //表单编辑
        form: function (url, w) {
            if (w != undefined) {
                i_w = w;
            }
            if (url.indexOf("?") == -1) {
                url += "?";
            }
            url += "&_t=" + new Date().getTime();
            layer.open({
                type: 2,
                anim: 2,
                title: false,
                shade: shade,
                area: [i_w + '%', '100%'],
                btn: ['<i class="layui-icon">&#xe605;</i>&nbsp;保存', '<i class="layui-icon">&#x1006;</i>&nbsp;关闭'],
                scrollbar: false,//禁用父页面滚动条
                closeBtn: 0,
                content: url,
                offset: 'r',
                btnAlign: 'c',
                resize: false
                //, skin: 'demo-class'
                ,
                yes: function (index, layero) {
                    var iframeWindow = $(layero).find("iframe")[0].contentWindow;
                    var submitBtn = $(iframeWindow.document.getElementById("submitBtn"));
                    submitBtn.click();
                }
                ,
                end: function (index, layero) {
                    //触发关闭事件后，执行此方法
                }
            });
        }

        //内容查看
        , view: function (url, w) {
            //隐藏父页面底部的按钮
            var parentLayer = parent.layer;
            if (parentLayer != undefined) {
                var parentIndex = parentLayer.getFrameIndex(window.name); //先得到当前iframe层的索引
                var obj = parent.document.getElementById("layui-layer" + parentIndex);
                $(obj).children(".layui-layer-btn-c").hide();//隐藏父页面按钮
            }

            if (w != undefined) {
                i_w = w;
            }
            layer.open({
                type: 2,
                anim: 2,
                title: false,
                shade: shade,
                area: [i_w + '%', '100%'],
                btn: ['<i class="layui-icon">&#x1006;</i>&nbsp;关闭'],
                scrollbar: false,//禁用父页面滚动条
                closeBtn: 0,
                resize: false,
                offset: 'r',
                btnAlign: 'c',
                content: url,
                end: function (index, layero) {
                    //触发关闭事件后，执行此方法
                    //显示父页面隐藏的底部按钮
                    if (parentLayer != undefined) {
                        if (parentLayer != undefined) {
                            $(obj).children(".layui-layer-btn-c").show();//显示按钮
                        }
                    }
                }
            });
        }

        , jmdfViewNoClose: function (url, w) {//内容查看
            if (w != undefined) {
                i_w = w;
            }
            var parentLayer = parent.layer;
            if (parentLayer != undefined) {
                var parentIndex = parentLayer.getFrameIndex(window.name); //先得到当前iframe层的索引
                var obj = parent.document.getElementById("layui-layer" + parentIndex);
                $(obj).children(".layui-layer-btn-c").hide();//隐藏父页面按钮
            }
            layer.open({
                type: 2,
                anim: 2,
                title: false,
                shade: shade,
                area: [i_w + '%', '100%'],
                scrollbar: false,//禁用父页面滚动条
                btn: [],
                closeBtn: 0,
                resize: false,
                offset: 'r',
                btnAlign: 'c',
                content: url,
                end: function (index, layero) {
                    if (parentLayer != undefined) {
                        $(obj).children(".layui-layer-btn-c").show();//显示按钮
                    }
                }
            });
        }

        //内容打印
        , print: function (url, w) {
            if (w == undefined) {
                w = i_w;
            }
            layer.open({
                type: 2,
                anim: 2,
                title: false,
                shade: shade,
                area: [w + '%', '100%'],
                btn: ['<i class="layui-icon layui-icon-print"></i>&nbsp;打印', '<i class="layui-icon">&#x1006;</i>&nbsp;关闭'],
                scrollbar: false,//禁用父页面滚动条
                closeBtn: 0,
                resize: false,
                offset: 'r',
                btnAlign: 'c',
                content: url,
                yes: function (index, layero) {
                    var iframeWindow = $(layero).find("iframe")[0].contentWindow;
                    iframeWindow.document.getElementById("printIframe").contentWindow.focus();    //获得焦点
                    iframeWindow.document.getElementById("printIframe").contentWindow.print();
                },
                end: function () {
                    //关闭后刷新父页面
                    location.reload()
                }
            });
        }

        //保存更新
        , saveOrUpdate: function (type, url, param) {
            $.ajax({
                type: type, url: url, data: param + "&_t=" + new Date().getTime()
                , success: function (data) {
                    if (data.code == "0001") {
                        layer.msg(data.message, {anim: 0, icon: 1, time: 1000});
                    } else {
                        layer.msg(data.message, {anim: 6, icon: 2, time: 3000});
                    }
                    loadData();
                }
            });
        }

        //信息删除
        , del: function (url) {
            layer.open({
                title: false,
                shade: shade,
                content: '确定删除吗？',
                btn: ['<i class="layui-icon">&#xe605;</i>&nbsp;确定', '<i class="layui-icon">&#x1006;</i>&nbsp;取消'],
                scrollbar: false,//禁用父页面滚动条
                closeBtn: 0,
                btnAlign: 'c'
                ,
                yes: function (index) {
                    $.get(url, {_t: new Date().getTime()}, function (data) {
                        if (data.code == "0001") {//成功
                            layer.msg(data.message, {anim: 0, icon: 1, time: 1000});
                        } else {//失败
                            layer.msg(data.message, {anim: 6, icon: 2, time: 3000});
                        }
                        loadData();
                        layer.close(index);
                    });
                }
            });
        }

        //异步操作
        , operator: function (url, info) {
            layer.open({
                title: false,
                shade: shade,
                content: info,
                btn: ['<i class="layui-icon">&#xe605;</i>&nbsp;确定', '<i class="layui-icon">&#x1006;</i>&nbsp;取消'],
                scrollbar: false,//禁用父页面滚动条
                closeBtn: 0,
                btnAlign: 'c',
                yes: function (index) {
                    $.get(url, {_t: new Date().getTime()}, function (data) {
                        if (data.code == "0001") {//成功
                            layer.msg(data.message, {anim: 0, icon: 1, time: 1000});
                        } else {//失败
                            layer.msg(data.message, {anim: 6, icon: 2, time: 3000});
                        }
                        loadData();
                        layer.close(index);
                    });
                }
            });
        }
        , exec: function (url) {
            $.ajax({
                type: 'get', url: url, data: "_t=" + new Date().getTime()
                , success: function (data) {
                    if (data.code == "0001") {
                        layer.msg(data.message, {anim: 0, icon: 1});
                    } else {
                        layer.msg(data.message, {anim: 6, icon: 2});
                    }
                }
                , error: function (data) {
                    alert(JSON.stringify(data));
                }
            });
        }
        //登录验证
        , login: function () {
            location.href = "/login/logout";
            // layer.open({
            //     type: 2
            //     , id: 'validateIsLogin'
            //     , title: "用户信息已失效，请重新登录"
            //     , closeBtn: 0
            //     , btnAlign: 'c'
            //     , content: "/login/minLogin"
            //     , area: ['500px', '400px']
            //     , end: function (index, layero) {
            //         //触发关闭事件后，执行此方法
            //         parent.reStart();
            //     }
            // });
        },
        //审核
        checkForm: function (url, w) {//表单编辑，左侧边框，确认按钮名称
            if (w == undefined) {
                w = i_w;
            }
            if (url.indexOf("?") == -1) {
                url += "?";
            }
            url += "&_t=" + new Date().getTime();

            var parentLayer = parent.layer;
            if (parentLayer != undefined) {
                var parentIndex = parentLayer.getFrameIndex(window.name); //先得到当前iframe层的索引
                var obj = parent.document.getElementById("layui-layer" + parentIndex);
                $(obj).children(".layui-layer-btn-c").hide();//隐藏父页面按钮
            }
            layer.open({
                type: 2,
                anim: 2,
                title: false,
                shade: shade,
                skin: 'check-form-class',
                area: [w + '%', '100%'],
                btn: ['<i class="layui-icon layui-icon-ok"></i>&nbsp;审核通过', '<i class="layui-icon layui-icon-circle"></i>&nbsp;审核退回', '<i class="layui-icon layui-icon-close"></i>&nbsp;关闭页面'],
                scrollbar: false,//禁用父页面滚动条
                closeBtn: 0,
                content: url,
                offset: 'r',
                btnAlign: 'c',
                resize: false
                , yes: function (index, layero) {
                    //审核通过，调用页面方法
                    var iframeWindow = $(layero).find("iframe")[0].contentWindow;
                    var submitBtn = $(iframeWindow.document.getElementById("successBtn"));
                    submitBtn.click();
                    return false;
                }
                , btn2: function (index, layero) {
                    //审核通过，调用页面方法
                    var iframeWindow = $(layero).find("iframe")[0].contentWindow;
                    var submitBtn = $(iframeWindow.document.getElementById("backBtn"));
                    submitBtn.click();
                    return false;
                }
                , btn3: function (index, layero) {
                    if (parentLayer != undefined) {
                        $(obj).children(".layui-layer-btn-c").show();//显示按钮
                    }
                }
            });
        },
        //结算
        settlementForm: function (url, w) {//表单编辑，左侧边框，确认按钮名称
            if (w == undefined) {
                w = i_w;
            }
            if (url.indexOf("?") == -1) {
                url += "?";
            }
            url += "&_t=" + new Date().getTime();

            var parentLayer = parent.layer;
            if (parentLayer != undefined) {
                var parentIndex = parentLayer.getFrameIndex(window.name); //先得到当前iframe层的索引
                var obj = parent.document.getElementById("layui-layer" + parentIndex);
                $(obj).children(".layui-layer-btn-c").hide();//隐藏父页面按钮
            }
            layer.open({
                type: 2,
                anim: 2,
                title: false,
                shade: shade,
                area: [w + '%', '100%'],
                btn: ['<i class="layui-icon layui-icon-ok"></i>&nbsp;设置为已结算', '<i class="layui-icon layui-icon-close"></i>&nbsp;关闭页面'],
                scrollbar: false,//禁用父页面滚动条
                closeBtn: 0,
                content: url,
                offset: 'r',
                btnAlign: 'c',
                resize: false
                , yes: function (index, layero) {
                    //审核通过，调用页面方法
                    var iframeWindow = $(layero).find("iframe")[0].contentWindow;
                    var submitBtn = $(iframeWindow.document.getElementById("successBtn"));
                    submitBtn.click();
                }
                , end: function (index, layero) {
                    if (parentLayer != undefined) {
                        $(obj).children(".layui-layer-btn-c").show();//显示按钮
                    }
                    //触发关闭事件后，执行此方法
                }
            });
        }
        , minForm: function (url, w, h) {
            if (w == undefined) {
                w = "500px";
            }
            if (h == undefined) {
                h = "360px";
            }
            layer.open({
                type: 2
                , anim: 5
                , title: false
                , scrollbar: false//禁用父页面滚动条
                , closeBtn: 0
                , content: url
                , area: [w, h]
                , btn: ['确定', '关闭']
                , yes: function (index, layero) {
                    var iframeWindow = $(layero).find("iframe")[0].contentWindow;
                    var submitBtn = $(iframeWindow.document.getElementById("submitBtn"));
                    submitBtn.click();
                }
                , end: function (index, layero) {
                    //触发关闭事件后，执行此方法
                }
            });
        },
        jmdfMiddleForm: function (url, w, h) {//表单编辑，居中显示
            if (w == undefined) {
                w = i_w;
            }
            if (h == undefined) {
                h = i_h;
            }
            if (url.indexOf("?") == -1) {
                url += "?";
            }
            url += "&_t=" + new Date().getTime();
            layer.open({
                type: 2,
                //closeBtn: 0, //不显示关闭按钮
                anim: 2,
                maxmin: true, //开启最大化最小化按钮
                btn: ['<i class="layui-icon layui-icon-ok"></i>&nbsp;保存', '<i class="layui-icon layui-icon-close"></i>&nbsp;关闭'],
                scrollbar: false,//禁用父页面滚动条
                area: [w + '%', h + '%'],
                shadeClose: true, //开启遮罩关闭
                content: url
                , yes: function (index, layero) {
                    var iframeWindow = $(layero).find("iframe")[0].contentWindow;
                    var submitBtn = $(iframeWindow.document.getElementById("submitBtn"));
                    submitBtn.click();
                }
                , end: function (index, layero) {
                    //触发关闭事件后，执行此方法
                }
            });
        },
        //打开模态框
        openFrame: function (ids, flag) {
            var title = "请再次确认需要入库的档案";
            if (flag == '3') {
                title = "请再次确认需要出库的档案";
            }
            layer.open({
                type: 2,
                title: [title, 'font-size:18px;color:red;'],
                closeBtn: 2,
                area: ['85%', '85%'],
                shadeClose: true, //开启遮罩关闭
                scrollbar: false,//禁用父页面滚动条
                content: "checkData?ids=" + ids + "&flag=" + flag,
                btn: '确认',
                yes: function (index, layero) {
                    var load = "";
                    var iframeWindow = $(layero).find("iframe")[0].contentWindow;
                    //转入操作状态
                    var inContent = $(iframeWindow.document.getElementById("inContent")).val();
                    //转出操作状态
                    var outContent = $(iframeWindow.document.getElementById("outContent")).val();
                    var inOutContent = (inContent == undefined || inContent == "") ? outContent : inContent;
                    $.ajax({
                        url: "sureData",
                        data: {
                            "familyIdList": ids,
                            "flag": flag,
                            "inOutContent": inOutContent
                        },
                        type: "post",
                        traditional: true,
                        beforeSend: function () {
                            if ((inContent == "" || inContent == undefined) && (outContent == "" || outContent == undefined)) {
                                layer.msg("请选择状态！", {anim: 6, icon: 2});
                                return false;
                            }
                            //加载层
                            load = layer.load(1, {shade: [0.8, '#393D49']});
                        },
                        success: function (data) {
                            layer.close(load);
                            if (data.code == "0001") {
                                parent.loadData;
                                layer.msg(data.message, {anim: 0, icon: 1, time: 1000}, function () {
                                    loadData();
                                    layer.close(index);
                                    layer.open({
                                        type: 2,
                                        title: ["导出本批次明细表", 'font-size:18px;color:red;'],
                                        closeBtn: 2,
                                        area: ['85%', '85%'],
                                        shadeClose: true, //开启遮罩关闭
                                        scrollbar: false,//禁用父页面滚动条
                                        content: "exportExcelData?ids=" + ids + "&flag=" + flag
                                    });
                                });
                            } else {
                                layer.msg(data.message, {anim: 6, icon: 2});
                            }
                        }
                    });
                }
            });
        },
        minView: function (url, w, h) {//内容查看
            if (w == undefined) {
                w = "500px";
            }
            if (h == undefined) {
                h = "360px";
            }
            if (url.indexOf("?") == -1) {
                url += "?";
            }
            layer.open({
                type: 2,
                anim: 5,
                title: false,
                area: [w, h],
                btn: ['<i class="layui-icon">&#x1006;</i>&nbsp;关闭'],
                closeBtn: 0,
                content: url
            });
        },
        minForm: function (url, w, h) {//内容查看修改
            if (w == undefined) {
                w = "500px";
            }
            if (h == undefined) {
                h = "360px";
            }
            if (url.indexOf("?") == -1) {
                url += "?";
            }
            layer.open({
                type: 2,
                anim: 5,
                title: false,
                area: [w, h],
                btn: ['<i class="layui-icon layui-icon-ok"></i>&nbsp;保存', '<i class="layui-icon">&#x1006;</i>&nbsp;关闭'],
                closeBtn: 0,
                content: url,
                yes: function (index, layero) {
                    var iframeWindow = $(layero).find("iframe")[0].contentWindow;
                    var submitBtn = $(iframeWindow.document.getElementById("submitBtn"));
                    submitBtn.click();
                },
                end: function (index, layero) {
                    //触发关闭事件后，执行此方法
                }

            });
        },
        loadTitleBack: function (parentIndex) {//加载导航栏退回，参数为父页面索引
            $("#back").click(function () {
                parent.loadData();
                parent.layer.close(parentIndex);
            });
        }
    };
    layui.link('/style/layerOpen.css');//引入css
    //输出接口
    exports('jmdf', obj);
});
