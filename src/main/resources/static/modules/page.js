/**
 扩展模块
 @author:wcf
 @date:2018.08.30
 @version:1.0
 **/

layui.define(["layer", "jquery", 'table'], function (exports) {
    var $ = layui.jquery, table = layui.table;
    var obj = {

        //渲染分页表格
        render: function (options) {
            options['loading'] = true;
            options['skin'] = 'row';
            options['even'] = false;
            options['text'] = {none: '暂无数据'};

            //分页设置
            var page = {
                layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'] //自定义分页布局
            };
            options['page'] = page;//开启分页
            options['limit'] = 15;//每页显示数量
            options['height'] = options['height'] || 'full-120';//外部自定义  ||  框架默认
            options['limits'] = [15, 30, 45, 60, 75, 90, 105, 120];//每页显示数量自定义

            //更改layui框架本身分页参数名称，适合后台框架分页名称
            var request = {
                pageName: '_pageNum' //页码的参数名称，默认：page
                , limitName: '_pageSize' //每页数据量的参数名，默认：limit
            }
            options['request'] = request;

            table.render(options);

            function loadData() {
                var obj = $(".layui-laypage-btn");
                if (obj.html() != undefined) {
                    obj.click();
                } else {
                    table.render(options);
                }
            }

            window.loadData = loadData;//子窗口调用（采用layui本身js写法，无法调用，所以采用此方法）

        }

        //刷新当前页（编辑、删除、上报更新操作等调用此方法）
        , reload: function () {
            loadData();
        }

        //初始化查询方法（searchFormId：查询form元素id名称；tableRenderId：table.render({id:''})中id名称）
        , search: function (searchFormId, tableRenderId) {
            var jsonData = {};
            var a = $("#" + searchFormId).serializeArray();
            $.each(a, function () {
                if (jsonData[this.name]) {
                    if (!jsonData[this.name].push) {
                        jsonData[this.name] = [jsonData[this.name]];
                    }
                    jsonData[this.name].push(this.value || '');
                } else {
                    jsonData[this.name] = this.value || '';
                }
            });
            //执行重载（查询）
            table.reload(tableRenderId, {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
                , where: jsonData
            });
        }
        , sort: function (tableLayFilter, tableRenderId) {
            //监听排序事件
            table.on('sort(' + tableLayFilter + ')', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
                table.reload(tableRenderId, {
                    initSort: obj //记录初始排序，如果不设的话，将无法标记表头的排序状态。
                    , where: { //请求参数（注意：这里面的参数可任意定义，并非下面固定的格式）
                        _orderField: obj.field.replace("_", ".") //排序字段
                        , _orderType: obj.type //排序方式
                    }
                });
            });
        }
    };

    //输出接口
    exports('page', obj);
});