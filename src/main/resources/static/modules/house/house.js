/**
 * Created by drl on 2019/4/4.
 */
layui.define(["layer", "jquery"], function (exports) {
    layui.link('/style/admin.css');//引入css
    var $ = layui.jquery;
    var obj = {

        //封装选房楼盘图（伪选为红色）选房时使用
        getChooseHousePicture: function (data) {
            var buildNum = data.buildNum;
            var floorList = data.floorList;
            var houseMap = data.houseMap;
            var unitList = data.unitList;
            var maxRoomByUnitMap = data.maxRoomByUnitMap;
            var colorMap = data.colorMap;
            var houseTypeMap = data.houseTypeMap;
            var houseNumMap = data.houseNumMap;
            // var directionMap = data.directionMap;
            // var houseCodeMap = data.houseCodeMap;
            var houseFlagMap = data.houseFlagMap;
            var pictureHtml = "<table class='houseTable' border='1'>";

            //单元
            pictureHtml = pictureHtml + "<tr class='units'><td>楼层</td>";
            for (var k = 0; k < unitList.length; k++) {
                var unitNum = unitList[k];
                var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
                pictureHtml = pictureHtml + "<td units='" + unitNum + "' colspan='" + maxRoomCode + "'>" + unitNum + "单元</td>";
            }
            pictureHtml = pictureHtml + "</tr>";

            //户型代码
            // pictureHtml = pictureHtml + "<tr class='units'>";
            // for (var k = 0; k < unitList.length; k++) {
            //     var unitNum = unitList[k];
            //     var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
            //     for (var roomNum = 1; roomNum <= maxRoomCode; roomNum++) {
            //         //房号10以下前缀+0
            //         if (roomNum < 10) {
            //             roomNum = "0" + roomNum;
            //         }
            //         var houseCode = houseCodeMap[unitNum][roomNum] == undefined ? "" : houseCodeMap[unitNum][roomNum];
            //         var houseCodeMemo = "";//户型特殊情况备注
            //         //埝A  1号楼1单元03房号、2单元02房号、3单元03房号特殊
            //         if (projectId == 3 && buildNum == "1" && unitNum == "1" && roomNum == "03") {
            //             houseCodeMemo = "<br/>(首层A2)"
            //         } else if (projectId == 3 && buildNum == "1" && unitNum == "2" && roomNum == "02") {
            //             houseCodeMemo = "<br/>(首层A2反)"
            //         } else if (projectId == 3 && buildNum == "1" && unitNum == "3" && roomNum == "03") {
            //             houseCodeMemo = "<br/>(首层A2)"
            //         } else if (projectId == 2 && buildNum == "14" && unitNum == "5" && roomNum == "01") {
            //             houseCode = "B1反";
            //             houseCodeMemo = "<br/>(顶层B2反)";
            //         } else if (projectId == 2 && buildNum == "15" && unitNum == "5" && roomNum == "01") {
            //             houseCode = "A1反";
            //             houseCodeMemo = "<br/>(顶层A3反)";
            //         }
            //         pictureHtml = pictureHtml + "<td units='" + unitNum + "'>" + houseCode + houseCodeMemo + "</td>";
            //     }
            // }
            // pictureHtml = pictureHtml + "</tr>";

            //居室和朝向
            // pictureHtml = pictureHtml + "<tr class='units'>";
            // for (var k = 0; k < unitList.length; k++) {
            //     var unitNum = unitList[k];
            //     var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
            //     for (var roomNum = 1; roomNum <= maxRoomCode; roomNum++) {
            //         //房号10以下前缀+0
            //         if (roomNum < 10) {
            //             roomNum = "0" + roomNum;
            //         }
            //
            //         var houseTypeMemo = "";//居室特殊情况备注
            //         //埝A  1号楼1单元03房号、2单元02房号、3单元03房号特殊
            //         if (projectId == 3 && buildNum == "1" && unitNum == "1" && roomNum == "03") {
            //             houseTypeMemo = "<br/>(首层一居)"
            //         } else if (projectId == 3 && buildNum == "1" && unitNum == "2" && roomNum == "02") {
            //             houseTypeMemo = "<br/>(首层一居)"
            //         } else if (projectId == 3 && buildNum == "1" && unitNum == "3" && roomNum == "03") {
            //             houseTypeMemo = "<br/>(首层一居)"
            //         }
            //
            //         var houseType = houseNumMap[unitNum][roomNum] == undefined ? "" : houseTypeMap[houseNumMap[unitNum][roomNum]];
            //         var direction = directionMap[unitNum][roomNum] == undefined ? "" : directionMap[unitNum][roomNum];
            //         pictureHtml = pictureHtml + "<td units='" + unitNum + "'>" + houseType + direction + houseTypeMemo + "</td>";
            //     }
            // }
            // pictureHtml = pictureHtml + "</tr>";

            //房源
            // for (var i = maxFloor; i > 0; i--) {
            for (var i = 0; i < floorList.length; i++) {
                var floorStr = floorList[i];
                pictureHtml = pictureHtml + "<tr><td>" + floorStr + "层&nbsp;&nbsp;</td>";
                for (var j = 0; j < unitList.length; j++) {
                    var unitNum = unitList[j];
                    var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
                    for (var roomNum = maxRoomCode; roomNum >= 1; roomNum--) {
                        //房号10以下前缀+0
                        if (parseInt(roomNum) < 10) {
                            roomNum = "0" + parseInt(roomNum);
                        }
                        var houseNum = floorStr + roomNum;
                        var house = houseMap[unitNum + "-" + houseNum];
                        if (house != undefined) {  //房源为空的不循环
                            var houseStatus = house.houseStatus != 1 ? 2 : house.houseStatus;
                            // var houseStatus = house.houseStatus;
                            var houseFlag = houseFlagMap == undefined ? true : houseFlagMap[house.id.toString()];
                            var houseColor = colorMap[houseStatus];
                            //如果可选
                            if (houseFlag) {
                                pictureHtml = pictureHtml + "<td units='" + unitNum + "' style='background-color: " + houseColor + ";' id='" + house.id + "' houseStatus='" + houseStatus + "' projectName='" + house.projectName + "' buildNum='" + house.buildNum + "' unitNum='" + house.unitNum + "' houseNum='" + house.houseNumFormat + "' buildArea='" + house.buildArea + "' houseType='" + houseTypeMap[house.houseType] + "' housePrice='" + house.housePrice + "'><div class='house'>" + house.houseNumFormat + "<br/>(<span style='font-style: oblique;'>" + house.buildArea + "</span>㎡)";
                            } else {
                                houseColor = houseStatus == 1 ? "lightgray" : houseColor;
                                pictureHtml = pictureHtml + "<td units='" + unitNum + "' style='background-color: " + houseColor + ";'><div class='house'>" + house.houseNumFormat + "<br/>(<span style='font-style: oblique;'>" + house.buildArea + "</span>㎡)";
                            }
                        } else {
                            pictureHtml = pictureHtml + "<td units='" + unitNum + "'><div>";
                        }
                        pictureHtml = pictureHtml + "</div></td>";
                    }
                }
                pictureHtml = pictureHtml + "</tr>";
            }
            pictureHtml = pictureHtml + "<tr class='units'><td>楼层</td>";

            //居室和朝向
            // for (var k = 0; k < unitList.length; k++) {
            //     var unitNum = unitList[k];
            //     var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
            //     for (var roomNum = 1; roomNum <= maxRoomCode; roomNum++) {
            //         //房号10以下前缀+0
            //         if (roomNum < 10) {
            //             roomNum = "0" + roomNum;
            //         }
            //
            //         var houseTypeMemo = "";//居室特殊情况备注
            //         //埝A  1号楼1单元03房号、2单元02房号、3单元03房号特殊
            //         if (projectId == 3 && buildNum == "1" && unitNum == "1" && roomNum == "03") {
            //             houseTypeMemo = "<br/>(首层一居)"
            //         } else if (projectId == 3 && buildNum == "1" && unitNum == "2" && roomNum == "02") {
            //             houseTypeMemo = "<br/>(首层一居)"
            //         } else if (projectId == 3 && buildNum == "1" && unitNum == "3" && roomNum == "03") {
            //             houseTypeMemo = "<br/>(首层一居)"
            //         }
            //
            //         var houseType = houseNumMap[unitNum][roomNum] == undefined ? "" : houseTypeMap[houseNumMap[unitNum][roomNum]];
            //         var direction = directionMap[unitNum][roomNum] == undefined ? "" : directionMap[unitNum][roomNum];
            //         pictureHtml = pictureHtml + "<td units='" + unitNum + "'>" + houseType + direction + houseTypeMemo + "</td>";
            //     }
            // }
            // pictureHtml = pictureHtml + "</tr>";

            //户型代码
            // pictureHtml = pictureHtml + "<tr class='units'>";
            // for (var k = 0; k < unitList.length; k++) {
            //     var unitNum = unitList[k];
            //     var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
            //     for (var roomNum = 1; roomNum <= maxRoomCode; roomNum++) {
            //         //房号10以下前缀+0
            //         if (roomNum < 10) {
            //             roomNum = "0" + roomNum;
            //         }
            //
            //         var houseCode = houseCodeMap[unitNum][roomNum] == undefined ? "" : houseCodeMap[unitNum][roomNum];
            //         var houseCodeMemo = "";//户型特殊情况备注
            //         //埝A  1号楼1单元03房号、2单元02房号、3单元03房号特殊
            //         if (projectId == 3 && buildNum == "1" && unitNum == "1" && roomNum == "03") {
            //             houseCodeMemo = "<br/>(首层A2)"
            //         } else if (projectId == 3 && buildNum == "1" && unitNum == "2" && roomNum == "02") {
            //             houseCodeMemo = "<br/>(首层A2反)"
            //         } else if (projectId == 3 && buildNum == "1" && unitNum == "3" && roomNum == "03") {
            //             houseCodeMemo = "<br/>(首层A2)"
            //         } else if (projectId == 2 && buildNum == "14" && unitNum == "5" && roomNum == "01") {
            //             houseCode = "B1反";
            //             houseCodeMemo = "<br/>(顶层B2反)";
            //         } else if (projectId == 2 && buildNum == "15" && unitNum == "5" && roomNum == "01") {
            //             houseCode = "A1反";
            //             houseCodeMemo = "<br/>(顶层A3反)";
            //         }
            //         pictureHtml = pictureHtml + "<td units='" + unitNum + "'>" + houseCode + houseCodeMemo + "</td>";
            //     }
            // }
            // pictureHtml = pictureHtml + "</tr>";

            //单元
            // pictureHtml = pictureHtml + "<tr class='units'>";
            for (var k = 0; k < unitList.length; k++) {
                var unitNum = unitList[k];
                var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
                pictureHtml = pictureHtml + "<td units='" + unitNum + "' colspan='" + maxRoomCode + "'>" + unitNum + "单元</td>";
            }
            pictureHtml = pictureHtml + "</tr>";
            pictureHtml = pictureHtml + "</table>";
            return pictureHtml;
        },

        //封装楼盘图（伪选为红色，楼盘图查看时使用，可查看购房人家庭）
        getPicture: function (data) {
            var projectId = data.projectId;
            var buildNum = data.buildNum;
            var maxFloor = data.maxFloor;
            var houseMap = data.houseMap;
            var houseNameMap = data.houseNameMap;
            var unitList = data.unitList;
            var maxRoomByUnitMap = data.maxRoomByUnitMap;
            var colorMap = data.colorMap;
            var houseTypeMap = data.houseTypeMap;
            var houseNumMap = data.houseNumMap;
            // var directionMap = data.directionMap;
            // var houseCodeMap = data.houseCodeMap;
            var pictureHtml = "<table class='houseTable' border='1'>";

            //单元
            pictureHtml = pictureHtml + "<tr class='units'><td>楼层</td>";
            for (var k = 0; k < unitList.length; k++) {
                var unitNum = unitList[k];
                var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
                pictureHtml = pictureHtml + "<td colspan='" + maxRoomCode + "'>" + unitNum + "单元</td>";
            }
            pictureHtml = pictureHtml + "</tr>";

            //户型代码
            // pictureHtml = pictureHtml + "<tr class='units'>";
            // for (var k = 0; k < unitList.length; k++) {
            //     var unitNum = unitList[k];
            //     var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
            //     for (var roomNum = 1; roomNum <= maxRoomCode; roomNum++) {
            //         //房号10以下前缀+0
            //         if (roomNum < 10) {
            //             roomNum = "0" + roomNum;
            //         }
            //
            //         var houseCode = houseCodeMap[unitNum][roomNum] == undefined ? "" : houseCodeMap[unitNum][roomNum];
            //         var houseCodeMemo = "";//户型特殊情况备注
            //         //埝A  1号楼1单元03房号、2单元02房号、3单元03房号特殊
            //         if (projectId == 3 && buildNum == "1" && unitNum == "1" && roomNum == "03") {
            //             houseCodeMemo = "<br/>(首层A2)"
            //         } else if (projectId == 3 && buildNum == "1" && unitNum == "2" && roomNum == "02") {
            //             houseCodeMemo = "<br/>(首层A2反)"
            //         } else if (projectId == 3 && buildNum == "1" && unitNum == "3" && roomNum == "03") {
            //             houseCodeMemo = "<br/>(首层A2)"
            //         } else if (projectId == 2 && buildNum == "14" && unitNum == "5" && roomNum == "01") {
            //             houseCode = "B1反";
            //             houseCodeMemo = "<br/>(顶层B2反)";
            //         } else if (projectId == 2 && buildNum == "15" && unitNum == "5" && roomNum == "01") {
            //             houseCode = "A1反";
            //             houseCodeMemo = "<br/>(顶层A3反)";
            //         }
            //         pictureHtml = pictureHtml + "<td units='" + unitNum + "'>" + houseCode + houseCodeMemo + "</td>";
            //     }
            // }
            // pictureHtml = pictureHtml + "</tr>";

            //居室和朝向
            // pictureHtml = pictureHtml + "<tr class='units'>";
            // for (var k = 0; k < unitList.length; k++) {
            //     var unitNum = unitList[k];
            //     var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
            //     for (var roomNum = 1; roomNum <= maxRoomCode; roomNum++) {
            //         //房号10以下前缀+0
            //         if (roomNum < 10) {
            //             roomNum = "0" + roomNum;
            //         }
            //
            //         var houseTypeMemo = "";//居室特殊情况备注
            //         //埝A  1号楼1单元03房号、2单元02房号、3单元03房号特殊
            //         if (projectId == 3 && buildNum == "1" && unitNum == "1" && roomNum == "03") {
            //             houseTypeMemo = "<br/>(首层一居)"
            //         } else if (projectId == 3 && buildNum == "1" && unitNum == "2" && roomNum == "02") {
            //             houseTypeMemo = "<br/>(首层一居)"
            //         } else if (projectId == 3 && buildNum == "1" && unitNum == "3" && roomNum == "03") {
            //             houseTypeMemo = "<br/>(首层一居)"
            //         }
            //
            //         var houseType = houseNumMap[unitNum][roomNum] == undefined ? "" : houseTypeMap[houseNumMap[unitNum][roomNum]];
            //         var direction = directionMap[unitNum][roomNum] == undefined ? "" : directionMap[unitNum][roomNum];
            //         pictureHtml = pictureHtml + "<td units='" + unitNum + "'>" + houseType + direction + houseTypeMemo + "</td>";
            //     }
            // }
            // pictureHtml = pictureHtml + "</tr>";

            //房源
            for (var i = maxFloor; i > 0; i--) {
                pictureHtml = pictureHtml + "<tr><td>" + i + "层&nbsp;&nbsp;</td>";
                var floorStr = i;
                //10层以下前缀+0
                if (i < 10) {
                    floorStr = "0" + i;
                }
                for (var j = 0; j < unitList.length; j++) {
                    var unitNum = unitList[j];
                    var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
                    for (var roomNum = 1; roomNum <= maxRoomCode; roomNum++) {
                        //房号10以下前缀+0
                        if (parseInt(roomNum) < 10) {
                            roomNum = "0" + parseInt(roomNum);
                        }
                        var houseNum = floorStr + roomNum;
                        var house = houseMap[unitNum + "-" + houseNum];
                        if (house != undefined) {  //房源为空的不循环
                            var houseStatus = house.houseStatus != 1 ? 2 : house.houseStatus;
                            var houseName = houseNameMap[house.id] == null ? "&nbsp;&nbsp;" : houseNameMap[house.id];
                            pictureHtml = pictureHtml + "<td style='background-color: " + colorMap[houseStatus] + ";' id='" + house.id + "' houseStatus='" + houseStatus + "'><div class='house'>" + house.houseNumFormat + "<br/>(<span style='font-style: oblique;'>" + house.buildArea + "</span>)<br/>" + houseName;
                        } else {
                            pictureHtml = pictureHtml + "<td><div>";
                        }
                        pictureHtml = pictureHtml + "</div></td>";
                    }
                }
                pictureHtml = pictureHtml + "</tr>";
            }
            pictureHtml = pictureHtml + "<tr class='units'><td>楼层</td>";

            //居室和朝向
            // for (var k = 0; k < unitList.length; k++) {
            //     var unitNum = unitList[k];
            //     var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
            //     for (var roomNum = 1; roomNum <= maxRoomCode; roomNum++) {
            //         //房号10以下前缀+0
            //         if (roomNum < 10) {
            //             roomNum = "0" + roomNum;
            //         }
            //
            //         var houseTypeMemo = "";//居室特殊情况备注
            //         //埝A  1号楼1单元03房号、2单元02房号、3单元03房号特殊
            //         if (projectId == 3 && buildNum == "1" && unitNum == "1" && roomNum == "03") {
            //             houseTypeMemo = "<br/>(首层一居)"
            //         } else if (projectId == 3 && buildNum == "1" && unitNum == "2" && roomNum == "02") {
            //             houseTypeMemo = "<br/>(首层一居)"
            //         } else if (projectId == 3 && buildNum == "1" && unitNum == "3" && roomNum == "03") {
            //             houseTypeMemo = "<br/>(首层一居)"
            //         }
            //
            //         var houseType = houseNumMap[unitNum][roomNum] == undefined ? "" : houseTypeMap[houseNumMap[unitNum][roomNum]];
            //         var direction = directionMap[unitNum][roomNum] == undefined ? "" : directionMap[unitNum][roomNum];
            //         pictureHtml = pictureHtml + "<td units='" + unitNum + "'>" + houseType + direction + houseTypeMemo + "</td>";
            //     }
            // }
            // pictureHtml = pictureHtml + "</tr>";

            //居室和朝向、户型代码
            // pictureHtml = pictureHtml + "<tr class='units'>";
            // for (var k = 0; k < unitList.length; k++) {
            //     var unitNum = unitList[k];
            //     var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
            //     for (var roomNum = 1; roomNum <= maxRoomCode; roomNum++) {
            //         //房号10以下前缀+0
            //         if (roomNum < 10) {
            //             roomNum = "0" + roomNum;
            //         }
            //
            //         var houseCode = houseCodeMap[unitNum][roomNum] == undefined ? "" : houseCodeMap[unitNum][roomNum];
            //         var houseCodeMemo = "";//户型特殊情况备注
            //         //埝A  1号楼1单元03房号、2单元02房号、3单元03房号特殊
            //         if (projectId == 3 && buildNum == "1" && unitNum == "1" && roomNum == "03") {
            //             houseCodeMemo = "<br/>(首层A2)"
            //         } else if (projectId == 3 && buildNum == "1" && unitNum == "2" && roomNum == "02") {
            //             houseCodeMemo = "<br/>(首层A2反)"
            //         } else if (projectId == 3 && buildNum == "1" && unitNum == "3" && roomNum == "03") {
            //             houseCodeMemo = "<br/>(首层A2)"
            //         } else if (projectId == 2 && buildNum == "14" && unitNum == "5" && roomNum == "01") {
            //             houseCode = "B1反";
            //             houseCodeMemo = "<br/>(顶层B2反)";
            //         } else if (projectId == 2 && buildNum == "15" && unitNum == "5" && roomNum == "01") {
            //             houseCode = "A1反";
            //             houseCodeMemo = "<br/>(顶层A3反)";
            //         }
            //         pictureHtml = pictureHtml + "<td units='" + unitNum + "'>" + houseCode + houseCodeMemo + "</td>";
            //     }
            // }
            // pictureHtml = pictureHtml + "</tr>";

            //单元
            // pictureHtml = pictureHtml + "<tr class='units'>";
            for (var k = 0; k < unitList.length; k++) {
                var unitNum = unitList[k];
                var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
                pictureHtml = pictureHtml + "<td colspan='" + maxRoomCode + "'>" + unitNum + "单元</td>";
            }
            pictureHtml = pictureHtml + "</tr>";
            pictureHtml = pictureHtml + "</table>";
            return pictureHtml;
        },

        //封装楼盘图（统计-楼盘图查看时使用，有房源所在项目）
        getMarkPicture: function (data) {
            var buildNum = data.buildNum;
            var floorList = data.floorList;
            var houseMap = data.houseMap;
            var unitList = data.unitList;
            var maxRoomByUnitMap = data.maxRoomByUnitMap;
            var colorMap = data.colorMap;
            var houseTypeMap = data.houseTypeMap;
            var houseNumMap = data.houseNumMap;
            var pictureHtml = "<table class='houseTable' border='1'>";

            //单元
            pictureHtml = pictureHtml + "<tr class='units'><td rowspan='2'>楼层</td>";
            for (var k = 0; k < unitList.length; k++) {
                var unitNum = unitList[k];
                var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
                pictureHtml = pictureHtml + "<td colspan='"+ maxRoomCode +"'>" + unitNum + "单元</td>";
            }
            pictureHtml = pictureHtml + "</tr>";

            //居室和朝向
            pictureHtml = pictureHtml + "<tr class='units'>";
            for (var k = 0; k < unitList.length; k++) {
                var unitNum = unitList[k];
                var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
                for (var roomNum = maxRoomCode; roomNum >= 1; roomNum--) {
                    //房号10以下前缀+0
                    if (parseInt(roomNum) < 10) {
                        roomNum = "0" + parseInt(roomNum);
                    }

                    var houseType = houseNumMap[unitNum][roomNum] == undefined ? "" : houseTypeMap[houseNumMap[unitNum][roomNum]];
                    pictureHtml = pictureHtml + "<td units='"+unitNum+"'>" + houseType + "</td>";
                }
            }
            pictureHtml = pictureHtml + "</tr>";

            //房源
            for (var i = 0; i < floorList.length; i++) {
                var floorStr = floorList[i];
                pictureHtml = pictureHtml + "<tr><td>" + floorStr + "层&nbsp;&nbsp;</td>";
                for (var j = 0; j < unitList.length; j++) {
                    var unitNum = unitList[j];
                    var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
                    for (var roomNum = maxRoomCode; roomNum >= 1; roomNum--) {
                        //房号10以下前缀+0
                        if (parseInt(roomNum) < 10) {
                            roomNum = "0" + parseInt(roomNum);
                        }
                        var houseNum = floorStr + roomNum;
                        var house = houseMap[unitNum + "-" + houseNum];
                        if (house != undefined) {  //房源为空的不循环
                            var houseStatus = house.houseStatus != 1 ? 2 : house.houseStatus;
                            pictureHtml = pictureHtml + "<td style='background-color: " + colorMap[houseStatus] + ";' id='" + house.id + "' houseStatus='" + houseStatus + "'><div class='house'>" + house.houseNumFormat;
                            if (houseStatus == 2) {
                                pictureHtml = pictureHtml + "<br/><span>已选</span>";
                            }
                            pictureHtml = pictureHtml + "<br/>(<span style='font-style: oblique;'>" + house.buildArea + "㎡</span>)";
                        }else{
                            pictureHtml = pictureHtml + "<td><div>";
                        }
                        pictureHtml = pictureHtml + "</div></td>";
                    }
                }
                pictureHtml = pictureHtml + "</tr>";
            }

            //居室和朝向
            pictureHtml = pictureHtml + "<tr class='units'><td rowspan='3'>楼层</td>";
            for (var k = 0; k < unitList.length; k++) {
                var unitNum = unitList[k];
                var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
                for (var roomNum = maxRoomCode; roomNum >= 1; roomNum--) {
                    //房号10以下前缀+0
                    if (parseInt(roomNum) < 10) {
                        roomNum = "0" + parseInt(roomNum);
                    }

                    var houseType = houseNumMap[unitNum][roomNum] == undefined ? "" : houseTypeMap[houseNumMap[unitNum][roomNum]];
                    pictureHtml = pictureHtml + "<td units='"+unitNum+"'>" + houseType + "</td>";
                }
            }
            pictureHtml = pictureHtml + "</tr>";

            //单元
            pictureHtml = pictureHtml + "<tr class='units'>";
            for (var k = 0; k < unitList.length; k++) {
                var unitNum = unitList[k];
                var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
                pictureHtml = pictureHtml + "<td colspan='"+ maxRoomCode +"'>" + unitNum + "单元</td>";
            }
            pictureHtml = pictureHtml + "</tr>";
            pictureHtml = pictureHtml + "</table>";
            return pictureHtml;
        },

        //封装楼盘图（平板楼盘图查看时使用，有房源所在项目）
        getAppMarkPicture: function (data) {
            var buildNum = data.buildNum;
            var floorList = data.floorList;
            var houseMap = data.houseMap;
            var unitList = data.unitList;
            var maxRoomByUnitMap = data.maxRoomByUnitMap;
            var colorMap = data.colorMap;
            var houseTypeMap = data.houseTypeMap;
            var houseNumMap = data.houseNumMap;
            var pictureHtml = "<table class='houseTable' border='1'>";

            //单元
            pictureHtml = pictureHtml + "<tr class='units'><td rowspan='2'>楼层</td>";
            for (var k = 0; k < unitList.length; k++) {
                var unitNum = unitList[k];
                var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
                pictureHtml = pictureHtml + "<td colspan='"+ maxRoomCode +"' style='font-size: 22px;font-weight: bolder'>" + unitNum + "单元</td>";
            }
            pictureHtml = pictureHtml + "</tr>";

            //居室和朝向
            pictureHtml = pictureHtml + "<tr class='units'>";
            for (var k = 0; k < unitList.length; k++) {
                var unitNum = unitList[k];
                var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
                for (var roomNum = maxRoomCode; roomNum >= 1; roomNum--) {
                    //房号10以下前缀+0
                    if (parseInt(roomNum) < 10) {
                        roomNum = "0" + parseInt(roomNum);
                    }

                    var houseType = houseNumMap[unitNum][roomNum] == undefined ? "" : houseTypeMap[houseNumMap[unitNum][roomNum]];
                    pictureHtml = pictureHtml + "<td units='"+unitNum+"'>" + houseType + "</td>";
                }
            }
            pictureHtml = pictureHtml + "</tr>";

            //房源
            for (var i = 0; i < floorList.length; i++) {
                var floorStr = floorList[i];
                pictureHtml = pictureHtml + "<tr><td style='font-weight: bolder;font-size: 22px;'>" + floorStr + "层&nbsp;&nbsp;</td>";
                for (var j = 0; j < unitList.length; j++) {
                    var unitNum = unitList[j];
                    var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
                    for (var roomNum = maxRoomCode; roomNum >= 1; roomNum--) {
                        //房号10以下前缀+0
                        if (parseInt(roomNum) < 10) {
                            roomNum = "0" + parseInt(roomNum);
                        }
                        var houseNum = floorStr + roomNum;
                        var house = houseMap[unitNum + "-" + houseNum];
                        if (house != undefined) {  //房源为空的不循环
                            var houseStatus = house.houseStatus != 1 ? 2 : house.houseStatus;
                            pictureHtml = pictureHtml + "<td style='background-color: " + colorMap[houseStatus] + ";' id='" + house.id + "' houseStatus='" + houseStatus + "'><div class='house' style='font-weight: bolder;font-size: 22px;'>" + house.houseNumFormat + "<div>"
                                + "<span style='font-style: oblique;'>" + house.lowerHousePrice + "</span>"
                                + "<br/><span style='font-style: oblique;'>" + house.higherHousePrice + "</span>"
                                + "<br/><span style='font-style: oblique;'>" + house.buildArea + "㎡</span>";
                        }else{
                            pictureHtml = pictureHtml + "<td>";
                        }
                        pictureHtml = pictureHtml + "</div></td>";
                    }
                }
                pictureHtml = pictureHtml + "</tr>";
            }

            //居室和朝向
            pictureHtml = pictureHtml + "<tr class='units'><td rowspan='3'>楼层</td>";
            for (var k = 0; k < unitList.length; k++) {
                var unitNum = unitList[k];
                var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
                for (var roomNum = maxRoomCode; roomNum >= 1; roomNum--) {
                    //房号10以下前缀+0
                    if (parseInt(roomNum) < 10) {
                        roomNum = "0" + parseInt(roomNum);
                    }

                    var houseType = houseNumMap[unitNum][roomNum] == undefined ? "" : houseTypeMap[houseNumMap[unitNum][roomNum]];
                    pictureHtml = pictureHtml + "<td units='"+unitNum+"'>" + houseType + "</td>";
                }
            }
            pictureHtml = pictureHtml + "</tr>";

            //单元
            pictureHtml = pictureHtml + "<tr class='units'>";
            for (var k = 0; k < unitList.length; k++) {
                var unitNum = unitList[k];
                var maxRoomCode = maxRoomByUnitMap[unitNum.toString()];//最大房号
                pictureHtml = pictureHtml + "<td colspan='"+ maxRoomCode +"'>" + unitNum + "单元</td>";
            }
            pictureHtml = pictureHtml + "</tr>";
            pictureHtml = pictureHtml + "</table>";
            return pictureHtml;
        },

    };
    //输出接口
    exports('house', obj);
});

