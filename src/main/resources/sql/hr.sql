### 企业人事 管理系统 表结构

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for data_base
-- ----------------------------
DROP TABLE IF EXISTS `data_base`;
CREATE TABLE `data_base`
(
    `id`             int(11) NOT NULL AUTO_INCREMENT,
    `file_date`      datetime                                                DEFAULT NULL,
    `file_date_long` bigint(20)                                              DEFAULT NULL,
    `file_name`      varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `file_path`      varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `file_size`      bigint(20)                                              DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of data_base
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for data_dict
-- ----------------------------
DROP TABLE IF EXISTS `data_dict`;
CREATE TABLE `data_dict`
(
    `id`          int(11) NOT NULL AUTO_INCREMENT,
    `memo`        varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `name`        varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `sort_index`  int(11)                                                 DEFAULT NULL,
    `value`       varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `parent_id`   int(11)                                                 DEFAULT NULL,
    `enabled`     int(11)                                                 DEFAULT NULL,
    `color`       varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `create_time` datetime                                                DEFAULT NULL,
    `update_time` datetime                                                DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE,
    KEY `FK4qek5xyihjo5www2vqnyjv2ib` (`parent_id`) USING BTREE,
    CONSTRAINT `FK4qek5xyihjo5www2vqnyjv2ib` FOREIGN KEY (`parent_id`) REFERENCES `data_dict` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB
  AUTO_INCREMENT = 140
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of data_dict
-- ----------------------------
BEGIN;
INSERT INTO `data_dict`
VALUES (7, NULL, '系统配置', 1, NULL, NULL, 0, NULL, NULL, NULL);
INSERT INTO `data_dict`
VALUES (11, NULL, '居室', 5, NULL, NULL, NULL, NULL, NULL, '2020-06-28 11:26:21');
INSERT INTO `data_dict`
VALUES (21, '', '系统名称', 10, '企业人事管理系统', 7, 0, '', NULL, '2022-01-13 16:12:04');
INSERT INTO `data_dict`
VALUES (22, '', '系统版本号', 20, 'Version1.01', 7, 0, '', NULL, '2019-11-13 16:23:01');
INSERT INTO `data_dict`
VALUES (28, NULL, '有效状态', 10, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `data_dict`
VALUES (29, '', '有效', 5, '0', 28, 0, '#0891fb', NULL, NULL);
INSERT INTO `data_dict`
VALUES (30, '', '无效', 10, '1', 28, 0, '#fb2b14', NULL, NULL);
INSERT INTO `data_dict`
VALUES (31, NULL, '用户状态', 15, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `data_dict`
VALUES (32, '', '正常', 5, '1', 31, 0, '#1c97f5', NULL, NULL);
INSERT INTO `data_dict`
VALUES (33, '', '锁定', 10, '2', 31, 0, '#e64b1c', NULL, NULL);
INSERT INTO `data_dict`
VALUES (38,
        '数据库自动备份时间间隔设置，参考：http://cron.qqe2.com/\n每隔五秒执行一次：0/5 * * * * *\n每隔三十分钟执行一次：0 0/10 * * * *\n每隔一个小时执行一次：0 0 0/1 * * *',
        'cronTrigger', 40, '0 0 0/1 * * *', 7, 1, '#f5481c', NULL, '2020-07-30 17:30:16');
INSERT INTO `data_dict`
VALUES (39, '1：启用验证码', '验证码', 30, '2', 7, 0, '', NULL, '2020-06-28 13:43:09');
INSERT INTO `data_dict`
VALUES (51, NULL, '是否', 30, NULL, NULL, NULL, NULL, '2020-06-29 09:30:52', '2020-06-29 09:30:52');
INSERT INTO `data_dict`
VALUES (52, '', '是', 10, '1', 51, 0, '', '2020-06-29 09:30:59', '2020-06-29 09:30:59');
INSERT INTO `data_dict`
VALUES (53, '', '否', 20, '2', 51, 0, '', '2020-06-29 09:31:05', '2020-06-29 09:31:05');
INSERT INTO `data_dict`
VALUES (62, NULL, '图片路径配置', 2, NULL, NULL, NULL, NULL, '2020-07-13 10:29:54', '2020-07-13 10:29:54');
INSERT INTO `data_dict`
VALUES (63, '', '图片路径', 10, 'D:\\project\\sy_scjg_bt\\pic\\', 62, 0, '', '2020-07-13 10:30:15', '2020-07-13 10:30:15');
INSERT INTO `data_dict`
VALUES (99, NULL, '是否正在发放住房补贴', 60, NULL, NULL, NULL, NULL, '2021-06-25 09:53:57', '2021-06-25 09:53:57');
INSERT INTO `data_dict`
VALUES (100, '', '是', 1, '1', 99, 0, '#1cf5bb', '2021-06-25 09:54:26', '2021-06-25 09:54:26');
INSERT INTO `data_dict`
VALUES (101, '', '否', 2, '2', 99, 0, '#f5391c', '2021-06-25 09:54:41', '2021-06-25 09:54:41');
INSERT INTO `data_dict`
VALUES (102, NULL, '职务名称', 70, NULL, NULL, NULL, NULL, '2021-06-25 13:55:58', '2021-06-25 13:55:58');
INSERT INTO `data_dict`
VALUES (103, '', '高级职称', 1, '001001', 102, 0, '', '2021-06-25 13:56:54', '2021-06-25 13:56:54');
INSERT INTO `data_dict`
VALUES (104, '', '中级职称', 2, '002001', 102, 0, '', '2021-06-25 13:57:19', '2021-06-25 13:57:19');
INSERT INTO `data_dict`
VALUES (105, '', '低级职称', 3, '003001', 102, 0, '', '2021-06-25 13:57:44', '2021-06-25 13:57:44');
INSERT INTO `data_dict`
VALUES (106, NULL, '情况类型', 80, NULL, NULL, NULL, NULL, '2021-06-27 10:12:57', '2021-06-27 10:12:57');
INSERT INTO `data_dict`
VALUES (107, '', '在职', 1, '1', 106, 0, '', '2021-06-27 10:13:11', '2021-06-27 10:13:11');
INSERT INTO `data_dict`
VALUES (108, '', '离退休', 2, '2', 106, 0, '', '2021-06-27 10:13:19', '2021-06-27 10:13:19');
INSERT INTO `data_dict`
VALUES (109, '', '其他', 3, '3', 106, 0, '', '2021-06-27 10:13:26', '2021-06-27 10:13:26');
INSERT INTO `data_dict`
VALUES (110, NULL, '职工类型', 90, NULL, NULL, NULL, NULL, '2021-06-27 10:15:43', '2021-06-27 10:15:43');
INSERT INTO `data_dict`
VALUES (111, '', '未达标职工', 1, '1', 110, 0, '', '2021-06-27 10:16:05', '2021-06-27 10:16:05');
INSERT INTO `data_dict`
VALUES (112, '', '新增未达标职工', 2, '2', 110, 0, '', '2021-06-27 10:16:11', '2021-06-27 10:16:11');
INSERT INTO `data_dict`
VALUES (113, '', '级差职工', 3, '3', 110, 0, '', '2021-06-27 10:16:20', '2021-06-27 10:16:20');
INSERT INTO `data_dict`
VALUES (114, NULL, '是否享受过政府补贴', 100, NULL, NULL, NULL, NULL, '2021-06-28 09:08:54', '2021-06-28 09:08:54');
INSERT INTO `data_dict`
VALUES (115, '', '未享受', 1, '1', 114, 0, '', '2021-06-28 09:09:07', '2021-06-28 09:09:07');
INSERT INTO `data_dict`
VALUES (116, '', '已享受', 2, '2', 114, 0, '', '2021-06-28 09:09:15', '2021-06-28 09:09:15');
INSERT INTO `data_dict`
VALUES (117, NULL, '操作类别', 110, NULL, NULL, NULL, NULL, '2021-06-29 13:42:38', '2021-06-29 13:42:38');
INSERT INTO `data_dict`
VALUES (118, '', '新增', 1, '1', 117, 0, '', '2021-06-29 13:42:56', '2021-06-29 13:42:56');
INSERT INTO `data_dict`
VALUES (119, '', '修改', 2, '2', 117, 0, '', '2021-06-29 13:43:02', '2021-06-29 13:43:02');
INSERT INTO `data_dict`
VALUES (120, '', '删除', 3, '3', 117, 0, '', '2021-06-29 13:43:16', '2021-06-29 13:43:16');
INSERT INTO `data_dict`
VALUES (121, NULL, '操作业务表类别', 120, NULL, NULL, NULL, NULL, '2021-06-29 13:46:54', '2021-06-29 13:46:54');
INSERT INTO `data_dict`
VALUES (122, '', '员工表', 1, '1', 121, 0, '', '2021-06-29 13:47:04', '2021-06-29 13:47:04');
INSERT INTO `data_dict`
VALUES (123, '', '无房职工申报表', 2, '2', 121, 0, '', '2021-06-29 13:47:12', '2021-06-29 13:47:12');
INSERT INTO `data_dict`
VALUES (124, '', '未达标职工申报表', 3, '3', 121, 0, '', '2021-06-29 13:47:19', '2021-06-29 13:47:19');
INSERT INTO `data_dict`
VALUES (125, '', '无房职工工资表', 4, '4', 121, 0, '', '2021-06-30 09:26:54', '2021-06-30 09:26:54');
INSERT INTO `data_dict`
VALUES (126, NULL, '配偶是否享受过补贴', 130, NULL, NULL, NULL, NULL, '2021-06-30 14:50:19', '2021-06-30 14:50:19');
INSERT INTO `data_dict`
VALUES (127, NULL, '编号规则', 140, NULL, NULL, NULL, NULL, '2021-08-02 16:19:25', '2021-08-02 16:19:25');
INSERT INTO `data_dict`
VALUES (128, '机关本级', 'JGBJ-', 1, '0010', 127, 0, '', '2021-08-02 16:20:00', '2021-08-02 16:21:07');
INSERT INTO `data_dict`
VALUES (129, '顺义区市场局计量检测所', 'JCS-', 2, '0011', 127, 0, '', '2021-08-02 16:20:14', '2021-08-02 17:33:27');
INSERT INTO `data_dict`
VALUES (130, '顺义区市场局特种设备检测所', 'TJS-', 3, '0012', 127, 0, '', '2021-08-02 16:20:30', '2021-08-02 17:33:31');
INSERT INTO `data_dict`
VALUES (131, '顺义区市场局食品药品安全监控中心', 'JKZX-', 4, '0013', 127, 0, '', '2021-08-02 16:20:51', '2021-08-02 17:33:35');
INSERT INTO `data_dict`
VALUES (132, NULL, '申报状态', 150, NULL, NULL, NULL, NULL, '2021-08-16 20:26:36', '2021-08-16 20:26:36');
INSERT INTO `data_dict`
VALUES (133, '', '未申报', 1, '1', 132, 0, '', '2021-08-16 20:26:51', '2021-08-16 20:26:51');
INSERT INTO `data_dict`
VALUES (134, '', '已申报', 2, '2', 132, 0, '', '2021-08-16 20:26:58', '2021-08-16 20:26:58');
INSERT INTO `data_dict`
VALUES (135, NULL, '部门名称', 160, NULL, NULL, NULL, NULL, '2021-08-16 20:30:24', '2021-08-16 20:30:24');
INSERT INTO `data_dict`
VALUES (136, '', '机关本机', 1, '0010', 135, 0, '', '2021-08-16 20:31:02', '2021-08-16 20:31:02');
INSERT INTO `data_dict`
VALUES (137, '', '检测所', 2, '0011', 135, 0, '', '2021-08-16 20:31:13', '2021-08-16 20:31:13');
INSERT INTO `data_dict`
VALUES (138, '', '特检所', 3, '0012', 135, 0, '', '2021-08-16 20:31:27', '2021-08-16 20:31:27');
INSERT INTO `data_dict`
VALUES (139, '', '监控中心', 4, '0013', 135, 0, '', '2021-08-16 20:31:43', '2021-08-16 20:31:43');
COMMIT;

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department`
(
    `id`          int(11) NOT NULL AUTO_INCREMENT,
    `code`        varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `name`        varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `sort_index`  int(11)                                                 DEFAULT NULL,
    `parent_id`   int(11)                                                 DEFAULT NULL,
    `state`       int(11) NOT NULL,
    `create_time` datetime                                                DEFAULT NULL,
    `update_time` datetime                                                DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE,
    KEY `FKmgsnnmudxrwqidn4f64q8rp4o` (`parent_id`) USING BTREE,
    CONSTRAINT `FKmgsnnmudxrwqidn4f64q8rp4o` FOREIGN KEY (`parent_id`) REFERENCES `department` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB
  AUTO_INCREMENT = 256
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of department
-- ----------------------------
BEGIN;
INSERT INTO `department`
VALUES (1, '001', '管理部门', 1, NULL, 1, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for log_business
-- ----------------------------
DROP TABLE IF EXISTS `log_business`;
CREATE TABLE `log_business`
(
    `id`               bigint(20) NOT NULL AUTO_INCREMENT,
    `class_method`     varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `create_date`      datetime                                                DEFAULT NULL,
    `create_date_long` bigint(20)                                              DEFAULT NULL,
    `ip`               varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `operation`        varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `uri`              text CHARACTER SET utf8 COLLATE utf8_general_ci,
    `url`              varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `username`         varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of log_business
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for log_system
-- ----------------------------
DROP TABLE IF EXISTS `log_system`;
CREATE TABLE `log_system`
(
    `id`               bigint(20) NOT NULL AUTO_INCREMENT,
    `class_method`     varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `create_date`      datetime                                                DEFAULT NULL,
    `create_date_long` bigint(20)                                              DEFAULT NULL,
    `ip`               varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `operation`        varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `uri`              text CHARACTER SET utf8 COLLATE utf8_general_ci,
    `url`              varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `username`         varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1118
  DEFAULT CHARSET = utf8
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of log_system
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for no_house_worker_apply
-- ----------------------------
DROP TABLE IF EXISTS `no_house_worker_apply`;
CREATE TABLE `no_house_worker_apply`
(
    `id`                       int(11) NOT NULL AUTO_INCREMENT,
    `create_time`              datetime                                                DEFAULT NULL,
    `update_time`              datetime                                                DEFAULT NULL,
    `id_card`                  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `is_paying_subsidies`      int(11)                                                 DEFAULT NULL,
    `last_subsidy_coefficient` decimal(19, 2)                                          DEFAULT NULL,
    `name`                     varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `subsidy_coefficient`      decimal(19, 2)                                          DEFAULT NULL,
    `total_subsidy`            decimal(19, 2)                                          DEFAULT NULL,
    `worker_info_id`           int(11)                                                 DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE,
    KEY `FKnn7m8r0hc3vec9dw4q8844bfs` (`worker_info_id`) USING BTREE,
    CONSTRAINT `FKnn7m8r0hc3vec9dw4q8844bfs` FOREIGN KEY (`worker_info_id`) REFERENCES `worker_info` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of no_house_worker_apply
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for no_standard_worker_apply
-- ----------------------------
DROP TABLE IF EXISTS `no_standard_worker_apply`;
CREATE TABLE `no_standard_worker_apply`
(
    `id`                       int(11) NOT NULL AUTO_INCREMENT,
    `create_time`              datetime                                                DEFAULT NULL,
    `update_time`              datetime                                                DEFAULT NULL,
    `bought_troops`            varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `bought_troops_subsidy`    decimal(19, 2)                                          DEFAULT NULL,
    `bought_unit`              varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `bought_unit_subsidy`      decimal(19, 2)                                          DEFAULT NULL,
    `build_area_standard`      decimal(19, 2)                                          DEFAULT NULL,
    `build_area_sure_standard` decimal(19, 2)                                          DEFAULT NULL,
    `department_code`          varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `difference_build_area`    decimal(19, 2)                                          DEFAULT NULL,
    `difference_subsidy`       decimal(19, 2)                                          DEFAULT NULL,
    `enjoyed_build_area`       decimal(19, 2)                                          DEFAULT NULL,
    `id_card`                  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `is_enjoyed`               int(11)                                                 DEFAULT NULL,
    `job_status`               int(11)                                                 DEFAULT NULL,
    `join_working_date`        date                                                    DEFAULT NULL,
    `living_address`           varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `memo`                     longtext CHARACTER SET utf8 COLLATE utf8_general_ci,
    `name`                     varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `num`                      int(11)                                                 DEFAULT NULL,
    `phone`                    varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `record_no`                varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `retire_date`              date                                                    DEFAULT NULL,
    `spouse_enjoyed_area`      decimal(19, 2)                                          DEFAULT NULL,
    `spouse_id_card`           varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `spouse_is_enjoyed`        int(11)                                                 DEFAULT NULL,
    `spouse_name`              varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `spouse_unit`              varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `start_working_date`       date                                                    DEFAULT NULL,
    `total_subsidy`            decimal(19, 2)                                          DEFAULT NULL,
    `worked_years`             decimal(19, 2)                                          DEFAULT NULL,
    `worker_type`              int(11)                                                 DEFAULT NULL,
    `last_position_id`         int(11)                                                 DEFAULT NULL,
    `position_id`              int(11)                                                 DEFAULT NULL,
    `retire_position_id`       int(11)                                                 DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE,
    KEY `FKh4tk01ljklt71r8eapide60rb` (`last_position_id`) USING BTREE,
    KEY `FK4gal12qllrbkw1l32mbu68hdf` (`position_id`) USING BTREE,
    KEY `FK2nahy497jklh7xb71n318dpku` (`retire_position_id`) USING BTREE,
    CONSTRAINT `FK2nahy497jklh7xb71n318dpku` FOREIGN KEY (`retire_position_id`) REFERENCES `position` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
    CONSTRAINT `FK4gal12qllrbkw1l32mbu68hdf` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
    CONSTRAINT `FKh4tk01ljklt71r8eapide60rb` FOREIGN KEY (`last_position_id`) REFERENCES `position` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of no_standard_worker_apply
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for operation
-- ----------------------------
DROP TABLE IF EXISTS `operation`;
CREATE TABLE `operation`
(
    `id`              int(11) NOT NULL AUTO_INCREMENT,
    `create_time`     datetime                                                DEFAULT NULL,
    `update_time`     datetime                                                DEFAULT NULL,
    `business_id`     int(11)                                                 DEFAULT NULL,
    `business_type`   int(11)                                                 DEFAULT NULL,
    `json_data`       longtext CHARACTER SET utf8 COLLATE utf8_general_ci,
    `operate_content` longtext CHARACTER SET utf8 COLLATE utf8_general_ci,
    `operate_type`    int(11)                                                 DEFAULT NULL,
    `name`            varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of operation
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for position
-- ----------------------------
DROP TABLE IF EXISTS `position`;
CREATE TABLE `position`
(
    `id`          int(11) NOT NULL AUTO_INCREMENT,
    `create_time` datetime                                                DEFAULT NULL,
    `update_time` datetime                                                DEFAULT NULL,
    `area`        decimal(19, 2)                                          DEFAULT NULL,
    `code`        varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `name`        varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 27
  DEFAULT CHARSET = utf8
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of position
-- ----------------------------
BEGIN;
INSERT INTO `position`
VALUES (4, NULL, NULL, 150.00, '001001', '正局级');
INSERT INTO `position`
VALUES (5, NULL, NULL, 135.00, '001002', '副局级');
INSERT INTO `position`
VALUES (6, NULL, NULL, 120.00, '001003', '正处级');
INSERT INTO `position`
VALUES (7, NULL, NULL, 105.00, '001004', '副处级、25年（含）以上工龄的正、副科级');
INSERT INTO `position`
VALUES (8, NULL, NULL, 90.00, '001005', '正副科级、25年（含)以上工龄的科员、办事员');
INSERT INTO `position`
VALUES (9, NULL, NULL, 75.00, '001006', '科级以下');
INSERT INTO `position`
VALUES (11, NULL, NULL, 150.00, '002001', '三级');
INSERT INTO `position`
VALUES (12, NULL, NULL, 135.00, '002002', '四级');
INSERT INTO `position`
VALUES (13, NULL, NULL, 120.00, '002003', '五级');
INSERT INTO `position`
VALUES (14, NULL, NULL, 105.00, '002004', '六级');
INSERT INTO `position`
VALUES (15, NULL, NULL, 90.00, '002005', '七、八级、25年（含）以上工龄的九、十级职员');
INSERT INTO `position`
VALUES (16, NULL, NULL, 75.00, '002006', '九级、十级职员');
INSERT INTO `position`
VALUES (18, NULL, NULL, 135.00, '003001', '正高');
INSERT INTO `position`
VALUES (19, NULL, NULL, 120.00, '003002', '副高');
INSERT INTO `position`
VALUES (20, NULL, NULL, 90.00, '003003', '中级');
INSERT INTO `position`
VALUES (21, NULL, NULL, 90.00, '003004', '七、八级、25年(含）以上工龄的初级师、士');
INSERT INTO `position`
VALUES (22, NULL, NULL, 75.00, '003005', '25年以下工龄的初级师、士');
INSERT INTO `position`
VALUES (24, NULL, NULL, 105.00, '004001', '高级技师');
INSERT INTO `position`
VALUES (25, NULL, NULL, 90.00, '004002', '高级工、技师、25年（含）以上工龄的初、中级技术工人、普通工人');
INSERT INTO `position`
VALUES (26, NULL, NULL, 75.00, '004003', '25年以下工龄的初、中级技术工人、普通工人');
COMMIT;

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission`
(
    `id`            int(11) NOT NULL AUTO_INCREMENT,
    `available`     bit(1)                                                            DEFAULT NULL,
    `name`          varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci           DEFAULT NULL,
    `parent_id`     int(20)                                                           DEFAULT NULL,
    `permission`    varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci           DEFAULT NULL,
    `resource_type` enum ('menu','button') CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `url`           varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci           DEFAULT NULL,
    `sort_index`    int(11)                                                           DEFAULT NULL,
    `enabled`       int(11)                                                           DEFAULT NULL,
    `memo`          varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci           DEFAULT NULL,
    `icon`          varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci           DEFAULT NULL,
    `create_time`   datetime                                                          DEFAULT NULL,
    `update_time`   datetime                                                          DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE,
    KEY `FKq4uf7xhrxa5dnbpf4a47rqvha` (`parent_id`) USING BTREE,
    CONSTRAINT `FKq4uf7xhrxa5dnbpf4a47rqvha` FOREIGN KEY (`parent_id`) REFERENCES `sys_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB
  AUTO_INCREMENT = 118
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_permission
-- ----------------------------
BEGIN;
INSERT INTO `sys_permission`
VALUES (1, b'0', '用户数据管理', 28, 'userInfo:list;userInfo:loadData', 'menu', '/permission/userInfo/list', 40, 0, '',
        'layui-icon-user', NULL, NULL);
INSERT INTO `sys_permission`
VALUES (2, b'0', '添加/修改', 1, 'userInfo:form;userInfo:save', 'button', '/permission/userInfo/form', 1, 0, '', NULL, NULL,
        NULL);
INSERT INTO `sys_permission`
VALUES (5, NULL, 'Shiro过滤链配置', 28, 'sysPermissionInit:manager;sysPermissionInit:loadData', 'menu',
        '/permission/sysPermissionInit/manager', 10, 0, '', 'layui-icon-set', NULL, NULL);
INSERT INTO `sys_permission`
VALUES (7, NULL, '删除', 5, 'sysPermissionInit:del', 'button', '/permission/sysPermissionInit/del', 15, 0, NULL, NULL,
        NULL, NULL);
INSERT INTO `sys_permission`
VALUES (8, NULL, '更新', 5, 'sysPermissionInit:updatePermission', 'button',
        '/permission/sysPermissionInit/updatePermission', 10, 0, NULL, NULL, NULL, '2019-10-25 11:05:58');
INSERT INTO `sys_permission`
VALUES (9, NULL, '编辑', 5, 'sysPermissionInit:form', 'button', '/permission/sysPermissionInit/form', 5, 0, NULL, NULL,
        NULL, '2019-10-25 11:06:14');
INSERT INTO `sys_permission`
VALUES (11, NULL, '数据字典配置', 28, 'dataDict:manager;dataDict:loadDataDict;dataDict:loadItemDataDict', 'menu',
        '/permission/dataDict/manager', 15, 0, '', 'layui-icon-read', NULL, NULL);
INSERT INTO `sys_permission`
VALUES (15, NULL, '删除', 11, 'dataDict:deleteDataDict', 'button', '/permission/dataDict/deleteDataDict', 15, 0, '', NULL,
        NULL, NULL);
INSERT INTO `sys_permission`
VALUES (16, NULL, '字典新增/修改', 11, 'dataDict:form;dataDict:save', 'button', '/permission/dataDict/form', 5, 0, '', NULL,
        NULL, '2019-10-25 11:10:30');
INSERT INTO `sys_permission`
VALUES (17, NULL, '启用', 11, 'dataDict:setEnabled', 'button', '/permission/dataDict/setEnabled', 10, 0, '', NULL, NULL,
        '2019-10-25 11:10:31');
INSERT INTO `sys_permission`
VALUES (23, b'0', 'Durid数据监控', 28, '', 'menu', '/druid/index.html', 5, 0, '', 'layui-icon-template', NULL, NULL);
INSERT INTO `sys_permission`
VALUES (28, b'0', '系统权限配置', NULL, '', 'menu', '', 999, 0, '', 'layui-icon-chart', NULL, '2020-06-28 11:24:11');
INSERT INTO `sys_permission`
VALUES (35, b'0', '功能菜单管理', 28, 'sysPermission:list;sysPermission:loadTreeData;sysPermission:loadButtonData', 'menu',
        '/permission/sysPermission/list', 20, 0, '', 'layui-icon-align-right', NULL, NULL);
INSERT INTO `sys_permission`
VALUES (43, b'0', '角色信息管理', 28, 'sysRole:list;sysRole:loadData', 'menu', '/permission/sysRole/list', 30, 0, '',
        'layui-icon-group', NULL, NULL);
INSERT INTO `sys_permission`
VALUES (44, b'0', '新增/维护', 43, 'sysRole:form;sysRole:save', 'button', '/permission/sysRole/form', 5, 0, '', NULL, NULL,
        '2019-10-25 11:14:07');
INSERT INTO `sys_permission`
VALUES (45, b'0', '删除', 43, 'sysRole:deleteData', 'button', '/permission/sysRole/deleteData', 10, 0, '', NULL, NULL,
        '2019-10-25 11:14:07');
INSERT INTO `sys_permission`
VALUES (46, b'0', '启用/禁用', 43, 'sysRole:setAvailable', 'button', '/permission/sysRole/setAvailable', 15, 0, '', NULL,
        NULL, '2019-10-25 11:14:08');
INSERT INTO `sys_permission`
VALUES (48, b'0', '菜单添加/修改', 35, 'sysPermission:formMenu;sysPermission:saveMenu', 'button',
        '/permission/sysPermission/formMenu', 10, 0, '', NULL, NULL, '2019-10-25 11:42:51');
INSERT INTO `sys_permission`
VALUES (49, b'0', '菜单删除', 35, 'sysPermission:deleteMenuData', 'button', '/permission/sysPermission/deleteMenuData', 15,
        0, '', NULL, NULL, NULL);
INSERT INTO `sys_permission`
VALUES (50, b'0', '功能添加/修改', 35, 'sysPermission:formButton;sysPermission:saveButton;sysPermission:saveCellEditButton',
        'button', '/permission/sysPermission/formButton', 15, 0, '', NULL, NULL, '2019-10-30 11:51:42');
INSERT INTO `sys_permission`
VALUES (51, b'0', '功能删除', 35, 'sysPermission:deleteButtonData', 'button', '/permission/sysPermission/deleteButtonData',
        20, 0, '', NULL, NULL, '2019-10-25 11:42:52');
INSERT INTO `sys_permission`
VALUES (53, b'0', '更新权限', 35, 'sysPermission:clearShiroCatch', 'button', '/permission/sysPermission/clearShiroCatch',
        30, 0, '', NULL, NULL, NULL);
INSERT INTO `sys_permission`
VALUES (54, b'0', '功能禁用/启用', 35, 'sysPermission:setEnabled', 'button', '/permission/sysPermission/setEnabled', 30, 0,
        '', NULL, NULL, NULL);
INSERT INTO `sys_permission`
VALUES (55, b'0', '用户状态设置', 1, 'userInfo:setState', 'button', '/permission/userInfo/setState', 10, 0, '', NULL, NULL,
        NULL);
INSERT INTO `sys_permission`
VALUES (56, b'0', '设置密码', 1, 'userInfo:pwdForm;userInfo:savePwd', 'button', '/permission/userInfo/pwdForm', 15, 0, '',
        NULL, NULL, NULL);
INSERT INTO `sys_permission`
VALUES (57, b'0', '分配权限', 43, 'sysRole:setPermission;sysRole:permissionJson;sysRole:saveRolePermission', 'button',
        '/permission/sysRole/setPermission', 20, 0, '', NULL, NULL, '2019-10-25 11:14:08');
INSERT INTO `sys_permission`
VALUES (58, b'0', '内容新增/修改', 11, 'dataDict:formItem;dataDict:saveItem', 'button', '/permission/dataDict/formItem', 20,
        0, '', NULL, NULL, NULL);
INSERT INTO `sys_permission`
VALUES (59, b'0', '用户删除', 1, 'userInfo:delById', 'button', '/permission/userInfo/delById', 20, 0, '', NULL, NULL, NULL);
INSERT INTO `sys_permission`
VALUES (60, b'0', '部门数据管理', 28, 'department:list;department:listJson', 'menu', '/permission/department/list', 35, 0, '',
        'layui-icon-auz', NULL, NULL);
INSERT INTO `sys_permission`
VALUES (61, b'0', '添加/修改', 60, 'department:form;department:save', 'button', '/permission/department/form', 10, 0, '',
        NULL, NULL, NULL);
INSERT INTO `sys_permission`
VALUES (62, b'0', '删除', 60, 'department:del', 'button', '/permission/department/del', 20, 0, '', NULL, NULL, NULL);
INSERT INTO `sys_permission`
VALUES (63, b'0', '数据备份管理', 28, 'dataBase:list;dataBase:listJson', 'menu', '/permission/dataBase/list', 200, 0, '',
        'layui-icon-date', NULL, NULL);
INSERT INTO `sys_permission`
VALUES (65, b'0', '在线用户查看', 28, 'userInfo:onlineUserInfo;userInfo:onlineUserInfoJson', 'menu',
        '/permission/userInfo/onlineUserInfo', 100, 0, '', 'layui-icon-user', NULL, NULL);
INSERT INTO `sys_permission`
VALUES (66, b'0', '踢出', 65, 'userInfo:logoutOnlineUser', 'button', '/permission/userInfo/logoutOnlineUser', 5, 0, '',
        NULL, NULL, NULL);
INSERT INTO `sys_permission`
VALUES (67, b'0', '系统日志查看', 28, 'logs:list;logs:loadData', 'menu', '/logs/list', 188, 0, '', 'layui-icon-star', NULL,
        NULL);
INSERT INTO `sys_permission`
VALUES (68, b'0', '初始化框架缓存', 35, 'sysPermission:clearCache', 'button', '/permission/sysPermission/clearCache', 30, 0,
        '', NULL, NULL, NULL);
INSERT INTO `sys_permission`
VALUES (96, b'0', '无房职工信息管理', NULL, '1', 'menu', '/business/noHouse/noHouseWorkerList', 10, 0, '',
        'layui-icon-rate-half', '2021-06-24 14:30:10', '2021-06-25 10:19:10');
INSERT INTO `sys_permission`
VALUES (98, b'0', '无房职工信息管理', 96, 'noHouse:noHouseList;noHouse:loadNoHouseList;noHouse:noHouseForm', 'menu',
        '/business/worker/noHouseList', 10, 0, '', 'layui-icon-rate-solid', '2021-06-25 10:18:39',
        '2021-06-28 10:46:25');
INSERT INTO `sys_permission`
VALUES (99, b'0', '无房职工确认管理', 96, 'noHouse:noHouseApplyList', 'menu', '/business/noHouse/noHouseApplyList', 20, 0, '',
        'layui-icon-login-qq', '2021-06-25 13:37:35', '2021-07-06 18:06:33');
INSERT INTO `sys_permission`
VALUES (100, b'0', '无房职工数据公示', 96, '', 'menu', '/business/noHouse/noHousePublicityList', 30, 0, '',
        'layui-icon-shrink-right', '2021-06-26 15:36:44', '2021-06-26 15:36:44');
INSERT INTO `sys_permission`
VALUES (101, b'0', '无房职工汇总信息', 96, '', 'menu', '/business/noHouse/noHouseSummaryList', 40, 0, '',
        'layui-icon-snowflake', '2021-06-26 18:26:42', '2021-06-26 18:26:42');
INSERT INTO `sys_permission`
VALUES (102, b'0', '无房职工清册管理', 96, '', 'menu', '/business/noHouse/noHouseInventoryList', 50, 0, '', 'layui-icon-senior',
        '2021-06-26 18:51:46', '2021-06-26 18:51:46');
INSERT INTO `sys_permission`
VALUES (103, b'0', '未达标职工信息管理', NULL, '', 'menu', '', 15, 0, '', 'layui-icon-spread-left', '2021-06-27 10:20:51',
        '2021-06-28 09:32:14');
INSERT INTO `sys_permission`
VALUES (104, b'0', '未达标职工申报管理', 103, '', 'menu', '/business/noStandard/noStandInfoList', 10, 0, '',
        'layui-icon-shrink-right', '2021-06-27 10:21:27', '2021-06-28 18:53:40');
INSERT INTO `sys_permission`
VALUES (105, b'0', '未达标职工确认管理', 103, '', 'menu', '/business/noStandard/noStandVerifyList', 20, 0, '',
        'layui-icon-snowflake', '2021-06-28 09:30:23', '2021-07-06 18:06:58');
INSERT INTO `sys_permission`
VALUES (106, b'0', '未达标职工公示管理', 103, '', 'menu', '/business/noStandard/noStandPublicList', 30, 0, '',
        'layui-icon-shrink-right', '2021-06-28 09:42:22', '2021-06-28 09:42:22');
INSERT INTO `sys_permission`
VALUES (107, b'0', '未达标职工信息汇总', 103, '', 'menu', '/business/noStandard/noStandSummaryList', 40, 0, '',
        'layui-icon-rate-half', '2021-06-28 10:29:36', '2021-06-28 10:29:36');
INSERT INTO `sys_permission`
VALUES (108, b'0', '未达标职工在职清册', 103, '', 'menu', '/business/noStandard/noStandQczzList', 50, 0, '',
        'layui-icon-shrink-right', '2021-06-28 10:50:29', '2021-06-28 11:26:44');
INSERT INTO `sys_permission`
VALUES (109, b'0', '未达标职工退休清册', 103, '', 'menu', '/business/noStandard/noStandTxList', 60, 0, '',
        'layui-icon-shrink-right', '2021-06-28 10:51:03', '2021-06-28 11:26:54');
INSERT INTO `sys_permission`
VALUES (110, b'0', '未达标职工申报管理', 103, '', 'menu', '/business/noStandard/noStandDeclareList', 15, 1, '',
        'layui-icon-shrink-right', '2021-06-28 16:25:44', '2021-07-06 18:07:04');
INSERT INTO `sys_permission`
VALUES (111, b'0', '操作记录管理', NULL, '', 'menu', '', 20, 0, '', 'layui-icon-shrink-right', '2021-06-29 15:08:17',
        '2021-06-29 15:08:17');
INSERT INTO `sys_permission`
VALUES (112, b'0', '操作记录列表', 111, '', 'menu', '/business/operation/operateLogList', 10, 0, '', 'layui-icon-spread-left',
        '2021-06-29 15:08:51', '2021-06-29 15:08:51');
INSERT INTO `sys_permission`
VALUES (114, b'0', '无房职工申报管理', 96, '', 'menu', '/business/noHouse/noHouseApplySbList', 15, 1, '',
        'layui-icon-shrink-right', '2021-07-06 18:00:14', '2021-08-22 11:22:24');
INSERT INTO `sys_permission`
VALUES (115, b'0', '数据统计分析', NULL, '', 'menu', '', 30, 0, '', 'layui-icon-spread-left', '2021-08-22 13:26:59',
        '2021-08-22 13:28:00');
INSERT INTO `sys_permission`
VALUES (116, b'0', '数据统计分析', 115, '', 'menu', '/business/statistics/statisticsMoney', 10, 0, '', 'layui-icon-vercode',
        '2021-08-22 13:28:17', '2021-08-22 13:28:17');
INSERT INTO `sys_permission`
VALUES (117, b'0', '未达标职工其他清册', 103, '', 'menu', '/business/noStandard/otherList', 70, 0, '', 'layui-icon-rate-half',
        '2021-09-02 14:38:48', '2021-09-02 14:38:48');
COMMIT;

-- ----------------------------
-- Table structure for sys_permission_init
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission_init`;
CREATE TABLE `sys_permission_init`
(
    `id`              int(11) NOT NULL AUTO_INCREMENT,
    `permission_init` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `url`             varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `sort_index`      int(11)                                                 DEFAULT NULL,
    `create_time`     datetime                                                DEFAULT NULL,
    `update_time`     datetime                                                DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 40
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_permission_init
-- ----------------------------
BEGIN;
INSERT INTO `sys_permission_init`
VALUES (1, 'anon', '/login/**', 13, NULL, NULL);
INSERT INTO `sys_permission_init`
VALUES (2, 'anon', '/webjars/**', 45, NULL, NULL);
INSERT INTO `sys_permission_init`
VALUES (3, 'anon', '/public/**', 40, NULL, NULL);
INSERT INTO `sys_permission_init`
VALUES (4, 'anon', '/druid/**', 35, NULL, NULL);
INSERT INTO `sys_permission_init`
VALUES (5, 'anon', '/static/**', 30, NULL, NULL);
INSERT INTO `sys_permission_init`
VALUES (6, 'anon', '/style/**', 25, NULL, NULL);
INSERT INTO `sys_permission_init`
VALUES (7, 'anon', '/lib/**', 20, NULL, NULL);
INSERT INTO `sys_permission_init`
VALUES (8, 'anon', '/layui/**', 15, NULL, NULL);
INSERT INTO `sys_permission_init`
VALUES (9, 'anon', '/resources/**', 50, NULL, NULL);
INSERT INTO `sys_permission_init`
VALUES (11, 'user', '/**', 9999, NULL, NULL);
INSERT INTO `sys_permission_init`
VALUES (19, 'anon', '/validateCodeServlet', 12, NULL, NULL);
INSERT INTO `sys_permission_init`
VALUES (33, 'anon', '/error/**', 52, NULL, NULL);
INSERT INTO `sys_permission_init`
VALUES (34, 'anon', '/modules/**', 27, NULL, NULL);
INSERT INTO `sys_permission_init`
VALUES (36, 'authc', '/upload/**', 10, NULL, '2019-09-09 10:51:39');
INSERT INTO `sys_permission_init`
VALUES (37, 'anon', '/plugins/**', 28, NULL, NULL);
INSERT INTO `sys_permission_init`
VALUES (38, 'anon', '/images/**', 29, NULL, NULL);
INSERT INTO `sys_permission_init`
VALUES (39, 'anon', '/dataSync/**', 55, '2020-07-03 15:00:25', '2020-07-03 15:00:25');
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`
(
    `id`          int(11) NOT NULL AUTO_INCREMENT,
    `available`   bit(1)                                                  DEFAULT NULL,
    `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `role`        varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `sort_index`  int(11)                                                 DEFAULT NULL,
    `create_time` datetime                                                DEFAULT NULL,
    `update_time` datetime                                                DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role`
VALUES (1, b'1', '管理员', 'admin', 2, NULL, NULL);
INSERT INTO `sys_role`
VALUES (2, b'1', '内业', '内业', 10, '2021-07-08 11:44:35', '2021-07-08 11:44:35');
INSERT INTO `sys_role`
VALUES (3, b'1', '领导', '市场监管局', 5, '2021-07-21 09:34:51', '2021-07-21 09:34:58');
COMMIT;

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission`
(
    `role_id`       int(11) NOT NULL,
    `permission_id` int(11) NOT NULL,
    KEY `FKomxrs8a388bknvhjokh440waq` (`permission_id`) USING BTREE,
    KEY `FK9q28ewrhntqeipl1t04kh1be7` (`role_id`) USING BTREE,
    CONSTRAINT `FK9q28ewrhntqeipl1t04kh1be7` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
    CONSTRAINT `FKomxrs8a388bknvhjokh440waq` FOREIGN KEY (`permission_id`) REFERENCES `sys_permission` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_role_permission
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_permission`
VALUES (1, 23);
INSERT INTO `sys_role_permission`
VALUES (1, 5);
INSERT INTO `sys_role_permission`
VALUES (1, 9);
INSERT INTO `sys_role_permission`
VALUES (1, 8);
INSERT INTO `sys_role_permission`
VALUES (1, 7);
INSERT INTO `sys_role_permission`
VALUES (1, 11);
INSERT INTO `sys_role_permission`
VALUES (1, 16);
INSERT INTO `sys_role_permission`
VALUES (1, 17);
INSERT INTO `sys_role_permission`
VALUES (1, 15);
INSERT INTO `sys_role_permission`
VALUES (1, 58);
INSERT INTO `sys_role_permission`
VALUES (1, 35);
INSERT INTO `sys_role_permission`
VALUES (1, 48);
INSERT INTO `sys_role_permission`
VALUES (1, 49);
INSERT INTO `sys_role_permission`
VALUES (1, 51);
INSERT INTO `sys_role_permission`
VALUES (1, 53);
INSERT INTO `sys_role_permission`
VALUES (1, 54);
INSERT INTO `sys_role_permission`
VALUES (1, 68);
INSERT INTO `sys_role_permission`
VALUES (1, 43);
INSERT INTO `sys_role_permission`
VALUES (1, 44);
INSERT INTO `sys_role_permission`
VALUES (1, 45);
INSERT INTO `sys_role_permission`
VALUES (1, 46);
INSERT INTO `sys_role_permission`
VALUES (1, 57);
INSERT INTO `sys_role_permission`
VALUES (1, 60);
INSERT INTO `sys_role_permission`
VALUES (1, 61);
INSERT INTO `sys_role_permission`
VALUES (1, 62);
INSERT INTO `sys_role_permission`
VALUES (1, 1);
INSERT INTO `sys_role_permission`
VALUES (1, 2);
INSERT INTO `sys_role_permission`
VALUES (1, 55);
INSERT INTO `sys_role_permission`
VALUES (1, 59);
INSERT INTO `sys_role_permission`
VALUES (1, 65);
INSERT INTO `sys_role_permission`
VALUES (1, 66);
INSERT INTO `sys_role_permission`
VALUES (1, 67);
INSERT INTO `sys_role_permission`
VALUES (1, 63);
INSERT INTO `sys_role_permission`
VALUES (1, 50);
INSERT INTO `sys_role_permission`
VALUES (1, 28);
INSERT INTO `sys_role_permission`
VALUES (1, 56);
INSERT INTO `sys_role_permission`
VALUES (1, 96);
INSERT INTO `sys_role_permission`
VALUES (1, 100);
INSERT INTO `sys_role_permission`
VALUES (1, 101);
INSERT INTO `sys_role_permission`
VALUES (1, 102);
INSERT INTO `sys_role_permission`
VALUES (1, 103);
INSERT INTO `sys_role_permission`
VALUES (1, 106);
INSERT INTO `sys_role_permission`
VALUES (1, 107);
INSERT INTO `sys_role_permission`
VALUES (1, 98);
INSERT INTO `sys_role_permission`
VALUES (1, 108);
INSERT INTO `sys_role_permission`
VALUES (1, 109);
INSERT INTO `sys_role_permission`
VALUES (1, 104);
INSERT INTO `sys_role_permission`
VALUES (1, 111);
INSERT INTO `sys_role_permission`
VALUES (1, 112);
INSERT INTO `sys_role_permission`
VALUES (1, 99);
INSERT INTO `sys_role_permission`
VALUES (1, 105);
INSERT INTO `sys_role_permission`
VALUES (1, 110);
INSERT INTO `sys_role_permission`
VALUES (1, 114);
INSERT INTO `sys_role_permission`
VALUES (1, 115);
INSERT INTO `sys_role_permission`
VALUES (1, 116);
INSERT INTO `sys_role_permission`
VALUES (1, 117);
COMMIT;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`
(
    `role_id` int(11) NOT NULL,
    `uid`     int(11) NOT NULL,
    KEY `FKgkmyslkrfeyn9ukmolvek8b8f` (`uid`) USING BTREE,
    KEY `FKhh52n8vd4ny9ff4x9fb8v65qx` (`role_id`) USING BTREE,
    CONSTRAINT `FKgkmyslkrfeyn9ukmolvek8b8f` FOREIGN KEY (`uid`) REFERENCES `user_info` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
    CONSTRAINT `FKhh52n8vd4ny9ff4x9fb8v65qx` FOREIGN KEY (`role_id`) REFERENCES `sys_role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_role`
VALUES (1, 1);
COMMIT;

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info`
(
    `id`            int(11) NOT NULL AUTO_INCREMENT,
    `name`          varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `password`      varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `salt`          varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `state`         int(4)  NOT NULL,
    `username`      varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `description`   varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `tel_phone`     varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `department_id` int(11)                                                 DEFAULT NULL,
    `pic`           varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `create_time`   datetime                                                DEFAULT NULL,
    `update_time`   datetime                                                DEFAULT NULL,
    `leavel`        int(11)                                                 DEFAULT NULL,
    `guid`          varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `is_sign`       int(11) NOT NULL                                        DEFAULT '1',
    `pin`           varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `ukey`          varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `check_code`    varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `check_date`    datetime                                                DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE KEY `UK_f2ksd6h8hsjtd57ipfq9myr64` (`username`) USING BTREE,
    KEY `FK6l1l771swrn930xtgw18u92il` (`department_id`) USING BTREE,
    CONSTRAINT `FK6l1l771swrn930xtgw18u92il` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB
  AUTO_INCREMENT = 104
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of user_info
-- ----------------------------
BEGIN;
INSERT INTO `user_info`
VALUES (1, '管理员', 'c341f8d730b9fe1009f8d583ff66609c', '03b964e8bcab2bde977ead846109ba9f', 1, 'admin', '超级管理员',
        '130518567222', 1, '/upload/pic/2018122621362980924f35cb59f7c41e48c5953baabe7b058.jpg', NULL,
        '2021-07-08 11:45:06', NULL, '', 1, '', '', NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for wage
-- ----------------------------
DROP TABLE IF EXISTS `wage`;
CREATE TABLE `wage`
(
    `id`             int(11) NOT NULL AUTO_INCREMENT,
    `create_time`    datetime                                                DEFAULT NULL,
    `update_time`    datetime                                                DEFAULT NULL,
    `id_card`        varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `money`          decimal(19, 2)                                          DEFAULT NULL,
    `month`          int(11)                                                 DEFAULT NULL,
    `name`           varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `year`           int(11)                                                 DEFAULT NULL,
    `worker_info_id` int(11)                                                 DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE,
    KEY `FK9753j9b90hx3nqa0tiedda0x6` (`worker_info_id`) USING BTREE,
    CONSTRAINT `FK9753j9b90hx3nqa0tiedda0x6` FOREIGN KEY (`worker_info_id`) REFERENCES `worker_info` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of wage
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for worker_info
-- ----------------------------
DROP TABLE IF EXISTS `worker_info`;
CREATE TABLE `worker_info`
(
    `id`                 int(11) NOT NULL AUTO_INCREMENT,
    `create_time`        datetime                                                DEFAULT NULL,
    `update_time`        datetime                                                DEFAULT NULL,
    `department_code`    varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `id_card`            varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `join_working_date`  date                                                    DEFAULT NULL,
    `living_address`     varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `memo`               longtext CHARACTER SET utf8 COLLATE utf8_general_ci,
    `name`               varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `num`                int(11)                                                 DEFAULT NULL,
    `record_no`          varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `retire_date`        date                                                    DEFAULT NULL,
    `spouse_id_card`     varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `spouse_name`        varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `spouse_unit`        varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
    `start_working_date` date                                                    DEFAULT NULL,
    `wages`              decimal(19, 2)                                          DEFAULT NULL,
    `worked_years`       decimal(19, 2)                                          DEFAULT NULL,
    `position_id`        int(11)                                                 DEFAULT NULL,
    `retire_position_id` int(11)                                                 DEFAULT NULL,
    PRIMARY KEY (`id`) USING BTREE,
    KEY `FK51399ogi7d2cio50de2wddhm4` (`position_id`) USING BTREE,
    KEY `FKjwmjbqh32ybwbusam9h73es60` (`retire_position_id`) USING BTREE,
    CONSTRAINT `FK51399ogi7d2cio50de2wddhm4` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
    CONSTRAINT `FKjwmjbqh32ybwbusam9h73es60` FOREIGN KEY (`retire_position_id`) REFERENCES `position` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of worker_info
-- ----------------------------
BEGIN;
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
