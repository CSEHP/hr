package com.app.utils;

import com.app.system.utils.dataType.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

/**
 * Created by wcf-pc on 2018/4/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DateUtilsTest {

    private static final Logger log = LoggerFactory.getLogger(DateUtilsTest.class);
    @Test
    public void testStringToDateDefault() throws Exception {
        System.out.println(DateUtils.dateToStringDefault(new Date()));
        log.debug("debug...");
    }

    @Test
    public void testDateToStringDefault() throws Exception {

        System.out.println(DateUtils.dateToStringFormat(new Date(),"mm-dd"));
    }

    @Test
    public void testDateToStringFormat() throws Exception {

    }

    @Test
    public void testStringToDateFormat() throws Exception {

    }
}